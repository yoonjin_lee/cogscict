public class ExpDataAgentHuman extends ExperimentData {
	public enum BoardType{BOTH_DD, AGENT_TI, GUI_TI};
	BoardType boardType;
	
	public ExpDataAgentHuman(){
		super();
		boardType = BoardType.BOTH_DD;
	}
	
	
	private String boardTypeToString(BoardType board){
		String bStr;
		if(board == BoardType.BOTH_DD)
			bStr = "BOTH_DD";
		else if(board == BoardType.AGENT_TI)
			bStr = "AGENT_TI";
		else bStr = "GUI_TI";
		
		return bStr;
				
	}
	
	public String toString(){
		String retVal = boardTypeToString(boardType) + "," + numOfRoundsPlayed + "," + timetoString() + "," +ReasonEndGameToString(reason);
		return retVal;
	}
	
}
