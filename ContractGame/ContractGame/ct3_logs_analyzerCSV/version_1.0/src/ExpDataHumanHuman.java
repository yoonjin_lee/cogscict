
public class ExpDataHumanHuman extends ExperimentData {
	public enum BoardType{BOTH_DD, FIRST_PLAYER_TI, SECOND_PLAYER_TI};
	
	public BoardType boardType = BoardType.BOTH_DD;
	
	private String boardTypeToString(BoardType board){
		String bStr;
		if(board == BoardType.BOTH_DD)
			bStr = "BOTH_DD";
		else if(board == BoardType.FIRST_PLAYER_TI)
			bStr = "FIRST_PLAYER_TI";
		else bStr = "SECOND_PLAYER_TI";
		
		return bStr;
				
	}
	
	@Override
	public String toString() {
		String retVal = boardTypeToString(boardType) + "," + numOfRoundsPlayed + "," + timetoString() + "," +ReasonEndGameToString(reason);
		return null;
	}

}
