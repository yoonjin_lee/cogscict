import java.util.Vector;

import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;

/**
 * This class holds the information about an offer. It holds its details, type
 * and the expected score if it will be executed.
 * 
 * @author Yael Blumberg
 */
public class Offer {

	private int proposer;
	private OfferType type;
	private Vector<OfferEstimation> estimations;
	private ChipsVec toSend;
	private ChipsVec actualSent;
	private ChipsVec toRecieve;
	private ChipsVec actualRecieved;
	private ChipSet proposerCurChips;
	private ChipSet responderCurChips;
	private ChipSet proposerLackingChips;
	private ChipSet responderLackingChips;	
	private boolean bAccepted;
	private int round;
	private ReliabilityData propRelaib;
	private ReliabilityData respRelaib;
	private ReliabilityData prevPropRelaib;
	private ReliabilityData prevRespRelaib;
	private double propWeightedPrevRelaibility;
	private double respWeightedPrevRelaibility;
	private int proposerNoConsec;
	private int responderNoConsec;
	
	public static int PROPOSER = 0;
	public static int RESPONDER = 1;
	
	// Constructor

	public Offer(int proposer, ChipsVec send, ChipsVec recieve, OfferType type,
			Double prevScore, Double resScore, Double hypScore,Double uVal) {
		
		estimations = new Vector<OfferEstimation>();
		estimations.add(PROPOSER, new OfferEstimation());
		estimations.add(RESPONDER, new OfferEstimation());
		setProposer(proposer);
		setType(type);
		setToSend(send);
		setToRecieve(recieve);
		setCurrentScore(prevScore,PROPOSER);
		setResultingScore(resScore,PROPOSER);
		setHypotheticalScore(hypScore,PROPOSER);
		setUtilityVal(uVal,PROPOSER);
		actualSent = new ChipsVec();
		actualRecieved = new ChipsVec();
		setPropRelaib(null);
		setRespRelaib(null);
		setProposerNoConsec(0);
		setResponderNoConsec(0);
	}

	// Getters and setters

	public int getProposer() {
		return proposer;
	}

	public void setProposer(int proposer) {
		this.proposer = proposer;
	}

	public OfferType getType() {
		return type;
	}

	public void setType(OfferType type) {
		this.type = type;
	}

	public Double getCurrentScore(int index) {
		return estimations.get(index).getCurrentScore();
	}

	public void setCurrentScore(Double currentScore, int index) {
		estimations.get(index).setCurrentScore(currentScore);
	}

	public Double getResultingScore(int index) {
		return estimations.get(index).getResultingScore();
	}

	public void setResultingScore(double resultingScore, int index) {
		estimations.get(index).setResultingScore(resultingScore);
	}

	public Double getHypotheticalScore(int index) {
		return estimations.get(index).getHypotheticalScore();
	}

	public void setHypotheticalScore(Double hypotheticalScore, int index) {
		estimations.get(index).setHypotheticalScore(hypotheticalScore);
	}

	public Double getUtilityVal(int index) {
		return estimations.get(index).getUtilityVal();
	}

	public void setUtilityVal(Double utilityVal, int index) {
		estimations.get(index).setUtilityVal(utilityVal);
	}

	public ChipsVec getToSend() {
		return toSend;
	}

	public void setToSend(ChipsVec toSend) {
		this.toSend = toSend;
	}

	public ChipsVec getToRecieve() {
		return toRecieve;
	}

	public void setToRecieve(ChipsVec toRecieve) {
		this.toRecieve = toRecieve;
	}

	public boolean isAccepted() {
		return bAccepted;
	}

	public void setAccepted(boolean bAccepted) {
		this.bAccepted = bAccepted;
	}

	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}

	public ChipsVec getActualSent() {
		return actualSent;
	}

	public void setActualSent(ChipsVec actualSent) {
		this.actualSent = actualSent;
	}

	public ChipsVec getActualRecieved() {
		return actualRecieved;
	}

	public void setActualRecieved(ChipsVec actualRecieved) {
		this.actualRecieved = actualRecieved;
	}

	public OfferEstimation getEstimation(int index) {
		return estimations.get(index);
	}
	
	public void setEstimation (OfferEstimation estimation, int index) {
		estimations.set(index, estimation);
	}
	
	public ReliabilityData getPropRelaib() {
		return propRelaib;
	}

	public void setPropRelaib(ReliabilityData propRelaib) {
		this.propRelaib = propRelaib;
	}

	public ReliabilityData getRespRelaib() {
		return respRelaib;
	}

	public void setRespRelaib(ReliabilityData respRelaib) {
		this.respRelaib = respRelaib;
	}

	public ReliabilityData getPrevPropRelaib() {
		return prevPropRelaib;
	}

	public void setPrevPropRelaib(ReliabilityData prevPropRelaib) {
		this.prevPropRelaib = prevPropRelaib;
	}

	public ReliabilityData getPrevRespRelaib() {
		return prevRespRelaib;
	}

	public void setPrevRespRelaib(ReliabilityData prevRespRelaib) {
		this.prevRespRelaib = prevRespRelaib;
	}

	public double getPropWeightedPrevRelaibility() {
		return propWeightedPrevRelaibility;
	}

	public void setPropWeightedPrevRelaibility(double propWPR, double propPR) {
		this.propWeightedPrevRelaibility = 0.3 * propWPR + 0.7 * propPR;
	}

	public double getRespWeightedPrevRelaibility() {
		return respWeightedPrevRelaibility;
	}

	public void setRespWeightedPrevRelaibility(double respWPR, double respPR) {
		this.respWeightedPrevRelaibility = 0.3 * respWPR + 0.7 * respPR;
	}

	public ChipSet getProposerCurChips() {
		return proposerCurChips;
	}

	public void setProposerCurChips(ChipSet proposerCurChips) {
		this.proposerCurChips = proposerCurChips;
	}

	public ChipSet getResponderCurChips() {
		return responderCurChips;
	}

	public void setResponderCurChips(ChipSet responderCurChips) {
		this.responderCurChips = responderCurChips;
	}

	public int getProposerNoConsec() {
		return proposerNoConsec;
	}

	public void setProposerNoConsec(int proposerNoConsec) {
		this.proposerNoConsec = proposerNoConsec;
	}

	public int getResponderNoConsec() {
		return responderNoConsec;
	}

	public void setResponderNoConsec(int responderNoConsec) {
		this.responderNoConsec = responderNoConsec;
	}

	public ChipSet getProposerLackingChips() {
		return proposerLackingChips;
	}

	public ChipSet getResponderLackingChips() {
		return responderLackingChips;
	}

	public void setResponderLackingChips(ChipSet responderLackingChips) {
		this.responderLackingChips = responderLackingChips;
	}

	public void setProposerLackingChips(ChipSet proposerLackingChips) {
		this.proposerLackingChips = proposerLackingChips;
	}

	public String offerDetailsToString() {

		String offer = "Send: " + toSend.toString();
		offer += "Receive: " + toRecieve.toString();

		return offer;
	}

	public String actualExchangeToString() {

		String offer = "Send: " + actualSent.toString();
		offer += "Receive: " + actualRecieved.toString();

		return offer;
	}
	
	public String toString() {
		return (getProposer() + "," + getType().toString() + ","
				+ getToSend().toString() + "," + getToRecieve().toString()
				+ "," + getCurrentScore(PROPOSER).toString() + ","
				+ getResultingScore(PROPOSER).toString() + ","
				+ getHypotheticalScore(PROPOSER).toString() + ","
				+ getUtilityVal(PROPOSER).toString()
				+ "," + getCurrentScore(RESPONDER).toString() + ","
				+ getResultingScore(RESPONDER).toString() + ","
				+ getHypotheticalScore(RESPONDER).toString() + ","
				+ getUtilityVal(RESPONDER).toString()+ "," 
				+ isAccepted() + "," + getActualSent().toString()
				+ "," + getActualRecieved().toString()
				+ "," + String.valueOf(getPropRelaib().promisedScore)	
				+ "," + String.valueOf(getPropRelaib().actualScore)
				+ "," + String.valueOf(getPropRelaib().promisedBenefit)	
				+ "," + String.valueOf(getPropRelaib().actualBenefit)				
				+ "," + String.valueOf(getPropRelaib().reliability)
				+ "," + String.valueOf(getRespRelaib().promisedScore)				
				+ "," + String.valueOf(getRespRelaib().actualScore)
				+ "," + String.valueOf(getRespRelaib().promisedBenefit)				
				+ "," + String.valueOf(getRespRelaib().actualBenefit)
				+ "," + String.valueOf(getRespRelaib().reliability))
				+ "," + String.valueOf(getPrevPropRelaib().promisedScore)	
				+ "," + String.valueOf(getPrevPropRelaib().actualScore)
				+ "," + String.valueOf(getPrevPropRelaib().promisedBenefit)	
				+ "," + String.valueOf(getPrevPropRelaib().actualBenefit)
				+ "," + String.valueOf(getPrevPropRelaib().reliability)
				+ "," + String.valueOf(getPropWeightedPrevRelaibility())
				+ "," + String.valueOf(getPrevRespRelaib().promisedScore)				
				+ "," + String.valueOf(getPrevRespRelaib().actualScore)
				+ "," + String.valueOf(getPrevRespRelaib().promisedBenefit)				
				+ "," + String.valueOf(getPrevRespRelaib().actualBenefit)				
				+ "," + String.valueOf(getPrevRespRelaib().reliability)
				+ "," + String.valueOf(getRespWeightedPrevRelaibility())
				+ "," + String.valueOf(getProposerNoConsec())
				+ "," + String.valueOf(getResponderNoConsec())
				+ "," + getProposerLackingChips().toString()
				+ "," + getProposerLackingChips().getNumChips()
				+ "," + getResponderLackingChips().toString()
				+ "," + getResponderLackingChips().getNumChips();
	}
}