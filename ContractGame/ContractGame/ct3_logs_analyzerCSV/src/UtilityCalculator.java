import java.util.LinkedHashSet;

import ctagents.alternateOffersAgent.ProposerResponderPlayer.EndingReasons;

import edu.harvard.eecs.airg.coloredtrails.server.ServerGameStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Board;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;
import edu.harvard.eecs.airg.coloredtrails.shared.types.PlayerStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.types.RowCol;

/**
 * This class is used to calculate the utility of the offers made. IMPORTANT:
 * The functions where copied from the class "Negotiator" of the alternative
 * offer protocol agent.
 * 
 * @author Yael Blumberg
 * 
 */
public class UtilityCalculator {

	private static final int MAX_NO_MOVE = 3;

	/*
	 * Calculates the utility value of an offer according to the game status.
	 */
	public static double calcExpectedValForAction(ChipSet possibleNewCs,
			ChipSet chipsToSend, ChipSet chipsToReceive, int numOfChipsLacking,
			int consecutiveNoMovements, EndingReasons isGameAboutToEnd,
			ServerGameStatus gs, int player, int opponent) {

		// Creating a copy of the player with the new chips
		PlayerStatus possibleNewPlayerStatus = (PlayerStatus) gs
				.getPlayerByPerGameId(player).clone();
		possibleNewPlayerStatus.setChips(possibleNewCs);
		double distance = gs.getBoard().getGoalLocations().get(0).dist(
				possibleNewPlayerStatus.getPosition());

		if (distance == 0)
			throw (new RuntimeException(
					"Shouldn't be in calcExpectedValForAction if distance to goal is zero"));

		// Checking how the exchange will effect the ability of the players to
		// move
		boolean willExchangeAllowMovementAgent = willExchangeAllowMovement(gs
				.getPlayerByPerGameId(player), chipsToSend, chipsToReceive, gs
				.getBoard());
		boolean willExchangeAllowMovementOther = willExchangeAllowMovement(gs
				.getPlayerByPerGameId(opponent), chipsToReceive, chipsToSend,
				gs.getBoard());

		// Calculating the expected score
		double score = Analyzer.scoreCalculator.inProcessScore(
				possibleNewPlayerStatus, gs.getBoard().getGoalLocations()
						.get(0), numOfChipsLacking, isGameAboutToEnd,
				willExchangeAllowMovementAgent, willExchangeAllowMovementOther);

		double expectedVal = score / distance
				- Math.pow(Math.E, consecutiveNoMovements);

		return expectedVal;
	}

	/*
	 * This function checks for a specific player and a specific exchange if the
	 * exchange is the one that will allow the movement. Therefore this function
	 * returns: 1) true, if the movement was allowed anyway without the exchange
	 * 2) false, if the movement is not allowed before the exchange and also not
	 * allowed after the exchange. 3) true, if the movement is allowed as a
	 * result of this exchange.
	 */
	private static boolean willExchangeAllowMovement(PlayerStatus player,
			ChipSet chipsToSend, ChipSet chipsToReceive, Board gameBoard) {

		boolean bWillAllow = false;
		boolean bCanMoveToANeigb = false;

		ChipSet playerChips = player.getChips();
		RowCol ppos = player.getPosition();
		LinkedHashSet<RowCol> neighbors = (LinkedHashSet<RowCol>) ppos
				.getNeighbors(gameBoard);

		// Checking if the player was able to move without the exchange
		for (RowCol rc : neighbors) {
			String color = gameBoard.getSquare(rc).getColor();

			if (playerChips.getNumChips(color) > 0) {
				bCanMoveToANeigb = true;
				break;
			}
		}

		// THIS IS DIFFERENT FROM THE AGENTS FUNCTION.
		// In the agent the function returns false in this condition
		if (bCanMoveToANeigb) {
			bWillAllow = true;
		} else {

			// Now check if we can move if the exchange was accepted:
			ChipSet chipsAfterExchange = ChipSet.addChipSets(playerChips,
					chipsToReceive);
			chipsAfterExchange = ChipSet.subChipSets(chipsAfterExchange,
					chipsToSend);
			for (RowCol rc : neighbors) {
				String color = gameBoard.getSquare(rc).getColor();
				if (chipsAfterExchange.getNumChips(color) > 0) {
					bWillAllow = true;
					break;
				}
			}
		}

		return bWillAllow;
	}

	/*
	 * Checks if the game is about to end and why.
	 */
	public static EndingReasons isGameAboutToEnd(int myConsecNoMove,
			int oppConsecNoMove, PlayerStatus opPlayer, Board gameBoaed) {

		// If I don't move this round the game is about to end.
		boolean bIHaveToMove = false;
		boolean bOppHasToMove = false;
		if (myConsecNoMove == MAX_NO_MOVE - 1)
			bIHaveToMove = true;

		// If opponent doesn't move this round the game is about to end.
		int opConsecNoMovement = oppConsecNoMove;
		if (opConsecNoMovement == MAX_NO_MOVE - 1)
			bOppHasToMove = true;
		if (bIHaveToMove && bOppHasToMove)
			return EndingReasons.BOTH_HAVE_TO_MOVE;

		if (bIHaveToMove)
			return EndingReasons.I_HAVE_TO_MOVE;
		if (bOppHasToMove)
			return EndingReasons.OPP_HAS_TO_MOVE;

		// if opponent is one square away of the goal and it has the chip to get
		// there the game is about to end:
		RowCol gpos = gameBoaed.getGoalLocations().get(0);
		RowCol ppos = opPlayer.getPosition();
		String goalColor = gameBoaed.getSquare(gpos).getColor();
		if (ppos.dist(gpos) == 1) {
			ChipSet opChips = opPlayer.getChips();
			boolean bHasColor = false;
			for (String color : opChips.getColors()) {
				if (color.equals(goalColor) && opChips.getNumChips(color) > 0) {
					bHasColor = true;
					break;
				}
			}
			if (bHasColor)
				return EndingReasons.OPP_ONE_STEP_FROM_GOAL;
			else
				return EndingReasons.NOT_ENDING;

		}
		return EndingReasons.NOT_ENDING;
	}
}