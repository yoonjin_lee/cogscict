
public class OfferType {
	Type offerType;
	public enum Type {_1TO1, _2TO1, _1TO2}; 
		
	public OfferType(int numToSend, int numToReceive){
		if(numToSend == numToReceive)
			offerType = Type._1TO1;
		else if(numToSend > numToReceive)
			offerType = Type._2TO1;
		else offerType = Type._1TO2;
	}
	
	public String toString(){
		String str;
		if(offerType == Type._1TO1)
			str = "1TO1";
		else if(offerType == Type._2TO1)
			str = "2TO1";
		else str = "1TO2";
		return str;
	}
}
