import java.util.Vector;

import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;

/**
 * This class implements a vector of Chips with different colors
 * @author Yael Blumberg
 *
 */
public class ChipsVec {

	private Vector<Chips> chipsVec;

	/*
	 * Default constructor
	 */
	public ChipsVec() {
		chipsVec = new Vector<Chips>();
	}
	
	/*
	 * Copy constructor
	 */
	public ChipsVec(ChipsVec copy) {
		chipsVec = new Vector<Chips>();
		for (Chips chp: copy.getChips()) {
			chipsVec.add(chp);
		}
	}
	
	/*
	 * Creates a chips vector from a string in the format of
	 * 'CTRed':0 'CTPurple':0 'CTGreen':0 'grey78':0 }
	 */
	public ChipsVec(String chips) {
		chipsVec = new Vector<Chips>();
		
		chips.trim();
		System.out.println("chips string to split: " + chips);
		
		String[] allColors = chips.split(" ");
		for (int currColor = 0; currColor < allColors.length - 1; currColor++) {
			String[] colorAndNo = allColors[currColor].split(":");
			Chips c = new Chips(colorAndNo[0], new Integer(colorAndNo[1]).intValue());
			if (c.number > 0)
				chipsVec.add(c);
		}
	}
	
	public Vector<Chips> getChips() {
		return chipsVec;
	}
	
	public boolean isEmpty(){
		if(chipsVec.isEmpty())
			return true;
		int sum = 0;
		for(Chips c: chipsVec){
			sum += c.number;
		}
		if(sum == 0)
			return true;
		return false;
	}
	
	public int getTotalNumChips(){
		int total = 0;
		for(Chips cs : chipsVec){
			total += cs.number;
		}
		return total;
	}
	
	/*
	 * return true if every element in chp is in the ChipsVec, except for the null element.
	 * that is, if the second set is empty it doesn't count as if it contained in the first.
	 */
	public boolean strictlyContains(ChipsVec chp){

		if (chp.getTotalNumChips() == 0) {
			return false;
		} else {
			return(this.toChipSet().contains(chp.toChipSet()));	
		}
	}
	
	public ChipSet toChipSet() {
		
		ChipSet cs = new ChipSet();
		for (Chips chp: this.getChips()) {
			cs = ChipSet.addChipSets(cs, chp.toChipSet());
		}
		return cs;
	}

	public String toString() {
		String chips = new String();
		for(Chips cs: this.getChips()){
			chips += cs.toString() + "; ";
		}
		
		return chips;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((chipsVec == null) ? 0 : chipsVec.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChipsVec other = (ChipsVec) obj;
		if (chipsVec == null) {
			if (other.chipsVec != null)
				return false;
		} else if (!chipsVec.equals(other.chipsVec))
			return false;
		return true;
	}
}