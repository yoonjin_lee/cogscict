import edu.harvard.eecs.airg.coloredtrails.shared.types.PhaseChangeHandler;

/**
 * This class is needed to create the ServerGameStatus class, but its
 * implementation isn't really needed.
 * 
 * @author Yael Blumberg
 * 
 */
public class DummyPhaseHandler implements PhaseChangeHandler {

	@Override
	public void beginPhase(String phaseName) {
		// TODO Auto-generated method stub

	}

	@Override
	public void endPhase(String phaseName) {
		// TODO Auto-generated method stub

	}

}
