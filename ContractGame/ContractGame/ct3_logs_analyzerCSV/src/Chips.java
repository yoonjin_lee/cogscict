import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;


public class Chips{
	String color;
	int number;
	public Chips(String color, int number){
		this.color = color;
		this.number = number;
	}

	public boolean equals (Object o)
	{   	
		Chips cs = (Chips) o;
		if((this.color.compareTo(cs.color) == 0) && (this.number == cs.number))
			return true;
		return false;
	}

	public String toString(){
		String str = "" + number + " " + color;
		return str;
	}
	
	public ChipSet toChipSet() {
		
		ChipSet cs = new ChipSet();
		cs.add(this.color.substring(1, this.color.length() - 1), this.number);
		
		return cs;
	}
}