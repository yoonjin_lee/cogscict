import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import edu.harvard.eecs.airg.coloredtrails.alglib.ShortestPaths;
import edu.harvard.eecs.airg.coloredtrails.server.ServerGameStatus;
import edu.harvard.eecs.airg.coloredtrails.server.ServerPhases;
import edu.harvard.eecs.airg.coloredtrails.shared.GameBoardCreator;
import edu.harvard.eecs.airg.coloredtrails.shared.Scoring;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Path;
import edu.harvard.eecs.airg.coloredtrails.shared.types.PlayerStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.types.RowCol;

public class AnalyzerCSV {

	// Creating a scoring object with the relevant weights to be used in score
	// calculations

	private Players players;
	private Vector<Integer >playersIDS;
	private ServerGameStatus gs = null;
	ChipSet curOriginalCSChips;
	int prevRound = 0;

	private void createExpirament(int csPin,int sp1Pin,int sp2Pin){
		players = new Players(3);
		players.addPlayer(csPin, 0);
		players.addPlayer(sp1Pin, 1);
		players.addPlayer(sp2Pin, 2);
		playersIDS=new Vector<Integer>();
		playersIDS.add(csPin);
		playersIDS.add(sp1Pin);
		playersIDS.add(sp2Pin);
		PlayerStatus csP = null;
		PlayerStatus ps1 = null;
		PlayerStatus ps2 = null;

		csP = new PlayerStatus(csPin);
		csP.setPerGameId(csPin);
		csP.setMovesAllowed(true);

		ps1 = new PlayerStatus(sp1Pin);
		ps1.setPerGameId(sp1Pin);
		ps1.setMovesAllowed(false);

		ps2 = new PlayerStatus(sp2Pin);
		ps2.setPerGameId(sp2Pin);
		ps2.setMovesAllowed(false);


		Set<PlayerStatus> players = new HashSet<PlayerStatus>();
		players.add(csP);
		players.add(ps1);
		players.add(ps2);

		gs = new ServerGameStatus();
		gs.setPlayers(players);

		ServerPhases ph = new ServerPhases(
				new DummyPhaseHandler());
		ph.addPhase("Strategy Prep Phase");
		ph.addPhase("Communication Phase");
		ph.addPhase("Exchange Phase");
		ph.addPhase("Movement Phase");
		ph.addPhase("Feedback Phase");
		gs.setPhases(ph);
		GameBoardCreator gbc = new GameBoardCreator(gs);
		gbc.createGameBoard("boardXml_Agent_TI_3Players.txt");
		Scoring s = new Scoring(150, 0, 5);
		gs.setScoring(s);
		curOriginalCSChips = null;
		prevRound = 0;
	}

	public static void main(String[] args) {
		AnalyzerCSV a=new AnalyzerCSV();

		// Checking if the arguments were entered
		if (args.length <4) {
			System.out.println("Paramters are: [CSVfilename] [CSpin] [SP1pin] [SP2pin] ");
		} else {
			try {
				// Getting the parameters and analyzing the data
				String csvfilename=args[0];
				int p0=Integer.parseInt(args[1]);
				int p1=Integer.parseInt(args[2]);
				int p2=Integer.parseInt(args[3]);
				a.analizeFile(csvfilename,p0,p1,p2);
			} catch (Exception e) {
				System.out.println("Paramters are: [CSVfilename] [CSpin] [SP1pin] [SP2pin] ");
			}
		}
	}

	private void analizeFile(String csvfilename,int csPin,int  sp1Pin,int sp2Pin) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(csvfilename));
			System.out.println("Processing file: " + csvfilename);
			String fixedfilename=csvfilename.substring(0,csvfilename.indexOf(".csv"))+"fixed.csv";
			String line;
			BufferedWriter out=new BufferedWriter(new FileWriter(fixedfilename));
			//	out.write(line+"\n");
			//	br.readLine();
			while ((line = br.readLine()) != null) {
				String newLine=doAnalizeLine(line,csPin, sp1Pin, sp2Pin);
				out.write(newLine+"\n");
			}
			out.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	String filename="";
	private String doAnalizeLine(String line,int csPin,int  sp1Pin,int sp2Pin) {
		String []tabs=line.split(",");

		try{
			String newFileName=(tabs[0]);
			if(newFileName.equals("filename")){
				return "filename,round,propID,respID,IsPropCustomer,chipsToSend,chipsToReceive,accept,propCurrentScore,respCurrentScore,actualSent,ActualRcvd,propResultingScore,respResultingScore,#ofCustomerMovesInRound,customerRowPositionAfterMovePhase,customerColPositionAfterMovePhase,Player100ChipsAfterMovePhase,Player100ScoreAfterMovePhase,Player200ChipsAfterMovePhase,Player200ScoreAfterMovePhase,Player300ChipsAfterMovePhase,Player300ScoreAfterMovePhase,isCommitment,generosityLevel,ClientReachedGoal";
			}
			if(!newFileName.contains(".csv"))
				return line;
			if(!newFileName.equals(filename)){
				filename=newFileName;
				this.createExpirament(csPin, sp1Pin, sp2Pin);
			}
			int round=Integer.parseInt(tabs[1]);
			int propID=Integer.parseInt(tabs[2]);
			int respID=Integer.parseInt(tabs[3]);
			int otherID=getOtherID(propID,respID);

			boolean isPropCostumer=Boolean.parseBoolean(tabs[4]);
			String chipsToSendStr=tabs[5];
			String chipsToRcvStr=tabs[6];
			String accept=tabs[7];
			int propCurScore=Integer.parseInt(tabs[8]);
			int resppCurScore=Integer.parseInt(tabs[9]);
			String accSent=tabs[10];
			String accRcv=tabs[11];
			String propResultingScr=tabs[12];
			String resppResultingScr=tabs[13];
			String numOfMoves=tabs[14];
			String rPoss=tabs[15];
			String cPoss=tabs[16];
			String p100ChipsAfter=tabs[17];
			String p100ScrAfter=tabs[18];
			String p200ChipsAfter=tabs[19];
			String p200ScrAfter=tabs[20];
			String p300ChipsAfter=tabs[21];
			String p300ScrAfter=tabs[22];
			String genoLevel="";//tabs[23];
			String reachGoal="";//tabs[24];
			try{
				genoLevel=tabs[23];
				reachGoal=tabs[24];
			}
			catch(Exception e){

			}
			if (round != prevRound)
			{
				System.out.println("prevRound = "+prevRound+"round= "+round);
				prevRound = round;
				PlayerStatus ps=gs.getPlayerByPerGameId(playersIDS.get(0).intValue());
					curOriginalCSChips = new ChipSet(ps.getChips());
					System.out.println("\n"+"Initialize  original  curCsChipset= "+curOriginalCSChips);
			}
			boolean isCommittment=isCommitment(isPropCostumer,propID,respID,otherID,chipsToSendStr,chipsToRcvStr);//
			if(propResultingScr.equals("")){
				String [] resScores=getResScores(chipsToSendStr,chipsToRcvStr,propCurScore,resppCurScore, isPropCostumer, isCommittment);
				propResultingScr=resScores[0];
				resppResultingScr=resScores[1];
			}
			updateGame(rPoss,cPoss,playersIDS.get(0).intValue(),playersIDS.get(1).intValue(),playersIDS.get(2).intValue(),p100ChipsAfter,p100ScrAfter,p200ChipsAfter,p200ScrAfter,p300ChipsAfter,p300ScrAfter);

			return tabs[0]+","+tabs[1]+","+propID+","+
			respID+","+
			isPropCostumer+","+
			chipsToSendStr+","+
			chipsToRcvStr+","+
			accept+","+
			propCurScore+","+
			resppCurScore+","+
			accSent+","+
			accRcv+","+
			propResultingScr+","+
			resppResultingScr+","+
			numOfMoves+","+
			rPoss+","+
			cPoss+","+
			p100ChipsAfter+","+
			p100ScrAfter+","+
			p200ChipsAfter+","+
			p200ScrAfter+","+
			p300ChipsAfter+","+
			p300ScrAfter+","+
			isCommittment+","+
			genoLevel+","+
			reachGoal;
		}
		catch(Exception e){
			line=addComma(line);
			return line;
		}
	}
	private String addComma(String line) {
		if(!line.contains(","))
			return line;
		String []strs=line.split(",");
		StringBuilder sb=new StringBuilder();
		for (int i = 0; i <= strs.length-3; i++) {
			sb.append(strs[i]+",");
		}
		sb.append(",");
		try{
		sb.append(strs[strs.length-2]+",");
		sb.append(strs[strs.length-1]);
		}
		catch(Exception e){
			return line;
		}
		return sb.toString();
	}
	private void updateGame(String rPoss, String cPoss,int p100ID,int p200ID,int p300ID ,String p100ChipsAfter,
			String p100ScrAfter, String p200ChipsAfter, String p200ScrAfter,
			String p300ChipsAfter, String p300ScrAfter) {
		updateCSPlayer(rPoss,cPoss,p100ID,p100ChipsAfter,p100ScrAfter);
		updateSPPlayer(p200ID, p200ChipsAfter, p200ScrAfter);
		updateSPPlayer(p300ID, p300ChipsAfter, p300ScrAfter);
	}

	private void updateSPPlayer(int id,
			String chipsAfter, String scrAfter) {
		PlayerStatus ps=gs.getPlayerByPerGameId(id);
		ps.setScore(Integer.parseInt(scrAfter));
		ps.setChips(getChipsSetFromText(chipsAfter));
	}
	private void updateCSPlayer(String rPoss, String cPoss, int id,
			String chipsAfter, String scrAfter) {
		RowCol pos=getRowCol(rPoss, cPoss);
		PlayerStatus ps=gs.getPlayerByPerGameId(id);
		ps.setPosition(pos);
		ps.setScore(Integer.parseInt(scrAfter));
		ps.setChips(getChipsSetFromText(chipsAfter));
	}
	private RowCol getRowCol(String rPoss,String cPoss){
		int rowStart = rPoss.indexOf("R:") + 2;
		int colStart = cPoss.indexOf("C:") + 2;
		int row = Integer.parseInt(rPoss.substring(rowStart,
				rowStart + 1));
		int col = Integer.parseInt(cPoss.substring(colStart,
				colStart + 1));
		return new RowCol(row, col);
	}
	private int getOtherID(int p1,int p2) {
		for (Integer ps : playersIDS) {
			if(ps!=p1&&ps!=p2)
				return ps;
		}
		return 0;
	}
	private boolean isCommitment(RowCol position,PlayerStatus propSP,PlayerStatus otherSp,ChipSet remainingChips){
		ChipSet oppCS=otherSp.getChips();
		ChipSet myTempCS=ChipSet.addChipSets(remainingChips, oppCS);
		System.out.println("remainingChips= "+remainingChips +"myTempCS = "+myTempCS+"oppCS = "+oppCS);
		Path p=findAvailPath(myTempCS, position, otherSp);
		if(p==null){
			return true;
		}
		return false; 
	}
	private Path findAvailPath(ChipSet chipsCS,RowCol position,PlayerStatus SP){
		RowCol dest = SP.getPosition();
		ArrayList<Path> paths = ShortestPaths.getShortestPaths(position, dest,gs.getBoard(), gs.getScoring(), ShortestPaths.NUM_PATHS_RELEVANT, chipsCS,SP.getChips());
		for (Path path : paths) {
			ChipSet reqierd=path.getRequiredChips(gs.getBoard());
			if(chipsCS.getMissingChips(reqierd).isEmpty())
				return path;
		}
		return null;
	}

	private ChipSet getOriginalCS()
	{
		return (new ChipSet(curOriginalCSChips));
	}
	private boolean isCommitment(boolean isPropCostumer, int propID, int respID,int otherID, String chipsToSendStr, String chipsToRcvStr){//, String rPoss, String cPoss, String p100ChipsAfter, String p200ChipsAfter, String p300ChipsAfter) {
		System.out.println("\n"+"chipsToSendStr= "+chipsToSendStr +"chipsToRcvStr = "+chipsToRcvStr);
		if(isPropCostumer){
			ChipSet toSend=getChipsSetFromText(chipsToSendStr);
			ChipSet toRcv=getChipsSetFromText(chipsToRcvStr);
			PlayerStatus cs=gs.getPlayerByPerGameId(propID);
			ChipSet curCsChipset = getOriginalCS(); //cs.getChips();
			System.out.println("\n"+"original  curCsChipset= "+curCsChipset);
			curCsChipset.addChipSet(toRcv);
			curCsChipset.subChipSet(toSend);
			PlayerStatus responder=gs.getPlayerByPerGameId(respID);
			PlayerStatus other=gs.getPlayerByPerGameId(otherID);
			System.out.println("\n"+"toSend = "+toSend+" toRcv= "+toRcv+" curCsChipset= "+curCsChipset);
			return isCommitment(cs.getPosition(), responder, other, curCsChipset);
		}
		else{
			ChipSet toSend=getChipsSetFromText(chipsToSendStr);
			ChipSet toRcv=getChipsSetFromText(chipsToRcvStr);
			PlayerStatus cs=gs.getPlayerByPerGameId(respID);
			ChipSet curCsChipset = getOriginalCS(); //cs.getChips();
			System.out.println("\n"+"original  curCsChipset= "+curCsChipset);			
			curCsChipset.addChipSet(toSend);
			curCsChipset.subChipSet(toRcv);
			PlayerStatus responder=gs.getPlayerByPerGameId(propID);
			PlayerStatus other=gs.getPlayerByPerGameId(otherID);
			System.out.println("\n"+"toSend = "+toSend+" toRcv= "+toRcv+" curCsChipset= "+curCsChipset);
			return isCommitment(cs.getPosition(), responder, other, curCsChipset);
		}
	}
	private ChipSet getChipsSetFromText(String line){
		int locOfTag=line.indexOf("'");
		if(locOfTag==-1){
			return new ChipSet();
		}else{
			ChipsVec playersChips = new ChipsVec(line.substring(locOfTag));
			return playersChips.toChipSet();
		}	
	}
	private String[] getResScores(String chipsToSendStr,
			String chipsToRcvStr, int propCurScore, int resppCurScore,boolean isPropCustumer,boolean isComit) {
		ChipSet toSend=getChipsSetFromText(chipsToSendStr);
		ChipSet toRcv=getChipsSetFromText(chipsToRcvStr);
		int commitScore=0;
//		if(isComit){
//			commitScore=150;
//		}
		String []x= {""+(propCurScore-(toSend.getNumChips()*5)+(toRcv.getNumChips()*5)+commitScore),
				""+(resppCurScore+(toSend.getNumChips()*5)-(toRcv.getNumChips()*5)+commitScore)		
		};
		return x;
	}
}