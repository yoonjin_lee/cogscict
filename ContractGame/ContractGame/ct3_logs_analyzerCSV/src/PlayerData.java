import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.Vector;

import ctagents.alternateOffersAgent.ProposerResponderPlayer.EndingReasons;

import edu.harvard.eecs.airg.coloredtrails.alglib.ShortestPaths;
import edu.harvard.eecs.airg.coloredtrails.server.ServerGameStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Path;
import edu.harvard.eecs.airg.coloredtrails.shared.types.PlayerStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.types.RowCol;

public class PlayerData {

	private int ID = -1;
	private int pin = -1;
	private int noConcecMovment;
	public double score = 0.0;
	public int numOfAcceptedOffers = 0;
	public int numOfRejectedOffers = 0;
	public int numOfFullyKeptAgreements = 0;
	public int numOfUnkeptAgreements = 0;
	public int numOfPartiallyKeptAgreements = 0;
	public int numOfPartiallyPlusKeptAgreements = 0;
	public int numOfProposals = 0;
	public boolean isProposerFirst = false;
	public boolean isTI = false;
	public int numOfStartingChips = 0;
	public String typeOfPlayer; // AGENT or HUMAN
	public boolean bReachedGoal = false;

	private Path chosenPath;

	public Vector<Integer> agreedActualSentDiff = new Vector<Integer>();
	public Vector<Integer> chipsBeyondAgreed = new Vector<Integer>();

	public PlayerData(int pin) {
		this.pin = pin;
		chosenPath = null;
		setNoConcecMovment(0);
	}

	public PlayerData() {
		setPin(0);
		chosenPath = null;
		setNoConcecMovment(0);
	}

	public void setPin(int pin) {
		this.pin = pin;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public int getNoConcecMovment() {
		return noConcecMovment;
	}

	public void setNoConcecMovment(int noConcecMovment) {
		this.noConcecMovment = noConcecMovment;
	}

	public int getPin() {
		return pin;
	}

	/*
	 * This method shortens the chosen path by one step from the start.
	 */
	public void removeFirstPointOnPath(RowCol curPos, RowCol newPos) {

		if (chosenPath != null) {

			LinkedList<RowCol> points;

			// Checking that the player really went to the next point and didn't
			// do something stupid like going back..
			if ((chosenPath.getPoint(1).equals(newPos))) {

				points = chosenPath.getPoints();
				points.remove(0);

			} else {
				points = new LinkedList<RowCol>();
				points.add(newPos);
				points.addAll(chosenPath.getPoints());
			}

			chosenPath = new Path();
			for (RowCol point : points) {

				chosenPath.addPathPoint(point);
			}
		}
	}

	/*
	 * This method moves the player on his chosen path until he can't move any
	 * further
	 */
	private void progressOnPath(ServerGameStatus gs) {

		PlayerStatus curPs = gs.getPlayerByPerGameId(this.pin);
		PlayerStatus oppPs = getOpponent(gs);

		// Finding the players path if it wasn't found before
		if (chosenPath == null) {

			chosenPath = ShortestPaths.getShortestPaths(curPs.getPosition(),
					gs.getBoard().getGoalLocations().get(0), gs.getBoard(),
					Analyzer.scoreCalculator, 1, curPs.getChips(),
					oppPs.getChips()).get(0);
		}

		LinkedList<RowCol> points = (LinkedList<RowCol>) chosenPath.getPoints();
		points.remove(0);

		for (RowCol point : points) {

			if (!gs.doMove(this.pin, point)) {
				break;
			}
		}
	}

	/*
	 * Gets the opponents object by it's pin. We assume there are only two
	 * players, thus retrieving the player with a different pin then me.
	 */
	private PlayerStatus getOpponent(ServerGameStatus gs) {
		PlayerStatus opponent = null;

		Set<PlayerStatus> players = gs.getPlayers();
		for (PlayerStatus ps : players)
			if (ps.getPerGameId() != this.pin) {
				opponent = ps;
				break;
			}
		return opponent;
	}

	/*
	 * Creates a copy of a game status with copies of the players objects.
	 */
	private ServerGameStatus copyGameStatus(ServerGameStatus gs) {
		ServerGameStatus gsCopy = new ServerGameStatus(gs);
		Set<PlayerStatus> newPlayers = new HashSet<PlayerStatus>();

		for (PlayerStatus ps : gs.getPlayers()) {
			PlayerStatus psNew = new PlayerStatus(ps);
			newPlayers.add(psNew);
		}
		gsCopy.setPlayers(newPlayers);

		return gsCopy;
	}

	private double calcScore(ServerGameStatus gs, ChipSet newChips) {
		double score = 0.0;

		// Creating a copy of the game status
		ServerGameStatus newGs = copyGameStatus(gs);

		// Changing the players chips
		newGs.getPlayerByPerGameId(getPin()).setChips(newChips);

		// Trying to go as far as possible closer to the goal
		progressOnPath(newGs);

		score = Analyzer.scoreCalculator.score(newGs
				.getPlayerByPerGameId(getPin()), newGs.getBoard()
				.getGoalLocations().get(0));

		return score;
	}

	public Offer addOffer(ChipsVec chipsToSend, ChipsVec chipsToReceive,
			ServerGameStatus gs, EndingReasons isGameAboutToEnd) {

		ChipSet sentChips = chipsToSend.toChipSet();
		ChipSet recievedChips = chipsToReceive.toChipSet();

		OfferType ofTy = new OfferType(sentChips.getNumChips(), recievedChips
				.getNumChips());

		// Simulating a game ending, and calculating the score before the
		// transfer.
		PlayerStatus curPs = new PlayerStatus(gs.getPlayerByPerGameId(this.pin));
		PlayerStatus oppPs = new PlayerStatus(getOpponent(gs));

		// Saving the current chips condition
		ChipSet myChips = new ChipSet(curPs.getChips());
		ChipSet oppChips = new ChipSet(oppPs.getChips());

		// Calculating the score if nothing is changed
		double scoreBefore = calcScore(gs, gs.getPlayerByPerGameId(
				this.getPin()).getChips());
		double scoreAfter;
		double hypotheticScore;
		double calculatedScore;
		double utilityVal = 0;
		boolean bAreChipsAvailable = true;

		// Checking if the offer is doable.
		// If its not, the score after doesn't change, and we simulate a
		// situation that the players had the needed chips and compute the
		// Hypothetical result
		if (!curPs.getChips().contains(sentChips)
				|| !oppPs.getChips().contains(recievedChips)) {

			bAreChipsAvailable = false;
			curPs.setChips(ChipSet.addChipSets(curPs.getChips(), curPs
					.getChips().getMissingChips(sentChips)));
			oppPs.setChips(ChipSet.addChipSets(oppPs.getChips(), oppPs
					.getChips().getMissingChips(recievedChips)));

		}

		// Creating the players to be if the offer will be accepted and
		// performed as promised.
		ChipSet newChips = ChipSet.subChipSets(curPs.getChips(), sentChips);
		newChips = ChipSet.addChipSets(newChips, recievedChips);

		// Calculating the score after the transfer.
		calculatedScore = calcScore(gs, newChips);

		// If all players had the chips the score we calculated is the
		// resulting, otherwise it's a hypothetical score
		if (bAreChipsAvailable) {
			scoreAfter = calculatedScore;
			hypotheticScore = scoreAfter;
		} else {
			scoreAfter = scoreBefore;
			hypotheticScore = calculatedScore;
		}

		// Calculating the utility value of the offer
		ChipSet chipsLacking = getLackingChips(gs);

		utilityVal = UtilityCalculator.calcExpectedValForAction(newChips,
				sentChips, recievedChips, chipsLacking.getNumChips(),
				getNoConcecMovment(), isGameAboutToEnd, gs, this.pin,
				getOpponent(gs).getPin());

		Offer newOffer = new Offer(this.pin, chipsToSend, chipsToReceive, ofTy,
				scoreBefore, scoreAfter, hypotheticScore, utilityVal);

		// Saving the current chips status. Needed for reliability calculation
		newOffer.setProposerCurChips(myChips);
		newOffer.setResponderCurChips(oppChips);
		newOffer.setProposerLackingChips(chipsLacking);

		return newOffer;
	}

	/*
	 * Calculating the chips missing for the player to reach his goal
	 */
	public ChipSet getLackingChips(ServerGameStatus game) {
		
		// Finding the players path if it wasn't found before
		if (chosenPath == null) {
			
			PlayerStatus curPs = game.getPlayerByPerGameId(this.pin);
			PlayerStatus oppPs = getOpponent(game);			

			chosenPath = ShortestPaths.getShortestPaths(curPs.getPosition(),
					game.getBoard().getGoalLocations().get(0), game.getBoard(),
					Analyzer.scoreCalculator, 1, curPs.getChips(),
					oppPs.getChips()).get(0);
		}

		ChipSet requiredChipsForPath = chosenPath.getRequiredChips(game
				.getBoard());
		ChipSet chipsLacking = game.getPlayerByPerGameId(this.pin).getChips()
				.getMissingChips(requiredChipsForPath);

		return chipsLacking;
	}

	/*
	 * Calculating the opponents reliability based on the expected score with
	 * the promised chips vs. the score with the actual chips.
	 */
	public ReliabilityData calcOppReliabilty(ServerGameStatus gs,
			Offer agreement) {

		ReliabilityData reliab = new ReliabilityData();

		ChipSet curChips;
		ChipSet promisedChips;
		ChipSet actualChips;

		// Calculating the chips amount if all the chips where sent as promised
		// and the amount with the chips actually sent
		if (getPin() != agreement.getProposer()) {

			curChips = agreement.getResponderCurChips();
			promisedChips = ChipSet.addChipSets(curChips, agreement.getToSend()
					.toChipSet());
			actualChips = ChipSet.addChipSets(curChips, agreement
					.getActualSent().toChipSet());
		} else {
			curChips = agreement.getProposerCurChips();
			promisedChips = ChipSet.addChipSets(curChips, agreement
					.getToRecieve().toChipSet());
			actualChips = ChipSet.addChipSets(curChips, agreement
					.getActualRecieved().toChipSet());
		}

		double currentScore = calcScore(gs, curChips);
		reliab.promisedScore = calcScore(gs, promisedChips);
		reliab.actualScore = calcScore(gs, actualChips);
		reliab.promisedBenefit = reliab.promisedScore - currentScore;
		reliab.actualBenefit = reliab.actualScore - currentScore;

		// If the denominator is zero, return 1
		// Actually, this is not quite accurate because when the denominator is
		// 0 and the numerator is positive, that means that the player was being
		// generous after having promised nothing
		if (reliab.promisedScore == currentScore) {
			reliab.reliability = 1;
		} else {
			reliab.reliability = (reliab.actualBenefit)
					/ (reliab.promisedBenefit);
		}

		return (reliab);
	}

	public String VectorOfIntToString(Vector<Integer> intVec) {
		String str = "[";
		for (Integer i : intVec)
			str += i.toString() + ";";
		str += "]";
		return str;
	}

	public String IdToString() {
		if (ID == -1) {
			return " ";
		} else {
			return String.valueOf(ID);
		}
	}

	class ChipsDifferenceAndBeyond {
		public int agreedActualSentDiff = 0;
		public int chipsBeyondAgreed = 0;
	}

	public ChipsDifferenceAndBeyond checkExchange(ChipsVec chipsToSend,
			ChipsVec actualSent) {
		System.out.println("checkExchange for: " + pin);
		System.out.println("chipsToSend: " + chipsToSend);
		System.out.println("actualSent: " + actualSent);
		ChipsDifferenceAndBeyond diffBeyond = new ChipsDifferenceAndBeyond();

		if (chipsToSend.equals(actualSent)) {
			numOfFullyKeptAgreements++;
			diffBeyond.agreedActualSentDiff = 0;
			diffBeyond.chipsBeyondAgreed = 0;
			System.out.println(" numOfFullyKeptAgreements: "
					+ numOfFullyKeptAgreements);
		} else if (chipsToSend.strictlyContains(actualSent)) {
			numOfPartiallyKeptAgreements++;
			int chipsToSendCount = chipsToSend.getTotalNumChips();
			int chipsActualSentCount = actualSent.getTotalNumChips();
			diffBeyond.agreedActualSentDiff = chipsToSendCount
					- chipsActualSentCount;
			System.out.println("numOfPartiallyKeptAgreements: "
					+ numOfPartiallyKeptAgreements);
		} else if (!actualSent.isEmpty()) {
			numOfPartiallyPlusKeptAgreements++;
			diffBeyond.agreedActualSentDiff = 0;
			for (Chips cToSend : chipsToSend.getChips()) {
				boolean bMatchColor = false;
				for (Chips cActual : actualSent.getChips()) {
					if (cToSend.color.compareTo(cActual.color) == 0) {
						if (cToSend.number >= cActual.number)
							diffBeyond.agreedActualSentDiff += cToSend.number
									- cActual.number;
						bMatchColor = true;
						break;
					}
				}
				if (!bMatchColor)
					diffBeyond.agreedActualSentDiff += cToSend.number;
			}
			for (Chips cActual : actualSent.getChips()) {
				boolean bMatchColor = false;
				for (Chips cToSend : chipsToSend.getChips()) {
					if (cActual.color.compareTo(cToSend.color) == 0) {
						if (cActual.number >= cToSend.number)
							diffBeyond.chipsBeyondAgreed += cActual.number
									- cToSend.number;
						bMatchColor = true;
						break;
					}
				}
				if (!bMatchColor)
					diffBeyond.chipsBeyondAgreed += cActual.number;
			}

			System.out.println("numOfPartiallyPlusKeptAgreements: "
					+ numOfPartiallyPlusKeptAgreements);
		} else {
			numOfUnkeptAgreements++;
			diffBeyond.agreedActualSentDiff = chipsToSend.getTotalNumChips();
			diffBeyond.chipsBeyondAgreed = 0;
			System.out.println("numOfUnkeptAgreements: "
					+ numOfUnkeptAgreements);
		}

		return diffBeyond;

	}

	public class AccRo {
		boolean bAccept;
		int round;
	}

	public void processAgreementsData(OffersVec offers) {
		for (Offer of : offers.getOffers()) {
			if (of.isAccepted()) {
				ChipsDifferenceAndBeyond cBd = checkExchange(of.getToSend(), of
						.getActualSent());
				agreedActualSentDiff.add(cBd.agreedActualSentDiff);
				chipsBeyondAgreed.add(cBd.chipsBeyondAgreed);
			}
		}
	}

	public String toString() {
		String retVal = typeOfPlayer + "," + isTI + "," + pin + ","
				+ IdToString() + "," + score + "," + bReachedGoal + ","
				+ numOfAcceptedOffers + "," + numOfRejectedOffers + ","
				+ numOfFullyKeptAgreements + ",";
		retVal += numOfUnkeptAgreements + "," + numOfPartiallyKeptAgreements
				+ "," + numOfPartiallyPlusKeptAgreements + ",";
		retVal += VectorOfIntToString(agreedActualSentDiff) + ","
				+ VectorOfIntToString(chipsBeyondAgreed) + ",";
		retVal += numOfProposals + "," + isProposerFirst;

		return retVal;
	}
}