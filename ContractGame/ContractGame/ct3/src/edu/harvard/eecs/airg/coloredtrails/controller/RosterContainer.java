package edu.harvard.eecs.airg.coloredtrails.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


/**
 * A serializable container for saving and loading game details (rosters)
 * @author kpozin
 *
 */
public class RosterContainer implements Serializable
{
	private static final long serialVersionUID = 2L;
	public AltOffersGameDetails[][] hhAGameDetails;
	public AltOffersGameDetails[][] hhBGameDetails;
	public AltOffersGameDetails[][] hcGameDetails;
	public AltOffersGameDetails[][] ccGameDetails;

	public static RosterContainer loadFromFile(String experimentId) throws ClassNotFoundException
	{
		RosterContainer rc = null;
		final String filename = experimentId + ".roster";
		try
		{
			File file = new File(filename);
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
			rc = (RosterContainer)in.readObject();
			in.close();
		}
		catch (IOException e)
		{
			System.out.println("Couldn't load " + filename + " (it might not exist yet).");
		}
		return rc;
	}
	
	public void saveToFile(String experimentId) throws IOException
	{
		File file = new File(experimentId + ".roster");
		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
		out.writeObject(this);
		out.close();
	}
}