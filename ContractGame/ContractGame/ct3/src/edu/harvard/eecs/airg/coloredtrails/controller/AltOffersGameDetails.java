/**
 * 
 */
package edu.harvard.eecs.airg.coloredtrails.controller;

import java.io.Serializable;

/**
 * Stores information about the players in a single AltOffers game
 * 
 * @author KPozin
 * 
 */
public class AltOffersGameDetails implements Serializable {
	private static final long serialVersionUID = 2L;

	public int[] playerPins = new int[2];
	public boolean[] playerAgent = new boolean[2];
	public boolean[] playerDependent = new boolean[2];
	public String[] playerNames = new String[2];

	public static final String[] dependencies = { "ID", "DI", "DD" };
	public static final String[] relationships = { "FRIEND", "STRANGER" };

	public AltOffersGameDetails() {
		playerNames[0] = "";
		playerNames[1] = "";
	}

	public String toString() {
		String player0type = playerAgent[0] ? "Agent" : "Human";
		String player1type = playerAgent[1] ? "Agent" : "Human";

		return "(" + playerPins[0] + " [" + playerNames[0] + "] " + player0type
				+ " " + getDependency(0) + ", " + playerPins[1] + " ["
				+ playerNames[1] + "] " + player1type + " " + getDependency(1)
				+ ")";
	}

	public String getDependency() {
		return (playerDependent[0] ? "D" : "I")
				+ (playerDependent[1] ? "D" : "I");
	}

	public String getDependency(int playerIndex) {
		return (playerDependent[playerIndex] ? "D" : "I");
	}

	public void setDependency(String dep) {
		dep = dep.toUpperCase();
		playerDependent[0] = (dep.charAt(0) == 'D' ? true : false);
		playerDependent[1] = (dep.charAt(1) == 'D' ? true : false);
	}

	public void switchOrder() {
		int tempPin = playerPins[0];
		boolean tempAgent = playerAgent[0];
		boolean tempDependent = playerDependent[0];

		playerPins[0] = playerPins[1];
		playerAgent[0] = playerAgent[1];
		playerDependent[0] = playerDependent[1];

		playerPins[1] = tempPin;
		playerAgent[1] = tempAgent;
		playerDependent[1] = tempDependent;
	}

	public void switchDependency() {
		boolean tempDependent = playerDependent[0];
		playerDependent[0] = playerDependent[1];
		playerDependent[1] = tempDependent;
	}
}