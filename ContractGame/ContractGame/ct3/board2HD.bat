:: controller receives two parameters; dependency relationship (mandatory) and frienship (optional)
:: Dependency relationship can be HD (human dependent) HI (Human independent) or IN (interdependent)
:: examples:
::  java -classpath "dist/ct3.jar;gameconfigs" AltOffersControl  HD FRIEND
 java -classpath "dist/ct3.jar;gameconfigs" AltOffersControl  HD
