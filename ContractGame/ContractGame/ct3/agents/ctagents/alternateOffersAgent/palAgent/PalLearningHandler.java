package ctagents.alternateOffersAgent.palAgent;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

import ctagents.alternateOffersAgent.LearningHandler;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;
import edu.harvard.eecs.airg.coloredtrails.shared.types.GameStatus;

public class PalLearningHandler extends LearningHandler {

	private GameStatus gs;
	private int agentId;

	private ArrayList<Double> previousReliability;
	private ArrayList<Double> weightedPreviousReliability;
	private ArrayList<Integer> lastOfferCalculated;

	public PalLearningHandler(GameStatus gs, int myId, String homeFolder) {

		// Creating the base reliability for both players according to a
		// heuristic value
		previousReliability = new ArrayList<Double>();
		weightedPreviousReliability = new ArrayList<Double>();

		// Creating the array indexes in advance so there will not be a problem
		// of creation order. 0.6 - Initialization in case we have problems with
		// the averages file
		previousReliability.add(0, 0.6);
		previousReliability.add(1, 0.6);

		// Reading the PR averages from a file according to the culture
		try {
			FileReader PRfile = new FileReader(homeFolder + "/PRavg.txt");
			BufferedReader bufPR = new BufferedReader(PRfile);
			double agentPR = Double.parseDouble(bufPR.readLine());
			double opPR = Double.parseDouble(bufPR.readLine());

			previousReliability.set(myId, agentPR);
			previousReliability.set(1 - myId, opPR);
		} catch (FileNotFoundException e) {
			System.out.println("Can't find PR averages file");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Problem reading PR averages file");
			e.printStackTrace();
		}

		weightedPreviousReliability.add(0, 1.0);
		weightedPreviousReliability.add(1, 1.0);

		// Calculating the WPR according to the base PR
		setWeightedPreviousReliability(0);
		setWeightedPreviousReliability(1);

		// Creating an array indicating when was the last calculation for each
		// player
		lastOfferCalculated = new ArrayList<Integer>();
		lastOfferCalculated.add(0, 0);
		lastOfferCalculated.add(1, 0);

		this.gs = gs;
		agentId = myId;
	}

	public double getPreviousReliability(int perGameId) {

		return previousReliability.get(perGameId);
	}

	public double getWeightedPreviousReliability(int perGameId) {

		return weightedPreviousReliability.get(perGameId);
	}

	/**
	 * Calculates the previous reliability to be calculated at the next round if
	 * a specific move will be made.
	 * 
	 * @param perGameId
	 *            - of the player we calculate it's reliability
	 * @param proposed
	 *            , actuallySent - the offer and the move that is about to be
	 *            done
	 */
	public double getHypotheticPR(int perGameId, ChipSet proposed,
			ChipSet actuallySent, ChipSet opChips) {

		// Getting the opponents details
		int opId = 1 - perGameId;

		double hypoPR = reliabilityCalculator(proposed, actuallySent, opChips,
				opId);

		// If the calculator returned an error, send the current PR
		if (hypoPR == -1) {
			hypoPR = getPreviousReliability(perGameId);
		}

		return hypoPR;
	}

	/*
	 * This is the basic formula for calculating the Weighted previous
	 * reliability.
	 */
	public double calcWPR(double prevWPR, double PR) {
		return 0.3 * prevWPR + 0.7 * PR;
	}

	/**
	 * This method calculates the PR and WPR for a specific player. It is done
	 * together because the PR has to be calculated before the WPR.
	 * 
	 * @param perGameId
	 * @param csBeforeExchange
	 */
	public void calcReliabilityParams(int perGameId, ChipSet csBeforeExchange) {

		// Making sure that the PR and WPR weren't already calculated for the
		// last offer, and that the last offer caused an agreement
		if ((lastOfferCalculated.get(perGameId) < getNumOfCommitmentsMade())
				&& (iKeptCommitmentAtPhase.lastElement() != NO_AGREEMENT)) {

			// Updating the WPR only if the PR was updated
			if (calcPreviousReliability(perGameId, csBeforeExchange)) {
				setWeightedPreviousReliability(perGameId);
			}

			// Indicating the last accepted offer that the calculation was made
			lastOfferCalculated.set(perGameId, getNumOfCommitmentsMade());
		}
	}

	/**
	 * This method calculates the WPR. It is only called by the
	 * calcReliabilityParams to make sure that the calculation is only done once
	 * for each agreement and only after the PR is updated
	 * 
	 * @param perGameId
	 */
	private void setWeightedPreviousReliability(int perGameId) {

		double newWPR = calcWPR(weightedPreviousReliability.get(perGameId),
				previousReliability.get(perGameId));

		weightedPreviousReliability.set(perGameId, newWPR);
	}

	/**
	 * This method calculates the PR. It is only called by the
	 * calcReliabilityParams to make sure that the calculation is only done once
	 * for each agreement.
	 * 
	 * @param player
	 *            - the player we want to calculate his reliability
	 * @param opCsBeforeExchange
	 *            - the chips of the opponent!
	 * @return true id the PR was updated
	 */
	private boolean calcPreviousReliability(int player,
			ChipSet opCsBeforeExchange) {

		ChipSet promisedCS;
		ChipSet sentCS;

		// Getting the relevant chip set according to the player being
		// evaluated
		if (player == agentId) {
			promisedCS = chipsOfferedByMe.lastElement();
			sentCS = chipsSentByMe.lastElement();
		} else {
			promisedCS = chipsOfferedByOp.lastElement();
			sentCS = chipsSentByOp.lastElement();
		}

		// If the player didn't promise any chips, there is no point in updating
		// the PR
		double newPR = reliabilityCalculator(promisedCS, sentCS,
				opCsBeforeExchange, 1 - player);
		if (newPR == -1) {
			return false;
		} else {
			previousReliability.set(player, newPR);
			return true;
		}
	}

	/**
	 * Basic function that calculates a players reliability according to the
	 * difference between the score he promised to the opposite player and the
	 * score he actually gave him
	 * 
	 * @param promised
	 *            , sent = the offer and the actual move
	 * @param opBeforeExchange
	 *            , opId = the data of the opponent of the player we calc it's
	 *            reliability
	 */
	private double reliabilityCalculator(ChipSet promised, ChipSet sent,
			ChipSet opBeforeExchange, int opId) {
		double reliability = 0.0;

		// If the opponent didn't promise any thing, there is no point in
		// updating the PR. Sending indication for it
		if (promised.getNumChips() == 0) {
			return -1;
		}

		// Calculating the chips amount if all the chips where sent as
		// promised and the amount with the chips actually sent
		ChipSet promisedChips = ChipSet.addChipSets(opBeforeExchange, promised);
		ChipSet withSentChips = ChipSet.addChipSets(opBeforeExchange, sent);

		double prevScore = FixedPathScoreUtil.getInstance(gs).getPlayerScore(
				opBeforeExchange, opId);
		double promisedScore = FixedPathScoreUtil.getInstance(gs)
				.getPlayerScore(promisedChips, opId);
		double actualScore = FixedPathScoreUtil.getInstance(gs).getPlayerScore(
				withSentChips, opId);

		double promisedBenefit = promisedScore - prevScore;
		double actualBenefit = actualScore - prevScore;

		// If the denominator is zero, return 1
		// Actually, this is not quite accurate because when the denominator
		// is 0 and the numerator is positive, that means that the player
		// was being generous after having promised nothing
		if (promisedScore == prevScore) {
			reliability = 1.0;
		} else {
			reliability = actualBenefit / promisedBenefit;
		}

		return reliability;
	}
}