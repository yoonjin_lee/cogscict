//////////////////////////////////////////////////////////////////////////
// File:		Reliability.java 					    				//
// Purpose:		Implements the reliability static class.				//
//////////////////////////////////////////////////////////////////////////
package ctagents.alternateOffersAgent.cluelessAgent;

//Imports from Java common framework
import java.util.Set;
import java.util.Random;
import java.lang.Math;

//Imports from CT framework
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;
import edu.harvard.eecs.airg.coloredtrails.shared.types.RowCol;
import edu.harvard.eecs.airg.coloredtrails.shared.Scoring;
import edu.harvard.eecs.airg.coloredtrails.shared.types.PlayerStatus;
import ctagents.alternateOffersAgent.Proposal;
import ctagents.FileLogger;

//////////////////////////////////////////////////////////////////////////
//Class:		Reliability		 					    				//
//Purpose:		Implements the reliability static methods.				//
//////////////////////////////////////////////////////////////////////////
public class Reliability
{
	// Debugging related members
	private static final boolean s_IsDebug = true;
	private static final FileLogger s_Logger = FileLogger.getInstance("Reliability");
	private static final TreeLogger s_TreeLogger = TreeLogger.getInstance();
	
	// Default reliability weight for last round
	private static double s_ReliabilityWeightForLastRound = 0.6;
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		logMsg													//
	// Purpose:		Logs a new message to the log file.						//
	// Parameters:	@ message - The message to log.							//
	// Remarks:		* Logs only when debug mode is activated.				//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private static void logMsg(String message)
	{
		// Write a new line with the message if we're in debug mode
		if (s_IsDebug)
		{
			s_Logger.writeln(message);
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getPlayerScore     					    				//
	// Purpose:		Gets the player's score.						 		//
	// Parameters:	@ playerPosition - the player's position.				//
	//				@ playerChips - the player's chips.						//
	//				@ goalPosition - the goal position.						//
	//				@ scoring - the scoring.								//
	// Returns:		double													//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////	
	public static double getPlayerScore(RowCol playerPosition,
										ChipSet playerChips,
										RowCol goalPosition,
										Scoring scoring)
	{
		PlayerStatus playerStatus = new PlayerStatus();
		
		// Set the player status
		playerStatus.setPosition(playerPosition);
		playerStatus.setChips(playerChips);
		
		// Return the score
		return scoring.score(playerStatus, goalPosition);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getRoundReliability										//
	// Purpose:		Gets the reliability factor of one round of transfers.	//
	// Parameters:	@ otherPlayerPosition - the other player's position.	//
	//				@ goalPosition - the goal position.						//
	//				@ scoring - the scoring.								//
	//				@ chipsMentToBeSentToOther - the chips that the other   //
	//											 player was supposed to		//
	//											 receive.					//
	//				@ chipsMentToBeSentByOther - the chips that the other   //
	//											 player was supposed to		//
	//											 send.						//
	//				@ otherPlayerScore - the score of the other player		//
	//									 AFTER the transfer has been made.  //
	// Returns:		Double which is a measure of the reliability score.		//
	// Remarks:		* One player's reliability is deduced from the other    //
	//				  player's expected score against its current score.	//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public static double getRoundReliability(RowCol otherPlayerPosition,
											 RowCol goalPosition,
											 Scoring scoring,
											 ChipSet otherPlayerOriginalChips,
											 ChipSet chipsMentToBeSentToOther,
											 ChipSet chipsActuallySentToOther,
											 double otherPlayerOriginalScore)
	{
		// Build chips for a full transfer and the actual transfer
		ChipSet chipsForFullTransfer = ChipSet.addChipSets(otherPlayerOriginalChips, chipsMentToBeSentToOther);
		ChipSet chipsActuallySent = ChipSet.addChipSets(otherPlayerOriginalChips, chipsActuallySentToOther);
		logMsg("getRoundReliability(): chips for full transfer (" + chipsForFullTransfer + ").");
		logMsg("getRoundReliability(): chips actually sent (" + chipsActuallySent + ").");
		
		// Calculate the scores
		double fullTransferScore = getPlayerScore(otherPlayerPosition, chipsForFullTransfer, goalPosition, scoring);
		double actualScore = getPlayerScore(otherPlayerPosition, chipsActuallySent, goalPosition, scoring);
		logMsg("getRoundReliability(): full transfer score (" + fullTransferScore + ").");
		logMsg("getRoundReliability(): actual score (" + actualScore + ").");
		logMsg("getRoundReliability(): original score (" + otherPlayerOriginalScore + ").");
		
		// Tree log
		s_TreeLogger.addTag("FullTransferScore", new Double(fullTransferScore));
		s_TreeLogger.addTag("ActualScore", new Double(actualScore));
		s_TreeLogger.addTag("OriginalScore", new Double(otherPlayerOriginalScore));
		
		// Calculate the benifits
		double actualBenifit = actualScore - otherPlayerOriginalScore;
		double promisedBenifit = fullTransferScore - otherPlayerOriginalScore;
		
		// Defend against division by zero
		if (promisedBenifit == 0.0)
		{
			logMsg("getRoundReliability(): defaulted the reliability.");
			return 1.0;
		}
		
		// Return the ratio
		return actualBenifit / promisedBenifit;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		setLastRoundWeight										//
	// Purpose:		Sets the last round reliability weight compared to the  //
	//				history weight.											//
	// Parameters:	@ reliabilityWeightForLastRound - the weight of the 	//
	//												  round of the			//
	//												  reliability.			//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public static void setLastRoundWeight(double reliabilityWeightForLastRound)
	{
		// Check for errors
		if ((0.0 > reliabilityWeightForLastRound) ||
			(1.0 < reliabilityWeightForLastRound))
		{
			throw new RuntimeException("The given weight should be a real number between 0 and 1.");
		}
		
		// Set and quit
		s_ReliabilityWeightForLastRound = reliabilityWeightForLastRound;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getAccumulatedReliability								//
	// Purpose:		Gets the weighted reliability for a new round given the	//
	//				reliability history.									//
	// Parameters:	@ historyReliability - the history reliability.			//
	//				@ newRoundReliability - the round reliability.			//
	//				@ numberOfTransfersSoFar - the number of transfers.		//
	// Returns:		Double which is the new reliability.					//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public static double getAccumulatedReliability(double historyReliability,
												   double newRoundReliability,
												   int numberOfTransfersSoFar)
	{
		double lastRoundWeight = s_ReliabilityWeightForLastRound;
		double historyWeight = 1.0 - lastRoundWeight;
		
		// Get the average reliability of the history
		double historyAverage = historyReliability / numberOfTransfersSoFar;
		
		// Get the new average
		double newAverage = (historyWeight * historyAverage) + (lastRoundWeight * newRoundReliability);

		// Return the weighted reliability
		return (numberOfTransfersSoFar + 1) * newAverage;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getChipSetFromReliability								//
	// Purpose:		Gets a chipset from the given reliability.				//
	// Parameters:	@ chipSet - the base chipset for the whole transfer.	//
	//				@ reliability - the given reliability.					//
	//				@ randomGenerator - the random generator.				//
	// Returns:		A chipset which is derived from the reliability and the //
	//				given base chipset.										//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public static ChipSet getChipSetFromReliability(ChipSet chipSet,
													double reliability,
													Random randomGenerator)
	{
		ChipSet result = new ChipSet();
		
		// Iterate upon colors
		for (String color : chipSet.getColors())
		{
			// Iterate upon all the chips from that color
			for (int counter = 0; counter < chipSet.getNumChips(color); counter++)
			{
				// Add that color given the reliability chance
				if (randomGenerator.nextDouble() <= reliability)
				{
					result.add(color, 1);
				}
			}
		}
		
		// Return the result
		return result;
	}
}
