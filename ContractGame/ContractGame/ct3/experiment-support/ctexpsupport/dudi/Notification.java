/*
Colored Trails

Copyright (C) 2007, President and Fellows of Harvard College.  All Rights Reserved.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ctexpsupport.dudi;

import java.util.ArrayList;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Goal;

/**
 * This class represents feedback from the subject to the system
 * about what goals have changed
 * 
 * @author Sevan G. Ficici
 *
 */
public class Notification
{
	public ArrayList<Goal> goals = new ArrayList<Goal>();
	
	public void clearNotification()
	{
		goals.clear();
	}
	
	public void addGoal(Goal g)
	{
		goals.add(g);
	}
	
	public void removeGoal(Goal g)
	{
		goals.remove(g);
	}
	
	public ArrayList<Goal> getGoals()
	{
		return goals;
	}
	
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("Notification:\n");
		for (Goal g : goals)
			sb.append(g);
		return sb.toString();
	}
}
