/*
	Colored Trails
	
	Copyright (C) 2007, President and Fellows of Harvard College.  All Rights Reserved.
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ctexpsupport.dudi;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This class represents one briefing from the system to the subject;
 * the briefing consists of a series of items, where each item (BriefingItem)
 * is a set of goals to watch in case they change in a particular way.
 * each element (BriefingElement) in an item specifies one goal and an 
 * associated change in that goal's value to watch for in conjunction with
 * the other goals and changes in the item.
 * 
 * @author Sevan G. Ficici
 *
 */
public class Briefing implements Serializable
{
	public ArrayList<BriefingItem> items = new ArrayList<BriefingItem>();
	
	public Briefing()
	{
		// nothing to do here
	}
	
	public Briefing(ArrayList<BriefingItem> items)
	{
		this.items = items;
	}
	
	public void addItem(BriefingItem bi)
	{
		items.add(bi);
	}
	
	public int getNumItems()
	{
		return items.size();
	}
	
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("Briefing:\n");
		for (BriefingItem bi : items)
			sb.append(bi);
		return sb.toString();
	}
}
