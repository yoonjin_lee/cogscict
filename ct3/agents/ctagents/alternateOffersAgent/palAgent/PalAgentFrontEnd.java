package ctagents.alternateOffersAgent.palAgent;

import ctagents.alternateOffersAgent.palAgent.PalPlayer.Culture;
import java.util.*;
import ctagents.FileLogger;


/**
 * Main class of the PAL (Personality Adaptor Learning) agent.
 * 
 * @author Yael Blumberg
 * 
 */
public final class PalAgentFrontEnd {
	
	public static void main(String[] args) {
		
		if ((args.length != 1 && args[0] != null && args[1] != null)) {
			PalPlayer agent = new PalPlayer(Integer.parseInt(args[0]),Integer.parseInt(args[1]));
			agent.setClientName(args[0]);
			String PALlogName;
			String PAL_LOG_PREFIX = "PAL_logConfig_";

			// Setting the log name according to the timestemp
			Long currTS = (new Date()).getTime();
			PALlogName = PAL_LOG_PREFIX + currTS;
			FileLogger.getInstance(PALlogName).writeln("A new PAL game has begun");

			agent.start();
		} else {
			System.out
					.println("You need to specify an id for the agent and the culture of the opponent: "
							+ Culture.ISRAEL.ordinal()
							+ "-Israel, "
							+ Culture.USA.ordinal()
							+ "-USA, "
							+ Culture.LEBANON.ordinal() + "-Lebanon");
		}
	}
}