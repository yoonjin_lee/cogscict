//////////////////////////////////////////////////////////////////////////
// File:		SushiRolloutSimulatorFrontEnd.java					   	//
// Purpose:		Implements the front end of the rollout simulator.		//
//////////////////////////////////////////////////////////////////////////
package ctagents.alternateOffersAgent.sushiAgent;

//Imports from Java common framework
import java.util.*;

//////////////////////////////////////////////////////////////////////////
// Class:		SushiRolloutSimulatorFrontEnd				  			//
// Purpose:		Implements the front end of the Sushi rollout			//
//				simulator.												//
//////////////////////////////////////////////////////////////////////////
public final class SushiRolloutSimulatorFrontEnd
{
	//////////////////////////////////////////////////////////////////////////
	// Method:		main							    					//
	// Purpose:		Implements the main function, which starts the player.	//
	// Parameters:	* args - The command line arguments. Their order:		//
	//						 1. The name of the agent.						//
	//						 2. The agent's initial reliability.			//
	//						 3. The opponent's initial reliability.			//
	//						 4. Whether we're logging.						//
	// Checked:		02/02/2013												//
	//////////////////////////////////////////////////////////////////////////
	public static void main(String[] args)
	{
		// Create the agent and parse its parameters
		double myInitialReliability = Double.parseDouble(args[1]);
		double opponentInitialReliability = Double.parseDouble(args[2]);
		boolean isLogging = Boolean.parseBoolean(args[3]);
		SushiPlayer agent = new SushiPlayer(myInitialReliability, opponentInitialReliability, "ISRAEL", isLogging);
		
		// Simulation for the rollout model
		agent.rolloutSimulationMode();
		
		// Give the agent a proper PIN number and start it
		agent.setClientName(args[0]);
		agent.start();
	}
}
