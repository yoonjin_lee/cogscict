//////////////////////////////////////////////////////////////////////////
// File:		GetAcceptChance.java					   				//
// Purpose:		Gets accept chance of opponent.							//
//////////////////////////////////////////////////////////////////////////
package ctagents.alternateOffersAgent.sushiAgent;

//Imports from Java common framework
import java.util.*;

//////////////////////////////////////////////////////////////////////////
// Class:		GetAcceptChance					  		  				//
// Purpose:		Gets accept chance of opponent.							//
//////////////////////////////////////////////////////////////////////////
public final class GetAcceptChance
{
	//////////////////////////////////////////////////////////////////////////
	// Method:		main							    					//
	// Purpose:		Implements the main function, which gets the accept		//
	//				chance of the opponent.									//
	// Parameters:	* args - The command line arguments. Their order:		//
	//						 1. Culture.									//
	//						 2. Board type.									//
	//						 3. My current score.							//
	//						 4. Opponent's current score.					//
	//						 5. My resulting score.							//
	//						 6. Opponent's resulting score.					//
	//						 7. Generosity level.							//
	//						 8. My needed colors.							//
	//						 9. My other colors.							//
	//						 10. Opponent needed colors.					//
	//						 11. Opponent other colors.						//
	//////////////////////////////////////////////////////////////////////////
	public static void main(String[] args)
	{
		// Get culture and board type
		String culture = args[0];
		String boardType = args[1];
		
		// Get current and resulting scores
		double myCurrentScore = Double.parseDouble(args[2]);
		double myResultingScore = Double.parseDouble(args[3]);
		double opponentCurrentScore = Double.parseDouble(args[4]);
		double opponentResultingScore = Double.parseDouble(args[5]);
		
		// Generosity level
		double generosityLevel = Double.parseDouble(args[6]);
		
		// Get needed colors and other colors
		int myNeededColors = Integer.parseInt(args[7]);
		int myOtherColors = Integer.parseInt(args[8]);
		int opponentNeededColors = Integer.parseInt(args[9]);
		int opponentOtherColors = Integer.parseInt(args[10]);
		
		// Initialize the Weka model
		String cultureFolder = "agents/ctagents/alternateOffersAgent/sushiAgent/" + culture + "/" + culture + "_" + boardType;
		WekaWrapper.setCultureFolder(cultureFolder);
		WekaWrapper.setCulture(WekaWrapper.Culture.valueOf(culture));
		
		// Build the instance values
		Object[] instanceValues = { myCurrentScore,
									myResultingScore,
									opponentCurrentScore,
									opponentResultingScore,
									generosityLevel,
									(double)myNeededColors,
									(double)myOtherColors,
									(double)opponentNeededColors,
									(double)opponentOtherColors,
									"TRUE" };
		
		// Get the accept probability from the Weka model
		double acceptChance;
		try
		{
			acceptChance = WekaWrapper.getAcceptProbability(instanceValues);
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			acceptChance = 0.5;
		}
		
		// Print the outcome
		System.out.println("Accept chance is: " + Double.toString(acceptChance));
	}
}
