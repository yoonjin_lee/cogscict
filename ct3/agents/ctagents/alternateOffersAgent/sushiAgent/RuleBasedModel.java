//////////////////////////////////////////////////////////////////////////
// File:		RuleBasedModel.java 			   						//
// Purpose:		Implements a rule based model rollout model.			//
//////////////////////////////////////////////////////////////////////////
package ctagents.alternateOffersAgent.sushiAgent;

// Imports from Java common framework
import java.util.Random;
import java.util.List;
import java.lang.Math;

// Imports from CT framework
import edu.harvard.eecs.airg.coloredtrails.shared.Scoring;
import edu.harvard.eecs.airg.coloredtrails.alglib.ShortestPaths;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Board;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;
import edu.harvard.eecs.airg.coloredtrails.shared.types.GameStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Path;
import edu.harvard.eecs.airg.coloredtrails.shared.types.RowCol;
import edu.harvard.eecs.airg.coloredtrails.shared.types.PlayerStatus;

//////////////////////////////////////////////////////////////////////////
// Class:		RuleBasedModel											//
// Purpose:		A rule based rollout model.								//
//////////////////////////////////////////////////////////////////////////
public class RuleBasedModel implements RolloutModel
{
	// A logger
	private XMLLogger m_Logger;
	
	// Saves last round reliabilities
	private double m_AgentLastRoundReliability;
	private double m_HumanLastRoundReliability;
	
	// Saves the last accepted offer and the last offer
	private Offer m_LastAcceptedOffer;
	private Offer m_LastOffer;
	
	// Whether we can reach the goal or not
	private boolean m_AgentReachingGoal;
	private boolean m_HumanReachingGoal;
	
	// The random generator
	private static Random s_RandomGenerator = null;
	
	// The random seed
	private static final long s_RandomSeed = 1337;
	
	private boolean dummy;		// TODO: remove
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		Constructor												//
	// Purpose:		Constructs a new rule based model instance.				//
	// Parameters:	* initialGameState - The initial game state.			//
	//				* myInitialReliability - My initial reliability.		//
	//				* opponentInitialReliability - The opponent's initial	//
	//											   reliability.				//
	//////////////////////////////////////////////////////////////////////////
	public RuleBasedModel(XMLLogger logger,
						  GameState initialGameState,
						  double myInitialReliability,
						  double opponentInitialReliability)
	{
		// Save the logger
		this.m_Logger = logger;
		
		// Save reliabilities
		this.m_AgentLastRoundReliability = myInitialReliability;
		this.m_HumanLastRoundReliability = opponentInitialReliability;
		
		// Nullify offers
		this.m_LastAcceptedOffer = null;
		this.m_LastOffer = null;
		
		// Initialize the random generator
		if (s_RandomGenerator == null)
		{
			s_RandomGenerator = new Random(s_RandomSeed);
		}
		
		// Save whether we can reach the goal
		this.m_AgentReachingGoal = initialGameState.canReachGoal(true);
		this.m_HumanReachingGoal = initialGameState.canReachGoal(false);
		
		dummy = true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		ceilDiv								    				//
	// Purpose:		Implements ceiling division.							//
	// Parameters:	* num - The numerator.									//
	//				* denum - The denumerator.								//
	//////////////////////////////////////////////////////////////////////////
	private static int ceilDiv(int num,
					    	   int denum)
	{
		// Implement ceiling division
		return (denum == 0 ? 0 : ((num + denum - 1) / denum));
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		onSimulationStart				    					//
	//////////////////////////////////////////////////////////////////////////
	public void onSimulationStart(GameState gameState)
	{
		// Logging
		this.m_Logger.beginTag("OnSimulationStart");
		this.m_Logger.addTag("AgentLastRoundReliability", new Double(this.m_AgentLastRoundReliability));
		this.m_Logger.addTag("HumanLastRoundReliability", new Double(this.m_HumanLastRoundReliability));
		this.m_Logger.addTag("AgentReachingGoal", new Boolean(this.m_AgentReachingGoal));
		this.m_Logger.addTag("HumanReachingGoal", new Boolean(this.m_HumanReachingGoal));

		// Update the last offer and last accepted offer
		this.m_LastOffer = gameState.getLastOffer();
		this.m_LastAcceptedOffer = gameState.getLastAcceptedOffer();
		this.m_Logger.addTag("LastOffer", new String(this.m_LastOffer == null ? "null" : this.m_LastOffer.toString()));
		this.m_Logger.addTag("LastAcceptedOffer", new String(this.m_LastAcceptedOffer == null ? "null" : this.m_LastAcceptedOffer.toString()));
		
		// Logging
		this.m_Logger.endTag("OnSimulationStart");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		onSendingChips					    					//
	//////////////////////////////////////////////////////////////////////////
	public void onSendingChips(GameState gameState,
							   ChipSet chipsSentByAgent,
							   ChipSet chipsSentByHuman)
	{
		// Logging
		this.m_Logger.beginTag("OnSendingChips");
		this.m_Logger.addTag("LastAcceptedOffer", new String(this.m_LastAcceptedOffer == null ? "null" : this.m_LastAcceptedOffer.toString()));
		this.m_Logger.addTag("ChipsSentByAgent", chipsSentByAgent);
		this.m_Logger.addTag("ChipsSentByHuman", chipsSentByHuman);
		
		// Update last round reliabilities of both players if it's relevant
		if (this.m_LastAcceptedOffer != null)
		{
			this.m_AgentLastRoundReliability = ReliabilityUtil.getRoundReliability(gameState.getBoard(),
																			       gameState.getScoring(),
																			       gameState.getPlayerChips(true),
																			       gameState.getPlayerChips(false),
																			       gameState.getPlayerPosition(false),
																			       true,
																			       this.m_LastAcceptedOffer,
																			       chipsSentByAgent);
			this.m_HumanLastRoundReliability = ReliabilityUtil.getRoundReliability(gameState.getBoard(),
																				   gameState.getScoring(),
																				   gameState.getPlayerChips(false),
																				   gameState.getPlayerChips(true),
																				   gameState.getPlayerPosition(true),
																				   false,
																				   this.m_LastAcceptedOffer,
																				   chipsSentByHuman);
			this.m_Logger.addTag("AgentReliability", this.m_AgentLastRoundReliability);
			this.m_Logger.addTag("HumanReliability", this.m_HumanLastRoundReliability);
		}
		
		if (dummy)
		{
			if (this.m_AgentLastRoundReliability != 1.0)
			{
				dummy = false;
			}
		}
		
		// Forget the last accepted offer and the last offer
		this.m_LastAcceptedOffer = null;
		this.m_LastOffer = null;
		this.m_Logger.endTag("OnSendingChips");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		onSentChips						    					//
	//////////////////////////////////////////////////////////////////////////
	public void onSentChips(GameState gameState)
	{
		// Logging
		this.m_Logger.beginTag("OnSentChips");
		
		// Save whether we can reach the goal
		this.m_AgentReachingGoal = gameState.canReachGoal(true);
		this.m_HumanReachingGoal = gameState.canReachGoal(false);
		this.m_Logger.addTag("AgentReachingGoal", new Boolean(this.m_AgentReachingGoal));
		this.m_Logger.addTag("HumanReachingGoal", new Boolean(this.m_HumanReachingGoal));
		this.m_Logger.addTag("AgentNeededUnownedChips", gameState.getPlayerNeededUnownedChips(true));
		this.m_Logger.addTag("HumanNeededUnownedChips", gameState.getPlayerNeededUnownedChips(false));
		this.m_Logger.endTag("OnSentChips");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		onProposal						    					//
	//////////////////////////////////////////////////////////////////////////
	public void onProposal(GameState gameState,
						   boolean isAgentTurn,
						   Offer offer)
	{
		// Logging
		this.m_Logger.beginTag("OnProposal");
		
		// Save the offer
		this.m_LastOffer = (offer == null ? null : new Offer(offer));
		this.m_Logger.addTag("Offer", offer);
		this.m_Logger.endTag("OnProposal");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		onResponse												//
	//////////////////////////////////////////////////////////////////////////
	public void onResponse(GameState gameState,
						   boolean isAgentTurn,
						   boolean response)
	{
		// Logging
		this.m_Logger.beginTag("OnResponse");
		this.m_Logger.addTag("Response", new Boolean(response));
		
		// If we accepted - save the last accepted offer, otherwise - forget the offers
		if (response)
		{
			this.m_LastAcceptedOffer = new Offer(this.m_LastOffer);
			this.m_Logger.addTag("LastAcceptedOffer", this.m_LastAcceptedOffer);
		}
		else
		{
			this.m_LastAcceptedOffer = null;
			this.m_LastOffer = null;
		}
		this.m_Logger.endTag("OnResponse");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		onGameOver												//
	//////////////////////////////////////////////////////////////////////////
	public void onGameOver(GameState gameState,
						   double agentScore,
						   double humanScore)
	{
		// Logging
		this.m_Logger.beginTag("OnGameOver");
		this.m_Logger.addTag("AgentScore", new Double(agentScore));
		this.m_Logger.addTag("HumanScore", new Double(humanScore));
		this.m_Logger.endTag("OnGameOver");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		onMove													//
	//////////////////////////////////////////////////////////////////////////
	public void onMove(GameState gameState,
					   boolean isAgentMove)
	{
		// Logging
		this.m_Logger.beginTag("OnMove");
		this.m_Logger.addTag("IsAgentMove", new Boolean(isAgentMove));
		this.m_Logger.endTag("OnMove");
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		getAgentOffer											//
	// Purpose:		Gets an agent's new offer.								//
	// Parameters:	* gameState - The current game state.					//
	//////////////////////////////////////////////////////////////////////////
	private Offer getAgentOffer(GameState gameState)
	{
		// Logging
		this.m_Logger.beginTag("GetAgentOffer");
		
		// Calculate sendable chips of both players
		ChipSet agentSendableChips = EquivalenceChips.subChipSetsNoNegatives(gameState.getPlayerChips(true), gameState.getPlayerNeededChips(true));
		ChipSet humanSendableChips = EquivalenceChips.subChipSetsNoNegatives(gameState.getPlayerChips(false), gameState.getPlayerNeededChips(false));
		this.m_Logger.addTag("AgentSendableChips", agentSendableChips);
		this.m_Logger.addTag("HumanSendableChips", humanSendableChips);
		
		// Return a random offer
		Offer result = EquivalenceChips.getRandomOffer(agentSendableChips, humanSendableChips, gameState.getPlayerNeededUnownedChips(true), gameState.getPlayerNeededUnownedChips(false), s_RandomGenerator);
		this.m_Logger.endTag("GetAgentOffer");
		return result;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getHumanOfferTI											//
	// Purpose:		Gets a human's new offer if the human can reach the		//
	//				goal.													//
	// Parameters:	* gameState - The current game state.					//
	//////////////////////////////////////////////////////////////////////////
	private Offer getHumanOfferTI(GameState gameState)
	{
		// Logging
		this.m_Logger.beginTag("GetHumanOfferTI");
		
		// These will save the number of meaningful chips sent by the agent and the chips sent by the human
		int meaningfulsSentByHuman = 1;
		int chipsSentByAgent = 0;
				
		// Conclude each player's chips
		ChipSet agentChips = gameState.getPlayerChips(true);
		ChipSet humanChips = gameState.getPlayerChips(false);
		this.m_Logger.addTag("HumanChips", humanChips);
		this.m_Logger.addTag("AgentChips", agentChips);
		
		// Calculate the needed chips of each player
		ChipSet agentNeededChips = gameState.getPlayerNeededChips(true);
		ChipSet humanNeededChips = gameState.getPlayerNeededChips(false);
		this.m_Logger.addTag("HumanNeededChips", humanNeededChips);
		this.m_Logger.addTag("AgentNeededChips", agentNeededChips);
		
		// Calculate the sendable chips by each player
		ChipSet chipsSendableByAgent = EquivalenceChips.subChipSetsNoNegatives(agentChips, agentNeededChips);
		ChipSet chipsSendableByHuman = EquivalenceChips.subChipSetsNoNegatives(humanChips, humanNeededChips);
		this.m_Logger.addTag("ChipsSendableByHuman", chipsSendableByHuman);
		this.m_Logger.addTag("ChipsSendableByAgent", chipsSendableByAgent);
		
		// Calculate the meaningful chips and other chips of the human
		ChipSet meaningfulChipsSentByHuman = EquivalenceChips.intersectChips(chipsSendableByHuman, gameState.getPlayerNeededUnownedChips(true));
		ChipSet otherChipsSentByHuman = EquivalenceChips.subChipSetsNoNegatives(chipsSendableByHuman, meaningfulChipsSentByHuman);
		
		// Only 1-3 offers
		chipsSentByAgent = 3;
		
		// Logging
		this.m_Logger.addTag("ChipsSentByAgent", new Integer(chipsSentByAgent));
		this.m_Logger.addTag("MeaningfulChipsSentByHuman", new Integer(meaningfulsSentByHuman));
		
		// Create the offer
		ChipSet sentByHuman = meaningfulChipsSentByHuman.getSubset(meaningfulsSentByHuman);
		ChipSet sentByAgent = chipsSendableByAgent.getSubset(chipsSentByAgent);
		
		// Send an empty offer if the agent doesn't have enough chips
		if (chipsSendableByAgent.getNumChips() != chipsSentByAgent)
		{
			this.m_Logger.addTag("Offer", "EmptySinceAgentLacksChips");
			this.m_Logger.endTag("GetHumanOfferTI");
			return new Offer(new ChipSet(), new ChipSet());
		}
				
		// Fix if not enough meaningful chips were taken
		int sendByHumanSoFar = sentByHuman.getNumChips();
		if (sendByHumanSoFar < meaningfulsSentByHuman)
		{
			sentByHuman = ChipSet.addChipSets(sentByHuman, otherChipsSentByHuman.getSubset(meaningfulsSentByHuman - sendByHumanSoFar));
		}
		
		// Return the new offer
		Offer offer = new Offer(sentByAgent, sentByHuman);
		this.m_Logger.addTag("Offer", offer);
		this.m_Logger.endTag("GetHumanOfferTI");
		return offer;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getHumanOfferTD											//
	// Purpose:		Gets a human's new offer if the human cannot reach the	//
	//				goal.													//
	// Parameters:	* gameState - The current game state.					//
	//////////////////////////////////////////////////////////////////////////
	private Offer getHumanOfferTD(GameState gameState)
	{
		// Logging
		this.m_Logger.beginTag("GetHumanOfferTD");
		
		// These will save the number of meaningful chips sent by the human and the chips sent by the agent
		int meaningfulsSentByAgent = 1;
		int chipsSentByHuman = 0;
				
		// Conclude each player's chips
		ChipSet agentChips = gameState.getPlayerChips(true);
		ChipSet humanChips = gameState.getPlayerChips(false);
		this.m_Logger.addTag("HumanChips", humanChips);
		this.m_Logger.addTag("AgentChips", agentChips);
		
		// Calculate the needed chips of each player
		ChipSet agentNeededChips = gameState.getPlayerNeededChips(true);
		ChipSet humanNeededChips = gameState.getPlayerNeededChips(false);
		this.m_Logger.addTag("HumanNeededChips", humanNeededChips);
		this.m_Logger.addTag("AgentNeededChips", agentNeededChips);
		
		// Calculate the sendable chips by each player
		ChipSet chipsSendableByAgent = EquivalenceChips.subChipSetsNoNegatives(agentChips, agentNeededChips);
		ChipSet chipsSendableByHuman = EquivalenceChips.subChipSetsNoNegatives(humanChips, humanNeededChips);
		this.m_Logger.addTag("ChipsSendableByHuman", chipsSendableByHuman);
		this.m_Logger.addTag("ChipsSendableByAgent", chipsSendableByAgent);
		
		// Calculate the meaningful chips and other chips of the agent
		ChipSet meaningfulChipsSentByAgent = EquivalenceChips.intersectChips(chipsSendableByAgent, gameState.getPlayerNeededUnownedChips(false));
		ChipSet otherChipsSentByAgent = EquivalenceChips.subChipSetsNoNegatives(chipsSendableByAgent, meaningfulChipsSentByAgent);
				
		// Making offers is a 50% - 50% chance between 1-1 and 2-1
		if (s_RandomGenerator.nextDouble() <= 0.5)
		{
			chipsSentByHuman = 2;
		}
		else
		{
			chipsSentByHuman = 1;
		}
		
		// Logging
		this.m_Logger.addTag("ChipsSentByHuman", new Integer(chipsSentByHuman));
		this.m_Logger.addTag("MeaningfulChipsSentByAgent", new Integer(meaningfulsSentByAgent));
		
		// Create the offer
		ChipSet sentByAgent = meaningfulChipsSentByAgent.getSubset(meaningfulsSentByAgent);
		ChipSet sentByHuman = chipsSendableByHuman.getSubset(chipsSentByHuman);
				
		// Fix if not enough meaningful chips were taken
		int sendByAgentSoFar = sentByAgent.getNumChips();
		if (sendByAgentSoFar < meaningfulsSentByAgent)
		{
			sentByAgent = ChipSet.addChipSets(sentByAgent, otherChipsSentByAgent.getSubset(meaningfulsSentByAgent - sendByAgentSoFar));
		}
		
		// Return the new offer
		Offer offer = new Offer(sentByAgent, sentByHuman);
		this.m_Logger.addTag("Offer", offer);
		this.m_Logger.endTag("GetHumanOfferTD");
		return offer;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getHumanOffer											//
	// Purpose:		Gets a human's new offer.								//
	// Parameters:	* gameState - The current game state.					//
	//////////////////////////////////////////////////////////////////////////
	private Offer getHumanOffer(GameState gameState)
	{
		// Logging
		this.m_Logger.beginTag("GetHumanOffer");
		this.m_Logger.addTag("HumanReachesTheGoal", this.m_HumanReachingGoal);
		
		// Call the right method
		Offer result = null;
		if (this.m_HumanReachingGoal)
		{
			result = getHumanOfferTI(gameState);
		}
		else
		{
			result = getHumanOfferTD(gameState);
		}
		
		// Logging
		this.m_Logger.endTag("GetHumanOffer");
		return result;			
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getOffer						    					//
	//////////////////////////////////////////////////////////////////////////
	public Offer getOffer(GameState gameState,
						  boolean isAgentTurn)
	{
		Offer offer = new Offer(new ChipSet(), new ChipSet());
		
		// Logging
		this.m_Logger.beginTag("GetOffer");
		this.m_Logger.addTag("IsAgentTurn", new Boolean(isAgentTurn));
		
		// Select the action according to the turn
		if (isAgentTurn)
		{
			offer = getAgentOffer(gameState);
		}
		else
		{
			offer = getHumanOffer(gameState);
		}
		
		// Return the offer
		this.m_Logger.addTag("Offer", offer);
		this.m_Logger.endTag("GetOffer");
		return offer;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getAgentResponse				    					//
	// Purpose:		Gets the agent's response.								//
	//////////////////////////////////////////////////////////////////////////
	private boolean getAgentResponse()
	{
		// It's the agent's response - so act uniformly random
		this.m_Logger.beginTag("GetAgentResponse");
		boolean response = s_RandomGenerator.nextBoolean();
		this.m_Logger.addTag("Response", new Boolean(response));
		this.m_Logger.endTag("GetAgentResponse");
		return response;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getHumanResponseTI					    				//
	// Purpose:		Gets the human's response if the human can reach the	//
	//				goal.													//
	// Parameters:	* gameState - The game state.							//
	//				* offer - The offer to consider.						//
	//////////////////////////////////////////////////////////////////////////
	private boolean getHumanResponseTI(GameState gameState,
									   Offer offer)
	{
		double acceptChance = 0.0;
		
		// Logging
		this.m_Logger.beginTag("GetHumanResponseTI");
		
		// Analyze the number of needed chips sent by the agent and the number of chips sent by the human
		int humanReceivedChips = offer.m_ChipsSentByAgent.getNumChips();
		
		int agentReceivedNeededChips = EquivalenceChips.intersectChips(gameState.getPlayerNeededUnownedChips(true), offer.m_ChipsSentByHuman).getNumChips();
		this.m_Logger.addTag("HumanReceivedChips", new Integer(humanReceivedChips));
		this.m_Logger.addTag("AgentReceivedNeededChips", new Integer(agentReceivedNeededChips));
		
		// Narrow down the needed chips
		/*
		int agentSendRatio = ceilDiv(humanReceivedChips, agentReceivedNeededChips);
		int humanSendRatio = ceilDiv(agentReceivedNeededChips, humanReceivedChips);
		this.m_Logger.addTag("AgentSendRatio", new Integer(agentSendRatio));
		this.m_Logger.addTag("HumanSendRatio", new Integer(humanSendRatio));
		
		// Select the ratio according to the hand-written table
		switch (agentSendRatio)
		{
		case 1:
			acceptChance = (this.m_AgentLastRoundReliability == 1.0 ? 1.0 : 0.0);
		case 2:
			acceptChance = 1.0;
			break;
		default:
			acceptChance = 0.0;
			break;
		}*/
		if (humanReceivedChips == 1 && agentReceivedNeededChips == 1)
		{
			acceptChance = (this.m_AgentLastRoundReliability == 1.0 ? 1.0 : 0.0);
		}
		if (humanReceivedChips == 3 && agentReceivedNeededChips == 1)
		{
			acceptChance = 1.0;
		}

		// Logging
		this.m_Logger.addTag("AcceptChance", new Double(acceptChance));
		
		// Act according to the accept chance
		this.m_Logger.endTag("GetHumanResponseTI");
		return (this.s_RandomGenerator.nextDouble() <= acceptChance);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getHumanResponseTD					    				//
	// Purpose:		Gets the human's response if the human cannot reach the	//
	//				goal.													//
	// Parameters:	* gameState - The game state.							//
	//				* offer - The offer to consider.						//
	//////////////////////////////////////////////////////////////////////////
	private boolean getHumanResponseTD(GameState gameState,
									   Offer offer)
	{
		double acceptChance = 0.0;
		
		// Logging
		this.m_Logger.beginTag("GetHumanResponseTD");
		
		// Analyze the number of needed chips sent by the agent and the number of chips sent by the human
		int agentReceivedChips = offer.m_ChipsSentByHuman.getNumChips();
		
		int humanReceivedNeededChips = EquivalenceChips.intersectChips(gameState.getPlayerNeededUnownedChips(false), offer.m_ChipsSentByAgent).getNumChips();
		this.m_Logger.addTag("AgentReceivedChips", new Integer(agentReceivedChips));
		this.m_Logger.addTag("HumanReceivedNeededChips", new Integer(humanReceivedNeededChips));
		
		// Narrow down the needed chips
		int humanSendRatio = ceilDiv(agentReceivedChips, humanReceivedNeededChips);
		int agentSendRatio = ceilDiv(humanReceivedNeededChips, agentReceivedChips);
		this.m_Logger.addTag("HumanSendRatio", new Integer(humanSendRatio));
		this.m_Logger.addTag("AgentSendRatio", new Integer(agentSendRatio));
		
		// Select the ratio according to the hand-written table
		if (agentSendRatio == 2)
		{
			acceptChance = 1.0;
		}
		else
		{
			switch (humanSendRatio)
			{
			case 1:
				acceptChance = 0.9;
				break;
			case 2:
				acceptChance = 0.9;
				break;
			case 3:
				acceptChance = 0.8;
				break;
			case 4:
				acceptChance = 0.3;
				break;
			default:
				acceptChance = 0.0;
				break;
			}
		}

		// Logging
		this.m_Logger.addTag("AcceptChance", new Double(acceptChance));
		
		// Act according to the accept chance
		this.m_Logger.endTag("GetHumanResponseTD");
		return (this.s_RandomGenerator.nextDouble() <= acceptChance);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getHumanResponse					    				//
	// Purpose:		Gets the human's response.								//
	// Parameters:	* gameState - The game state.							//
	//				* offer - The offer to consider.						//
	//////////////////////////////////////////////////////////////////////////
	private boolean getHumanResponse(GameState gameState,
								     Offer offer)
	{
		// Logging
		this.m_Logger.beginTag("GetHumanResponse");
		this.m_Logger.addTag("HumanReachesTheGoal", this.m_HumanReachingGoal);
		
		// Call the right method
		boolean result = false;
		if (this.m_HumanReachingGoal)
		{
			result = getHumanResponseTI(gameState, offer);
		}
		else
		{
			result = getHumanResponseTD(gameState, offer);
		}
		
		// Logging
		this.m_Logger.endTag("GetHumanResponse");
		return result;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getResponse						    					//
	//////////////////////////////////////////////////////////////////////////
	public boolean getResponse(GameState gameState,
							   boolean isAgentTurn,
							   Offer offer)
	{
		boolean response = false;
		
		// Logging
		this.m_Logger.beginTag("GetResponse");
		this.m_Logger.addTag("IsAgentTurn", new Boolean(isAgentTurn));
		this.m_Logger.addTag("Offer", offer);
		
		// Get the response according to the turn
		if (isAgentTurn)
		{
			response = getAgentResponse();
		}
		else
		{
			response = getHumanResponse(gameState, offer);
		}

		// Logging
		this.m_Logger.addTag("Response", new Boolean(response));
		this.m_Logger.endTag("GetResponse");
		return response;
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		getAgentExchange					    				//
	// Purpose:		Gets the agent's exchange.								//
	// Parameters:	* lastAcceptedOffer - The last accepted offer.			//
	//////////////////////////////////////////////////////////////////////////
	private ChipSet getAgentExchange(Offer lastAcceptedOffer)
	{
		// Uniformly choose how many chips to send
		int toSend = this.s_RandomGenerator.nextInt(lastAcceptedOffer.m_ChipsSentByMe.getNumChips() + 1);
		return lastAcceptedOffer.m_ChipsSentByMe.getSubset(toSend);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getHumanExchangeTI					    				//
	// Purpose:		Gets the human's exchange if the human can reach the	//
	//				goal.													//
	// Parameters:	* gameState - The game state.							//
	//				* lastAcceptedOffer - The last accepted offer.			//
	//////////////////////////////////////////////////////////////////////////
	private ChipSet getHumanExchangeTI(GameState gameState,
									   Offer lastAcceptedOffer)
	{
		// Logging
		this.m_Logger.beginTag("GetHumanExchangeTI");
		
		// This will keep the result
		ChipSet result = new ChipSet();
		
		// We will have a send chance
		double sendChance = 0.0;
		
		// If the agent becomes independent - cheat
		if (gameState.willPlayerBecomeIndependent(true, lastAcceptedOffer.m_ChipsSentByAgent, lastAcceptedOffer.m_ChipsSentByHuman))
		{
			sendChance = (dummy ? 1.0 : 0.0);
			dummy = false;
		}
		else
		{
			// Send only if the agent was fully reliable in the previous round
			sendChance = (this.m_AgentLastRoundReliability == 1.0 ? 1.0 : 0.0);
		}
				
		// Send based on the chance
		this.m_Logger.addTag("SendChance", new Double(sendChance));
		if (s_RandomGenerator.nextDouble() <= sendChance)
		{
			result = new ChipSet(lastAcceptedOffer.m_ChipsSentByOpponent);
		}

		// Return the result
		this.m_Logger.addTag("Exchange", result);
		this.m_Logger.endTag("GetHumanExchangeTI");
		return result;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getHumanExchangeTD					    				//
	// Purpose:		Gets the human's exchange if the human cannot reach the	//
	//				goal.													//
	// Parameters:	* gameState - The game state.							//
	//				* lastAcceptedOffer - The last accepted offer.			//
	//////////////////////////////////////////////////////////////////////////
	private ChipSet getHumanExchangeTD(GameState gameState,
									   Offer lastAcceptedOffer)
	{
		// Logging
		this.m_Logger.beginTag("GetHumanExchangeTD");
		
		// This will keep the result
		ChipSet result = new ChipSet();
		
		// We will have a send chance
		double sendChance = 0.0;
		
		// If the agent was fully reliable - send everything
		this.m_Logger.addTag("AgentLastRoundReliability", new Double(this.m_AgentLastRoundReliability));
		if (1.0 == this.m_AgentLastRoundReliability)
		{
			sendChance = 1.0;
		}
		else
		{
			// Analyze the number of needed chips by the human
			int humanReceivedNeededChips = EquivalenceChips.intersectChips(gameState.getPlayerNeededUnownedChips(false), lastAcceptedOffer.m_ChipsSentByAgent).getNumChips();
			int agentReceivedChips = lastAcceptedOffer.m_ChipsSentByOpponent.getNumChips();
			this.m_Logger.addTag("AgentReceivedChips", new Integer(agentReceivedChips));
			this.m_Logger.addTag("HumanReceivedNeededChips", new Integer(humanReceivedNeededChips));
			
			// If the offer is X-Y, send with a chance of 0.3X/Y
			int minTerm = Math.min(agentReceivedChips, humanReceivedNeededChips);
			int maxTerm = Math.max(agentReceivedChips, humanReceivedNeededChips);
			sendChance = 0.3 * (maxTerm == 0 ? 0.0 : (((double)minTerm) / maxTerm));
		}
		
		// Send based on the chance
		this.m_Logger.addTag("SendChance", new Double(sendChance));
		if (s_RandomGenerator.nextDouble() <= sendChance)
		{
			result = new ChipSet(lastAcceptedOffer.m_ChipsSentByOpponent);
		}

		// Return the result
		this.m_Logger.addTag("Exchange", result);
		this.m_Logger.endTag("GetHumanExchangeTD");
		return result;
	}
	
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getHumanExchange					    				//
	// Purpose:		Gets the human's exchange.								//
	// Parameters:	* gameState - The game state.							//
	//				* lastAcceptedOffer - The last accepted offer.			//
	//////////////////////////////////////////////////////////////////////////
	private ChipSet getHumanExchange(GameState gameState,
									 Offer lastAcceptedOffer)
	{
		// Logging
		this.m_Logger.beginTag("GetHumanExchangeTD");
		this.m_Logger.addTag("HumanReachesTheGoal", this.m_HumanReachingGoal);
		
		// Call the right method
		ChipSet result = null;
		if (this.m_HumanReachingGoal)
		{
			result = getHumanExchangeTI(gameState, lastAcceptedOffer);
		}
		else
		{
			result = getHumanExchangeTD(gameState, lastAcceptedOffer);
		}
		
		// Logging
		this.m_Logger.endTag("GetHumanExchangeTD");
		return result;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getExchange												//
	//////////////////////////////////////////////////////////////////////////
	public ChipSet getExchange(GameState gameState,
							   boolean isAgentTurn,
							   Offer lastAcceptedOffer)
	{
		ChipSet exchange = new ChipSet();
		
		// Logger
		this.m_Logger.beginTag("GetExchange");
		this.m_Logger.addTag("IsAgentTurn", new Boolean(isAgentTurn));
		this.m_Logger.addTag("LastAcceptedOffer", new String(null == lastAcceptedOffer ? "null" : lastAcceptedOffer.toString()));
		
		// Verify that something was accepted
		if (null != lastAcceptedOffer)
		{
			// Get the exchange according to the turn
			if (isAgentTurn)
			{
				exchange = getAgentExchange(lastAcceptedOffer);
			}
			else
			{
				exchange = getHumanExchange(gameState, lastAcceptedOffer);
			}
		}
		
		// Return the exchange
		this.m_Logger.addTag("Exchange", exchange);
		this.m_Logger.endTag("GetExchange");
		return exchange;
	}
}
