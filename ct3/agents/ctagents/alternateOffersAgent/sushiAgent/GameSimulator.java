//////////////////////////////////////////////////////////////////////////
// File:		GameSimulator.java 					    				//
// Purpose:		Implements the game simulator class.					//
//				The game simulator class is able to simulate the game	//
//				of CT (both players).									//
//////////////////////////////////////////////////////////////////////////
package ctagents.alternateOffersAgent.sushiAgent;

// Imports from Java common framework
import java.util.Collections;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;
import java.util.List;
import java.lang.Math;
import java.util.HashMap;
import java.util.Map;
import java.io.IOException;

// Imports from CT framework
import edu.harvard.eecs.airg.coloredtrails.shared.Scoring;
import edu.harvard.eecs.airg.coloredtrails.alglib.ShortestPaths;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Board;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;
import edu.harvard.eecs.airg.coloredtrails.shared.types.GameStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Path;
import edu.harvard.eecs.airg.coloredtrails.shared.types.RowCol;
import edu.harvard.eecs.airg.coloredtrails.shared.types.PlayerStatus;

//////////////////////////////////////////////////////////////////////////
// Class:		GameSimulator											//
// Purpose:		Simulates games of CT.									//
//////////////////////////////////////////////////////////////////////////
public class GameSimulator
{
	//////////////////////////////////////////////////////////////////////////
	// Class:		GameTreeNode											//
	// Purpose:		Operates as a game tree node.							//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	private class GameTreeNode
	{		
		// The game state
		private GameState m_GameState;
		
		// Sum of scores for me and for the opponent
		private double m_MySumOfScores;
		private double m_OpponentSumOfScores;
		
		// The number of simulations
		private int m_NumberOfSimulations;
		
		// The node's children nodes and the parent node
		private GameTreeNode m_Parent;
		private Map<Object, GameTreeNode> m_Children;
		
		// The node's depth
		private int m_Depth;
		
		// Whether to use maximum expectation for the opponent or not
		private static final boolean s_UseExpectationForOpponent = true;
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		Constructor												//
		// Purpose:		Constructs a new instance of a game tree node.			//
		// Parameters:	* gameState - The game state saved in the node.			//
		//				* parent - The parent of the node.						//
		// Checked:		31/01/2013												//
		//////////////////////////////////////////////////////////////////////////
		public GameTreeNode(GameState gameState,
							GameTreeNode parent)
		{
			// Save the parent
			this.m_Parent = parent;
			
			// Copy the game state
			this.m_GameState = new GameState(gameState);
			
			// Initialize the children
			this.m_Children = new HashMap();
			List<Object> allActions = gameState.getAllPossibleActions();
			for (Object action : allActions)
			{
				this.m_Children.put(action, null);
			}
			
			// Initialize other simple memebers
			this.m_MySumOfScores = 0.0;
			this.m_OpponentSumOfScores = 0.0;
			this.m_NumberOfSimulations = 0;
			
			// Initialize depth
			this.m_Depth = (parent == null ? 0 : parent.m_Depth + 1);
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		expectationSelection									//
		// Purpose:		Performs an expectation selection.						//
		//////////////////////////////////////////////////////////////////////////
		private GameTreeNode expectationSelection()
		{
			Object playerAction = null;
			GameTreeNode expectedNode = null;
			
			// Get the opponent's action from the rollout model
			switch (this.m_GameState.getGamePhase())
			{
			case PROP1:
			case PROP2:
				playerAction = this.m_GameState.getRolloutModel().getOffer(this.m_GameState, this.m_GameState.isAgentTurn());
				break;
			case RESP1:
			case RESP2:
				playerAction = this.m_GameState.getRolloutModel().getResponse(this.m_GameState, this.m_GameState.isAgentTurn(), this.m_GameState.getLastOffer());
				break;
			case EXCH1:
			case EXCH2:
				playerAction = this.m_GameState.getRolloutModel().getExchange(this.m_GameState, this.m_GameState.isAgentTurn(), this.m_GameState.getLastAcceptedOffer());
				break;
			}
			s_Logger.addTag("ExpectedAction", playerAction);
			
			// Add if it wasn't explored
			if (this.m_Children.get(playerAction) == null)
			{
				s_Logger.addTag("Node", "New");
				GameState newGameState = new GameState(this.m_GameState);
				newGameState.doAction(playerAction);
				GameTreeNode newChildNode = new GameTreeNode(newGameState, this);
				this.m_Children.put(playerAction, newChildNode);
				expectedNode = newChildNode;
				expectedNode.m_GameState.setRolloutModel(this.m_GameState.getRolloutModel());
			}
			else
			{
				s_Logger.addTag("Node", "Existing");
				updateRolloutInTreeTravel(playerAction);
				this.m_Children.get(playerAction).m_GameState.setRolloutModel(this.m_GameState.getRolloutModel());
				expectedNode = this.m_Children.get(playerAction).treePolicySelection();
			}
			
			// Return the expected node
			return expectedNode;
		}

		//////////////////////////////////////////////////////////////////////////
		// Method:		expandUnexploredAction									//
		// Purpose:		Expands the children of the current node.				//
		//////////////////////////////////////////////////////////////////////////
		private GameTreeNode expandUnexploredAction()
		{
			Object unexploredAction = null;
			
			// Search for the unexplored child and create it
			for (Map.Entry<Object, GameTreeNode> entry : this.m_Children.entrySet())
			{
				// If we found the unexplored child
				if (entry.getValue() == null)
				{
					unexploredAction = entry.getKey();
					s_Logger.addTag("UnexploredAction", unexploredAction);
					break;
				}
			}
			
			// Create the child node and return it
			GameState newGameState = new GameState(this.m_GameState);
			newGameState.doAction(unexploredAction);
			GameTreeNode newChildNode = new GameTreeNode(newGameState, this);
			this.m_Children.put(unexploredAction, newChildNode);
			newGameState.setRolloutModel(this.m_GameState.getRolloutModel());
			return newChildNode;
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		getExplorationCoefficientFromState						//
		// Purpose:		Gets the appropriate exploration coefficient from the	//
		//				given node's game state.								//
		//////////////////////////////////////////////////////////////////////////
		private double getExplorationCoefficientFromState()
		{
			// Return the correct value according to the game phase
			switch (this.m_GameState.getGamePhase())
			{
			case PROP1:
			case PROP2:
				return s_ExplorationCoefficientForOffers;
			case RESP1:
			case RESP2:
				return s_ExplorationCoefficientForResponses;
			case EXCH1:
			case EXCH2:
				return s_ExplorationCoefficientForExchanges;
			}
			
			// Should never happen
			return 0.0;
		}
		
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		getActionFromUCB										//
		// Purpose:		Performs the UCB1 policy upon actions.					//
		//////////////////////////////////////////////////////////////////////////
		private Object getActionFromUCB()
		{
			// Logging
			s_Logger.addTag("NumberOfSimulations", new Integer(this.m_NumberOfSimulations));
			s_Logger.addTag("Children", new Integer(this.m_Children.size()));
			
			// Get the exploration coefficient
			double explorationCoefficient =  getExplorationCoefficientFromState();
			
			// These will save the selected action and the maximum UCB formula
			Object selectedAction = null;
			double maxUCB = 0.0;
			
			// Iterate all actions and search for the maximum exploration-exploitation term in a child node
			for (Map.Entry<Object, GameTreeNode> entry : this.m_Children.entrySet())
			{
				GameTreeNode child = entry.getValue();
				
				// Get the correct exploitation term according to the self's player's turn
				double exploitationTerm = (this.m_GameState.isMyTurn() ? child.m_MySumOfScores : child.m_OpponentSumOfScores) / child.m_NumberOfSimulations;

				// Get the correct exploration term
				double explorationTerm = explorationCoefficient * Math.sqrt(Math.log(this.m_NumberOfSimulations) / child.m_NumberOfSimulations);
				
				// Maximize the overall term
				double currentUCB = exploitationTerm + explorationTerm;
				if ((selectedAction == null) || (maxUCB < currentUCB))
				{
					maxUCB = currentUCB;
					selectedAction = entry.getKey();
				}
			}
			
			// Now that we have a child node
			s_Logger.addTag("MaxUCB", selectedAction);
			
			// Return the action
			return selectedAction;
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		performUCB												//
		// Purpose:		Performs the UCB1 policy upon nodes.					//
		//////////////////////////////////////////////////////////////////////////
		private GameTreeNode performUCB()
		{
			// Just get the action and act recursively
			Object selectedAction = getActionFromUCB();
			updateRolloutInTreeTravel(selectedAction);
			this.m_Children.get(selectedAction).m_GameState.setRolloutModel(this.m_GameState.getRolloutModel());
			return this.m_Children.get(selectedAction).treePolicySelection();
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		updateRolloutInTreeTravel								//
		// Purpose:		Updates the rollout model with the action on the tree.	//
		// Parameters:	* actionToBeTaken - The action about to be taken.		//
		//////////////////////////////////////////////////////////////////////////
		private void updateRolloutInTreeTravel(Object actionToBeTaken)
		{	
			// Update according to the action and the game phase
			switch (this.m_GameState.getGamePhase())
			{
			case PROP1:
			case PROP2:
				this.m_GameState.getRolloutModel().onProposal(this.m_GameState, this.m_GameState.isAgentTurn(), (Offer)actionToBeTaken);
				break;
			case RESP1:
			case RESP2:
				this.m_GameState.getRolloutModel().onResponse(this.m_GameState, this.m_GameState.isAgentTurn(), (Boolean)actionToBeTaken);
				break;
			case EXCH1:
				break;
			case EXCH2:
				ChipSet firstExchangeChips = this.m_GameState.getFirstExchangeChips();
				ChipSet agentSentChips = (this.m_GameState.isAgentTurn() ? (ChipSet)actionToBeTaken : firstExchangeChips);
				ChipSet humanSentChips = (this.m_GameState.isAgentTurn() ? firstExchangeChips : (ChipSet)actionToBeTaken);
				this.m_GameState.getRolloutModel().onSendingChips(this.m_GameState, agentSentChips, humanSentChips);
				this.m_GameState.getRolloutModel().onSentChips(this.m_GameState);
				break;
			}
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		treePolicySelection										//
		// Purpose:		Performs the tree policy selection and returns the node	//
		//				for a new rollout. This method also attaches the new	//
		//				node to the tree (expansion).							//
		// Checked:		31/01/2013												//
		//////////////////////////////////////////////////////////////////////////
		public GameTreeNode treePolicySelection()
		{
			GameTreeNode resultNode = null;
			s_Logger.beginTag("TreePolicySelection");
			
			// Return self if this is a terminal node
			if (this.m_GameState.isGameOver())
			{
				s_Logger.addTag("GameOver", "GameOver");
				s_Logger.endTag("TreePolicySelection");
				return this;
			}
			
			// If we're using expectation for opponent
			if ((s_UseExpectationForOpponent) && (!this.m_GameState.isMyTurn()))
			{
				resultNode = expectationSelection();
			}
			else
			{				
				// If not all actions were explored
				if (this.m_NumberOfSimulations < this.m_Children.size() + 1)
				{
					resultNode = expandUnexploredAction();
				}
				else
				{
					resultNode = performUCB();
				}
			}
			
			// Return the result
			s_Logger.endTag("TreePolicySelection");
			return resultNode;
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		getBestAverageAction									//
		// Purpose:		Returns the action with the best average.				//
		// Checked:		31/01/2013												//
		//////////////////////////////////////////////////////////////////////////
		public Object getBestAverageAction()
		{
			// These will save the best action and the best result
			Object bestAction = null;
			double bestResult = 0.0;
			
			// Iterate all actions and search for the maximum 
			s_Logger.beginTag("BestAction");
			s_Logger.addTag("RootSimulations", new Integer(this.m_NumberOfSimulations));
			s_Logger.addTag("AgentTurn", new Boolean(this.m_GameState.isMyTurn()));
			for (Map.Entry<Object, GameTreeNode> entry : this.m_Children.entrySet())
			{
				// Skip unexplored children
				GameTreeNode child = entry.getValue();
				if (child == null)
				{
					continue;
				}
				
				// Calculate the child's score
				double childScore = (this.m_GameState.isMyTurn() ? child.m_MySumOfScores : child.m_OpponentSumOfScores) / child.m_NumberOfSimulations;
				
				// Logging
				s_Logger.beginTag("Action");
				s_Logger.addTag("Key", entry.getKey());
				s_Logger.addTag("Score", new Double(childScore));
				s_Logger.addTag("NumberOfSimulations", new Integer(child.m_NumberOfSimulations));
				s_Logger.endTag("Action");
				
				// Maximize the child's average score
				if ((bestAction == null) || (bestResult < childScore))
				{
					bestResult = childScore;
					bestAction = entry.getKey();
				}
			}
			
			// Return the best action
			s_Logger.addTag("BestAction", bestAction);
			s_Logger.endTag("BestAction");
			s_Logger.flush();
			return bestAction;
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		defaultPolicyRollout									//
		// Purpose:		Performs the default policy rollout and returns the		//
		//				terminal GameState. This does not affect the current	//
		//				node or any of its members.								//
		// Checked:		31/01/2013												//
		//////////////////////////////////////////////////////////////////////////
		public GameState defaultPolicyRollout()
		{
			// Just perform the rollout upon the copy of the game state
			GameState result = new GameState(this.m_GameState);
			result.rollout();
			return result;
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		backupScores											//
		// Purpose:		Performs backup upon the given scores recursively.		//
		// Parameters:	* myScore - My score.									//
		//				* opponentScore - The opponent's score.					//
		// Checked:		31/01/2013												//
		//////////////////////////////////////////////////////////////////////////
		public void backupScores(double myScore,
								 double opponentScore)
		{			
			// Update the sum of scores and the number of simulations
			this.m_MySumOfScores += myScore;
			this.m_OpponentSumOfScores += opponentScore;
			this.m_NumberOfSimulations++;
			
			// Recursively update parent
			if (this.m_Parent != null)
			{
				this.m_Parent.backupScores(myScore, opponentScore);
			}
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		recursivePrint											//
		// Purpose:		Print the subtree based in the current node.			//
		// Parameters:	* indentationLevel - The indentation level.				//
		//////////////////////////////////////////////////////////////////////////
		public String recursivePrint(int indentationLevel)
		{
			String result = "";
			String indentation = "";
			
			// Add indentation
			for (int counter = 0; counter < indentationLevel; counter++)
			{
				indentation += "\t";
			}
			
			// Self representation
			result += indentation + "[ MY SCORE " + Double.toString(this.m_MySumOfScores / this.m_NumberOfSimulations) + " ]\n";
			result += indentation + "[ OP SCORE " + Double.toString(this.m_OpponentSumOfScores / this.m_NumberOfSimulations) + " ]\n";
			result += indentation + "[ #SIMULAT " + Integer.toString(this.m_NumberOfSimulations) + " ]\n";
			result += indentation + "[ PLAYER   " + new String(this.m_GameState.isAgentTurn() ? "Agent" : "Human") + " ]\n";
			
			// Iterate all actions
			for (Map.Entry<Object, GameTreeNode> entry : this.m_Children.entrySet())
			{
				// Skip null children
				GameTreeNode childNode = entry.getValue();
				if (childNode == null)
				{
					continue;
				}
				
				// Call recursively
				result += indentation + "\t (" + entry.getKey().toString() + ")\n" + childNode.recursivePrint(indentationLevel + 1);
			}
			
			// Return the result
			return result;
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		getDepth												//
		// Purpose:		Gets the depth of the current node.						//
		//////////////////////////////////////////////////////////////////////////
		public int getDepth()
		{
			// Just return the depth
			return this.m_Depth;
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		walk							    					//
		// Purpose:		Walks upon the nodes based on the given moves.			//
		// Parameters:	* moves - The moves to conduct from the root.			//
		//////////////////////////////////////////////////////////////////////////
		public GameTreeNode walk(List<Object> moves)
		{
			// Stopping condition
			if (moves.size() == 0)
			{
				return this;
			}
			
			// Call recursively
			GameTreeNode child = this.m_Children.get(moves.get(0));
			return (child == null ? null : child.walk(moves.subList(1, moves.size())));
		}
	}	
	
	//////////////////////////////////////////////////////////////////////////
	// Class:		GameTree												//
	// Purpose:		The game tree.											//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	private class GameTree
	{
		// Saves the root of the tree
		private GameTreeNode m_Root;
		
		// Saves the initial state
		private GameState m_InitialState;
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		Constructor												//
		// Purpose:		Constructs a new game tree.								//
		// Parameters:	* gameState - The initial game state.					//
		// Checked:		31/01/2013												//
		//////////////////////////////////////////////////////////////////////////
		public GameTree(GameState gameState)
		{
			// Don't save the root yet
			this.m_Root = null;
			
			// Just save the initial state
			this.m_InitialState = new GameState(gameState);
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		getRootState											//
		// Purpose:		Gets the root state of the tree.						//
		//////////////////////////////////////////////////////////////////////////
		public GameState getRootState()
		{
			// Just return the valid state
			return (this.m_Root == null ? this.m_InitialState : this.m_Root.m_GameState);
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		discountedScore											//
		// Purpose:		Return the discounted score.							//
		// Parameters:	* depth - The depth of the node.						//
		//				* score - The given score.								//
		//////////////////////////////////////////////////////////////////////////
		private double discountedScore(int depth,
									   double score)
		{
			// Return the discounted score
			return Math.pow(s_DiscountFactor, depth) * score;
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		treeSearchIteration										//
		// Purpose:		Perform one tree search iteration.						//
		// Checked:		31/01/2013												//
		//////////////////////////////////////////////////////////////////////////
		public void treeSearchIteration()
		{
			// Select a node using the tree policy selection
			GameTreeNode selectedNode = null;
			if (this.m_Root == null)
			{
				this.m_Root = new GameTreeNode(this.m_InitialState, null);
				this.m_InitialState = null;
				selectedNode = this.m_Root;
			}
			else
			{
				selectedNode = this.m_Root.treePolicySelection();
			}
			
			// Perform a rollout upon this node
			Integer rolloutSteps = 0;
			GameState terminalState = selectedNode.defaultPolicyRollout();
			int totalDepth = selectedNode.getDepth() + terminalState.getRolloutSteps();
			
			// Logging
			double myNormalizedScore = terminalState.getScore(true);		// TODO: normalized
			double opponentNormalizedScore = terminalState.getScore(false); // TODO: normalized
			s_Logger.beginTag("FinalScores");
			s_Logger.addTag("MyFinalScore", new Double(terminalState.getScore(true)));
			s_Logger.addTag("OpponentFinalScore", new Double(terminalState.getScore(false)));
			s_Logger.addTag("TotalDepth", new Integer(totalDepth));
			s_Logger.addTag("MyNormalizedScore", new Double(myNormalizedScore));
			s_Logger.addTag("OpponentNormalizedScore", new Double(opponentNormalizedScore ));
			s_Logger.endTag("FinalScores");
			
			// Discount the scores
			double myDiscountedScore = discountedScore(totalDepth, myNormalizedScore);
			double opponentDiscountedScore = discountedScore(totalDepth, opponentNormalizedScore);
			
			// Backup the normalized scores
			selectedNode.backupScores(myDiscountedScore, opponentDiscountedScore);
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		getBestAction											//
		// Purpose:		Gets the action with the best score.					//
		// Checked:		31/01/2013												//
		//////////////////////////////////////////////////////////////////////////
		public Object getBestAction()
		{
			// Just return the root's best action
			return this.m_Root.getBestAverageAction();
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		toString												//
		// Purpose:		Prints the entire tree.									//
		//////////////////////////////////////////////////////////////////////////
		@Override
		public String toString()
		{
			// Just print the root node
			return this.m_Root.recursivePrint(0);
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		getSubtree						    					//
		// Purpose:		Gets the game tree's subtree.							//
		// Parameters:	* moves - The moves to conduct from the root.			//
		//////////////////////////////////////////////////////////////////////////	
		public GameTreeNode getSubtree(List<Object> moves)
		{
			// Walk upon the tree
			return (this.m_Root == null ? null : this.m_Root.walk(moves));
		}
	}
	
	// The log name
	private static final String s_LogName = "SushiAgentLog";
	
	// Saves the game tree
	private GameTree m_GameTree;
	
	// Saves the culture
	private String m_Culture;
	
	// Logging
	private static final XMLLogger s_Logger = XMLLogger.getInstance(s_LogName);
	
	// The exploration coefficients // TODO:  // 1.0 / Math.sqrt(2.0);
	private static final double s_ExplorationCoefficientForOffers = 400.0; // 1.0 / Math.sqrt(2.0);; //400.0;
	private static final double s_ExplorationCoefficientForResponses = 120.0; //1.0 / Math.sqrt(2.0); //120.0;
	private static final double s_ExplorationCoefficientForExchanges = 135.0; //1.0 / Math.sqrt(2.0); //135.0;
	
	// Whether to create a session for each new simulation
	private static final boolean s_CreateSessionForSimulation = true;
	
	// The discount factor
	private static final double s_DiscountFactor = 1.0;
	
	// Whether we're logging
	public static boolean s_IsLogging = false;
	
	// Whether the rollout model is rule based or Weka based
	public boolean m_IsRuleBasedRolloutModel;
	
	// Initial reliabilitites
	private double m_MyInitialReliability;
	private double m_OpponentInitialReliability;
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		Constructor						    					//
	// Purpose:		Creates a new game simulator.							//
	// Parameters:	* board - The board.									//
	// 				* scoring - The scoring.								//
	// 				* phase - The game phase.								//
	//				* isFirstProposerWasMe - Whether I was the first		//
	//										 proposer of the opponent.		//
	// 				* myChips - My chips.									//
	// 				* myPosition - My position.								//
	// 				* myDormantSteps - My dormant steps.					//
	// 				* opponentChips - Opponent's chips.						//
	// 				* opponentPosition - Opponent's position.				//
	// 				* opponentDormantSteps - Opponent's dormant steps.		//
	//				* lastOffer - The last given offer, applicable only for	//
	//							  responses.								//
	//				* lastAcceptedOffer - The last accepted offer,			//
	//									  applicable only for exchanges.	//
	//				* myLastReliability - My last reliability.				//
	//				* opponentLastReliability - The opponent's last			//
	//											reliability.				//
	//				* culture - The opponent's culture.						//
	//				* isRuleBased - Whether the rollout model is rule based	//
	//								or Weka based.							//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public GameSimulator(Board board,
						 Scoring scoring,
						 GamePhase phase,
						 Boolean isFirstProposerWasMe,
						 ChipSet myChips,
						 RowCol myPosition,
						 Integer myDormantRounds,
						 ChipSet opponentChips,
						 RowCol opponentPosition,
						 Integer opponentDormantRounds,
						 Offer lastOffer,
						 Offer lastAcceptedOffer,
						 double myLastReliability,
						 double opponentLastReliability,
						 String culture,
						 boolean isRuleBased)
	{
		// Activate the logger if we're logging
		if (s_IsLogging)
		{
			s_Logger.activate();
		}
		s_Logger.beginTag("StartConditions");
		s_Logger.addTag("GamePhase", phase);
		s_Logger.addTag("IsFirstProposerWasMe", isFirstProposerWasMe);
		s_Logger.addTag("MyChips", myChips);
		s_Logger.addTag("MyPosition", myPosition);
		s_Logger.addTag("MyDormantRounds", myDormantRounds);
		s_Logger.addTag("OpponentChips", opponentChips);
		s_Logger.addTag("OpponentPosition", opponentPosition);
		s_Logger.addTag("OpponentDormantRounds", opponentDormantRounds);
		s_Logger.endTag("StartConditions");
		
		// Just initialize the game tree with a fresh new state
		GameState gameState = new GameState(s_Logger, board, scoring, phase, isFirstProposerWasMe, myChips, myPosition, myDormantRounds, opponentChips, opponentPosition, opponentDormantRounds, lastOffer, lastAcceptedOffer);
		
		// Initialize the initial reliability
		this.m_MyInitialReliability = myLastReliability;
		this.m_OpponentInitialReliability = opponentLastReliability;
		this.m_GameTree = new GameTree(gameState);
		
		// Save the culture
		this.m_Culture = new String(culture);
		
		// Save the model type
		this.m_IsRuleBasedRolloutModel = isRuleBased;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		monteCarloTreeSearch			    					//
	// Purpose:		Searches through the game tree and returns the best		//
	//				action found after the timeout is over.					//
	// Parameters:	* timeoutSeconds - The timeout in seconds.				//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public Object monteCarloTreeSearch(long timeoutSeconds)
	{
		Object bestAction = null;
		
		// Try-catch block for logger flushing
		try
		{
			// Saves the start time
			s_Logger.beginTag("MonteCarloTreeSearch");
			long startTime = System.currentTimeMillis();
			
			// While there is still time
			while (System.currentTimeMillis() - startTime < timeoutSeconds * 1000)
			{
				// Start a new session
				if (s_CreateSessionForSimulation)
				{
					s_Logger.startNewSession();
				}
				
				// Initialize a new rollout model
				RolloutModel rolloutModel = null;
				if (this.m_IsRuleBasedRolloutModel)
				{
					rolloutModel = new RuleBasedModel(s_Logger, this.m_GameTree.getRootState(), this.m_MyInitialReliability, this.m_OpponentInitialReliability);
				}
				else
				{
					rolloutModel = new WekaBasedModel(s_Logger, this.m_GameTree.getRootState(), this.m_MyInitialReliability, this.m_OpponentInitialReliability, this.m_Culture);
				}
				this.m_GameTree.getRootState().setRolloutModel(rolloutModel);
				rolloutModel.onSimulationStart(this.m_GameTree.getRootState());
				
				// Perform an MCTS iteration
				this.m_GameTree.treeSearchIteration();
			}
			
			// Get the best action
			bestAction = this.m_GameTree.getBestAction();
			
			// Logging
			s_Logger.addTag("BestAction", bestAction);
			s_Logger.endTag("MonteCarloTreeSearch");
			s_Logger.addTag("SearchTree", this.m_GameTree.toString());
			s_Logger.flush();
		}
		catch (Exception exception)
		{
			// Print the stack trace
			System.out.println("======= EXCEPTION =======");
			exception.printStackTrace();
			System.out.println("=========================");
			
			// Logger flushing and exception forwarding
			s_Logger.flush();
		}
		return bestAction;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getSubtree						    					//
	// Purpose:		Gets the game tree's subtree.							//
	// Parameters:	* moves - The moves to conduct from the root.			//
	//////////////////////////////////////////////////////////////////////////	
	private GameTreeNode getSubtree(List<Object> moves)
	{
		// Return the subtree from the game tree
		return this.m_GameTree.getSubtree(moves);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		loadPreviousSimulationData		    					//
	// Purpose:		Loads data from a previous simulation.					//
	// Parameters:	* other - The simulation to extract data from.			//
	//				* moves - The moves to that were taken from the given	//
	//						  previous simulation.							//
	//////////////////////////////////////////////////////////////////////////	
	public void loadPreviousSimulationData(GameSimulator other,
										   List<Object> moves)
	{		
		/*// Don't let a NULL simulation disturb
		if ((other == null) || (moves == null))
		{
			return;
		}
		
		// Logging
		s_Logger.beginTag("PreviousLoader");
		s_Logger.beginTag("Moves");
		for (Object move : moves)
		{
			s_Logger.addTag("Move", move);			
		}
		s_Logger.endTag("Moves");
		
		// Get the subtree from the other simulation
		GameTreeNode subtree = other.getSubtree(moves);
		if (subtree == null)
		{
			return;
		}
		
		// Logging
		s_Logger.addTag("NumberOfLoadedSimulations", new Integer(subtree.m_NumberOfSimulations));
		
		// Load the data
		this.m_GameTree.m_Root = subtree;
		s_Logger.endTag("PreviousLoader");*/
		return;
	}
}
