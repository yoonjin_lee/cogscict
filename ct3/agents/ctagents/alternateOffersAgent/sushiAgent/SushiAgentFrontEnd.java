//////////////////////////////////////////////////////////////////////////
// File:		SushiAgentFrontEnd.java					   				//
// Purpose:		Implements the front end of Sushi.						//
//////////////////////////////////////////////////////////////////////////
package ctagents.alternateOffersAgent.sushiAgent;

//Imports from Java common framework
import java.util.*;

//////////////////////////////////////////////////////////////////////////
// Class:		SushiAgentFrontEnd				  		  				//
// Purpose:		Implements the front end of Sushi.						//
// Checked:		02/02/2013												//
//////////////////////////////////////////////////////////////////////////
public final class SushiAgentFrontEnd 
{
	//////////////////////////////////////////////////////////////////////////
	// Method:		main							    					//
	// Purpose:		Implements the main function, which starts the player.	//
	// Parameters:	* args - The command line arguments. Their order:		//
	//						 1. The name of the agent.						//
	//						 2. The agent's initial reliability.			//
	//						 3. The opponent's culture.						//
	//						 4. The opponent's initial reliability.			//
	//						 5. Whether we're logging or not.				//
	// Checked:		02/02/2013												//
	//////////////////////////////////////////////////////////////////////////
	public static void main(String[] args)
	{
		// Create the agent and parse its parameters
		double myInitialReliability = Double.parseDouble(args[1]);
		double opponentInitialReliability = Double.parseDouble(args[2]);
		String culture = args[3];
		boolean isLogging = Boolean.parseBoolean(args[4]);
		SushiPlayer agent = new SushiPlayer(myInitialReliability, opponentInitialReliability, culture, isLogging);
		
		// Give the agent a proper PIN number and start it
		agent.setClientName(args[0]);
		agent.start();
	}
}
