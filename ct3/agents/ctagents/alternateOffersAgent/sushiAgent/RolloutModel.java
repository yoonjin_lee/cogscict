//////////////////////////////////////////////////////////////////////////
// File:		RolloutModel.java 					    				//
// Purpose:		Declares a rollout model interface.						//
//////////////////////////////////////////////////////////////////////////
package ctagents.alternateOffersAgent.sushiAgent;

// Imports from CT framework
import edu.harvard.eecs.airg.coloredtrails.shared.Scoring;
import edu.harvard.eecs.airg.coloredtrails.alglib.ShortestPaths;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Board;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;
import edu.harvard.eecs.airg.coloredtrails.shared.types.GameStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Path;
import edu.harvard.eecs.airg.coloredtrails.shared.types.RowCol;
import edu.harvard.eecs.airg.coloredtrails.shared.types.PlayerStatus;

//////////////////////////////////////////////////////////////////////////
// Interface:	RolloutModel											//
// Purpose:		Implements the default policy, used in rollouts.		//
//////////////////////////////////////////////////////////////////////////
public interface RolloutModel
{
	//////////////////////////////////////////////////////////////////////////
	// Method:		onSimulationStart				    					//
	// Purpose:		Call this method whenever a new simulation starts.		//
	// Parameters:	* gameState - The game state.							//
	//////////////////////////////////////////////////////////////////////////
	public void onSimulationStart(GameState gameState);
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		onSendingChips					    					//
	// Purpose:		Call this method whenever chips are sent. Note that		//
	//				This method is called prior to sending chips.			//
	// Parameters:	* gameState - The game state.							//
	//				* chipsSentByAgent - The chips sent by the agent.		//
	//				* chipsSentByHuman - The chips sent by the human.		//
	//////////////////////////////////////////////////////////////////////////
	public void onSendingChips(GameState gameState,
							   ChipSet chipsSentByAgent,
							   ChipSet chipsSentByHuman);
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		onSentChips						    					//
	// Purpose:		Call this method whenever chips are sent. Note that		//
	//				This method is called after sending chips.				//
	// Parameters:	* gameState - The game state.							//
	//////////////////////////////////////////////////////////////////////////
	public void onSentChips(GameState gameState);
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		onProposal						    					//
	// Purpose:		Call this method on a proposal.							//
	// Parameters:	* gameState - The game state.							//
	//				* isAgentTurn - Whether it's the agent's turn or the	//
	//								human's.								//
	//				* offer - The offer.									//
	//////////////////////////////////////////////////////////////////////////
	public void onProposal(GameState gameState,
						   boolean isAgentTurn,
						   Offer offer);
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		onResponse												//
	// Purpose:		Call this method on a response.							//
	// Parameters:	* gameState - The game state.							//
	//				* isAgentTurn - Whether it's the agent's turn or the	//
	//								human's.								//
	//				* response - The response.								//
	//////////////////////////////////////////////////////////////////////////
	public void onResponse(GameState gameState,
						   boolean isAgentTurn,
						   boolean response);
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		onGameOver												//
	// Purpose:		Call this method when the game is over.					//
	// Parameters:	* gameState - The game state.							//
	//				* myScore - The agent's score.							//
	//				* opponentScore - The human's score.					//
	//////////////////////////////////////////////////////////////////////////
	public void onGameOver(GameState gameState,
						   double agentScore,
						   double humanScore);
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		onMove													//
	// Purpose:		Call this method when a player move.					//
	// Parameters:	* gameState - The game state.							//
	//				* isAgentMove - Whether it's the agent's or the human's //
	//								move.									//
	//////////////////////////////////////////////////////////////////////////
	public void onMove(GameState gameState,
					   boolean isAgentMove);

	//////////////////////////////////////////////////////////////////////////
	// Method:		getOffer						    					//
	// Purpose:		Gets an offer from the model.							//
	//				* isAgentTurn - Whether it's the agent's turn or the	//
	//								human's.								//
	//////////////////////////////////////////////////////////////////////////
	public Offer getOffer(GameState gameState,
						  boolean isAgentTurn);
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getResponse						    					//
	// Purpose:		Gets a response for an offer from the model.			//
	// Parameters:	* gameState - The game state.							//
	//				* isAgentTurn - Whether it's the agent's turn or the	//
	//								human's.								//
	//				* offer - The offer to consider.						//
	//////////////////////////////////////////////////////////////////////////
	public boolean getResponse(GameState gameState,
							   boolean isAgentTurn,
							   Offer offer);
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getExchange												//
	// Purpose:		Gets the exchange chips for the last given offer from	//
	//				the model.												//
	// Parameters:	* gameState - The game state.							//
	//				* isAgentTurn - Whether it's the agent's turn or the	//
	//								human's.								//
	//				* lastAcceptedOffer - The last accepted offer.			//
	//////////////////////////////////////////////////////////////////////////
	public ChipSet getExchange(GameState gameState,
							   boolean isAgentTurn,
							   Offer lastAcceptedOffer);
}