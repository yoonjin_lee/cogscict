//////////////////////////////////////////////////////////////////////////
// File:		EquivalenceChips.java 				    				//
// Purpose:		Implements equivalence chips class.						//
//////////////////////////////////////////////////////////////////////////
package ctagents.alternateOffersAgent.sushiAgent;

// Imports from Java common framework
import java.util.List;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;

//Imports from CT framework
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;

//////////////////////////////////////////////////////////////////////////
// Class:		EquivalenceChips										//
// Purpose:		Saves equivalence chips of an equivalence class.		//
//				The equivalence class is build of a quadsome:			//
//				1. Number of chips needed by both players.				//
//				2. Number of chips exclusively needed by me.			//
//				3. Number of chips exclusively needed by the opponent.	//
//				4. Number of other chips.								//
//////////////////////////////////////////////////////////////////////////
public class EquivalenceChips
{
	// The maximum number of chips in a generated offer
	private static final int s_MaxSentChipsInGeneratedOffer = 3;
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		intersectChips											//
	// Purpose:		Intersect chips.										//
	// Parameters:	* chipsX - One of the chip sets.						//
	// 				* chipsY - The other chip set.							//
	//////////////////////////////////////////////////////////////////////////
	public static ChipSet intersectChips(ChipSet chipsX,
										 ChipSet chipsY)
	{
		ChipSet result = new ChipSet();
		
		// Iterate the colors
		for (String color : chipsX.getColors())
		{
			// Intersect chips
			result.set(color, Math.min(chipsX.getNumChips(color), chipsY.getNumChips(color)));
		}
		
		// Return the result
		return result;
	}	
		
	//////////////////////////////////////////////////////////////////////////
	// Method:		getRandomOffer					  						//
	// Purpose:		Gets a new random offer.								//
	// Parameters:	* mySendableChips - The chips sendable by me.			//
	//				* opponentSendableChips - The chips sendable by the		//
	//										  opponent.						//
	//				* myNeededChips - The chips needed by me.				//
	//				* opponentNeededChips - The chips needed by the			//
	//										opponent.						//
	//				* randomGenerator - The random generator.				//
	//////////////////////////////////////////////////////////////////////////
	public static Offer getRandomOffer(ChipSet mySendableChips,
									   ChipSet opponentSendableChips,
									   ChipSet myNeededChips,
									   ChipSet opponentNeededChips,
									   Random randomGenerator)
	{
		// Calculate the number of meaningful chips by each player
		ChipSet mySendableMeaningfulChips = intersectChips(mySendableChips, opponentNeededChips);
		ChipSet opponentSendableMeaningfulChips = intersectChips(opponentSendableChips, myNeededChips);
		int maxSendableByMe = mySendableMeaningfulChips.getNumChips();
		int maxSendableByOpponent = opponentSendableMeaningfulChips.getNumChips();
		
		// Calculate the other chips of each player
		ChipSet mySendableOtherChips = subChipSetsNoNegatives(mySendableChips, mySendableMeaningfulChips);
		ChipSet opponentSendableOtherChips = subChipSetsNoNegatives(opponentSendableChips, opponentSendableMeaningfulChips);
		
		// Randomize a new offer
		int myMeaningfuls = randomGenerator.nextInt(Math.min(s_MaxSentChipsInGeneratedOffer, maxSendableByMe) + 1);
		int opponentMeaningfuls = randomGenerator.nextInt(Math.min(s_MaxSentChipsInGeneratedOffer, maxSendableByOpponent) + 1);
		ChipSet sentByMe = mySendableMeaningfulChips.getSubset(myMeaningfuls);
		ChipSet sentByOpponent = opponentSendableMeaningfulChips.getSubset(opponentMeaningfuls);
		
		// If a player has zero meaningfuls - add other chips
		if ((0 == myMeaningfuls) || (0 == opponentMeaningfuls))
		{
			if ((0 == myMeaningfuls) && (mySendableOtherChips.getNumChips() > 0))
			{
				sentByMe.addChipSet(mySendableOtherChips.getSubset(1 + randomGenerator.nextInt(Math.min(s_MaxSentChipsInGeneratedOffer, mySendableOtherChips.getNumChips()))));
			}
			if ((0 == opponentMeaningfuls) && (opponentSendableOtherChips.getNumChips() > 0))
			{
				sentByOpponent.addChipSet(opponentSendableOtherChips.getSubset(1 + randomGenerator.nextInt(Math.min(s_MaxSentChipsInGeneratedOffer, opponentSendableOtherChips.getNumChips()))));
			}
		}
		else
		{
			// Choose randomly whether to add chips to someone
			if (randomGenerator.nextBoolean())
			{
				// Choose whether to add to the opponent
				if (randomGenerator.nextBoolean())
				{
					int sendOthers = Math.min(s_MaxSentChipsInGeneratedOffer - myMeaningfuls, mySendableOtherChips.getNumChips());
					if (sendOthers > 0)
					{
						sentByMe.addChipSet(mySendableOtherChips.getSubset(1 + randomGenerator.nextInt(sendOthers)));
					}
				}
				else
				{
					int sendOthers = Math.min(s_MaxSentChipsInGeneratedOffer - opponentMeaningfuls, opponentSendableOtherChips.getNumChips());
					if (sendOthers > 0)
					{
						sentByOpponent.addChipSet(opponentSendableOtherChips.getSubset(1 + randomGenerator.nextInt(sendOthers)));
					}
				}
			}
		}
		
		// Return the offer
		return new Offer(sentByMe, sentByOpponent);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getAllEquivalentOffers			  						//
	// Purpose:		Gets all possible equivalent offers.					//
	// Parameters:	* mySendableChips - The chips sendable by me.			//
	//				* opponentSendableChips - The chips sendable by the		//
	//										  opponent.						//
	//				* myNeededChips - The chips needed by me.				//
	//				* opponentNeededChips - The chips needed by the			//
	//										opponent.						//
	//////////////////////////////////////////////////////////////////////////
	public static List<Object> getAllEquivalentOffers(ChipSet mySendableChips,
													  ChipSet opponentSendableChips,
													  ChipSet myNeededChips,
													  ChipSet opponentNeededChips)
	{
		List<Object> result = new ArrayList();
		
		// Calculate the number of meaningful chips by each player
		ChipSet mySendableMeaningfulChips = intersectChips(mySendableChips, opponentNeededChips);
		ChipSet opponentSendableMeaningfulChips = intersectChips(opponentSendableChips, myNeededChips);
		int maxSendableByMe = mySendableMeaningfulChips.getNumChips();
		int maxSendableByOpponent = opponentSendableMeaningfulChips.getNumChips();
		
		// Calculate the other chips of each player
		ChipSet mySendableOtherChips = subChipSetsNoNegatives(mySendableChips, mySendableMeaningfulChips);
		ChipSet opponentSendableOtherChips = subChipSetsNoNegatives(opponentSendableChips, opponentSendableMeaningfulChips);
		
		// Calculate other number of chips
		int mySendableOtherChipsNum = mySendableOtherChips.getNumChips();
		int opponentSendableOtherChipsNum = opponentSendableOtherChips.getNumChips();
		
		// Generate all of the options
		for (int myMeaningfuls = 0; myMeaningfuls <= Math.min(s_MaxSentChipsInGeneratedOffer, maxSendableByMe); myMeaningfuls++)
		{
			for (int opponentMeaningfuls = 0; opponentMeaningfuls <= Math.min(s_MaxSentChipsInGeneratedOffer, maxSendableByOpponent); opponentMeaningfuls++)
			{
				// Generate the sends
				ChipSet sentByMe = mySendableMeaningfulChips.getSubset(myMeaningfuls);
				ChipSet sentByOpponent = opponentSendableMeaningfulChips.getSubset(opponentMeaningfuls);
				
				// Add an offer without other chips
				if ((myMeaningfuls > 0) && (opponentMeaningfuls > 0))
				{
					result.add(new Offer(sentByMe, sentByOpponent));
				}
				
				// Add other chips to my sends
				if ((opponentMeaningfuls > 0) && (myMeaningfuls == 0))
				{
					if (mySendableOtherChipsNum > 0)
					{
						result.add(new Offer(ChipSet.addChipSets(sentByMe, mySendableOtherChips.getSubset(1)), sentByOpponent));
					}
					if (mySendableOtherChipsNum > 1)
					{
						result.add(new Offer(ChipSet.addChipSets(sentByMe, mySendableOtherChips.getSubset(2)), sentByOpponent));
					}
				}
				
				// Add other chips to the opponent's sends
				if ((myMeaningfuls > 0) && (opponentMeaningfuls == 0))
				{
					if (opponentSendableOtherChipsNum > 0)
					{
						result.add(new Offer(sentByMe, ChipSet.addChipSets(sentByOpponent, opponentSendableOtherChips.getSubset(1))));
					}
					if (opponentSendableOtherChipsNum > 1)
					{
						result.add(new Offer(sentByMe, ChipSet.addChipSets(sentByOpponent, opponentSendableOtherChips.getSubset(2))));
					}
				}
			}
		}
		
		// If no options - add an empty proposal
		if (result.size() == 0)
		{
			result.add(new Offer(new ChipSet(), new ChipSet()));
		}
		
		// Return the result
		return result;
	}
		
	//////////////////////////////////////////////////////////////////////////
	// Method:		subChipSetsNoNegatives			  						//
	// Purpose:		Subs chip sets without negatives.						//
	// Parameters:	* chipsX - The chip set to sub from.					//
	//				* chipsY - The chip set to be subbed.					//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public static ChipSet subChipSetsNoNegatives(ChipSet chipsX,
												 ChipSet chipsY)
	{
		ChipSet result = new ChipSet();
		
		// Iterate the colors
		for (String color : chipsX.getColors())
		{
			// Get the number of subbed chips
			int subbedChips = Math.max(0, chipsX.getNumChips(color) - chipsY.getNumChips(color));
			
			// Save into result
			result.set(color, subbedChips);
		}
		
		// Return the result
		return result;
	}
}