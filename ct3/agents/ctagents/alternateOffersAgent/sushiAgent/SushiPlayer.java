//////////////////////////////////////////////////////////////////////////
// File:		SushiPlayer.java 				   						//
// Purpose:		Implements the core of the Sushi agent.					//
//////////////////////////////////////////////////////////////////////////
package ctagents.alternateOffersAgent.sushiAgent;

// Imports from Java common framework
import java.util.ArrayList;
import java.util.Set;
import java.util.List;
import java.lang.Math;

// Imports from CT framework
import edu.harvard.eecs.airg.coloredtrails.alglib.ShortestPaths;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Phases;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.DiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.BasicProposalDiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.BasicProposalDiscussionDiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;
import edu.harvard.eecs.airg.coloredtrails.shared.types.RowCol;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Path;
import edu.harvard.eecs.airg.coloredtrails.shared.Scoring;
import edu.harvard.eecs.airg.coloredtrails.shared.types.PlayerStatus;
import edu.harvard.eecs.airg.coloredtrails.agent.events.RoleChangedEventListener;
import ctagents.alternateOffersAgent.Proposal;
import ctagents.alternateOffersAgent.SimplePlayer;
import ctagents.FileLogger;

//////////////////////////////////////////////////////////////////////////
// Class:		SushiPlayer							    				//
// Purpose:		Implements the Sushi player.							//
// Checked:		01/02/2013												//
//////////////////////////////////////////////////////////////////////////
public class SushiPlayer extends SimplePlayer implements RoleChangedEventListener
{
	// Whether we're in a rollout simulation mode or not
	private boolean m_IsRolloutSimulationMode;
	
	// The rollout model logger, used only in a rollout model simulation mode
	private XMLLogger m_RolloutModelLogger;
		
	// Saves whether Clueless is the first proposer
	private boolean m_IsFirstProposer;
	
	// Saves the last exchange
	private ChipSet m_LastExchange;
	
	// Saves whether this is the first communications round
	private boolean m_FirstCommunicationsRound;
	
	// Saves the last accepted offer
	private Offer m_LastAcceptedOffer;
		
	// Reliability related members
	private double m_MyReliabilitySum;
	private double m_OpponentReliabilitySum;
	
	// Transfers so far
	private int m_Transfers;
	
	// Opponent's culture
	private String m_Culture;
	
	// Saves the opponents' last chips
	private ChipSet m_MyLastChips;
	private ChipSet m_OpponentLastChips;
	
	// Saves the last round reliability weight
	private double m_LastRoundReliabilityWeight;
	
	// Saves the scoring
	private Scoring m_Scoring;
	
	// The last simulation and the moves from it
	private GameSimulator m_LastSimulation;
	private List<Object> m_MovesFromPreviousSimulation;
	
	// Saves the minimal offer that was sent
	private Offer m_MinimalOffer;
		
	// Maximum number of dormant rounds
	private static final int s_MaxDormantRounds = 2;
	
	// Upper limit for offers
	private static final int s_OffersUpperLimitSeconds = 60;
	
	// Upper limit for responses
	private static final int s_ReponsesUpperLimitSeconds = 30;
	
	// Upper limit for exchanges
	private static final int s_ExchangesUpperLimitSeconds = 30;
	
	// Whether we use a rule based rollout model or a Weka based one
	private static final boolean s_IsRuleBasedRolloutModel = false;

	//////////////////////////////////////////////////////////////////////////
	// Method:		debugPrint												//
	// Purpose:		Prints a debug message.									//
	// Parameters:	* message - The message to print.						//
	// Checked:		02/02/2013												//
	//////////////////////////////////////////////////////////////////////////
	static private void debugPrint(String message)
	{
		// Just print
		System.out.println("\t(SUSHI): " + message);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		SushiPlayer constructor									//
	// Purpose:		Initializes the Sushi agent.							//
	// Parameters:	* myInitialReliability - My initial reliability.		//
	//				* opponentInitialReliability - The opponent's initial	//
	//											   reliability.				//
	//				* culture - The opponent's culture.						//
	//				* isLogging - Whether we're logging or not.				//
	//////////////////////////////////////////////////////////////////////////
	public SushiPlayer(double myInitialReliability,
					   double opponentInitialReliability,
					   String culture,
					   boolean isLogging)
	{
		// Initialize parent
		super();
		
		// Last simulation and the moves are NULL
		this.m_LastSimulation = null;
		this.m_MovesFromPreviousSimulation = null;
		
		// Whether we're logging
		GameSimulator.s_IsLogging = isLogging;
		
		// We are not in a rollout simulation mode by default
		this.m_IsRolloutSimulationMode = false;
		this.m_RolloutModelLogger = null;
		
		// We are a listener for the role changing event
		client.addRoleChangedEventListener(this);
		
		// I'm not the first proposer unless deduced otherwise
		this.m_IsFirstProposer = false;
		
		// The last accepted offer
		this.m_LastAcceptedOffer = null;
		
		// The first round of communications
		this.m_FirstCommunicationsRound = true;
		
		// Save the culture
		this.m_Culture = new String(culture);
		
		// Initialize the reliability measures
		this.m_MyReliabilitySum = myInitialReliability;
		this.m_OpponentReliabilitySum = opponentInitialReliability;
		debugPrint("My reliability: " + Double.toString(myInitialReliability));
		debugPrint("Opponent reliability: " + Double.toString(opponentInitialReliability));
		
		// We consider the number of transfers to be one
		this.m_Transfers = 1;
		
		// Initialize the scoring
		this.m_Scoring = null;
		
		// Initialize the last exchange
		this.m_LastExchange = null;
		
		// Initialize minimal exchange
		this.m_MinimalOffer = null;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getMe													//
	// Purpose:		Returns my player status.								//
	// Checked:		01/02/2013												//
	//////////////////////////////////////////////////////////////////////////
	private PlayerStatus getMe()
	{
		// Done easily using the client game status
		return client.getGameStatus().getMyPlayer();
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getOpponent												//
	// Purpose:		Returns the opponent's player status.					//
	// Checked:		01/02/2013												//
	//////////////////////////////////////////////////////////////////////////
	private PlayerStatus getOpponent()
	{
		// Iterate the players
		for (PlayerStatus playerStatus : client.getGameStatus().getPlayers())
		{
			// If the PIN numbers disagree then it's the opponent
			if (playerStatus.getPin() != getMe().getPin())
			{
				return (PlayerStatus)(playerStatus.clone());
			}
		}
		
		// Not found
		throw new RuntimeException("Opponent ID not found.");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getPhaseTimeoutSeconds									//
	// Purpose:		Gets the phase timeout in seconds.						//
	//////////////////////////////////////////////////////////////////////////
	private long getPhaseTimeoutSeconds()
	{
		// Return the current phase timeout
		return client.getGameStatus().getPhases().getPhaseDuration() - client.getGameStatus().getPhases().getCurrentSecsElapsed();
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getChipsToExchange										//
	// Purpose:		Gets the chips to send for a exchange.					//
	// Parameters:	* offer - the last accepted offer.						//
	//////////////////////////////////////////////////////////////////////////
	private ChipSet getChipsToExchange(Offer offer)
	{
		// Initialize
		debugPrint("Getting chips for an exchange");
		this.m_LastExchange = null;
		
		// Look for an empty offer
		if (null == offer)
		{
			debugPrint("Defaulted to an empty set");
			this.m_LastExchange = new ChipSet();
			
			// Add to the last moves
			if (this.m_MovesFromPreviousSimulation != null)
			{
				this.m_MovesFromPreviousSimulation.add(new ChipSet());
			}
			
			// Quit
			return new ChipSet();
		}
		
		// If we're in the rollout simulation mode - just get what's necessary
		if (this.m_IsRolloutSimulationMode)
		{
			// Just return the exchange from the model simulation
			GameState currentState = new GameState(this.m_RolloutModelLogger,
												   client.getGameStatus().getBoard(),
												   this.m_Scoring,
												   GamePhase.EXCH2,
												   !this.m_IsFirstProposer,
												   getOpponent().getChips(),
												   getOpponent().getPosition(),
												   getMe().getHisDormantRounds(),
												   getMe().getChips(),
												   getMe().getPosition(),
												   getMe().getMyDormantRounds(),
												   null,
												   offer.switchSides());
			RolloutModel rolloutModelSimulation = new RuleBasedModel(this.m_RolloutModelLogger, currentState, this.m_OpponentReliabilitySum / this.m_Transfers, this.m_MyReliabilitySum / this.m_Transfers);
			this.m_LastExchange = new ChipSet(rolloutModelSimulation.getExchange(currentState, false, offer.switchSides()));
			debugPrint("Rollout simulation - Got exchange " + this.m_LastExchange.toString());
			this.m_RolloutModelLogger.flush();
			return new ChipSet(this.m_LastExchange);
		}

		// Create a simulation of the current game status
		debugPrint("Last accepted offer is " + offer.toString());
		GameSimulator simulation = new GameSimulator(client.getGameStatus().getBoard(),
													 this.m_Scoring,
													 GamePhase.EXCH1,
													 this.m_IsFirstProposer,
													 getMe().getChips(),
													 getMe().getPosition(),
													 getMe().getMyDormantRounds(),
													 getOpponent().getChips(),
													 getOpponent().getPosition(),
													 getMe().getHisDormantRounds(),
													 null,
													 offer,
													 this.m_MyReliabilitySum / this.m_Transfers,
													 this.m_OpponentReliabilitySum / this.m_Transfers,
													 this.m_Culture,
													 s_IsRuleBasedRolloutModel);
		
		// Try to load from previous simulations
		simulation.loadPreviousSimulationData(this.m_LastSimulation, this.m_MovesFromPreviousSimulation);
		
		// Simulate the exchange
		long timeout = Math.min((((getPhaseTimeoutSeconds() - 1) * 3) / 4), s_ExchangesUpperLimitSeconds);
		debugPrint("Timeout is " + Long.toString(timeout));
		ChipSet bestExchange = (ChipSet)(simulation.monteCarloTreeSearch(timeout));
		debugPrint("Got exchange: " + bestExchange.toString());
		
		// Save the last simulation and initialize the moves
		this.m_LastSimulation = simulation;
		this.m_MovesFromPreviousSimulation = new ArrayList();
		this.m_MovesFromPreviousSimulation.add(bestExchange);
		
		// Save the last exchange
		this.m_LastExchange = new ChipSet(bestExchange);
		
		// Return the simulation's best exchange
		return bestExchange;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		createOffer												//
	// Purpose:		Generates an offer.										//
	//////////////////////////////////////////////////////////////////////////
	private Offer createOffer()
	{
		// Initialize
		debugPrint("Creating an offer");
		this.m_LastAcceptedOffer = null;
		
		// If we're in the rollout simulation mode - just get what's necessary
		if (this.m_IsRolloutSimulationMode)
		{
			// Just return the offer from the model simulation
			GameState currentState = new GameState(this.m_RolloutModelLogger,
												   client.getGameStatus().getBoard(),
												   this.m_Scoring,
												   (this.m_FirstCommunicationsRound ? GamePhase.PROP1 : GamePhase.PROP2),
												   !this.m_IsFirstProposer,
												   getOpponent().getChips(),
												   getOpponent().getPosition(),
												   getMe().getHisDormantRounds(),
												   getMe().getChips(),
												   getMe().getPosition(),
												   getMe().getMyDormantRounds(),
												   null,
												   null);
			RolloutModel rolloutModelSimulation = new RuleBasedModel(this.m_RolloutModelLogger, currentState, this.m_OpponentReliabilitySum / this.m_Transfers, this.m_MyReliabilitySum / this.m_Transfers);
			Offer offer = rolloutModelSimulation.getOffer(currentState, false).switchSides();
			this.m_RolloutModelLogger.flush();
			debugPrint("Rollout simulation - Got offer " + offer.toString());
			return offer;
		}
		
		// Create a simulation of the current game status
		GameSimulator simulation = new GameSimulator(client.getGameStatus().getBoard(),
													 this.m_Scoring,
													 (this.m_FirstCommunicationsRound ? GamePhase.PROP1 : GamePhase.PROP2),
													 this.m_IsFirstProposer,
													 getMe().getChips(),
													 getMe().getPosition(),
													 getMe().getMyDormantRounds(),
													 getOpponent().getChips(),
													 getOpponent().getPosition(),
													 getMe().getHisDormantRounds(),
													 null,
													 null,
													 this.m_MyReliabilitySum / this.m_Transfers,
													 this.m_OpponentReliabilitySum / this.m_Transfers,
													 this.m_Culture,
													 s_IsRuleBasedRolloutModel);
		
		// Try to load from previous simulations
		simulation.loadPreviousSimulationData(this.m_LastSimulation, this.m_MovesFromPreviousSimulation);
		
		// Simulate the proposition
		long timeout = Math.min((((getPhaseTimeoutSeconds() - 1) * 3) / 4), s_OffersUpperLimitSeconds);
		debugPrint("Timeout is " + Long.toString(timeout));
		Offer bestOffer = (Offer)(simulation.monteCarloTreeSearch(timeout));
		debugPrint("Got offer: " + bestOffer.toString());
		
		// Save the minimal offer
		if (bestOffer.getNumChips() > 0)
		{
			if ((this.m_MinimalOffer == null) || this.m_MinimalOffer.getNumChips() < bestOffer.getNumChips())
			{
				this.m_MinimalOffer = new Offer(bestOffer);
			}
		}
		else
		{
			// The offer was empty - try to send the minimal offer
			debugPrint("Got an empty offer - trying the minimal offer");
			if (this.m_MinimalOffer != null)
			{
				bestOffer = new Offer(this.m_MinimalOffer);
			}
		}
		
		// Save the last simulation and initialize the moves
		this.m_LastSimulation = simulation;
		this.m_MovesFromPreviousSimulation = new ArrayList();
		this.m_MovesFromPreviousSimulation.add(bestOffer);
		
		// Return the simulation's best offer
		return bestOffer;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		respondToOffer											//
	// Purpose:		Responds to an offer.									//
	// Parameters:	* offer - The offer to respond to.						//
	//////////////////////////////////////////////////////////////////////////
	private boolean respondToOffer(Offer offer)
	{
		// Initialize
		debugPrint("Responding to an offer " + offer.toString());
		this.m_LastAcceptedOffer = null;

		// If we're in the rollout simulation mode - just get what's necessary
		if (this.m_IsRolloutSimulationMode)
		{
			// Just return the response from the model simulation - note that the game state's sides has switched!
			GameState currentState = new GameState(this.m_RolloutModelLogger,
												   client.getGameStatus().getBoard(),
												   this.m_Scoring,
												   (this.m_FirstCommunicationsRound ? GamePhase.RESP1 : GamePhase.RESP2),
												   !this.m_IsFirstProposer,
												   getOpponent().getChips(),
												   getOpponent().getPosition(),
												   getMe().getHisDormantRounds(),
												   getMe().getChips(),
												   getMe().getPosition(),
												   getMe().getMyDormantRounds(),
												   offer.switchSides(),
												   null);
			RolloutModel rolloutModelSimulation = new RuleBasedModel(this.m_RolloutModelLogger, currentState, this.m_OpponentReliabilitySum / this.m_Transfers, this.m_MyReliabilitySum / this.m_Transfers);
			boolean response = rolloutModelSimulation.getResponse(currentState, false, offer.switchSides());
			this.m_RolloutModelLogger.flush();
			debugPrint("Rollout simulation - Got response " + new Boolean(response).toString());
			return response;
		}
		
		// Create a simulation of the current game status
		GameSimulator simulation = new GameSimulator(client.getGameStatus().getBoard(),
													 this.m_Scoring,
													 (this.m_FirstCommunicationsRound ? GamePhase.RESP1 : GamePhase.RESP2),
													 this.m_IsFirstProposer,
													 getMe().getChips(),
													 getMe().getPosition(),
													 getMe().getMyDormantRounds(),
													 getOpponent().getChips(),
													 getOpponent().getPosition(),
													 getMe().getHisDormantRounds(),
													 offer,
													 null,
													 this.m_MyReliabilitySum / this.m_Transfers,
													 this.m_OpponentReliabilitySum / this.m_Transfers,
													 this.m_Culture,
													 s_IsRuleBasedRolloutModel);
		
		// Try to load from previous simulations
		simulation.loadPreviousSimulationData(this.m_LastSimulation, this.m_MovesFromPreviousSimulation);
		
		// Simulate the response
		long timeout = Math.min((((getPhaseTimeoutSeconds() - 1) * 3) / 4), s_ReponsesUpperLimitSeconds);
		debugPrint("Timeout is " + Long.toString(timeout));
		boolean bestResponse = (Boolean)(simulation.monteCarloTreeSearch(timeout));
		debugPrint("Response is " + Boolean.toString(bestResponse));
		
		// Save the last simulation and initialize the moves
		this.m_LastSimulation = simulation;
		this.m_MovesFromPreviousSimulation = new ArrayList();
		this.m_MovesFromPreviousSimulation.add(new Boolean(bestResponse));
		
		// Return the simulation's best response
		return bestResponse;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		onReceipt												//
	// Purpose:		Receives a discourse message.							//
	// Parameters:	* discourseMessage - the discourse message.				//
	// Remarks:		* This method is a part of SimplePlayer implementation. //
	//				* The discourse message can either be:					//
	//				  1. A response to my previous offer.					//
	//				  2. A new offer by the opponent.						//
	//////////////////////////////////////////////////////////////////////////
	public void onReceipt(DiscourseMessage discourseMessage)
	{		
		// Update scoring
		if (null == this.m_Scoring)
		{
			this.m_Scoring = client.getGameStatus().getScoring();
		}
		
		// If the message is a response to my offer
		if (discourseMessage instanceof BasicProposalDiscussionDiscourseMessage)
		{
			// The opponent responded -- if he accepts my offer then save it
			BasicProposalDiscussionDiscourseMessage response = (BasicProposalDiscussionDiscourseMessage)discourseMessage;
			debugPrint("Got a response " + Boolean.toString(response.accepted()));
			
			// Add to the last moves
			if (this.m_MovesFromPreviousSimulation != null)
			{
				this.m_MovesFromPreviousSimulation.add(new Boolean(response.accepted()));
			}
			
			// Save information regarding to the response
			if (response.accepted())
			{
				this.m_FirstCommunicationsRound = true;
				this.m_LastAcceptedOffer = new Offer(response.getChipsSentByProposer(), response.getChipsSentByResponder());
				debugPrint("Original offer was " + this.m_LastAcceptedOffer.toString());
			}
			else
			{
				this.m_FirstCommunicationsRound = !this.m_FirstCommunicationsRound;
				this.m_LastAcceptedOffer = null;
			}
			return;
		}
		
		// If the message is an offer
		if (discourseMessage instanceof BasicProposalDiscourseMessage)
		{
			// The message is an offer
			BasicProposalDiscourseMessage proposal = (BasicProposalDiscourseMessage)discourseMessage;
			Offer offerToConsider = new Offer(proposal.getChipsSentByResponder(), proposal.getChipsSentByProposer());
			debugPrint("Got an offer " + offerToConsider.toString());
			
			// Add to the last moves
			if (this.m_MovesFromPreviousSimulation != null)
			{
				this.m_MovesFromPreviousSimulation.add(offerToConsider);
			}
			
			// Build a response to the proposal
			BasicProposalDiscussionDiscourseMessage response = new BasicProposalDiscussionDiscourseMessage(proposal);
			
			// If the proposal should be accepted by us
			if (respondToOffer(offerToConsider))
			{
				debugPrint("Offer was accepted");
				this.m_LastAcceptedOffer = offerToConsider;
				this.m_FirstCommunicationsRound = true;
				response.acceptOffer();
			}
			else
			{
				debugPrint("Offer was rejected");
				this.m_LastAcceptedOffer = null;
				this.m_FirstCommunicationsRound = !this.m_FirstCommunicationsRound;
				response.rejectOffer();
			}
			
			// Send the response
			client.communication.sendDiscourseRequest(response);
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		roleChanged												//
	// Purpose:		Called whenever a role has changed.						//
	// Remarks:		* This method is a part of SimplePlayer implementation. //
	//				* The discourse message can either be:					//
	//				  1. A response to my previous offer.					//
	//				  2. A new offer by the opponent.						//
	//////////////////////////////////////////////////////////////////////////
	public void roleChanged()
	{		
		// Update scoring
		if (null == this.m_Scoring)
		{
			this.m_Scoring = client.getGameStatus().getScoring();
		}
		
		// Get the current phase
		String phaseName = client.getGameStatus().getPhases().getCurrentPhaseName();
		
		// Communication phase 
		if (phaseName.equals("Communication Phase"))
		{
			String roleStr = getMe().getRole();
			debugPrint("Role has changed, new role is: " + roleStr);
			
			// If we are a proposer we should make an offer
			if (roleStr.equals("Proposer"))
			{
				// Get my offer
				Offer myOffer = createOffer();
				debugPrint("Created the offer " + myOffer.toString());
				
				// Get the IDs
				int proposerId = getMe().getPerGameId();
				int responderId = getOpponent().getPerGameId();
				
				// Create the proposal message
				BasicProposalDiscourseMessage proposal = new BasicProposalDiscourseMessage(proposerId, responderId, -1, myOffer.m_ChipsSentByMe, myOffer.m_ChipsSentByOpponent);
				client.communication.sendDiscourseRequest(proposal);
			}
		}
	}	
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getChipsThatWereSentToMe								//
	// Purpose:		Conclude the chips that were sent to me.				//
	// Remarks:		* Must be called during the movement phase.				//
	//////////////////////////////////////////////////////////////////////////
	private ChipSet getChipsThatWereSentToMe()
	{
		// My chips before the exchange
		ChipSet chipsBeforeExchange = this.m_MyLastChips;
		
		// The chips that I sent
		ChipSet chipsSentByMe = new ChipSet();
		if (null != this.m_LastAcceptedOffer)
		{
			chipsSentByMe = this.m_LastExchange;
		}

		// My current chips
		ChipSet myCurrentChips = getMe().getChips();
		
		// The chips after I sent them but without receiving any
		ChipSet chipsAfterSendingWithoutReceiving = new ChipSet();
		if (chipsBeforeExchange.contains(chipsSentByMe))
		{
			chipsAfterSendingWithoutReceiving = ChipSet.subChipSets(chipsBeforeExchange, chipsSentByMe);
		}
		
		// The chips that were sent to me
		ChipSet chipsSentToMe = ChipSet.subChipSets(myCurrentChips, chipsAfterSendingWithoutReceiving);
		for (String color : chipsSentToMe.getColors())
		{
			if (chipsSentToMe.getNumChips(color) < 0)
			{
				chipsSentToMe.set(color, 0);
			}
		}
		
		// Return the result
		return chipsSentToMe;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getResultingScore										//
	// Purpose:		Calculate the resulting score.							//
	// Parmeters:	* playerChips - The player's chips.						//
	//				* playerPosition - The player's position.				//
	//////////////////////////////////////////////////////////////////////////
	private double getResultingScore(ChipSet playerChips,
									 RowCol playerPosition)
	{
		PlayerStatus playerStatus = new PlayerStatus();
		int pathPositionIndex = 0;
		
		// Copy the player chips and the position
		ChipSet newPlayerChips = new ChipSet(playerChips);
		RowCol newPlayerPosition = new RowCol(playerPosition);
		
		// Get the entire game chips
		ChipSet myChips = getMe().getChips();
		ChipSet opponentChips = getOpponent().getChips();
		
		// Get as close as possible towards the goal
		Path pathTowardsGoal = ShortestPaths.getShortestPaths(newPlayerPosition, client.getGameStatus().getBoard().getGoalLocations().get(0), client.getGameStatus().getBoard(), client.getGameStatus().getScoring(), 1, myChips, opponentChips).get(0);
		
		// Look for the position on the goal
		for (pathPositionIndex = 0; pathPositionIndex < pathTowardsGoal.getNumPoints(); pathPositionIndex++)
		{
			if (pathTowardsGoal.getPoint(pathPositionIndex).equals(newPlayerPosition))
			{
				break;
			}
		}
		
		// Try to walk as much as we can towards the goal
		boolean notReachingGoal = false;
		for (int currentPosition = pathPositionIndex + 1; currentPosition < pathTowardsGoal.getNumPoints(); currentPosition++)
		{
			String currentColor = client.getGameStatus().getBoard().getSquare(pathTowardsGoal.getPoint(currentPosition)).getColor();
			if (newPlayerChips.getNumChips(currentColor) > 0)
			{
				newPlayerChips.add(currentColor, -1);
			}
			else
			{
				newPlayerPosition = pathTowardsGoal.getPoint(currentPosition - 1);
				notReachingGoal = true;
				break;
			}
		}
		if (!notReachingGoal)
		{
			newPlayerPosition = client.getGameStatus().getBoard().getGoalLocations().get(0);
		}
		
		// Set the player status
		playerStatus.setPosition(newPlayerPosition);
		playerStatus.setChips(newPlayerChips);
		
		// Return the score
		return this.m_Scoring.score(playerStatus, client.getGameStatus().getBoard().getGoalLocations().get(0));
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getRoundReliability										//
	// Purpose:		Gets the round reliability of the given player.			//
	// Parmeters:	* otherPlayerPosition - The other player's position.	//
	//				* otherPlayerOriginalChips - The other player's chips	//
	//											 before the exchange.		//
	//				* chipsMentToBeSentToOther - The full exchange chips	//
	//											 that were ment to be sent	//
	//											 to the other player.		//
	//				* chipsSentToOther - The chips that were actuallt sent. //
	//////////////////////////////////////////////////////////////////////////
	private double getRoundReliability(RowCol otherPlayerPosition,
									   ChipSet otherPlayerOriginalChips,
									   ChipSet chipsMentToBeSentToOther,
									   ChipSet chipsSentToOther)
	{
		// Calculate the chips for a full exchange
		ChipSet chipsForFullExchange = ChipSet.addChipSets(otherPlayerOriginalChips, chipsMentToBeSentToOther);
		ChipSet chipsActuallySent = ChipSet.addChipSets(otherPlayerOriginalChips, chipsSentToOther);
		
		// Calculate scores
		double fullExchangeScore = getResultingScore(chipsForFullExchange, otherPlayerPosition);
		double actualScore = getResultingScore(chipsActuallySent, otherPlayerPosition);
		double currentScore = getResultingScore(otherPlayerOriginalChips, otherPlayerPosition);
		
		// Calculates benifits
		double fullBenifit = fullExchangeScore - currentScore;
		double actualBenifit = actualScore - currentScore;
		
		// Calculate the results
		if (0.0 == fullBenifit)
		{
			return 1.0;
		}
		return actualBenifit / fullBenifit;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		updateReliabilitiesAfterExchange						//
	// Purpose:		Update reliabilities after a exchange.					//
	// Remarks:		* Should be called from the movement phase.				//
	//////////////////////////////////////////////////////////////////////////
	private void updateReliabilitiesAfterExchange()
	{
		// If an offer was not accepted
		if (null == this.m_LastAcceptedOffer)
		{
			// Add to the last moves
			if (this.m_MovesFromPreviousSimulation != null)
			{
				this.m_MovesFromPreviousSimulation.add(new ChipSet());
			}
			
			// Quit
			return;
		}
		
		// Get the chips that were sent by the players
		ChipSet chipsSentByMe = new ChipSet();
		if (null != this.m_LastExchange)
		{
			chipsSentByMe = this.m_LastExchange;
		}
		ChipSet chipsSentByOpponent = getChipsThatWereSentToMe();
		
		// Add to the last moves
		if (this.m_MovesFromPreviousSimulation != null)
		{
			this.m_MovesFromPreviousSimulation.add(chipsSentByOpponent);
		}

		// Whether to update or not
		boolean updateAgent = (this.m_LastAcceptedOffer.m_ChipsSentByAgent.getNumChips() > 0);
		boolean updateHuman = (this.m_LastAcceptedOffer.m_ChipsSentByHuman.getNumChips() > 0);
		
		// Calculate round reliabilities
		double myRoundReliability = getRoundReliability(getOpponent().getPosition(),
												   		this.m_OpponentLastChips,
												   		this.m_LastAcceptedOffer.m_ChipsSentByMe,
												   		chipsSentByMe);
		debugPrint("My round reliability is " + Double.toString(myRoundReliability));
		double opponentRoundReliability = getRoundReliability(getMe().getPosition(),
															  this.m_MyLastChips,
															  this.m_LastAcceptedOffer.m_ChipsSentByOpponent,
															  chipsSentByOpponent);
		debugPrint("Opponent's round reliability is " + Double.toString(opponentRoundReliability));
		
		// Fix for empty offers
		if (!updateAgent)
		{
			myRoundReliability = this.m_MyReliabilitySum / this.m_Transfers;
		}
		if (!updateHuman)
		{
			opponentRoundReliability = this.m_OpponentReliabilitySum / this.m_Transfers;
		}
		
		// Update reliabilities
		if (s_IsRuleBasedRolloutModel)
		{
			this.m_MyReliabilitySum = myRoundReliability * (this.m_Transfers + 1);
			this.m_OpponentReliabilitySum = opponentRoundReliability * (this.m_Transfers + 1);
		}
		else
		{
			this.m_MyReliabilitySum = WekaBasedModel.getWeightedReliability(this.m_MyReliabilitySum / this.m_Transfers, myRoundReliability) * (this.m_Transfers + 1);
			this.m_OpponentReliabilitySum = WekaBasedModel.getWeightedReliability(this.m_OpponentReliabilitySum / this.m_Transfers, opponentRoundReliability) * (this.m_Transfers + 1);
		}
		this.m_Transfers++;
		debugPrint("My reliability is " + Double.toString(this.m_MyReliabilitySum / this.m_Transfers));
		debugPrint("Opponent reliability is " + Double.toString(this.m_OpponentReliabilitySum / this.m_Transfers));
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		phaseAdvanced											//
	// Purpose:		A callback for phase advances.							//
	// Parameters:	* discourseMessage - the discourse message.				//
	// Remarks:		* This method is a part of SimplePlayer implementation. //
	//				* The discourse message can either be:					//
	//				  1. A response to my previous offer.					//
	//				  2. A new offer by the opponent.						//
	//////////////////////////////////////////////////////////////////////////
	public void phaseAdvanced(Phases phases)
	{
		// Update scoring
		if (null == this.m_Scoring)
		{
			this.m_Scoring = client.getGameStatus().getScoring();
		}
		
		// Get the current phase
		String phaseName = client.getGameStatus().getPhases().getCurrentPhaseName();
		debugPrint("Moved to phase " + phaseName);
		
		// Communication phase 
		if (phaseName.equals("Communication Phase"))
		{
			// Get the role
			String roleStr = getMe().getRole();
			
			// Deduce whether I'm the first proposer
			this.m_IsFirstProposer = roleStr.equals("Proposer");
			
			// Make an offer if I'm the proposer
			if (this.m_IsFirstProposer)
			{
				// Get my offer
				Offer myOffer = createOffer();
				debugPrint("Created the offer " + myOffer.toString());
				
				// Get the IDs
				int proposerId = getMe().getPerGameId();
				int responderId = getOpponent().getPerGameId();
				
				// Create the proposal message
				BasicProposalDiscourseMessage proposal = new BasicProposalDiscourseMessage(proposerId, responderId, -1, myOffer.m_ChipsSentByMe, myOffer.m_ChipsSentByOpponent);
				client.communication.sendDiscourseRequest(proposal);
			}
		}
	
		// Move phase
		if (phaseName.equals("Movement Phase"))
		{
			updateReliabilitiesAfterExchange();
			
			// Check if the number of dormant rounds requires moving
			if (getMe().getMyDormantRounds() >= s_MaxDormantRounds)
			{
				// Try to move if it is possible
				RowCol goalPosition = client.getGameStatus().getBoard().getGoalLocations().get(0);

				// Looking for the paths towards the goal
				ArrayList<Path> paths = ShortestPaths.getShortestPaths(getMe().getPosition(), goalPosition, client.getGameStatus().getBoard(), this.m_Scoring, 10, getMe().getChips(), getOpponent().getChips());

				// Iterate the paths
				for (Path path : paths)
				{
					RowCol pointToMove = path.getPoint(1);
					
					// Get the needed color for the path
					String neededColor = client.getGameStatus().getBoard().getSquare(pointToMove).getColor();
					
					// Check if the color is in the current chips
					if (getMe().getChips().getNumChips(neededColor) > 0)
					{
						client.communication.sendMoveRequest(pointToMove);
						break;
					}
				}
			}
		}
		
		// Exchange phase
		if (phaseName.equals("Exchange Phase"))
		{
			// Save the last chips
			this.m_MyLastChips = new ChipSet(getMe().getChips());
			this.m_OpponentLastChips = new ChipSet(getOpponent().getChips());
						
			// Send only if we accepted a proposal
			ChipSet chipsToExchange = new ChipSet();
			if (null != this.m_LastAcceptedOffer)
			{
				chipsToExchange = getChipsToExchange(this.m_LastAcceptedOffer);
				if (!(this.m_LastAcceptedOffer.m_ChipsSentByMe.contains(chipsToExchange)))
				{
					chipsToExchange = new ChipSet();
					this.m_LastExchange = new ChipSet(chipsToExchange);
					debugPrint("Last exchange is now " + this.m_LastExchange.toString());
				}
			}
			client.communication.sendTransferRequest(getOpponent().getPerGameId(), chipsToExchange);
		}
		
		// Feedback phase
		if (phaseName.equals("Feedback Phase"))
		{
			// Initialize everything
			this.m_LastExchange = null;
			this.m_LastAcceptedOffer = null;
		}
		
		// Strategy prepreration phase
		if (phaseName.equals("Strategy Prep Phase"))
		{

		}		
	}	

	//////////////////////////////////////////////////////////////////////////
	// Method:		rolloutSimulationMode									//
	// Purpose:		Marks a rollout simulation mode.						//
	//////////////////////////////////////////////////////////////////////////
	public void rolloutSimulationMode()
	{
		// Mark the rollout simulation mode
		this.m_IsRolloutSimulationMode = true;
		debugPrint("=== ROLLOUT SIMULATION MODE ===");
		
		// Initialize its logger
		this.m_RolloutModelLogger = XMLLogger.getInstance("SushiRolloutModelSimulation.xml");
		this.m_RolloutModelLogger.startNewSession();
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		isUsingWeka												//
	// Purpose:		Indicates whether we're using Weka or not.				//
	//////////////////////////////////////////////////////////////////////////
	public static boolean isUsingWeka()
	{
		// Just return the correct result
		return !s_IsRuleBasedRolloutModel;
	}
}
