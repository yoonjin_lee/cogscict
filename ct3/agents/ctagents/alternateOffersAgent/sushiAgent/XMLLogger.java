//////////////////////////////////////////////////////////////////////////
// File:		XMLLogger.java 					    					//
// Purpose:		Implements an XML logger.								//
//////////////////////////////////////////////////////////////////////////
package ctagents.alternateOffersAgent.sushiAgent;

// Imports from Java common framework
import java.util.HashMap;
import java.util.Map;
import java.io.IOException;
import java.io.FileWriter;

//////////////////////////////////////////////////////////////////////////
// Class:		XMLLogger												//
// Purpose:		Performs XML logging.									//
//////////////////////////////////////////////////////////////////////////
public class XMLLogger
{
	// The file writer
	private FileWriter m_Logger;
	
	// The indentation level
	private int m_IndentionLevel;
	
	// The log counter
	private int m_Counter;
	
	// The instance name
	private String m_InstanceName;
	
	// Whether logging is activated
	private boolean m_Activated;
	
	// The instances map
	private static Map<String, XMLLogger> s_Instances = new HashMap();
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getInstance						    					//
	// Purpose:		Gets an instance.										//
	// Parameters:	* instanceName - The instance name.						//
	//////////////////////////////////////////////////////////////////////////
	public static XMLLogger getInstance(String instanceName)
	{
		// If the instance doesn't exist - create it and return it
		if (!(s_Instances.containsKey(instanceName)))
		{
			s_Instances.put(new String(instanceName), new XMLLogger(instanceName));
		}
		return s_Instances.get(instanceName);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		startNewSession					    					//
	// Purpose:		Starts a new logging session.							//
	//////////////////////////////////////////////////////////////////////////
	public void startNewSession()
	{		
		// Flush
		flush();
		
		// Increase counter
		this.m_Counter++;
		
		// Initialize the file writer
		try
		{
			if (this.m_Activated)
			{
				this.m_Logger = new FileWriter(this.m_InstanceName + "_" + Integer.toString(this.m_Counter) + ".xml");
			}
		}
		catch (IOException e)
		{
			throw new RuntimeException("Problem creating tree logger.");
		}
		
		// No indention
		this.m_IndentionLevel = 0;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		Constructor						    					//
	// Purpose:		Creates a new XML logger.								//
	// Parameters:	* instanceName - The instance name.						//
	//////////////////////////////////////////////////////////////////////////
	private XMLLogger(String instanceName)
	{
		// Counter is zero
		this.m_Counter = 0;
		
		// Save the instance name
		this.m_InstanceName = new String(instanceName);
		
		// Not activated by default
		this.m_Activated = false;
		
		// Initialize the file writer
		this.m_Logger = null;
		
		// No indention
		this.m_IndentionLevel = 0;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		writeData						    					//
	// Purpose:		Writes data.											//
	// Parameters:	* data - The data to write to the file.					//
	//////////////////////////////////////////////////////////////////////////
	private void writeData(String data)
	{
		// Write the data
		try
		{
			if (this.m_Activated)
			{
				this.m_Logger.write(data);
			}
		}
		catch (IOException e)
		{
			throw new RuntimeException("Problem writing to the tree logger.");
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		startTagNoLinebreaks									//
	// Purpose:		Starts a tag without line breaks.						//
	// Parameters:	* tagName - The name of the tag.						//
	//////////////////////////////////////////////////////////////////////////
	private void startTagNoLinebreaks(String tagName)
	{			
		// Indent
		for (int counter = 0; counter < this.m_IndentionLevel; counter++)
		{
			writeData("\t");
		}
		
		// Write the tag beginning
		writeData("<" + tagName + ">");
		
		// Increase intention
		this.m_IndentionLevel++;		
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		endTagNoIndent					    					//
	// Purpose:		Ends a previously opened tag without indentation.		//
	// Parameters:	* tagName - The name of the tag.						//
	//////////////////////////////////////////////////////////////////////////
	private void endTagNoIndent(String tagName)
	{			
		// Write the tag beginning
		writeData("</" + tagName + ">\r\n");
		
		// Decrease intention
		if (this.m_IndentionLevel > 0)
		{
			this.m_IndentionLevel--;
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		endTag							    					//
	// Purpose:		Ends a previously opened tag.							//
	// Parameters:	* tagName - The name of the tag.						//
	//////////////////////////////////////////////////////////////////////////
	public void endTag(String tagName)
	{
		// Indent
		if (this.m_IndentionLevel > 0)
		{
			for (int counter = 0; counter < this.m_IndentionLevel - 1; counter++)
			{
				writeData("\t");
			}
		}
		
		// End the tag
		endTagNoIndent(tagName);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		addTag							    					//
	// Purpose:		Adds a tag with data inside.							//
	// Parameters:	* tagName - The name of the tag.						//
	//				* tagData - The tag's data.								//
	//////////////////////////////////////////////////////////////////////////
	public void addTag(String tagName,
					   Object tagData)
	{			
		// Write the tag to the log
		startTagNoLinebreaks(tagName);
		writeData(tagData.toString());
		endTagNoIndent(tagName);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		flush							    					//
	// Purpose:		Flushes data.											//
	//////////////////////////////////////////////////////////////////////////
	public void flush()
	{		
		// Flush
		try
		{
			if ((this.m_Activated) && (this.m_Logger != null))
			{
				this.m_Logger.flush();
			}
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		beginTag						    					//
	// Purpose:		Begins a new tag.										//
	// Parameters:	* tagName - The name of the tag.						//
	//////////////////////////////////////////////////////////////////////////
	public void beginTag(String tagName)
	{
		// Start the tag
		startTagNoLinebreaks(tagName);
		writeData("\r\n");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		activate						    					//
	// Purpose:		Activated loggging.										//
	//////////////////////////////////////////////////////////////////////////
	public void activate()
	{
		// Activate
		this.m_Activated = true;
		
		// Start a new session
		startNewSession();
	}
}