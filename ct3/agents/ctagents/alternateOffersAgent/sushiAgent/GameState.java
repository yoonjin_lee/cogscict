//////////////////////////////////////////////////////////////////////////
// File:		SushiPlayer.java 				   						//
// Purpose:		Implements the core of the Sushi agent.					//
//////////////////////////////////////////////////////////////////////////
package ctagents.alternateOffersAgent.sushiAgent;

//Imports from Java common framework
import java.util.ArrayList;
import java.util.List;

//Imports from CT framework
import edu.harvard.eecs.airg.coloredtrails.shared.Scoring;
import edu.harvard.eecs.airg.coloredtrails.alglib.ShortestPaths;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Board;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;
import edu.harvard.eecs.airg.coloredtrails.shared.types.GameStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Path;
import edu.harvard.eecs.airg.coloredtrails.shared.types.RowCol;
import edu.harvard.eecs.airg.coloredtrails.shared.types.PlayerStatus;

//////////////////////////////////////////////////////////////////////////
// Class:		GameState												//
// Purpose:		Saves game states of CT.								//
// Checked:		31/01/2013												//
//////////////////////////////////////////////////////////////////////////
public class GameState
{
	// A file logger
	private XMLLogger m_Logger;
	
	// The maximum number of dormant steps
	private final static int s_MaxDormantRounds = 3;
	
	// The maximum number of chips in an exchange, not including the full exchange
	private final static int s_MaxExchangeChips = 2;
	
	// The rollout model
	private RolloutModel m_RolloutModel;
	
	// The board, scoring and goal position
	private Board m_Board;
	private Scoring m_Scoring;
	private RowCol m_GoalPosition;
	
	// The game phase
	private GamePhase m_GamePhase;

	// The last offer, last accepted offer and the first exchange chips
	private Offer m_LastOffer;
	private Offer m_LastAcceptedOffer;
	private ChipSet m_FirstExchangeChips;
	
	// Whether I was the first proposer or the opponent
	private boolean m_IsFirstProposerWasMe;
			
	// My chips, position and dormant rounds
	private ChipSet m_MyChips;
	private RowCol m_MyPosition;
	private int m_MyDormantRounds;
	private ChipSet m_MyNeededChips;
	
	// Opponent's chips, position and dormant rounds
	private ChipSet m_OpponentChips;
	private RowCol m_OpponentPosition;
	private int m_OpponentDormantRounds;
	private ChipSet m_OpponentNeededChips;
	
	// Maximum and minimum scores for both players
	private double m_MyMaxScore;
	private double m_MyMinScore;
	private double m_OpponentMaxScore;
	private double m_OpponentMinScore;
	
	// The number of rollout steps
	private int m_RolloutSteps;
	
	// Cache for all of the possible actions
	List<Object> m_AllPossibleActionsCache;
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		Constructor						    					//
	// Purpose:		Creates a new game state.								//
	// Parameters:	* logger - File logger.									//
	//				* board - The board.									//
	// 				* scoring - The scoring.								//
	// 				* phase - The game phase. This cannot be EXCH2!			//
	//				* isFirstProposerWasMe - Whether I was the first		//
	//										 proposer of the opponent.		//
	// 				* myChips - My chips.									//
	// 				* myPosition - My position.								//
	// 				* myDormantSteps - My dormant steps.					//
	// 				* opponentChips - Opponent's chips.						//
	// 				* opponentPosition - Opponent's position.				//
	// 				* opponentDormantSteps - Opponent's dormant steps.		//
	//				* lastOffer - The last given offer, applicable only for	//
	//							  responses.								//
	//				* lastAcceptedOffer - The last accepted offer,			//
	//									  applicable only for exchanges.	//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public GameState(XMLLogger logger,
					 Board board,
					 Scoring scoring,
					 GamePhase phase,
					 boolean isFirstProposerWasMe,
					 ChipSet myChips,
					 RowCol myPosition,
					 int myDormantRounds,
					 ChipSet opponentChips,
					 RowCol opponentPosition,
					 int opponentDormantRounds,
					 Offer lastOffer,
					 Offer lastAcceptedOffer)
	{
		// Save the logger
		this.m_Logger = logger;
		
		// Save the rollout model
		this.m_RolloutModel = null;
		
		// Copy game status members
		this.m_Board = new Board(board);
		this.m_Scoring = new Scoring(scoring);
		this.m_GamePhase = phase;
		this.m_IsFirstProposerWasMe = isFirstProposerWasMe;
		
		// Calculate the goal position
		this.m_GoalPosition = new RowCol(board.getGoalLocations().get(0));
		
		// The last offer, last accepted offer and the first exchange chips aren't saved
		this.m_LastOffer = (lastOffer == null ? null : new Offer(lastOffer));
		this.m_LastAcceptedOffer = (lastAcceptedOffer == null ? null : new Offer(lastAcceptedOffer));
		this.m_FirstExchangeChips = null;
		
		// Copy agent's members
		this.m_MyChips = new ChipSet(myChips);
		this.m_MyPosition = new RowCol(myPosition);
		this.m_MyDormantRounds = myDormantRounds;
		
		// Copy opponent's members
		this.m_OpponentChips = new ChipSet(opponentChips);
		this.m_OpponentPosition = new RowCol(opponentPosition);
		this.m_OpponentDormantRounds = opponentDormantRounds;
		
		// Calculate needed chips
		this.m_MyNeededChips = ShortestPaths.getShortestPaths(myPosition, this.m_GoalPosition, board, scoring, 1, myChips, opponentChips).get(0).getRequiredChips(board);
		this.m_OpponentNeededChips = ShortestPaths.getShortestPaths(opponentPosition, this.m_GoalPosition, board, scoring, 1, myChips, opponentChips).get(0).getRequiredChips(board);
		
		// Calculate the maximum and minimum score
		this.m_MyMinScore = scoring.distweight * (ShortestPaths.getShortestPaths(myPosition, board.getGoalLocations().get(0), board, scoring, 1, myChips, opponentChips).get(0).getNumPoints());
		this.m_MyMaxScore = scoring.goalweight  + (scoring.chipweight * (myChips.getNumChips() + opponentChips.getNumChips()));
		this.m_OpponentMinScore = scoring.distweight * (ShortestPaths.getShortestPaths(opponentPosition, board.getGoalLocations().get(0), board, scoring, 1, opponentChips, myChips).get(0).getNumPoints());
		this.m_OpponentMaxScore = scoring.goalweight  + (scoring.chipweight * (opponentChips.getNumChips() + myChips.getNumChips()));
		
		// No rollout steps
		this.m_RolloutSteps = 0;
		
		// Save nothing for all of the possible actions
		this.m_AllPossibleActionsCache = null;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		logAllState												//
	// Purpose:		Logs the entire state to the logger.					//
	//////////////////////////////////////////////////////////////////////////
	private void logAllState()
	{
		// Just log everything
		this.m_Logger.beginTag("State");
		this.m_Logger.addTag("MyChips", this.m_MyChips);
		this.m_Logger.addTag("MyPosition", this.m_MyPosition);
		this.m_Logger.addTag("MyDormantRounds", new Integer(this.m_MyDormantRounds));
		this.m_Logger.addTag("MyNeededChips", this.m_MyNeededChips);
		this.m_Logger.addTag("OpponentChips", this.m_OpponentChips);
		this.m_Logger.addTag("OpponentPosition", this.m_OpponentPosition);
		this.m_Logger.addTag("OpponentDormantRounds", new Integer(this.m_OpponentDormantRounds));
		this.m_Logger.addTag("OpponentNeededChips", this.m_OpponentNeededChips);			
		this.m_Logger.addTag("LastOffer", (this.m_LastOffer == null ? "null" : this.m_LastOffer));
		this.m_Logger.addTag("LastAcceptedOffer", (this.m_LastAcceptedOffer == null ? "null" : this.m_LastAcceptedOffer));
		this.m_Logger.addTag("IsFirstProposerWasMe", new Boolean(this.m_IsFirstProposerWasMe));
		this.m_Logger.addTag("GamePhase", this.m_GamePhase);
		this.m_Logger.endTag("State");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		Copy constructor					   					//
	// Purpose:		Copies a game state.									//
	// Parameters:	* otherGameState - The game state to copy from.			//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public GameState(GameState otherGameState)
	{
		// Copy the logger
		this.m_Logger = otherGameState.m_Logger;
		
		// The board, scoring and goal position
		this.m_Board = otherGameState.m_Board;
		this.m_Scoring = otherGameState.m_Scoring;
		this.m_GoalPosition = otherGameState.m_GoalPosition;
		
		// The rollout model
		this.m_RolloutModel = otherGameState.m_RolloutModel;
		
		// The game phase
		this.m_GamePhase = otherGameState.m_GamePhase;
	
		// The last offer, last accepted offer and the first exchange chips
		this.m_LastOffer = (otherGameState.m_LastOffer == null ? null : new Offer(otherGameState.m_LastOffer));
		this.m_LastAcceptedOffer = (otherGameState.m_LastAcceptedOffer == null ? null : new Offer(otherGameState.m_LastAcceptedOffer));
		this.m_FirstExchangeChips = (otherGameState.m_FirstExchangeChips == null ? null : new ChipSet(otherGameState.m_FirstExchangeChips));
		
		// Whether I was the first proposer or the opponent
		this.m_IsFirstProposerWasMe = otherGameState.m_IsFirstProposerWasMe;
				
		// My chips, position and dormant rounds
		this.m_MyChips = new ChipSet(otherGameState.m_MyChips);
		this.m_MyPosition = new RowCol(otherGameState.m_MyPosition);
		this.m_MyDormantRounds = otherGameState.m_MyDormantRounds;
		this.m_MyNeededChips = new ChipSet(otherGameState.m_MyNeededChips);
		
		// Opponent's chips, position and dormant rounds
		this.m_OpponentChips = new ChipSet(otherGameState.m_OpponentChips);
		this.m_OpponentPosition = new RowCol(otherGameState.m_OpponentPosition);
		this.m_OpponentDormantRounds = otherGameState.m_OpponentDormantRounds;
		this.m_OpponentNeededChips = new ChipSet(otherGameState.m_OpponentNeededChips);
		
		// The maximum and minimum score
		this.m_MyMinScore = otherGameState.m_MyMinScore;
		this.m_MyMaxScore = otherGameState.m_MyMaxScore;
		this.m_OpponentMinScore = otherGameState.m_OpponentMinScore;
		this.m_OpponentMaxScore = otherGameState.m_OpponentMaxScore;
		
		// The rollout steps
		this.m_RolloutSteps = otherGameState.m_RolloutSteps;
		
		// Clean up the cache
		this.m_AllPossibleActionsCache = null;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		isMyTurn						    					//
	// Purpose:		Determines whether it's my turn or the opponent's.		//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public boolean isMyTurn()
	{
		// Calculate according to the phase and whether I was the first proposer or not
		switch (this.m_GamePhase)
		{
		case PROP1:
		case RESP2:
			return this.m_IsFirstProposerWasMe;
		case EXCH1:
			return true;
		case EXCH2:
			return false;
		default:
			return !(this.m_IsFirstProposerWasMe);
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		isMyTurn						    					//
	// Purpose:		Determines whether it's the agent's turn or the			//
	//				human's.												//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public boolean isAgentTurn()
	{
		// It's just a synonym
		return isMyTurn();
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		isGameOver						    					//
	// Purpose:		Determines whether the game is over or not.				//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public boolean isGameOver()
	{
		// Answer according to the game phase
		return (this.m_GamePhase == GamePhase.OVER);
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		getAllPossibleResponses									//
	// Purpose:		Returns a list of all of the possible responses.		//
	//				Applicable only in a response phase.					//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	private List<Object> getAllPossibleResponses()
	{
		// Just build a fixed list with True and False
		List<Object> result = new ArrayList();
		result.add(new Boolean(false));
		result.add(new Boolean(true));
		return result;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		willPlayerBecomeIndependent								//
	// Purpose:		Specifies whether a given player will become			//
	//				independent given his receive and send chips.			//
	// Parameters:	* isAgent - Whether it's the agent or the human.		//
	//				* playerSendChips - The chips sent by the player.		//
	//				* playerReceiveChips - The chips received by the given	//
	//									   player.							//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public boolean willPlayerBecomeIndependent(boolean isAgent,
											   ChipSet playerSendChips,
											   ChipSet playerReceiveChips)
	{
		boolean result = false;
		ChipSet playerNeededChips = getPlayerNeededChips(isAgent);
		
		// Calculate the chips of the player after exchanging
		ChipSet playerChips = ChipSet.subChipSets(ChipSet.addChipSets((isAgent ? this.m_MyChips : this.m_OpponentChips), playerReceiveChips), playerSendChips);
		
		// Check if the player's chips after the exchange contain the needed chips
		return playerChips.contains(playerNeededChips);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getAllPossibleExchanges									//
	// Purpose:		Returns a list of all of the possible responses.		//
	//				Applicable only in an exchange phase.					//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	private List<Object> getAllPossibleExchanges()
	{
		List<Object> result = new ArrayList();
		boolean isFullInResult = false;
		
		// If there is no last accepted offer - quit
		if (this.m_LastAcceptedOffer == null)
		{
			result.add(new ChipSet());
			return result;
		}
		
		// Calculate the full exchange chip set
		ChipSet fullExchange = (isMyTurn() ? this.m_LastAcceptedOffer.m_ChipsSentByMe : this.m_LastAcceptedOffer.m_ChipsSentByOpponent);
		
		// Calculate the needed chips in the exchange of both players
		ChipSet myNeededChipsInExchange = EquivalenceChips.intersectChips(fullExchange, getPlayerNeededUnownedChips(true));
		ChipSet opponentNeededChipsInExchange = EquivalenceChips.intersectChips(fullExchange, getPlayerNeededUnownedChips(false));
		
		// Calculate the send options
		ChipSet playerNeededChips = (isMyTurn() ? myNeededChipsInExchange : opponentNeededChipsInExchange);
		ChipSet otherNeededChips = (isMyTurn() ? opponentNeededChipsInExchange : myNeededChipsInExchange);
		ChipSet sendOptions = EquivalenceChips.subChipSetsNoNegatives(otherNeededChips, playerNeededChips);
		
		// Take representatives for each send option
		for (int toSend = 0; toSend <= s_MaxExchangeChips; toSend++)
		{
			// Leave loop if we want too much
			if (sendOptions.getNumChips() < toSend)
			{
				break;
			}
			
			// Add to result
			ChipSet toAdd = sendOptions.getSubset(toSend);
			isFullInResult = (isFullInResult || (toAdd.equals(fullExchange)));
			result.add(toAdd);
		}
		
		// Optionally add the full exchange
		if (!isFullInResult)
		{
			result.add(fullExchange);
		}
		
		// Return the result
		return result;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		filterOfferResults										//
	// Purpose:		Filters offers.											//
	// Parameters:	* unfilteredResult - The list of possible offers to		//
	//									 filter.							//
	//////////////////////////////////////////////////////////////////////////
	private List<Object> filterOfferResults(List<Object> unfilteredResult)
	{
		List<Object> result = null;
		
		// If we're not using Weka - don't filter
		if (!SushiPlayer.isUsingWeka())
		{
			return unfilteredResult;
		}

		// Filter
		result = new ArrayList();
		for (Object offerObject : unfilteredResult)
		{
			Offer offer = (Offer)offerObject;

			// Calculate the current scores
			double myCurrentScore = getScore(true);
			double opponentCurrentScore = getScore(false);		
			
			// Calculate resulting scores
			double myResultingScore = ReliabilityUtil.getResultingScore(getBoard(), getScoring(), getPlayerChips(true), getPlayerChips(false), getPlayerPosition(true), offer.m_ChipsSentByHuman, offer.m_ChipsSentByAgent);
			double opponentResultingScore = ReliabilityUtil.getResultingScore(getBoard(), getScoring(), getPlayerChips(false), getPlayerChips(true), getPlayerPosition(false), offer.m_ChipsSentByAgent, offer.m_ChipsSentByHuman);
			
			// Filter if offer shouldn't be considered
			if (!WekaBasedModel.shouldConsiderOffer(this, offer, myCurrentScore, myResultingScore, opponentCurrentScore, opponentResultingScore))
			{
				continue;
			}
			
			// Add the offer
			result.add(offerObject);
		}
		
		// Add the empty offer if it didn't exist an no other offers exists
		if (result.size() == 0)
		{
			result.add(new Offer(new ChipSet(), new ChipSet()));
		}
		
		// Return the result
		return result;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getAllPossibleOffers									//
	// Purpose:		Returns a list of all of the possible offers.			//
	//				Applicable only in an offers phase.						//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	private List<Object> getAllPossibleOffers()
	{
		List<Object> result = null;
		
		// Calculate sendable chips of both players
		ChipSet mySendableChips = EquivalenceChips.subChipSetsNoNegatives(this.m_MyChips, this.m_MyNeededChips);
		ChipSet opponentSendableChips = EquivalenceChips.subChipSetsNoNegatives(this.m_OpponentChips, this.m_OpponentNeededChips);
		
		// Return all of the possible offers from the equivalence chips
		result = EquivalenceChips.getAllEquivalentOffers(mySendableChips, opponentSendableChips, getPlayerNeededUnownedChips(true), getPlayerNeededUnownedChips(false));
		
		// Filter results
		result = filterOfferResults(result);
		
		// Return the result
		return result;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getAllPossibleActions									//
	// Purpose:		Returns a list of all of the possible actions.			//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public List<Object> getAllPossibleActions()
	{
		// Caching implementation
		if (this.m_AllPossibleActionsCache == null)
		{
			// Return the possible actions depending on the phase
			switch (this.m_GamePhase)
			{
			case RESP1:
			case RESP2:
				this.m_AllPossibleActionsCache = getAllPossibleResponses();
				break;
			case PROP1:
			case PROP2:
				this.m_AllPossibleActionsCache = getAllPossibleOffers();
				break;
			case EXCH1:
			case EXCH2:
				this.m_AllPossibleActionsCache = getAllPossibleExchanges();
				break;
			case OVER:
				this.m_AllPossibleActionsCache = new ArrayList();
				break;
			}
		}
		
		// Return from cache
		return this.m_AllPossibleActionsCache;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		propose													//
	// Purpose:		Propose an offer. Applicable only in an offers phase.	//
	// Parameters:	* offer - The offer.									//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	private void propose(Offer offer)
	{
		// Logging
		this.m_Logger.beginTag("Propose");
		logAllState();
		this.m_Logger.addTag("Offer", offer);
		
		// Notice the rollout model
		this.m_RolloutModel.onProposal(this, isMyTurn(), new Offer(offer));
		
		// Just set the last offer and move along the game phase
		this.m_LastOffer = new Offer(offer);
		if (this.m_GamePhase == GamePhase.PROP1)
		{
			this.m_GamePhase = GamePhase.RESP1;
		}
		else
		{
			this.m_GamePhase = GamePhase.RESP2;
		}
		
		// Logging
		this.m_Logger.endTag("Propose");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		respond													//
	// Purpose:		Responds to the last offer. Applicable only in a		//
	//				response phase.											//
	// Parameters:	* response - The response to the last offer.			//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	private void respond(Boolean response)
	{
		// Logging
		this.m_Logger.beginTag("Response");
		logAllState();
		this.m_Logger.addTag("Response", response);
		
		// If we agreed - save the last accepted offer
		if (response)
		{
			this.m_LastAcceptedOffer = new Offer(this.m_LastOffer);
		}
		
		// Notice the rollout model
		this.m_RolloutModel.onResponse(this, isMyTurn(), response);
		
		// Nullify the last offer
		this.m_LastOffer = null;
		
		// Move on with the game
		if ((!response) && (this.m_GamePhase == GamePhase.RESP1))
		{
			this.m_GamePhase = GamePhase.PROP2;
		}
		else
		{
			this.m_GamePhase = GamePhase.EXCH1;
		}
		
		// Logging
		this.m_Logger.endTag("Response");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		exchange												//
	// Purpose:		Exchange chips. Applicable only in an exchange phase.	//
	// Parameters:	* chipsToSend - The chips to send.						//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	private void exchange(ChipSet chipsToSend)
	{
		// Logging
		this.m_Logger.beginTag("Exchange");
		logAllState();
		this.m_Logger.addTag("ChipsToSend", chipsToSend);
		
		// If we're in the first exchange, just save it
		if (this.m_GamePhase == GamePhase.EXCH1)
		{
			this.m_FirstExchangeChips = new ChipSet(chipsToSend);
			this.m_GamePhase = GamePhase.EXCH2;
			this.m_Logger.endTag("Exchange");
			return;
		}
		
		// Here we're in the second exchange, so make the transaction
		ChipSet mySendChips = this.m_FirstExchangeChips;
		ChipSet opponentSendChips = chipsToSend;
		makeExchange(mySendChips, opponentSendChips);
		
		// Nullify the last accepted offer and the first exchange chips
		this.m_LastAcceptedOffer = null;
		this.m_FirstExchangeChips = null;
		
		// Move players
		move();
		
		// Logging
		this.m_Logger.endTag("Exchange");
	}	
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		makeExchange											//
	// Purpose:		Actually exchange chips.								//
	// Parameters:	* mySendChips - The chips sent by me.					//
	// Parameters:	* opponentSendChips - The chips sent by the opponent.	//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	private void makeExchange(ChipSet mySendChips,
							  ChipSet opponentSendChips)
	{
		ChipSet myNewChips = ChipSet.subChipSets(ChipSet.addChipSets(this.m_MyChips, opponentSendChips), mySendChips);
		ChipSet opponentNewChips = ChipSet.subChipSets(ChipSet.addChipSets(this.m_OpponentChips, mySendChips), opponentSendChips);
		
		// Update the rollout model
		this.m_RolloutModel.onSendingChips(this, mySendChips, opponentSendChips);
		
		// Commit the exchange
		this.m_MyChips = myNewChips;
		this.m_OpponentChips = opponentNewChips;
		
		// Notify the rollout model
		this.m_RolloutModel.onSentChips(this);
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		increaseDormantRounds									//
	// Purpose:		Increase dormant rounds to the given players.			//
	// Parameters:	* isMe - Whether it's me or the opponent.				//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	private void increaseDormantRounds(boolean isMe)
	{
		// Just increase
		if (isMe)
		{
			this.m_MyDormantRounds++;
		}
		else
		{
			this.m_OpponentDormantRounds++;
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		tryToMove												//
	// Purpose:		Try to move the player as much as it can.				//
	// Parameters:	* isMe - Whether it's me or the opponent.				//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	private void tryToMove(boolean isMe)
	{
		ChipSet playerChips = (isMe ? this.m_MyChips : this.m_OpponentChips);
		ChipSet otherPlayerChips = (isMe ? this.m_OpponentChips : this.m_MyChips);
		RowCol playerPosition = (isMe ? this.m_MyPosition : this.m_OpponentPosition);
		
		// Logging
		this.m_Logger.beginTag("MoveTry");
		this.m_Logger.addTag("IsMe", new Boolean(isMe));
		
		// If the number of dormant rounds is not a threat - quit
		if ((isMe ? this.m_MyDormantRounds : this.m_OpponentDormantRounds) < s_MaxDormantRounds - 1)
		{
			increaseDormantRounds(isMe);
			this.m_Logger.endTag("MoveTry");
			return;
		}
		
		// Get the relevant path
		ArrayList<Path> paths = ShortestPaths.getShortestPaths(playerPosition, this.m_GoalPosition, this.m_Board, this.m_Scoring, 1, playerChips, otherPlayerChips);
		if (paths.size() == 0)
		{
			increaseDormantRounds(isMe);
			this.m_Logger.endTag("MoveTry");
			return;
		}
		
		// Get the needed color and check if it's in the player's posession
		RowCol pointToMove = paths.get(0).getPoint(1);
		String neededColor = this.m_Board.getSquare(pointToMove).getColor();
		this.m_Logger.addTag("NeededColor", neededColor);
		if (playerChips.getNumChips(neededColor) == 0)
		{
			increaseDormantRounds(isMe);
			this.m_Logger.endTag("MoveTry");
			return;
		}
		
		// It's possible to move
		playerChips.add(neededColor, -1);
		playerPosition = new RowCol(pointToMove);
		if (isMe)
		{
			this.m_MyPosition = playerPosition;
		}
		else
		{
			this.m_OpponentPosition = playerPosition;
		}

		// Update dormant rounds and needed chips
		if (isMe)
		{
			this.m_MyDormantRounds = 0;
			this.m_MyNeededChips.set(neededColor, Math.max(this.m_MyNeededChips.getNumChips(neededColor) - 1, 0));
		}
		else
		{
			this.m_OpponentDormantRounds = 0;
			this.m_OpponentNeededChips.set(neededColor, Math.max(this.m_OpponentNeededChips.getNumChips(neededColor) - 1, 0));
		}
		
		// Logging
		this.m_Logger.addTag("NewChips", playerChips);
		this.m_Logger.addTag("NewPosition", playerPosition);
		
		// Notice the rollout model
		this.m_RolloutModel.onMove(this, isMe);
		this.m_Logger.endTag("MoveTry");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		move													//
	// Purpose:		Try to move the players as much as they can. Also		//
	//				updates dormant steps and moves on to the next phase.	//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	private void move()
	{
		// Try to move both players
		tryToMove(true);
		tryToMove(false);
		
		// Check if the game is over because someone reached the goal
		if (this.m_MyPosition.equals(this.m_GoalPosition) || this.m_OpponentPosition.equals(this.m_GoalPosition))
		{
			this.m_RolloutModel.onGameOver(this, getCurrentScore(true), getCurrentScore(false));
			this.m_GamePhase = GamePhase.OVER;
			return;
		}
		
		// Check if the game is over due to dormant steps
		if (Math.max(this.m_MyDormantRounds, this.m_OpponentDormantRounds) >= s_MaxDormantRounds)
		{
			this.m_RolloutModel.onGameOver(this, getCurrentScore(true), getCurrentScore(false));
			this.m_GamePhase = GamePhase.OVER;
			return;
		}
		
		// Check if the game ends because that two of the agents reach the goal
		if (canReachGoal(true) && canReachGoal(false))
		{
			this.m_RolloutModel.onGameOver(this, getCurrentScore(true), getCurrentScore(false));
			this.m_GamePhase = GamePhase.OVER;
			return;
		}
		
		// Game is not over, reverse the first proposer and continue to the first communications phase
		this.m_GamePhase = GamePhase.PROP1;
		this.m_IsFirstProposerWasMe = !(this.m_IsFirstProposerWasMe);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		doAction												//
	// Purpose:		Performs an action on the game.							//
	// Parameters:	* action - The action, depending on the phase.			//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////		
	public void doAction(Object action)
	{
		// Cleans the cache since it's dirty
		this.m_AllPossibleActionsCache = null;
		
		// Perform the action depending on the phase
		switch (this.m_GamePhase)
		{
		case PROP1:
		case PROP2:
			propose((Offer)action);
			break;
		case RESP1:
		case RESP2:
			respond((Boolean)action);
			break;
		case EXCH1:
		case EXCH2:
			exchange((ChipSet)action);
			break;
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		rollout													//
	// Purpose:		Performs a rollout. A rollout is a simulation which is	//
	//				continued until a terminal state is reached.			//
	//////////////////////////////////////////////////////////////////////////
	public void rollout()
	{
		// Logging
		this.m_Logger.beginTag("Rollout");
		logAllState();
		
		// Simulate until the game has ended
		while (!isGameOver())
		{
			// Clean the cache since it's dirty
			this.m_AllPossibleActionsCache = null;
			
			// Increase the number of steps
			this.m_RolloutSteps++;
			
			// Select an action according to the game phase and the rollout model
			switch(this.m_GamePhase)
			{
			case PROP1:
			case PROP2:
				propose(this.m_RolloutModel.getOffer(this, isMyTurn()));
				break;
			case RESP1:
			case RESP2:
				respond(this.m_RolloutModel.getResponse(this, isMyTurn(), this.m_LastOffer));
				break;
			case EXCH1:
			case EXCH2:
				exchange(this.m_RolloutModel.getExchange(this, isMyTurn(), this.m_LastAcceptedOffer));
				break;
			}
		}
		
		// Logging
		this.m_Logger.addTag("MyScore", new Double(getScore(true)));
		this.m_Logger.addTag("OpponentScore", new Double(getScore(false)));
		this.m_Logger.addTag("NumberOfSteps", new Integer(this.m_RolloutSteps));
		this.m_Logger.endTag("Rollout");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getRolloutSteps											//
	// Purpose:		Returns the number of rollout steps.					//
	//////////////////////////////////////////////////////////////////////////
	public int getRolloutSteps()
	{
		// Just return the number of steps
		return this.m_RolloutSteps;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getCurrentScore											//
	// Purpose:		Gets the current score of the specified player.			//
	// Parameters:	* isMe - Specifies the player.							//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	private double getCurrentScore(boolean isMe)
	{
		ChipSet playerChips = (isMe ? this.m_MyChips : this.m_OpponentChips);
		ChipSet otherChips = (isMe ? this.m_OpponentChips : this.m_MyChips);
		RowCol playerPosition = (isMe ? this.m_MyPosition : this.m_OpponentPosition);
		
		// Return according to the reliability utility
		return ReliabilityUtil.getCurrentScore(this.m_Board, this.m_Scoring, playerChips, otherChips, playerPosition);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getScore												//
	// Purpose:		Gets the final score of the specified player.			//
	// Parameters:	* isAgent - Whether it's the agent or the human.		//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public double getScore(boolean isAgent)
	{
		// Just get the current score
		return getCurrentScore(isAgent);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getNormalizedScore										//
	// Purpose:		Gets the final normalized score of the specified		//
	//				player.													//
	// Parameters:	* isAgent - Specifies the player.						//
	// Checked:		03/02/2013												//
	//////////////////////////////////////////////////////////////////////////
	public double getNormalizedScore(boolean isAgent)
	{
		// Get the minimal and maximal score
		double minScore = (isAgent ? this.m_MyMinScore : this.m_OpponentMinScore);
		double maxScore = (isAgent ? this.m_MyMaxScore : this.m_OpponentMaxScore);
		
		// Normalize
		return ((getCurrentScore(isAgent) - minScore) / (maxScore - minScore));
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getBoard												//
	// Purpose:		Gets board of the game.									//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public Board getBoard()
	{
		// Return a copy of the board
		return new Board(this.m_Board);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getScoring												//
	// Purpose:		Gets scoring of the game.								//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public Scoring getScoring()
	{
		// Return a copy of the scoring
		return new Scoring(this.m_Scoring);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getPlayerChips											//
	// Purpose:		Gets the given player's current chips.					//
	// Parameters:	* isAgent - Whether it's the agent or the human.		//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public ChipSet getPlayerChips(boolean isAgent)
	{
		// Return a copy of the given player's chips
		return new ChipSet(isAgent ? this.m_MyChips : this.m_OpponentChips);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getPlayerPosition										//
	// Purpose:		Gets the given player's current position.				//
	// Parameters:	* isAgent - Whether it's the agent or the human.		//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public RowCol getPlayerPosition(boolean isAgent)
	{
		// Return a copy of the given player's chips
		return new RowCol(isAgent ? this.m_MyPosition : this.m_OpponentPosition);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getPlayerDormantRounds									//
	// Purpose:		Gets the given player's dormant rounds.					//
	// Parameters:	* isAgent - Whether it's the agent or the human.		//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public int getPlayerDormantRounds(boolean isAgent)
	{
		// Return the given player's dormant rounds
		return (isAgent ? this.m_MyDormantRounds : this.m_OpponentDormantRounds);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getPlayerNeededChips									//
	// Purpose:		Gets the given player's current needed chips.			//
	// Parameters:	* isAgent - Whether it's the agent or the human.		//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public ChipSet getPlayerNeededChips(boolean isAgent)
	{
		// Return a copy of the given player's needed chips
		return new ChipSet(isAgent ? this.m_MyNeededChips : this.m_OpponentNeededChips);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getPlayerNeededUnownedChips								//
	// Purpose:		Gets the given player's current needed chips minus the	//
	//				chips that he already owns.								//
	// Parameters:	* isAgent - Whether it's the agent or the human.		//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public ChipSet getPlayerNeededUnownedChips(boolean isAgent)
	{
		// Just substract the owned chips from the needed chips
		return EquivalenceChips.subChipSetsNoNegatives(getPlayerNeededChips(isAgent), getPlayerChips(isAgent));
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		isFirstProposerWasMe									//
	// Purpose:		Indicates whether the first prooser was me.				//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public boolean isFirstProposerWasMe()
	{
		// Just return the value
		return this.m_IsFirstProposerWasMe;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		isFirstProposerWasAgent									//
	// Purpose:		Indicates whether the first prooser was the agent.		//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public boolean isFirstProposerWasAgent()
	{
		// It's just a synonym
		return isFirstProposerWasMe();
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		canReachGoal											//
	// Purpose:		Indicates whether the given player can reach the goal.	//
	// Parameters:	* isAgent - Whether it's the agent or the human.		//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public boolean canReachGoal(boolean isAgent)
	{		
		// The player can reach the goal if and only if the number of unowned needed chips is zero
		return (getPlayerNeededUnownedChips(isAgent).getNumChips() == 0);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getRolloutModel											//
	// Purpose:		Get the rollout model of the game state.				//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public RolloutModel getRolloutModel()
	{
		// Return the rollout model
		return this.m_RolloutModel;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getGamePhase											//
	// Purpose:		Gets the game phase.									//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public GamePhase getGamePhase()
	{
		// Return the game phase
		return this.m_GamePhase;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getLastOffer											//
	// Purpose:		Gets the last offer.									//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public Offer getLastOffer()
	{
		// Return the offer
		return (this.m_LastOffer == null ? null : new Offer(this.m_LastOffer));
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getLastAcceptedOffer									//
	// Purpose:		Gets the last accepted offer.							//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public Offer getLastAcceptedOffer()
	{
		// Return the offer
		return (this.m_LastAcceptedOffer == null ? null : new Offer(this.m_LastAcceptedOffer));
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getFirstExchangeChips									//
	// Purpose:		Gets the first exchange chips.							//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public ChipSet getFirstExchangeChips()
	{
		return (this.m_FirstExchangeChips == null ? null : new ChipSet(this.m_FirstExchangeChips));
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		setRolloutModel											//
	// Purpose:		Sets the rollout model.									//
	// Parameters:	* rolloutModel - The rollout model.						//
	//////////////////////////////////////////////////////////////////////////
	public void setRolloutModel(RolloutModel rolloutModel)
	{
		// Set the rollout model
		this.m_RolloutModel = rolloutModel;
	}
}