package ctagents.alternateOffersAgent;

import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;

public class TaskDependency {
	ChipSet chipsNeededToGoal;
	ChipSet chipsIHave;
	
	TaskDependency(ChipSet need, ChipSet have){
		chipsNeededToGoal = need;
		chipsIHave = have;
	}
	
	TaskDependency(){}
	
	boolean IsTaskDependant(){
		if(chipsIHave.contains(chipsNeededToGoal))
			return false;
		return true;
	}
	
	void updateChipsNeeded(ChipSet need){
		chipsNeededToGoal = need;
	}
	
	void updateChipsHave(ChipSet have) {
		chipsIHave = have;
	}

}
