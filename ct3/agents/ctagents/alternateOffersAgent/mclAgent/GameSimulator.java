//////////////////////////////////////////////////////////////////////////
// File:		GameSimulator.java 					    				//
// Purpose:		Implements the game simulator class.					//
//				The game simulator class is able to simulate the game	//
//				of CT (both players).									//
//////////////////////////////////////////////////////////////////////////
package ctagents.alternateOffersAgent.mclAgent;

// Imports from Java common framework
import java.util.Collections;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.HashSet;
import java.util.TreeMap;
import java.util.List;
import java.lang.Math;
import java.util.LinkedHashSet;
import java.util.HashMap;
import java.util.Map;

// Imports from CT framework
import edu.harvard.eecs.airg.coloredtrails.shared.Scoring;
import edu.harvard.eecs.airg.coloredtrails.alglib.ShortestPaths;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Board;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;
import edu.harvard.eecs.airg.coloredtrails.shared.types.GameStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Path;
import edu.harvard.eecs.airg.coloredtrails.shared.types.RowCol;
import edu.harvard.eecs.airg.coloredtrails.shared.types.PlayerStatus;
import ctagents.alternateOffersAgent.Proposal;

public class GameSimulator
{
	// Debugging and logging members
	private static final String s_MainInstanceName = "MclTreeLog.txt";
	private static final String s_SamplesInstanceName = "MclSamplesTreeLog.txt";
	private static final boolean s_IsDebug = true;
	private static final TreeLogger s_TreeLogger = TreeLogger.getInstance(s_MainInstanceName);
	private static final TreeLogger s_SamplesLogger = TreeLogger.getInstance(s_SamplesInstanceName);

	// Weka related parameters
	private static final boolean s_UseWekaModels = true;
	private static final String s_WekaModelsBaseFolder = "agents/ctagents/alternateOffersAgent/mclAgent/";
	private static final Culture s_Culture = Culture.ISRAEL;
	
	// The maximum number of dormant steps
	private final static int s_MaxDormantSteps = 3;
	
	// The maximum number of chips in a simulated transfer
	private final static int s_MaxChipsInSimulatedTransfer = 3;

	// The maximum number of chips in a transfer sampling
	private final static int s_MaxChipsInTransfer = 3;
	
	// Branching factors
	private static int s_BranchingFactorForResponses = 10000;
	private static int s_BranchingFactorForProposals = 10000;
	private static int s_BranchingFactorForTransfers = 10000;
	
	// Last round reliability weight
	private static double s_LastRoundReliabilityWeight = 0.7;
	
	// My score weight
	private static double s_MyScoreWeight = 0.95;
	
	// Specifies whether the game is over
	private boolean m_GameOver;
	
	// Saves the scoring
	private Scoring m_Scoring;

	// Saves the board
	private Board m_Board;

	// The random generator
	private static Random s_RandomGenerator = new Random();

	// Saves whether the first proposer was me
	private boolean m_FirstProposerIsMe;

	// Saves the goal position
	private RowCol m_GoalPosition;

	// Owned chips
	private ChipSet m_MyChips;
	private ChipSet m_OpponentChips;

	// Needed chips and colors
	private static Set<String> s_MyOriginalNeededColors = null;
	private static Set<String> s_OpponentOriginalNeededColors = null;
	private ChipSet m_BothNeededChips;
	private ChipSet m_MyNeededChips;
	private ChipSet m_OpponentNeededChips;

	// Positions
	private RowCol m_MyPosition;
	private RowCol m_OpponentPosition;

	// Reliabilities
	private double m_MyReliabilitySum;
	private double m_OpponentReliabilitySum;
	private int m_RoundsSoFar;

	// Dormant steps
	private int m_MyDormantSteps;
	private int m_OpponentDormantSteps;
	
	// Maximum and minimum scores
	private int m_MaxScore;
	private int m_MinScore;

	//////////////////////////////////////////////////////////////////////////
	// Enum:		Culture													//
	// Purpose:		Enumerates a countrty for the Weka models.				//
	//////////////////////////////////////////////////////////////////////////
	public enum Culture
	{
		ISRAEL,
		USA,
		LEBANON
	};
	
	//////////////////////////////////////////////////////////////////////////
	// Enum:		ActionType												//
	// Purpose:		The type of action.										//
	//////////////////////////////////////////////////////////////////////////
	public enum ActionType
	{
		RESPONSE,
		PROPOSE,
		TRANSFER
	}

	//////////////////////////////////////////////////////////////////////////
	// Enum:		BoardType												//
	// Purpose:		Enumerates a board type from the agent's point of view.	//
	//////////////////////////////////////////////////////////////////////////
	public enum BoardType
	{
		DD,
		TD,
		TI
	};
	
	//////////////////////////////////////////////////////////////////////////
	// Class:		EquivalenceClass 					    				//
	// Purpose:		Implements the equivalence class of chip sets.			//
	//				This class divides a chip set into four components:		//
	//				1. The number of chips needed by both opponents.		//
	//				2. The number of chips needed only by me.				//
	//				3. The number of chips needed only by the opponent.		//
	//				4. The number of un-wanted chips.						//
	//////////////////////////////////////////////////////////////////////////	
	private class EquivalenceClass
	{
		// Holds the original chip set
		public ChipSet m_OriginalChipSet;
		
		// Keeps the number of chips needed by both
		public int m_ChipsNeededByBoth;
		
		// Keeps the number of chips needed only by me
		public int m_ChipsNeededByMe;
		
		// Keeps the number of chips needed only by the opponent
		public int m_ChipsNeededByOpponent;
		
		// Keeps the number of un-wanted chips
		public int m_ChipsNotNeeded;
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		EquivalenceClass constructor		    				//
		// Purpose:		Constracts the instance of the equivalence class.		//
		// Parameters:	* originalChipSet - the original chip set.				//
		//////////////////////////////////////////////////////////////////////////	
		public EquivalenceClass(ChipSet originalChipSet)
		{
			// Calculate the actual needed chips
			ChipSet myActualNeededChips = subChipSetsNoNegatives(ChipSet.addChipSets(m_BothNeededChips, m_MyNeededChips), m_MyChips);
			ChipSet opponentActualNeededChips = subChipSetsNoNegatives(ChipSet.addChipSets(m_BothNeededChips, m_OpponentNeededChips), m_OpponentChips);
			ChipSet bothActualNeededChips = intersectChipSetsAndSubstract(myActualNeededChips, opponentActualNeededChips);
			
			// Save the original chip set
			this.m_OriginalChipSet = new ChipSet(originalChipSet);
			
			// Initialize fields
			this.m_ChipsNeededByBoth = 0;
			this.m_ChipsNeededByMe = 0;
			this.m_ChipsNeededByOpponent = 0;
			this.m_ChipsNotNeeded = 0;
			
			// Iterate the entire chip set
			for (String color : originalChipSet.getColors())
			{
				// Get the number of chips of that color
				int chipsOfColor = originalChipSet.getNumChips(color);
				
				// Add the number of needed chips by both
				int neededChipsByBoth = Math.min(chipsOfColor, bothActualNeededChips.getNumChips(color));
				this.m_ChipsNeededByBoth += neededChipsByBoth;
				chipsOfColor -= neededChipsByBoth;
				if (chipsOfColor <= 0)
				{
					continue;
				}
				
				// Add the number of needed chips by me
				int neededChipsByMe = Math.min(chipsOfColor, myActualNeededChips.getNumChips(color));
				this.m_ChipsNeededByMe += neededChipsByMe;
				chipsOfColor -= neededChipsByMe;
				if (chipsOfColor <= 0)
				{
					continue;
				}
				
				// Add the number of needed chips by the opponent
				int neededChipsByOpponent = Math.min(chipsOfColor, opponentActualNeededChips.getNumChips(color));
				this.m_ChipsNeededByOpponent += neededChipsByOpponent;
				chipsOfColor -= neededChipsByOpponent;
				if (chipsOfColor <= 0)
				{
					continue;
				}
				
				// The rest of the chips are not needed
				this.m_ChipsNotNeeded += chipsOfColor;
			}
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		EquivalenceClass copy constructor		  				//
		// Purpose:		Constracts the instance of the equivalence class.		//
		// Parameters:	* otherEquivalenceClass - another instance.				//
		// Remarks:		* Just copies inner fields.								//
		//////////////////////////////////////////////////////////////////////////	
		public EquivalenceClass(EquivalenceClass otherEquivalenceClass)
		{
			// Just copy inner members
			this.m_OriginalChipSet = new ChipSet(otherEquivalenceClass.m_OriginalChipSet);
			this.m_ChipsNeededByBoth = otherEquivalenceClass.m_ChipsNeededByBoth;
			this.m_ChipsNeededByMe = otherEquivalenceClass.m_ChipsNeededByMe;
			this.m_ChipsNeededByOpponent = otherEquivalenceClass.m_ChipsNeededByOpponent;
			this.m_ChipsNotNeeded = otherEquivalenceClass.m_ChipsNotNeeded;
		}

		//////////////////////////////////////////////////////////////////////////
		// Method:		equals							  						//
		// Purpose:		Overrides equals method in order to use in a set.		//
		// Parameters:	* comparedObject - another instance.					//
		// Returns:		boolean													//
		// Remarks:		* Compares just inner integers.							//
		//////////////////////////////////////////////////////////////////////////	
		@Override
		public boolean equals(Object comparedObject)
		{
			// Self compare
			if (this == comparedObject)
			{
				return true;
			}

			// Null compare
			if (null == comparedObject)
			{
				return false;
			}

			// Other class compare
			if (getClass() != comparedObject.getClass())
			{
				return false;
			}

			// The class is our class
			EquivalenceClass comparedChips = (EquivalenceClass)comparedObject;

			// Compare inside integers without looking at the original chip set
			return ((this.m_ChipsNeededByBoth == comparedChips.m_ChipsNeededByBoth) &&
					(this.m_ChipsNeededByMe == comparedChips.m_ChipsNeededByMe) &&
					(this.m_ChipsNeededByOpponent == comparedChips.m_ChipsNeededByOpponent) &&
					(this.m_ChipsNotNeeded == comparedChips.m_ChipsNotNeeded));
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Class:		EquivalenceClassPair 					    			//
	// Purpose:		The equivalence class of a pair of chip sets.			//
	//////////////////////////////////////////////////////////////////////////
	private class EquivalenceClassPair
	{
		// Just save the pair
		private EquivalenceClass m_SendEq;
		private EquivalenceClass m_ReceiveEq;

		//////////////////////////////////////////////////////////////////////////
		// Method:		EquivalenceClassPair constructor		  				//
		// Purpose:		Constracts the instance of the equivalence class pair.	//
		// Parameters:	* sendEq - the send equivalence class.					//
		//				* receiveEq - the receive equivalence class.			//
		//////////////////////////////////////////////////////////////////////////	
		public EquivalenceClassPair(EquivalenceClass sendEq,
									EquivalenceClass receiveEq)
		{
			// Just copy inner members
			this.m_ReceiveEq = new EquivalenceClass(receiveEq);
			this.m_SendEq = new EquivalenceClass(sendEq);
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		EquivalenceClassPair copy constructor		  			//
		// Purpose:		Constracts the instance of the equivalence class pair.	//
		// Parameters:	* otherEquivalenceClassPair - another instance.			//
		// Remarks:		* Just copies inner fields.								//
		//////////////////////////////////////////////////////////////////////////	
		public EquivalenceClassPair(EquivalenceClassPair otherEquivalenceClass)
		{
			// Just copy inner members
			this.m_ReceiveEq = new EquivalenceClass(otherEquivalenceClass.m_ReceiveEq);
			this.m_SendEq = new EquivalenceClass(otherEquivalenceClass.m_SendEq);
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		equals							  						//
		// Purpose:		Overrides equals method in order to use in a set.		//
		// Parameters:	* comparedObject - another instance.					//
		// Returns:		boolean													//
		// Remarks:		* Compares just inner integers.							//
		//////////////////////////////////////////////////////////////////////////	
		@Override
		public boolean equals(Object comparedObject)
		{
			// Self compare
			if (this == comparedObject)
			{
				return true;
			}

			// Null compare
			if (null == comparedObject)
			{
				return false;
			}

			// Other class compare
			if (getClass() != comparedObject.getClass())
			{
				return false;
			}

			// The class is our class
			EquivalenceClassPair comparedEq = (EquivalenceClassPair)comparedObject;
			
			// Compare inner members
			if ((comparedEq.m_SendEq.equals(this.m_SendEq)) &&
				(comparedEq.m_ReceiveEq.equals(this.m_ReceiveEq)))
			{
				return true;
			}
			return false;
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		isNarrowed						  						//
		// Purpose:		Specifies whether the classes are narrowed, meaning 	//
		//				that there is no smaller equivalent offer.				//
		// Returns:		boolean													//
		//////////////////////////////////////////////////////////////////////////	
		public boolean isNarrowed()
		{
			int sum = 0;

			// Check if the multipication sum is zero
			// If it is - it means that all of the features have at least one zero
			// So the sum if zero if and only if the equivalences are narrowed
			sum += (this.m_SendEq.m_ChipsNeededByBoth * this.m_ReceiveEq.m_ChipsNeededByBoth);
			sum += (this.m_SendEq.m_ChipsNeededByMe * this.m_ReceiveEq.m_ChipsNeededByMe);
			sum += (this.m_SendEq.m_ChipsNeededByOpponent * this.m_ReceiveEq.m_ChipsNeededByOpponent);
			sum += (this.m_SendEq.m_ChipsNotNeeded * this.m_ReceiveEq.m_ChipsNotNeeded);

			// Return the result
			return (0 == sum);
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		toProposal						  						//
		// Purpose:		Returns the class as a proposal.						//
		// Returns:		Proposal												//
		//////////////////////////////////////////////////////////////////////////
		public Proposal toProposal()
		{
			// Just return the inner field
			return new Proposal(this.m_ReceiveEq.m_OriginalChipSet, this.m_SendEq.m_OriginalChipSet);
		}
	}
	
	
	//////////////////////////////////////////////////////////////////////////
	// Class:		DirectNodeData	 					    				//
	// Purpose:		Implements a container for a direct node in the MCMC	//
	//				tree.													//
	//				This class saves these parameters:						//
	//				1. The sum of scores.									//
	//				2. The sum of normalized scores.						//
	//				3. The number of occurences for this node.				//
	//////////////////////////////////////////////////////////////////////////	
	private class DirectNodeData
	{
		// The kept exposed data
		public double m_SumOfScores;
		public double m_SumOfNormalizedScores;
		public int m_NumberOfOccurences;
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		DirectNodeData constructor		    					//
		// Purpose:		Constracts the instance.								//
		// Parameters:	* firstScore - The first simulation score.				//
		//				* firstNormalizedScore - The first simulation			//
		//										 normalized score.				//
		//////////////////////////////////////////////////////////////////////////
		public DirectNodeData(double firstScore,
							  double firstNormalizedScore)
		{
			// Just initialize the fields properly
			this.m_SumOfScores = firstScore;
			this.m_SumOfNormalizedScores = firstNormalizedScore;
			this.m_NumberOfOccurences = 1;
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		addSimulation					    					//
		// Purpose:		Adds a new simulation score to this container.			//
		// Parameters:	* newScore - The simulation's score.					//
		//				* newNormalizedScore - The simulation's normalized		//
		//									   score.							//
		//////////////////////////////////////////////////////////////////////////
		public void addSimulation(double newScore,
								  double newNormalizedScore)
		{
			// Sum the scores and increase the number of occurences
			this.m_SumOfScores += newScore;
			this.m_SumOfNormalizedScores += newNormalizedScore;
			this.m_NumberOfOccurences++;
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		setLastRoundReliabilityWeight							//
	// Purpose:		Sets the last round reliability weight.					//
	// Parameters:	* lastRoundReliabilityWeight - The last round			//
	//											   reliability weight.		//
	//////////////////////////////////////////////////////////////////////////
	public static void setLastRoundReliabilityWeight(double lastRoundReliabilityWeight)
	{
		// Set my score weight
		s_LastRoundReliabilityWeight = lastRoundReliabilityWeight;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		setMyScoreWeight										//
	// Purpose:		Sets my score weight in the scoring utility function.	//
	// Parameters:	* myScoreWeight - My score weight.						//
	//////////////////////////////////////////////////////////////////////////
	public static void setMyScoreWeight(double myScoreWeight)
	{
		// Set my score weight
		s_MyScoreWeight = myScoreWeight;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		setBranchingFactor										//
	// Purpose:		Sets the branching factor for sampling.					//
	// Parameters:	* branchingFactor - The branching factor.				//
	//				* actionType - The action type.							//
	//////////////////////////////////////////////////////////////////////////
	public static void setBranchingFactor(int branchingFactor,
										  ActionType actionType)
	{
		// Set the branching factor
		switch (actionType)
		{
		case RESPONSE:
			s_BranchingFactorForResponses = branchingFactor;
			break;
		case PROPOSE:
			s_BranchingFactorForProposals = branchingFactor;
			break;
		case TRANSFER:
			s_BranchingFactorForTransfers = branchingFactor;
			break;
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getSendChipsFromProposal								//
	// Purpose:		Get the send chips from a given proposal.				//
	// Parameters:	* proposal - The proposal.								//
	//				* isMe - Whether it's my send chips or the opponent's.	//
	//////////////////////////////////////////////////////////////////////////
	private static ChipSet getSendChipsFromProposal(Proposal proposal,
													boolean isMe)
	{
		return (isMe ? proposal.chipsToSend : proposal.chipsToReceive);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getNeededColors											//
	// Purpose:		Get the needed colors for a player. The needed colors	//
	//				are colors of chips that the player does not posses		//
	//				but needs to get to the goal.							//
	// Parameters:	* neededChips - The needed chips.						//
	//				* ownedChips - The chips that the player owns.			//
	//////////////////////////////////////////////////////////////////////////
	private static Set<String> getNeededColors(ChipSet neededChips,
											   ChipSet ownedChips)
   {
		Set<String> result = new LinkedHashSet();
		
		// Find the needed colors
		for (String color : neededChips.getColors())
		{
			if ((neededChips.getNumChips(color) > 0) && (ownedChips.getNumChips(color) == 0))
			{
				result.add(color);
			}
		}
		
		// Return the result
		return result;
   }
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		Constructor						    					//
	// Purpose:		Creates a new game simulator.							//
	// Parameters:	* scoring - The scoring.								//
	// 				* board - The board.									//
	// 				* firstProposerIsMe - Whether I'm the first propose.	//
	// 				* myChips - My chips.									//
	// 				* opponentChips - Opponent's chips.						//
	// 				* myPosition - My position.								//
	// 				* opponentPosition - Opponent's position.				//
	// 				* myReliability - My reliability.						//
	// 				* opponentReliability - Opponent's reliability.			//
	// 				* roundsSoFar - The number of rounds so far.			//
	// 				* myDormantSteps - My dormant steps.					//
	// 				* opponentDormantSteps - Opponent's dormant steps.		//
	//////////////////////////////////////////////////////////////////////////
	public GameSimulator(Scoring scoring,
			 			 Board board,
			 			 boolean firstProposerIsMe,
			 			 ChipSet myChips,
			 			 ChipSet opponentChips,
			 			 RowCol myPosition,
			 			 RowCol opponentPosition,
			 			 double myReliability,
			 			 double opponentReliability,
			 			 int roundsSoFar,
			 			 int myDormantSteps,
			 			 int opponentDormantSteps)
	{
		// Initialize the tree logger
		if (s_IsDebug)
		{
			s_TreeLogger.activate();
			s_SamplesLogger.activate();
		}
		
		// Copy scoring and board
		this.m_Scoring = new Scoring(scoring);
		this.m_Board = new Board(board);
		
		// Calculate the needed chips according to the shortest path to the goal
		ChipSet myOriginalNeededChips = ShortestPaths.getShortestPaths(myPosition, board.getGoalLocations().get(0), board, scoring, 10, myChips, opponentChips).get(0).getRequiredChips(board);
		ChipSet opponentOriginalNeededChips = ShortestPaths.getShortestPaths(opponentPosition, board.getGoalLocations().get(0), board, scoring, 10, myChips, opponentChips).get(0).getRequiredChips(board);
		
		// Calculate the intersection of the needed chips and substract from the other chip sets
		ChipSet myNeededChips = new ChipSet(myOriginalNeededChips);
		ChipSet opponentNeededChips = new ChipSet(opponentOriginalNeededChips);
		this.m_BothNeededChips = intersectChipSetsAndSubstract(myNeededChips, opponentNeededChips);
		this.m_MyNeededChips = myNeededChips;
		this.m_OpponentNeededChips = opponentNeededChips;
		
		// Calculate the needed colors
		if (null == s_MyOriginalNeededColors)
		{
			s_MyOriginalNeededColors = getNeededColors(myNeededChips, myChips);
		}
		if (null == s_OpponentOriginalNeededColors)
		{
			s_OpponentOriginalNeededColors = getNeededColors(opponentNeededChips, opponentChips);
		}
		
		// Save the goal position
		this.m_GoalPosition = new RowCol(board.getGoalLocations().get(0));
		
		// Copy simple members
		this.m_FirstProposerIsMe = firstProposerIsMe;
		
		// Copy chips
		this.m_MyChips = new ChipSet(myChips);
		this.m_OpponentChips = new ChipSet(opponentChips);
		
		// Copy positions
		this.m_MyPosition = new RowCol(myPosition);
		this.m_OpponentPosition = new RowCol(opponentPosition);
		
		// Reliability related members
		this.m_MyReliabilitySum = myReliability * roundsSoFar;
		this.m_OpponentReliabilitySum = opponentReliability * roundsSoFar;
		this.m_RoundsSoFar = roundsSoFar;
		
		// Dormant steps
		this.m_MyDormantSteps = myDormantSteps;
		this.m_OpponentDormantSteps = opponentDormantSteps;
		
		// Initialize the Weka models
		initWekaModels();
		
		// Calculate the maximum and minimum score
		this.m_MinScore = this.m_Scoring.distweight * (ShortestPaths.getShortestPaths(myPosition, board.getGoalLocations().get(0), board, scoring, 10, myChips, opponentChips).get(0).getNumPoints());
		this.m_MaxScore = this.m_Scoring.goalweight + (this.m_Scoring.chipweight * (myChips.getNumChips() + opponentChips.getNumChips()));
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		Copy Constructor					   					//
	// Purpose:		Creates a GameSimulator from another instance.			//
	// Parameters:	* otherSimulation - The other instance.					//
	//////////////////////////////////////////////////////////////////////////
	private GameSimulator(GameSimulator otherSimulation)
	{
		// Copy scoring and board
		this.m_Scoring = otherSimulation.m_Scoring;
		this.m_Board = new Board(otherSimulation.m_Board);
			
		// Needed chips stay the same
		this.m_BothNeededChips = new ChipSet(otherSimulation.m_BothNeededChips);
		this.m_MyNeededChips = new ChipSet(otherSimulation.m_MyNeededChips);
		this.m_OpponentNeededChips = new ChipSet(otherSimulation.m_OpponentNeededChips);

		// Copy the goal position
		this.m_GoalPosition = otherSimulation.m_GoalPosition;
				
		// Copy simple members
		this.m_FirstProposerIsMe = otherSimulation.m_FirstProposerIsMe;
				
		// Copy chips
		this.m_MyChips = new ChipSet(otherSimulation.m_MyChips);
		this.m_OpponentChips = new ChipSet(otherSimulation.m_OpponentChips);
				
		// Copy positions
		this.m_MyPosition = new RowCol(otherSimulation.m_MyPosition);
		this.m_OpponentPosition = new RowCol(otherSimulation.m_OpponentPosition);
				
		// Reliability related members
		this.m_MyReliabilitySum = otherSimulation.m_MyReliabilitySum;
		this.m_OpponentReliabilitySum = otherSimulation.m_OpponentReliabilitySum;
		this.m_RoundsSoFar = otherSimulation.m_RoundsSoFar;
				
		// Dormant steps
		this.m_MyDormantSteps = otherSimulation.m_MyDormantSteps;
		this.m_OpponentDormantSteps = otherSimulation.m_OpponentDormantSteps;
		
		// Maximum and minimum scores
		this.m_MaxScore = otherSimulation.m_MaxScore;
		this.m_MinScore = otherSimulation.m_MinScore;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		analyzeBoardType				  						//
	// Purpose:		Analyzes the board type according to the game state.	//
	//////////////////////////////////////////////////////////////////////////
	private BoardType analyzeBoardType()
	{
		BoardType result;
		
		// Calculate needed chips
		ChipSet myNeededChips = ChipSet.addChipSets(this.m_MyNeededChips, this.m_BothNeededChips);
		ChipSet opponentNeededChips = ChipSet.addChipSets(this.m_OpponentNeededChips, this.m_BothNeededChips);
		
		// Calculate the missing chips of both sides
		ChipSet myMissingChips = this.m_MyChips.getMissingChips(myNeededChips);
		ChipSet opponentMissingChips = this.m_OpponentChips.getMissingChips(opponentNeededChips);
		
		// Figure out the board type
		if (myMissingChips.getNumChips() > 0)
		{
			result = ((opponentMissingChips.getNumChips() > 0) ? BoardType.DD : BoardType.TD);
		}
		else
		{
			result = BoardType.TI;
		}
		
		// Return the result
		return result;
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		initWekaModels					  						//
	// Purpose:		Initializes the WekaModels according to the board type. //
	//////////////////////////////////////////////////////////////////////////
	private void initWekaModels()
	{
		// If only if we're using the Weka models
		if (s_UseWekaModels)
		{
			// Get the board type
			BoardType newBoardType = analyzeBoardType();
			
			// The model is from the opponent's point of view
			// So, we must switch sides between DI and TI
			switch (newBoardType)
			{
			case TI:
				newBoardType = BoardType.TD;
				break;
			case TD:
				newBoardType = BoardType.TI;
				break;
			}
				
			// Build the culture folder and update the Weka wrapper
			String cultureFolder = s_WekaModelsBaseFolder + s_Culture.toString() + "/" + s_Culture.toString() + "_" + newBoardType.toString();
			WekaWrapper.setCultureFolder(cultureFolder);
			WekaWrapper.setCulture(s_Culture);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		logStateToTreeLogger									//
	// Purpose:		Logs the state to the tree logger.						//
	//////////////////////////////////////////////////////////////////////////
	private void logStateToTreeLogger()
	{
		// Begin a tag
		s_TreeLogger.beginTag("State");

		// Log owned chips
		s_TreeLogger.addMyChips(this.m_MyChips);
		s_TreeLogger.addOpponentChips(this.m_OpponentChips);

		// Log needed chips
		s_TreeLogger.addBothNeededChips(this.m_BothNeededChips);
		s_TreeLogger.addMyNeededChips(this.m_MyNeededChips);
		s_TreeLogger.addOpponentNeededChips(this.m_OpponentNeededChips);
		
		// Log positions
		s_TreeLogger.addMyPosition(this.m_MyPosition);
		s_TreeLogger.addOpponentPosition(this.m_OpponentPosition);

		// Log reliabilities
		s_TreeLogger.addMyReliability(this.m_MyReliabilitySum / this.m_RoundsSoFar);
		s_TreeLogger.addOpponentReliability(this.m_OpponentReliabilitySum / this.m_RoundsSoFar);

		// Log dormant steps
		s_TreeLogger.addMyDormantSteps(this.m_MyDormantSteps);
		s_TreeLogger.addOpponentDormantSteps(this.m_OpponentDormantSteps);

		// End the tag
		s_TreeLogger.endTag("State");
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		getNormalizedScore         			    				//
	// Purpose:		Gets the noramalized score for a given simulation.		//
	// Returns:		double													//
	//////////////////////////////////////////////////////////////////////////	
	private double getNormalizedScore()
	{
		double weightedScore = getScore();
		double normalizedScore = (weightedScore - this.m_MinScore) / (this.m_MaxScore - this.m_MinScore);		
		return normalizedScore;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getScore         					    				//
	// Purpose:		Gets the score for a given simulation.					//
	// Returns:		double													//
	//////////////////////////////////////////////////////////////////////////	
	private double getScore()
	{		
		// Calculate scores
		double myScore = getCurrentScore(true);
		double opponentScore = getCurrentScore(false);
		
		// Calculate weighted score
		double opponentScoreWeight = 1.0 - s_MyScoreWeight;
		double weightedScore = s_MyScoreWeight * myScore + opponentScoreWeight * opponentScore;
		return weightedScore;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		intersectChipSetsAndSubstract							//
	// Purpose:		Intersects chip sets and substracts the intersection.	//
	// Parameters:	* leftChipSet - The left chip set.						//
	//				* rightChipSet - The right chip set.					//
	// Returns:		The intersection between the chip sets.					//
	// Remarks:		* The given chip sets change (chips are substracted 	//
	//				  from them).											//
	//////////////////////////////////////////////////////////////////////////
	private static ChipSet intersectChipSetsAndSubstract(ChipSet leftChipSet,
														 ChipSet rightChipSet)
	{
		ChipSet result = new ChipSet();
		
		// Iterate the left chip set
		for (String color : leftChipSet.getColors())
		{
			// Calculate the number of chips of that color in each chip set
			int numberOfChipsInLeft = leftChipSet.getNumChips(color);
			int numberOfChipsInRight = rightChipSet.getNumChips(color);
			
			// The number of chips for that color in the intersection is the minimum
			int numberOfChipsInBoth = Math.min(numberOfChipsInLeft, numberOfChipsInRight);
			
			// Add the minimum to the result
			result.add(color, numberOfChipsInBoth);
			
			// Substract from left and right
			leftChipSet.add(color, -1 * numberOfChipsInBoth);
			rightChipSet.add(color, -1 * numberOfChipsInBoth);
		}
		
		// Return result
		return result;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		subChipSetsNoNegatives									//
	// Purpose:		Substracts chip sets without getting to negative		//
	//				values.													//
	// Parameters:	* leftChipSet - The left chip set.						//
	//				* rightChipSet - The right chip set.					//
	//////////////////////////////////////////////////////////////////////////
	private static ChipSet subChipSetsNoNegatives(ChipSet leftChipSet,
									 			  ChipSet rightChipSet)
	{
		ChipSet result = new ChipSet();
		
		// Iterate the left chip set
		for (String color : leftChipSet.getColors())
		{
			// Calculate the number of chips of that color in each chip set
			int numberOfChipsInLeft = leftChipSet.getNumChips(color);
			int numberOfChipsInRight = rightChipSet.getNumChips(color);
			
			// Substract without reaching below zero
			int numberOfChipsToAdd = Math.max(numberOfChipsInLeft - numberOfChipsInRight, 0);
			
			// Add to the result
			result.add(color, numberOfChipsToAdd);
		}
		
		// Return result
		return result;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getResultingScore										//
	// Purpose:		Gets the resulting score of the specified player.		//
	// Parameters:	* isMe - Specifies the player.							//
	//				* chipsToReceive - The received chips.					//
	//				* chipsToSend - The sent chips.							//
	//////////////////////////////////////////////////////////////////////////
	private double getResultingScore(boolean isMe,
									 ChipSet chipsToReceive,
									 ChipSet chipsToSend)
	{
		PlayerStatus playerStatus = new PlayerStatus();
		ChipSet playerChips = new ChipSet(isMe ? this.m_MyChips : this.m_OpponentChips);
		RowCol playerPosition = new RowCol(isMe ? this.m_MyPosition : this.m_OpponentPosition);
		int pathPositionIndex = 0;
		
		// Add and substract the chips
		playerChips = ChipSet.subChipSets(ChipSet.addChipSets(playerChips, chipsToReceive), chipsToSend);
		
		// Get as close as possible towards the goal
		Path pathTowardsGoal = ShortestPaths.getShortestPaths(playerPosition, this.m_Board.getGoalLocations().get(0), this.m_Board, this.m_Scoring, 1, this.m_MyChips, this.m_OpponentChips).get(0);
		
		// Look for the position on the goal
		for (pathPositionIndex = 0; pathPositionIndex < pathTowardsGoal.getNumPoints(); pathPositionIndex++)
		{
			if (pathTowardsGoal.getPoint(pathPositionIndex).equals(playerPosition))
			{
				break;
			}
		}
		
		// Try to walk as much as we can towards the goal
		boolean notReachingGoal = false;
		for (int currentPosition = pathPositionIndex + 1; currentPosition < pathTowardsGoal.getNumPoints(); currentPosition++)
		{
			String currentColor = this.m_Board.getSquare(pathTowardsGoal.getPoint(currentPosition)).getColor();
			if (playerChips.getNumChips(currentColor) > 0)
			{
				playerChips.add(currentColor, -1);
			}
			else
			{
				playerPosition = pathTowardsGoal.getPoint(currentPosition - 1);
				notReachingGoal = true;
				break;
			}
		}
		if (!notReachingGoal)
		{
			playerPosition = this.m_Board.getGoalLocations().get(0);
		}
		
		// Set the player status
		playerStatus.setPosition(playerPosition);
		playerStatus.setChips(playerChips);
		
		// Return the score
		return this.m_Scoring.score(playerStatus, this.m_GoalPosition);
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		getCurrentScore											//
	// Purpose:		Gets the current score of the specified player.			//
	// Parameters:	* isMe - Specifies the player.							//
	//////////////////////////////////////////////////////////////////////////
	private double getCurrentScore(boolean isMe)
	{
		// Just return the resulting score without exchanging anything
		return getResultingScore(isMe, new ChipSet(), new ChipSet());
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getNeededChipsInChipset									//
	// Purpose:		Gets the needed chips in a given chipset.				//
	// Parameters:	* isMe - Specifies if the chips are needed by me or by	//
	//						 the opponent.									//
	//				* receivingChips - The chips that the player receives.	//
	//////////////////////////////////////////////////////////////////////////
	private ChipSet getNeededChipsInChipset(boolean isMe,
											ChipSet receivingChips)
	{
		// Calculate the actual needed chips
		ChipSet actualNeededChips = subChipSetsNoNegatives(ChipSet.addChipSets(this.m_BothNeededChips, (isMe ? this.m_MyNeededChips : this.m_OpponentNeededChips)), (isMe ? this.m_MyChips : this.m_OpponentChips));

		// Return the intersection
		return intersectChipSetsAndSubstract(new ChipSet(receivingChips), actualNeededChips);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getNumberOfNeededChips									//
	// Purpose:		Gets the number needed chips in a given chipset. The	//
	//				needed chips are the chips that are in the offer and	//
	//				the given player did not originally own.				//
	// Parameters:	* isMe - Specifies if the chips are needed by me or by	//
	//						 the opponent.									//
	//				* receivingChips - The chips that the player receives.	//
	//////////////////////////////////////////////////////////////////////////
	private int getNumberOfNeededChips(boolean isMe,
									   ChipSet receivingChips)
	{
		int result = 0;
		
		// If the player can reach the goal, 
		if (canReachToGoal(isMe))
		{
			return 0;
		}
		
		// Gets the needed colors of the given player
		Set<String> neededColors = (isMe ? s_MyOriginalNeededColors : s_OpponentOriginalNeededColors);
		
		// Get the needed colors
		for (String color : neededColors)
		{			
			result += Math.max(0, receivingChips.getNumChips(color));
		}
		
		// Return result
		return result;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getGenerosityLevelForWekaModel							//
	// Purpose:		Get the generosity level for a given proposal.			//
	//				This method is used by the Weka culture models.			//
	// Returns:		A double between 0.0 and 1.0.							//
	// Parameters:	* proposal - the given proposal.						//
	//	`			* isMe - whether it's me or the opponent.				//
	//////////////////////////////////////////////////////////////////////////
	private static double getGenerosityLevelForWekaModel(Proposal proposal,
														 boolean isMe)
	{
		// Calculate how many chips I send against how many chips I receive
		int diffrenceOfSend = getSendChipsFromProposal(proposal, true).getNumChips() - getSendChipsFromProposal(proposal, false).getNumChips();
		if (diffrenceOfSend == 0)
		{
			return 0.5;
		}
		if (diffrenceOfSend > 0)
		{
			return (isMe ? 0.0 : 1.0);
		}
		return (isMe ? 1.0 : 0.0);
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		getOpponentResponseChance								//
	// Purpose:		Gets the opponent's positive response chance.			//
	// Parameters:	* proposal - The proposal to consider.					//
	//////////////////////////////////////////////////////////////////////////
	private double getOpponentResponseChance(Proposal proposal)
	{
		s_TreeLogger.beginTag("OpponentResponseProbability");
		
		// Get the generosity level
		double generosityLevel = getGenerosityLevelForWekaModel(proposal, false);
		
		// Get scores
		double myCurrentScore = getCurrentScore(true);
		double opponentCurrentScore = getCurrentScore(false);
		double myResultingScore = getResultingScore(true, getSendChipsFromProposal(proposal, false), getSendChipsFromProposal(proposal, true));
		double opponentResultingScore = getResultingScore(false, getSendChipsFromProposal(proposal, true), getSendChipsFromProposal(proposal, false));

		// Get needed colors
		int myNeededColors = getNumberOfNeededChips(true, getSendChipsFromProposal(proposal, false));
		int opponentNeededColors = getNumberOfNeededChips(false, getSendChipsFromProposal(proposal, true));

		// Get other colors
		int myOtherColors = getSendChipsFromProposal(proposal, false).getNumChips() - myNeededColors;
		int opponentOtherColors = getSendChipsFromProposal(proposal, true).getNumChips() - opponentNeededColors;

		// Tree logging
		s_TreeLogger.addTag("Proposal", proposal);
		s_TreeLogger.addTag("MyCurrentScore", myCurrentScore);
		s_TreeLogger.addTag("MyResultingScore", myResultingScore);
		s_TreeLogger.addTag("OpponentCurrentScore", opponentCurrentScore);
		s_TreeLogger.addTag("OpponentResultingScore", opponentResultingScore);
		s_TreeLogger.addTag("GenerosityLevel", generosityLevel);
		s_TreeLogger.addTag("MyNeededColors", myNeededColors);
		s_TreeLogger.addTag("MyOtherColors", myOtherColors);
		s_TreeLogger.addTag("OpponentNeededColors", opponentNeededColors);
		s_TreeLogger.addTag("OpponentOtherColors", opponentOtherColors);
		
		// Build the instance values
		Object[] instanceValues = { myCurrentScore,
									myResultingScore,
									opponentCurrentScore,
									opponentResultingScore,
									generosityLevel,
									(double)myNeededColors,
									(double)myOtherColors,
									(double)opponentNeededColors,
									(double)opponentOtherColors,
									"TRUE" };
		
		// Get the accept probability from the Weka model
		double acceptChance;
		try
		{
			acceptChance = WekaWrapper.getAcceptProbability(instanceValues);
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			acceptChance = 0.5;
		}
		
		// Finish
		s_TreeLogger.addTag("AcceptProbability", acceptChance);
		s_TreeLogger.endTag("OpponentResponseProbability");
		return acceptChance;
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		getMyResponseChance										//
	// Purpose:		Gets my positive response chance.						//
	// Parameters:	* proposal - The proposal to consider.					//
	//////////////////////////////////////////////////////////////////////////
	private double getMyResponseChance(Proposal proposal)
	{		
		// Get the generosity level
		double generosityLevel = getGenerosityLevelForWekaModel(proposal, true);
		
		// Get scores
		double myCurrentScore = getCurrentScore(true);
		double opponentCurrentScore = getCurrentScore(false);
		double myResultingScore = getResultingScore(true, getSendChipsFromProposal(proposal, false), getSendChipsFromProposal(proposal, true));
		double opponentResultingScore = getResultingScore(false, getSendChipsFromProposal(proposal, true), getSendChipsFromProposal(proposal, false));

		// Get needed colors
		int myNeededColors = getNumberOfNeededChips(true, getSendChipsFromProposal(proposal, false));
		int opponentNeededColors = getNumberOfNeededChips(false, getSendChipsFromProposal(proposal, true));

		// Get other colors
		int myOtherColors = getSendChipsFromProposal(proposal, false).getNumChips() - myNeededColors;
		int opponentOtherColors = getSendChipsFromProposal(proposal, true).getNumChips() - opponentNeededColors;
		
		// Build the instance values
		Object[] instanceValues = { opponentCurrentScore,
									opponentResultingScore,
									myCurrentScore,
									myResultingScore,
									generosityLevel,
									(double)opponentNeededColors,
									(double)opponentOtherColors,
									(double)myNeededColors,
									(double)myOtherColors,
									"TRUE" };
		
		// Get the accept probability from the Weka model
		double acceptChance;
		try
		{
			acceptChance = WekaWrapper.getAcceptProbability(instanceValues);
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			acceptChance = 0.5;
		}
		
		// Finish
		return acceptChance;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		respond													//
	// Purpose:		Simulates a response to the given proposal.				//
	// Parameters:	* isFirstProposal - Whether it is the first proposal	//
	//									in the current game phase.			//
	//				* proposal - The proposal to consider.					//
	//////////////////////////////////////////////////////////////////////////
	private void respond(boolean isFirstProposal,
						 Proposal proposal)
	{
		boolean currentResponse = false;

		// Finish if the game is over
		if (this.m_GameOver)
		{
			return;
		}

		// Tree logging
		s_TreeLogger.beginTag("Response");
		s_TreeLogger.addTag("IsFirstProposal", isFirstProposal);
		s_TreeLogger.addTag("Proposal", proposal);

		// Understand who responses
		boolean isMyResponse = (isFirstProposal != this.m_FirstProposerIsMe);
		s_TreeLogger.addTag("IsMyResponse", isMyResponse);

		// Respond - uniformly for agent, by biased chance for opponent
		if (isMyResponse)
		{
			// TODO JBO
			//currentResponse = s_RandomGenerator.nextBoolean();
			double acceptChance = getMyResponseChance(proposal);
			currentResponse = (s_RandomGenerator.nextDouble() <= acceptChance);
			// END OF TODO JBO
		}
		else
		{
			double acceptChance = getOpponentResponseChance(proposal);
			s_TreeLogger.addTag("OpponentAcceptProbability", acceptChance);
			currentResponse = (s_RandomGenerator.nextDouble() <= acceptChance);
		}

		// Continue simulating
		s_TreeLogger.addTag("Response", currentResponse);
		if (currentResponse)
		{
			transfer(proposal);
		}
		else
		{
			if (isFirstProposal)
			{
				propose(false);
			}
			else
			{
				transfer(null);
			}
		}
		s_TreeLogger.endTag("Response");
	}

	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getRandomSubset											//
	// Purpose:		Gets a random subset from the given chipset.			//
	// Parameters:	* original - The original chipset.						//
	// Remarks:		* TODO: remove after moved to PURB rules.				//
	//////////////////////////////////////////////////////////////////////////
	private ChipSet getRandomSubset(ChipSet original)
	{
		ChipSet result = new ChipSet();
		
		// Iterate colors and add each color uniformly
		for (String color : original.getColors())
		{
			int numOfChips = original.getNumChips(color);
			if (numOfChips <= 0)
			{
				continue;
			}
			
			int toAdd = Math.min(s_RandomGenerator.nextInt(numOfChips + 1), s_MaxChipsInSimulatedTransfer - result.getNumChips());
			result.add(color, toAdd);
			if (result.getNumChips() >= s_MaxChipsInSimulatedTransfer)
			{
				break;
			}
		}
		
		// Return the result
		return result;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getChipsToSend											//
	// Purpose:		Get random chips to send.								//
	// Parameters:	* isMe - Whether it's me or the opponent.				//
	// Remarks:		* TODO: remove after moved to PURB rules.				//
	//////////////////////////////////////////////////////////////////////////
	private ChipSet getChipsToSend(boolean isMe)
	{
		ChipSet result = new ChipSet();
		
		// Get the needed chips and owned chips
		ChipSet playerOwnedChips = (isMe ? this.m_MyChips : this.m_OpponentChips);
		ChipSet playerNeededChips = ChipSet.addChipSets(this.m_BothNeededChips, (isMe ? this.m_MyNeededChips : this.m_OpponentNeededChips));
		ChipSet otherPlayerOwnedChips = (isMe ? this.m_OpponentChips : this.m_MyChips);
		ChipSet otherPlayerNeededChips = ChipSet.addChipSets(this.m_BothNeededChips, (isMe ? this.m_OpponentNeededChips : this.m_MyNeededChips));
		ChipSet otherPlayerMissingChips = subChipSetsNoNegatives(otherPlayerNeededChips, new ChipSet(otherPlayerOwnedChips));
		
		// Calculate the sendable chips
		ChipSet optionsToSend = subChipSetsNoNegatives(new ChipSet(playerOwnedChips), playerNeededChips);
		
		// Calculate the meaningful chips to send
		ChipSet otherChipsToSend = optionsToSend;
		ChipSet meaningfulChipsToSend = intersectChipSetsAndSubstract(otherChipsToSend, new ChipSet(otherPlayerMissingChips));
		
		// Select a random number of meaningful chips
		int maxNumberOfChipsToSend = s_RandomGenerator.nextInt(2) + 1;
		result = meaningfulChipsToSend.getSubset(maxNumberOfChipsToSend);
		if (result.getNumChips() < maxNumberOfChipsToSend)
		{
			result.addChipSet(otherChipsToSend.getSubset(maxNumberOfChipsToSend - result.getNumChips()));
		}
		
		// Return the result
		return result;
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		propose													//
	// Purpose:		Simulates a proposal.									//
	// Parameters:	* isFirstProposal - Whether it is the first proposal	//
	//									in the current game phase.			//
	//////////////////////////////////////////////////////////////////////////
	private void propose(boolean isFirstProposal)
	{
		Proposal currentProposal = new Proposal(new ChipSet(), new ChipSet());
		
		// Finish if the game is over
		if (this.m_GameOver)
		{
			return;
		}

		// Tree logging
		s_TreeLogger.beginTag("Propose");
		s_TreeLogger.addTag("IsFirstProposal", isFirstProposal);

		// Understand who responses
		boolean isMyProposal = (isFirstProposal == this.m_FirstProposerIsMe);
		s_TreeLogger.addTag("IsMyProposal", isMyProposal);

		// TODO - change to PURB's rules
		if (isMyProposal)
		{
			currentProposal.chipsToSend = getChipsToSend(true);
			currentProposal.chipsToReceive = getChipsToSend(false);
		}
		else
		{
			currentProposal.chipsToSend = getChipsToSend(true);
			currentProposal.chipsToReceive = getChipsToSend(false);
		}
		
		// Narrow down proposals
		for (String color : getSendChipsFromProposal(currentProposal, true).getColors())
		{
			int minNumber = Math.min(getSendChipsFromProposal(currentProposal, true).getNumChips(color), getSendChipsFromProposal(currentProposal, false).getNumChips(color));
			if ((minNumber == getSendChipsFromProposal(currentProposal, true).getNumChips()) && (minNumber == getSendChipsFromProposal(currentProposal, false).getNumChips()))
			{
				// Will cause zero-zero, so make some versus nothing
				getSendChipsFromProposal(currentProposal, true).add(color, -minNumber);
			}
			else
			{
				getSendChipsFromProposal(currentProposal, true).add(color, -minNumber);
				getSendChipsFromProposal(currentProposal, false).add(color, -minNumber);
			}
		}
		
		// END OF TODO

		// Continue simulating
		s_TreeLogger.addTag("Proposal", currentProposal);
		respond(isFirstProposal, currentProposal);
		s_TreeLogger.endTag("Propose");		
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getOpponentSendChips									//
	// Purpose:		Gets the opponent's simulated chips for a transfer.		//
	// Parameters:	* lastAcceptedProposal - The last accepted proposal.	//
	//										 This value is NULL if no		//
	//										 proposal was accepted.			//
	//////////////////////////////////////////////////////////////////////////
	private ChipSet getOpponentSendChips(Proposal lastAcceptedProposal)
	{
		ChipSet opponentSendChips = new ChipSet();
		s_TreeLogger.beginTag("OpponentSendChips");
		
		// Get reliabilities
		double myReliability = this.m_MyReliabilitySum / this.m_RoundsSoFar;
		double opponentReliability = this.m_OpponentReliabilitySum / this.m_RoundsSoFar;
		
		// Get scores
		double myCurrentScore = getCurrentScore(true);
		double opponentCurrentScore = getCurrentScore(false);
		double myResultingScore = getResultingScore(true, getSendChipsFromProposal(lastAcceptedProposal, false), getSendChipsFromProposal(lastAcceptedProposal, true));
		double opponentResultingScore = getResultingScore(false, getSendChipsFromProposal(lastAcceptedProposal, true), getSendChipsFromProposal(lastAcceptedProposal, false));

		// Get needed colors
		int myNeededColors = getNumberOfNeededChips(true, getSendChipsFromProposal(lastAcceptedProposal, false));
		int opponentNeededColors = getNumberOfNeededChips(false, getSendChipsFromProposal(lastAcceptedProposal, true));

		// Get other colors
		int myOtherColors = getSendChipsFromProposal(lastAcceptedProposal, false).getNumChips() - myNeededColors;
		int opponentOtherColors = getSendChipsFromProposal(lastAcceptedProposal, true).getNumChips() - opponentNeededColors;

		// Tree logging
		s_TreeLogger.addTag("Proposal", lastAcceptedProposal);
		s_TreeLogger.addTag("MyReliability", myReliability);
		s_TreeLogger.addTag("OpponentReliability", opponentReliability);
		s_TreeLogger.addTag("MyCurrentScore", myCurrentScore);
		s_TreeLogger.addTag("MyResultingScore", myResultingScore);
		s_TreeLogger.addTag("OpponentCurrentScore", opponentCurrentScore);
		s_TreeLogger.addTag("OpponentResultingScore", opponentResultingScore);
		s_TreeLogger.addTag("MyNeededColors", myNeededColors);
		s_TreeLogger.addTag("MyOtherColors", myOtherColors);
		s_TreeLogger.addTag("OpponentNeededColors", opponentNeededColors);
		s_TreeLogger.addTag("OpponentOtherColors", opponentOtherColors);
		
		// Get from the Weka wrapper
		double[] probabilitiesFulfill = { 0 };
		Object[] instanceValuesFulfill = { myReliability,
										   opponentReliability,
										   myCurrentScore,
										   myResultingScore,
										   opponentCurrentScore,
										   opponentResultingScore,
										   (double)myNeededColors,
										   (double)myOtherColors,
										   (double)opponentNeededColors,
										   (double)opponentOtherColors,
										   "1" };
		try
		{
			probabilitiesFulfill = WekaWrapper.getTransferProbabilities(instanceValuesFulfill);
			s_TreeLogger.addTag("ProbabilityForFulfillingProposal", probabilitiesFulfill[1]);
			if (s_RandomGenerator.nextDouble() <= probabilitiesFulfill[1])
			{
				opponentSendChips = new ChipSet(getSendChipsFromProposal(lastAcceptedProposal, false));
			}
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
		}
		
		// Return the send chips
		s_TreeLogger.addTag("OpponentSendChips", opponentSendChips);
		s_TreeLogger.endTag("OpponentSendChips");
		return opponentSendChips;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getMySendChips											//
	// Purpose:		Gets my simulated chips for a transfer.					//
	// Parameters:	* lastAcceptedProposal - The last accepted proposal.	//
	//										 This value is NULL if no		//
	//										 proposal was accepted.			//
	//////////////////////////////////////////////////////////////////////////
	private ChipSet getMySendChips(Proposal lastAcceptedProposal)
	{
		ChipSet mySendChips = new ChipSet();
		
		// Get reliabilities
		double myReliability = this.m_MyReliabilitySum / this.m_RoundsSoFar;
		double opponentReliability = this.m_OpponentReliabilitySum / this.m_RoundsSoFar;
		
		// Get scores
		double myCurrentScore = getCurrentScore(true);
		double opponentCurrentScore = getCurrentScore(false);
		double myResultingScore = getResultingScore(true, getSendChipsFromProposal(lastAcceptedProposal, false), getSendChipsFromProposal(lastAcceptedProposal, true));
		double opponentResultingScore = getResultingScore(false, getSendChipsFromProposal(lastAcceptedProposal, true), getSendChipsFromProposal(lastAcceptedProposal, false));

		// Get needed colors
		int myNeededColors = getNumberOfNeededChips(true, getSendChipsFromProposal(lastAcceptedProposal, false));
		int opponentNeededColors = getNumberOfNeededChips(false, getSendChipsFromProposal(lastAcceptedProposal, true));

		// Get other colors
		int myOtherColors = getSendChipsFromProposal(lastAcceptedProposal, false).getNumChips() - myNeededColors;
		int opponentOtherColors = getSendChipsFromProposal(lastAcceptedProposal, true).getNumChips() - opponentNeededColors;

		// Get from the Weka wrapper
		double[] probabilitiesFulfill = { 0 };
		Object[] instanceValuesFulfill = { opponentReliability,
										   myReliability,
										   opponentCurrentScore,
										   opponentResultingScore,
										   myCurrentScore,
										   myResultingScore,
										   (double)opponentNeededColors,
										   (double)opponentOtherColors,
										   (double)myNeededColors,
										   (double)myOtherColors,
										   "1" };
		try
		{
			probabilitiesFulfill = WekaWrapper.getTransferProbabilities(instanceValuesFulfill);
			s_TreeLogger.addTag("ProbabilityForFulfillingProposal", probabilitiesFulfill[1]);
			if (s_RandomGenerator.nextDouble() <= probabilitiesFulfill[1])
			{
				mySendChips = new ChipSet(getSendChipsFromProposal(lastAcceptedProposal, true));
			}
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
		}
		
		// Return the send chips
		return mySendChips;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getRoundReliability										//
	// Purpose:		Return the round reliability of the given player.		//
	// Parameters:	* isMe - Whether it's my reliability of the opponent's.	//
	//				* lastAcceptedProposal - The last accepted proposal.	//
	//				* chipsSentToOther - The chips that were sent to the	//
	//									 other player.						//
	//////////////////////////////////////////////////////////////////////////
	private double getRoundReliability(boolean isMe,
									   Proposal lastAcceptedProposal,
									   ChipSet chipsSentToOther)
	{
		// Get the chips that were ment to be sent to the other player
		ChipSet chipsMentToBeSentToOther = getSendChipsFromProposal(lastAcceptedProposal, isMe);
		
		// Calculate scores
		double fullTransferScore = getResultingScore(!isMe, chipsMentToBeSentToOther, new ChipSet());
		double actualScore = getResultingScore(!isMe, chipsSentToOther, new ChipSet());
		double currentScore = getCurrentScore(!isMe);
		
		// Calculates benifits
		double fullBenifit = fullTransferScore - currentScore;
		double actualBenifit = actualScore - currentScore;
		
		// Calculate the results
		if (0.0 == fullBenifit)
		{
			return 1.0;
		}
		return actualBenifit / fullBenifit;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getWeightedReliability									//
	// Purpose:		Gets the weighted reliability.							//
	// Parameters:	* sumSoFar - The sum of reliabilities so far.			//
	//				* roundReliability - The new round's reliability.		//
	//////////////////////////////////////////////////////////////////////////
	private double getWeightedReliability(double sumSoFar,
										  double roundReliability)
	{
		// Calculate the weights
		double historyWeight = (1.0 - s_LastRoundReliabilityWeight);
		
		// Calculate the reliability history
		double reliabilityHistory = sumSoFar / this.m_RoundsSoFar;
		
		// Calculate the weighted reliability
		return (historyWeight * reliabilityHistory + s_LastRoundReliabilityWeight * roundReliability);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		doTransfer												//
	// Purpose:		Does the actual transfer of chips.						//
	// Parameters:	* lastAcceptedProposal - The last accepted proposal.	//
	//				* chipsSentByMe - Chips sent by me.						//
	//				* chipsSenyByOpponent - Chips sent by the opponent.		//
	//////////////////////////////////////////////////////////////////////////
	private void doTransfer(Proposal lastAcceptedProposal,
							ChipSet chipsSentByMe,
							ChipSet chipsSentByOpponent)
	{
		s_TreeLogger.beginTag("ChipTransfer");
		
		// Tree logging
		s_TreeLogger.addTag("MyOldReliability", this.m_MyReliabilitySum / this.m_RoundsSoFar);
		s_TreeLogger.addTag("OpponentOldReliability", this.m_OpponentReliabilitySum / this.m_RoundsSoFar);
		s_TreeLogger.addTag("LastAcceptedProposal", lastAcceptedProposal);
		s_TreeLogger.addTag("ChipsSentByMe", chipsSentByMe);
		s_TreeLogger.addTag("ChipsSentByOpponent", chipsSentByOpponent);
		
		// Calculate the new chips
		ChipSet myNewChips = ChipSet.subChipSets(ChipSet.addChipSets(this.m_MyChips, chipsSentByOpponent), chipsSentByMe);
		ChipSet opponentNewChips = ChipSet.subChipSets(ChipSet.addChipSets(this.m_OpponentChips, chipsSentByMe), chipsSentByOpponent);
		
		// Calculate new reliabilities
		double myRoundReliability = getRoundReliability(true, lastAcceptedProposal, chipsSentByMe);
		double opponentRoundReliability = getRoundReliability(false, lastAcceptedProposal, chipsSentByOpponent);
		s_TreeLogger.addTag("MyRoundReliability", myRoundReliability);
		s_TreeLogger.addTag("OpponentRoundReliability", opponentRoundReliability);
		
		// Update the reliabilities sum
		int roundsSoFar = this.m_RoundsSoFar + 1;
		this.m_MyReliabilitySum = getWeightedReliability(this.m_MyReliabilitySum, myRoundReliability) * roundsSoFar;
		this.m_OpponentReliabilitySum = getWeightedReliability(this.m_OpponentReliabilitySum, opponentRoundReliability) * roundsSoFar;
		this.m_RoundsSoFar = roundsSoFar;

		// Update chips
		this.m_MyChips = myNewChips;
		this.m_OpponentChips = opponentNewChips;
		
		// Tree logging
		s_TreeLogger.addTag("MyNewReliability", this.m_MyReliabilitySum / this.m_RoundsSoFar);
		s_TreeLogger.addTag("OpponentNewReliability", this.m_OpponentReliabilitySum / this.m_RoundsSoFar);
		s_TreeLogger.endTag("ChipTransfer");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getOptionsToSendChips									//
	// Purpose:		Gets the options to send chips.							//
	// Parameters:	* fullTransferChips - The chips for a full transfer.	//
	//////////////////////////////////////////////////////////////////////////
	private Set<ChipSet> getOptionsToSendChips(ChipSet fullTransferChips)
	{
		Set<ChipSet> result = new LinkedHashSet();
		
		// Get my and the opponent's needed chips
		ChipSet myNeededChips = getNeededChipsInChipset(true, fullTransferChips);
		ChipSet opponentNeededChips = getNeededChipsInChipset(false, fullTransferChips);
		
		// The send options are those that are not in my needed chips and are in the opponent's
		ChipSet sendOptions = subChipSetsNoNegatives(opponentNeededChips, myNeededChips);
		
		// Take representatives for each option
		for (int toTake = 0; toTake <= s_MaxChipsInTransfer; toTake++)
		{
			// Break if you cannot take more chips
			if (sendOptions.getNumChips() < toTake)
			{
				break;
			}
			
			// Get that amount
			ChipSet currentOption = sendOptions.getSubset(toTake);
			result.add(currentOption);
		}
		
		// Add the empty chip set and the full exchange as well
		result.add(new ChipSet());
		boolean isFullInResult = false;
		for (ChipSet resultChips : result)
		{
			if (resultChips.equals(fullTransferChips))
			{
				isFullInResult = true;
				break;
			}
		}
		if (!isFullInResult)
		{
			result.add(new ChipSet(fullTransferChips));
		}
		
		// Return the result
		return result;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		transfer												//
	// Purpose:		Simulates a transfer.									//
	// Parameters:	* lastAcceptedProposal - The last accepted proposal.	//
	//										 This value is NULL if no		//
	//										 proposal was accepted.			//
	//////////////////////////////////////////////////////////////////////////
	private void transfer(Proposal lastAcceptedProposal)
	{	
		// Finish if the game is over
		if (this.m_GameOver)
		{
			return;
		}

		// Tree logging
		s_TreeLogger.beginTag("Transfer");

		// Do something only if there is a last-accepted proposal
		if (null != lastAcceptedProposal)
		{
			// Choose what I transfer
			// TODO JBO
			//Set<ChipSet> sendOptions = getOptionsToSendChips(getSendChipsFromProposal(lastAcceptedProposal, true));
			//Object allSendOptionsArray[] = sendOptions.toArray();
			//int randomIndex = s_RandomGenerator.nextInt(sendOptions.size());
			//ChipSet mySendChips = (ChipSet)(allSendOptionsArray[randomIndex]);
			ChipSet mySendChips = getMySendChips(lastAcceptedProposal);
			s_TreeLogger.addTag("MySendChips", mySendChips);
			// END OF TODO JBO
			
			// Choose what the opponent transfers
			s_TreeLogger.addTag("LastAcceptedProposal", lastAcceptedProposal);
			ChipSet opponentSendChips = getOpponentSendChips(lastAcceptedProposal);
			s_TreeLogger.addTag("OpponentSendChips", opponentSendChips);
			
			// Do the actual transfer
			doTransfer(lastAcceptedProposal, mySendChips, opponentSendChips);	
		}
		
		// Continue simulating
		move();
		s_TreeLogger.endTag("Transfer");
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		canReachToGoal											//
	// Purpose:		Specifies whether the given player can reach the goal.	//
	// Parameters:	* isMe - Specifies whether it's me or the opponent.		//
	//////////////////////////////////////////////////////////////////////////
	private boolean canReachToGoal(boolean isMe)
	{
		// Get the chips and position
		ChipSet playerChips = (isMe ? this.m_MyChips : this.m_OpponentChips);
		RowCol playerPosition = (isMe ? this.m_MyPosition : this.m_OpponentPosition);
		
		// Get the relevant path
		ArrayList<Path> paths = ShortestPaths.getShortestPaths(playerPosition,
															   this.m_GoalPosition,
															   this.m_Board,
															   this.m_Scoring,
															   1,
															   playerChips,
															   new ChipSet());
		
		// If the player can reach the goal then there is at least one path
		return (paths.size() > 0);
	}
	
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		tryToMoveToGoal		      					    		//
	// Purpose:		Tries the move towards the goal.						//
	// Parameters:	* isMe - specifies whether it's me or the opponent.		//
	//////////////////////////////////////////////////////////////////////////	
	private void tryToMoveToGoal(boolean isMe)
	{
		RowCol currentPosition = (isMe ? this.m_MyPosition : this.m_OpponentPosition);
		ChipSet currentChips = (isMe ? this.m_MyChips : this.m_OpponentChips);
		
		// Tree log
		s_TreeLogger.beginTag("TryToMoveToGoal");
		s_TreeLogger.addTag("CurrentPosition", currentPosition);
		
		// Get the relevant path
		ArrayList<Path> paths = ShortestPaths.getShortestPaths(currentPosition,
															   this.m_GoalPosition,
															   this.m_Board,
															   this.m_Scoring,
															   1,
															   this.m_MyChips,
															   this.m_OpponentChips);
		if (paths.size() == 0)
		{
			s_TreeLogger.addTag("NewPosition", currentPosition);
			increaseDormantSteps(isMe);
		}
		else
		{
			// Get the next point and the required color
			RowCol pointToMove = paths.get(0).getPoint(1);
			String neededColor = this.m_Board.getSquare(pointToMove).getColor();
			
			// Check if the color is in the player's posession
			if (currentChips.getNumChips(neededColor) == 0)
			{
				s_TreeLogger.addTag("NewPosition", currentPosition);
				increaseDormantSteps(isMe);
			}
			else
			{
				// It's possible to move
				s_TreeLogger.addTag("NewPosition", pointToMove);
				if (isMe)
				{
					this.m_MyPosition = pointToMove;
					this.m_MyChips.add(neededColor, -1);
					this.m_MyDormantSteps = 0;
				}
				else
				{
					this.m_OpponentPosition = pointToMove;
					this.m_OpponentChips.add(neededColor, -1);
					this.m_OpponentDormantSteps = 0;
				}
			}
		}
		
		// Finish
		s_TreeLogger.endTag("TryToMoveToGoal");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		increaseDormantSteps      					    		//
	// Purpose:		Increases the number of dormant steps.					//
	// Parameters:	* isMe - specifies whether it's me or the opponent.		//
	//////////////////////////////////////////////////////////////////////////	
	private void increaseDormantSteps(boolean isMe)
	{
		// Just increase the dormant steps
		if (isMe)
		{
			this.m_MyDormantSteps++;
		}
		else
		{
			this.m_OpponentDormantSteps++;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		checkForGameEnding      					    		//
	// Purpose:		Checks if the game has finished and marks it in the		//
	//				member which specifies that the game is over.			//
	//////////////////////////////////////////////////////////////////////////	
	private void checkForGameEnding()
	{		
		// Check for dormant steps
		if (Math.max(this.m_MyDormantSteps, this.m_OpponentDormantSteps) >= s_MaxDormantSteps)
		{
			s_TreeLogger.addTag("GameEndingReason", "DormantSteps");
			this.m_GameOver = true;
		}
		
		// Check if someone reached the goal
		if ((this.m_MyPosition.equals(this.m_GoalPosition)) ||
			(this.m_OpponentPosition.equals(this.m_GoalPosition)))
		{
			s_TreeLogger.addTag("GameEndingReason", "SomeoneReachedTheGoal");
			this.m_GameOver = true;
		}
		
		// Check if both players can reach the goal
		if ((canReachToGoal(true)) && (canReachToGoal(false)))
		{
			s_TreeLogger.addTag("GameEndingReason", "BothPlayersCanReachTheGoal");
			this.m_GameOver = true;
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		move													//
	// Purpose:		Simulates moves of both players.						//
	//////////////////////////////////////////////////////////////////////////
	private void move()
	{		
		// Finish if the game is over
		if (this.m_GameOver)
		{
			return;
		}
	
		// Game logging
		s_TreeLogger.beginTag("Move");
		
		// Moves both sides if possible and if the dormant steps cause game ending
		if (this.m_MyDormantSteps >= s_MaxDormantSteps - 1)
		{
			s_TreeLogger.beginTag("MyMove");
			tryToMoveToGoal(true);
			s_TreeLogger.endTag("MyMove");
		}
		else
		{
			increaseDormantSteps(true);
		}
		if (this.m_OpponentDormantSteps >= s_MaxDormantSteps - 1)
		{
			s_TreeLogger.beginTag("OpponentMove");
			tryToMoveToGoal(false);
			s_TreeLogger.endTag("OpponentMove");
		}
		else
		{
			increaseDormantSteps(false);
		}
		
		// Switch sides of proposers
		this.m_FirstProposerIsMe = !(this.m_FirstProposerIsMe);	
		
		// Check for game ending
		checkForGameEnding();
		
		// Propose
		propose(true);
		s_TreeLogger.endTag("Move");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		treePolicy												//
	// Purpose:		Implements the tree policy.								//
	// Parameters:	* allOptions - All of the options to explore.			//
	//				* directNodesData - The collected data so far.			//
	//				* exploredIterations - The number of iterations so far. //
	//////////////////////////////////////////////////////////////////////////
	private static <T> T treePolicy(Set<T> allOptions,
							 		Map<T, DirectNodeData> directNodesData,
							 		int exploredIterations)
	{
		double bestValue = 0.0;
		double treePolicyExplorationFactor = 1.0 / Math.sqrt(2.0);
		T bestObject = null;
		
		// Look for a node not explored yet
		if (allOptions.size() > directNodesData.size())
		{
			for (T key : allOptions)
			{
				if (!directNodesData.containsKey(key))
				{
					return key;
				}
			}
		}
		
		// Find the best value for the policy
		for (T key : allOptions)
		{
			int keyOccurences = directNodesData.get(key).m_NumberOfOccurences;
			double explorationValue = (2.0 * treePolicyExplorationFactor) * Math.sqrt((2.0 * Math.log(exploredIterations)) / keyOccurences);
			double expectationValue = directNodesData.get(key).m_SumOfNormalizedScores / keyOccurences;
			double currentValue = expectationValue + explorationValue;
			if ((null == bestObject) || (bestValue < currentValue))
			{
				bestValue = currentValue;
				bestObject = key;
			}
		}
		
		// Return the best object
		return bestObject;
	}
	
	
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getSendableChips										//
	// Purpose:		Gets all of sensable chips of the given player.			//
	// Parameters:	* isMe - Specifies whether it's me or the opponent.		//
	//////////////////////////////////////////////////////////////////////////
	private ChipSet getSendableChips(boolean isMe)
	{
		// Get the needed chips of the player
		ChipSet neededChips = ChipSet.addChipSets(this.m_BothNeededChips, (isMe ? this.m_MyNeededChips : this.m_OpponentNeededChips));
		
		// The sendable chips are those that the player has and don't need
		return subChipSetsNoNegatives((isMe ? this.m_MyChips : this.m_OpponentChips), neededChips);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getAllProposalsForSampling								//
	// Purpose:		Gets all of the proposals for sampling.					//
	//////////////////////////////////////////////////////////////////////////	
	private Set<Proposal> getAllProposalsForSampling()
	{
		s_TreeLogger.beginTag("ProposalsForSampling");
		
		// Define the result and the equivalent class pairs
		Set<Proposal> result = new LinkedHashSet();
		Set<EquivalenceClassPair> equivalentProposals = new LinkedHashSet();		
		
		// Get the sendable chips
		ChipSet sendableByMe = getSendableChips(true);
		ChipSet sendableByOpponent = getSendableChips(false);
				
		// Get all the relevant chip sets
		Set<ChipSet> allSends = ChipSet.getUniquePowerSet(sendableByMe);
		Set<ChipSet> allReceives = ChipSet.getUniquePowerSet(sendableByOpponent);
				
		// Build all relevant proposals
		for (ChipSet sendChips : allSends)
		{
			for (ChipSet receiveChips : allReceives)
			{	
				// Reject the empty sets
				if ((0 == sendChips.getNumChips()) || (0 == receiveChips.getNumChips()))
				{
					continue;
				}

				// Get the equivalence class for sending and receiving
				EquivalenceClass sendEq = new EquivalenceClass(sendChips);
				EquivalenceClass receiveEq = new EquivalenceClass(receiveChips);
				EquivalenceClassPair eqPair = new EquivalenceClassPair(sendEq, receiveEq);
				
				// Reject the proposal if the classes are not narrowed
				if (!eqPair.isNarrowed())
				{
					continue;
				}
						
				// Reject the proposal if it has too many chips
				if ((s_MaxChipsInSimulatedTransfer < sendChips.getNumChips()) || (s_MaxChipsInSimulatedTransfer < receiveChips.getNumChips()))
				{
					continue;
				}
				
				// Now we can add the proposal
				equivalentProposals.add(eqPair);
			}
		}
		
		// The result is defined by the original proposals
		s_TreeLogger.beginTag("AllProposalsForSampling");
		for (EquivalenceClassPair eqPair : equivalentProposals)
		{
			result.add(eqPair.toProposal());
			s_TreeLogger.addTag("Proposal", eqPair.toProposal());
		}
		s_TreeLogger.endTag("AllProposalsForSampling");

		// Return the result
		s_TreeLogger.endTag("ProposalsForSampling");
		return result;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		sampleProposals											//
	// Purpose:		Samples proposals.										//
	// Parameters:	* isFirstProposal - Whether it is the first proposal	//
	//									in the current game phase.			//
	//				* timeLimitSeconds - the time limit, in seconds.		//
	//////////////////////////////////////////////////////////////////////////
	public Proposal sampleProposals(boolean isFirstProposal,
								    long timeLimitSeconds)
	{
		Proposal bestProposal = new Proposal(new ChipSet(), new ChipSet());
		double bestAverage = 0.0;
		boolean bestScoreCalculated = false;
		Map<Proposal, DirectNodeData> directNodesData = new HashMap();
		long startTime = System.currentTimeMillis();

		// Tree logging
		s_TreeLogger.startNew();
		s_SamplesLogger.beginTag("ProposalsSampling");
		s_SamplesLogger.addTag("FirstProposalInPhase", isFirstProposal);
		s_TreeLogger.beginTag("ProposalsSampling");
		s_TreeLogger.addTag("FirstProposalInPhase", isFirstProposal);
		logStateToTreeLogger();
		
		// Get all proposals
		Set<Proposal> allProposals = getAllProposalsForSampling();

		// Start to sample
		s_TreeLogger.beginTag("Sampling");
		for (int counter = 0; counter < s_BranchingFactorForProposals; counter++)
		{
			// Optionally break out of the loop
			long currentTime = System.currentTimeMillis();
			if (currentTime - startTime >= timeLimitSeconds * 1000)
			{
				break;
			}
			
			// Get the current proposal from the tree policy
			Proposal currentProposal = (Proposal)treePolicy(allProposals, directNodesData, counter);

			// Create the simulation
			GameSimulator currentSimulation = new GameSimulator(this);

			// Simulate
			s_TreeLogger.beginTag("Proposal");
			s_TreeLogger.addTag("Proposal", currentProposal);
			s_TreeLogger.beginTag("Simulation");
			currentSimulation.respond(isFirstProposal, currentProposal);
			s_TreeLogger.addTag("Score", currentSimulation.getScore());
			s_TreeLogger.endTag("Simulation");
			s_TreeLogger.endTag("Proposal");

			// Add to direct nodes data
			if (directNodesData.containsKey(currentProposal))
			{
				DirectNodeData gatheredData = (DirectNodeData)(directNodesData.get(currentProposal));
				gatheredData.addSimulation(currentSimulation.getScore(), currentSimulation.getNormalizedScore());
				directNodesData.put(currentProposal, gatheredData);
			}
			else
			{
				DirectNodeData gatheredData = new DirectNodeData(currentSimulation.getScore(), currentSimulation.getNormalizedScore());
				directNodesData.put(currentProposal, gatheredData);
			}
		}
		s_TreeLogger.endTag("Sampling");
		
		// Find the best simulation
		for (Map.Entry<Proposal, DirectNodeData> entry : directNodesData.entrySet())
		{
			// Get the gathered data
			DirectNodeData gatheredData = entry.getValue();
			double currentAverage = gatheredData.m_SumOfScores / gatheredData.m_NumberOfOccurences;
			s_TreeLogger.beginTag("SimulationSummary");
			s_TreeLogger.addTag("Proposal", entry.getKey());
			s_TreeLogger.addTag("Occurences",  gatheredData.m_NumberOfOccurences);
			s_TreeLogger.addTag("Average", currentAverage);
			s_TreeLogger.endTag("SimulationSummary");
			s_SamplesLogger.beginTag("SimulationSummary");
			s_SamplesLogger.addTag("Proposal", entry.getKey());
			s_SamplesLogger.addTag("Occurences",  gatheredData.m_NumberOfOccurences);
			s_SamplesLogger.addTag("Average", currentAverage);
			s_SamplesLogger.endTag("SimulationSummary");
			
			// Maximize average
			if ((!bestScoreCalculated) || (currentAverage > bestAverage))
			{
				bestAverage = currentAverage;
				bestProposal = new Proposal(entry.getKey());
				bestScoreCalculated = true;
			}
		}

		// Tree logging
		s_TreeLogger.addTag("BestProposal", bestProposal);
		s_TreeLogger.addTag("BestAverageScore", bestAverage);
		s_TreeLogger.endTag("ProposalsSampling");
		s_TreeLogger.flush();
		s_SamplesLogger.addTag("BestProposal", bestProposal);
		s_SamplesLogger.addTag("BestAverageScore", bestAverage);
		s_SamplesLogger.endTag("ProposalsSampling");
		s_SamplesLogger.flush();
		return bestProposal;
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		sampleResponses											//
	// Purpose:		Samples responses.										//
	// Parameters:	* isFirstProposal - Whether it is the first proposal	//
	//									in the current game phase.			//
	//				* proposal - The proposal to consider.					//
	//				* timeLimitSeconds - the time limit, in seconds.		//
	//////////////////////////////////////////////////////////////////////////
	public boolean sampleResponses(boolean isFirstProposal,
								   Proposal proposal,
								   long timeLimitSeconds)
	{
		Map<Boolean, DirectNodeData> directNodesData = new HashMap();
		boolean bestResponse = false;
		double bestAverage = 0.0;
		boolean bestScoreCalculated = false;
		long startTime = System.currentTimeMillis();
		
		// Tree logging
		s_TreeLogger.startNew();
		s_SamplesLogger.beginTag("ResponsesSampling");
		s_SamplesLogger.addTag("FirstProposalInPhase", isFirstProposal);
		s_SamplesLogger.addTag("ProposalToConsider", proposal);
		s_TreeLogger.beginTag("ResponsesSampling");
		s_TreeLogger.addTag("FirstProposalInPhase", isFirstProposal);
		s_TreeLogger.addTag("ProposalToConsider", proposal);
		logStateToTreeLogger();

		// Get all responses
		Set<Boolean> allResponses = new HashSet();
		allResponses.add(new Boolean(false));
		allResponses.add(new Boolean(true));

		// Start to sample
		s_TreeLogger.beginTag("Sampling");
		for (int counter = 0; counter < s_BranchingFactorForResponses; counter++)
		{
			// Optionally break out of the loop
			long currentTime = System.currentTimeMillis();
			if (currentTime - startTime >= timeLimitSeconds * 1000)
			{
				break;
			}
			
			// Get the current response from the tree policy
			boolean currentResponse = (Boolean)treePolicy(allResponses, directNodesData, counter);

			// Create the simulation
			GameSimulator currentSimulation = new GameSimulator(this);

			// Simulate
			s_TreeLogger.beginTag("Response");
			s_TreeLogger.addTag("Response", currentResponse);
			s_TreeLogger.beginTag("Simulation");
			if (currentResponse)
			{
				currentSimulation.transfer(proposal);
			}
			else
			{
				if (isFirstProposal)
				{
					currentSimulation.propose(false);
				}
				else
				{
					currentSimulation.transfer(null);
				}
			}
			s_TreeLogger.addTag("Score", currentSimulation.getScore());
			s_TreeLogger.endTag("Simulation");
			s_TreeLogger.endTag("Response");

			// Add to direct nodes data
			if (directNodesData.containsKey(currentResponse))
			{
				DirectNodeData gatheredData = (DirectNodeData)(directNodesData.get(currentResponse));
				gatheredData.addSimulation(currentSimulation.getScore(), currentSimulation.getNormalizedScore());
				directNodesData.put(currentResponse, gatheredData);
			}
			else
			{
				DirectNodeData gatheredData = new DirectNodeData(currentSimulation.getScore(), currentSimulation.getNormalizedScore());
				directNodesData.put(currentResponse, gatheredData);
			}
		}
		s_TreeLogger.endTag("Sampling");
		
		// Find the best simulation
		for (Map.Entry<Boolean, DirectNodeData> entry : directNodesData.entrySet())
		{
			// Get the gathered data
			DirectNodeData gatheredData = entry.getValue();
			double currentAverage = gatheredData.m_SumOfScores / gatheredData.m_NumberOfOccurences;
			s_TreeLogger.beginTag("SimulationSummary");
			s_TreeLogger.addTag("Response", entry.getKey());
			s_TreeLogger.addTag("Occurences",  gatheredData.m_NumberOfOccurences);
			s_TreeLogger.addTag("Average", currentAverage);
			s_TreeLogger.endTag("SimulationSummary");
			s_SamplesLogger.beginTag("SimulationSummary");
			s_SamplesLogger.addTag("Response", entry.getKey());
			s_SamplesLogger.addTag("Occurences",  gatheredData.m_NumberOfOccurences);
			s_SamplesLogger.addTag("Average", currentAverage);
			s_SamplesLogger.endTag("SimulationSummary");
			
			// Maximize average
			if ((!bestScoreCalculated) || (currentAverage > bestAverage))
			{
				bestAverage = currentAverage;
				bestResponse = new Boolean(entry.getKey());
				bestScoreCalculated = true;
			}
		}

		// Tree logging
		s_TreeLogger.addTag("BestResponse", bestResponse);
		s_TreeLogger.addTag("BestAverageScore", bestAverage);
		s_TreeLogger.endTag("ResponsesSampling");
		s_TreeLogger.flush();
		s_SamplesLogger.endTag("ResponsesSampling");
		s_SamplesLogger.addTag("BestAverageScore", bestAverage);
		s_SamplesLogger.endTag("ResponsesSampling");
		s_SamplesLogger.flush();
		return bestResponse;
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		SampleTransfers											//
	// Purpose:		Samples transfers.										//
	// Parameters:	* lastAcceptedProposal - the last accepted proposal.	//
	//				* timeLimitSeconds - the time limit, in seconds.		//
	//////////////////////////////////////////////////////////////////////////
	public ChipSet SampleTransfers(Proposal lastAcceptedProposal,
								   long timeLimitSeconds)
	{
		ChipSet bestTransfer = new ChipSet();
		double bestAverage = 0.0;
		boolean bestScoreCalculated = false;
		Map<ChipSet, DirectNodeData> directNodesData = new HashMap();
		long startTime = System.currentTimeMillis();

		// Tree logging
		s_TreeLogger.startNew();
		s_SamplesLogger.beginTag("TransfersSampling");
		s_SamplesLogger.addTag("LastAcceptedProposal", lastAcceptedProposal);
		s_TreeLogger.beginTag("TransfersSampling");
		s_TreeLogger.addTag("LastAcceptedProposal", lastAcceptedProposal);
		logStateToTreeLogger();

		// Get all transfers
		Set<ChipSet> allTransfers = getOptionsToSendChips(getSendChipsFromProposal(lastAcceptedProposal, true));

		// Start to sample
		s_TreeLogger.beginTag("Sampling");
		for (int counter = 0; counter < s_BranchingFactorForTransfers; counter++)
		{
			// Optionally break out of the loop
			long currentTime = System.currentTimeMillis();
			if (currentTime - startTime >= timeLimitSeconds * 1000)
			{
				break;
			}
			
			// Get the current transfer from the tree policy
			ChipSet currentTransfer = (ChipSet)treePolicy(allTransfers, directNodesData, counter);
			
			// Get the opponent's sent chips
			ChipSet opponentTransfer = getOpponentSendChips(lastAcceptedProposal);

			// Create the simulation
			GameSimulator currentSimulation = new GameSimulator(this);

			// Simulate
			s_TreeLogger.beginTag("Transfer");
			s_TreeLogger.addTag("Transfer", currentTransfer);
			s_TreeLogger.beginTag("Simulation");
			currentSimulation.doTransfer(lastAcceptedProposal, currentTransfer, opponentTransfer);
			currentSimulation.move();
			s_TreeLogger.addTag("Score", currentSimulation.getScore());
			s_TreeLogger.endTag("Simulation");
			s_TreeLogger.endTag("Transfer");

			// Add to direct nodes data
			if (directNodesData.containsKey(currentTransfer))
			{
				DirectNodeData gatheredData = (DirectNodeData)(directNodesData.get(currentTransfer));
				gatheredData.addSimulation(currentSimulation.getScore(), currentSimulation.getNormalizedScore());
				directNodesData.put(currentTransfer, gatheredData);
			}
			else
			{
				DirectNodeData gatheredData = new DirectNodeData(currentSimulation.getScore(), currentSimulation.getNormalizedScore());
				directNodesData.put(currentTransfer, gatheredData);
			}
		}
		s_TreeLogger.endTag("Sampling");
		
		// Find the best simulation
		for (Map.Entry<ChipSet, DirectNodeData> entry : directNodesData.entrySet())
		{
			// Get the gathered data
			DirectNodeData gatheredData = entry.getValue();
			double currentAverage = gatheredData.m_SumOfScores / gatheredData.m_NumberOfOccurences;
			s_TreeLogger.beginTag("SimulationSummary");
			s_TreeLogger.addTag("Transfer", entry.getKey());
			s_TreeLogger.addTag("Occurences",  gatheredData.m_NumberOfOccurences);
			s_TreeLogger.addTag("Average", currentAverage);
			s_TreeLogger.endTag("SimulationSummary");
			s_SamplesLogger.beginTag("SimulationSummary");
			s_SamplesLogger.addTag("Transfer", entry.getKey());
			s_SamplesLogger.addTag("Occurences",  gatheredData.m_NumberOfOccurences);
			s_SamplesLogger.addTag("Average", currentAverage);
			s_SamplesLogger.endTag("SimulationSummary");
			
			// Maximize average
			if ((!bestScoreCalculated) || (currentAverage > bestAverage))
			{
				bestAverage = currentAverage;
				bestTransfer = new ChipSet(entry.getKey());
				bestScoreCalculated = true;
			}
		}

		// Tree logging
		s_TreeLogger.addTag("BestTransfer", bestTransfer);
		s_TreeLogger.addTag("BestAverageScore", bestAverage);
		s_TreeLogger.endTag("TransfersSampling");
		s_TreeLogger.flush();
		s_SamplesLogger.addTag("BestTransfer", bestTransfer);
		s_SamplesLogger.addTag("BestAverageScore", bestAverage);
		s_SamplesLogger.endTag("TransfersSampling");
		s_SamplesLogger.flush();
		return bestTransfer;
	}
}
