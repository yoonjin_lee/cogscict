package ctagents.alternateOffersAgent;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;
import java.util.Vector;
import java.util.Collections;

import ctagents.FileLogger;
import ctagents.alternateOffersAgent.ProposerResponderPlayer.EndingReasons;
import ctagents.alternateOffersAgent.purbAgent.Personality;
import ctagents.alternateOffersAgent.purbAgent.Personality.PersonalityType;

import edu.harvard.eecs.airg.coloredtrails.agent.ColoredTrailsClientImpl;
import edu.harvard.eecs.airg.coloredtrails.alglib.ShortestPaths;
import edu.harvard.eecs.airg.coloredtrails.client.ClientGameStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.Scoring;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Path;
import edu.harvard.eecs.airg.coloredtrails.shared.types.PlayerStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.types.RowCol;

/**
 * <b>Description</b> The Negotiator class is the brain of the agent. It comes
 * up with different possible proposals according to the information obtained
 * until each point.
 * 
 * @author Yael Ejgenberg
 */
public class Negotiator {

	boolean bSentPromisedchips = false;
	boolean bSentSubSetPromisedchips = false;
	Path chosenPath = null;
	Personality oppCurrentPersonalityEstimation;
	Personality myPersonality;
	int numOfTimesMadeOffer = 0;
	boolean isMadeOfferFirstTime = true;
	ArrayList<Proposal> alreadySentProposals = null;
	Scoring scoring = null;

	private static final double EPSILON = 0.39;
	private static final double EPSILON_IF_DEPENDENT = 1.4;
	private static final int MAX_EXCHANGE_CHIPS = 5;

	// NUM_OF_CERTAINTY_CATEGORIES has to correspond to the amount of categories
	// specified,
	// for example LOW_NUM_OF_CHIPS,MED_NUM_OF_CHIPS, HIGH_NUM_OF_CHIPS are 3
	// categories, therefore NUM_OF_CERTAINTY_CATEGORIES = 3
	private static final int LOW_NUM_OF_CHIPS = 1;
	private static final int MED_NUM_OF_CHIPS = 2;
	private static final int HIGH_NUM_OF_CHIPS = 3;
	private static final int NUM_OF_CERTAINTY_CATEGORIES = 3;

	double[] certaintyWeightsArray = new double[NUM_OF_CERTAINTY_CATEGORIES];

	HashMap<Action, double[]> UFweightsMap = new HashMap<Action, double[]>();

	public enum Action {
		INIT_REQ_MSG, INIT_1TO1, INIT_2TO1, INIT_1TO2, REJECT_MSG, COMMIT_EXCHANGE_WITH_CHIPS, COMMIT_EXCHANGE_WITHOUT_CHIPS, SEND_PROMISED_CHIPS, SEND_SUBSET_PROMISED_CHIPS, NOT_KEEP_COMMITMENT, INIT_LIAR_MSG_TO_UNREL, INIT_LIAR_MSG_TO_RELIAB, IGNORE_MSG, SEND_SOMETHING, NOT_SEND_ANYTHING
	};

	HashMap<Action, Action[]> availableResponsesMap = new HashMap<Action, Action[]>();

	private ColoredTrailsClientImpl client;

	public Negotiator(ColoredTrailsClientImpl client,
			Personality myPersonality, Scoring scoring) {
		this.client = client;
		setWeightsMap();
		setCertaintyArray();
		this.myPersonality = myPersonality;
		this.oppCurrentPersonalityEstimation = null;
		this.scoring = scoring;
		alreadySentProposals = new ArrayList<Proposal>();
		setAvailableResponsesMap();
	}

	private void setCertaintyArray() {
		certaintyWeightsArray[0] = 3.1;
		certaintyWeightsArray[1] = 2.0;
		certaintyWeightsArray[2] = 0.5;
	}

	// The chosen path is set each time the negotiator returns a proposal in
	// make offer.
	// It is the responsibility of the caller to get the chosen path after
	// getting a proposal, since the proposal
	// was based on choosing the specific path.
	public Path getChosenPath() {
		return chosenPath;
	}

	protected class OfferDetails {
		Proposal bestProposal;
		Path p;
		double uf;

		public OfferDetails(Proposal pr, Path p, double uf) {
			this.bestProposal = pr;
			this.p = p;
			this.uf = uf;
		}

		public String toString() {
			String str = "UFProposal: chips to send= "
					+ bestProposal.chipsToSend + "\n\r chips to get= "
					+ bestProposal.chipsToReceive + " UF = " + uf;
			return str;
		}
	}

	class OfferDetailsComparator implements Comparator<Object> {

		public int compare(Object o1, Object o2) {
			OfferDetails u1 = (OfferDetails) o1;
			OfferDetails u2 = (OfferDetails) o2;
			int val;
			if (u1.uf > u2.uf)
				val = 1;
			else if (u1.uf < u2.uf)
				val = -1;
			else
				val = 0;
			return val;
		}
	}

	public Proposal makeOffer(Personality oppCurrentPersonalityEstimation,
			int consecNoMovement, EndingReasons isGameAboutToEnd,
			int numOfIterations, boolean hasThereBeenAgreement) {
		OfferDetails of = makeOfferDetails(oppCurrentPersonalityEstimation,
				consecNoMovement, isGameAboutToEnd, numOfIterations,
				hasThereBeenAgreement);
		return of.bestProposal;
	}

	private OfferDetails makeOfferDetails(
			Personality oppCurrentPersonalityEstimation, int consecNoMovement,
			EndingReasons isGameAboutToEnd, int numOfIterations,
			boolean hasThereBeenAgreement) {

		this.oppCurrentPersonalityEstimation = oppCurrentPersonalityEstimation;
		ClientGameStatus cgs = client.getGameStatus();
		ChipSet myChips = cgs.getMyPlayer().getChips();

		FileLogger.getInstance("Agent")
				.writeln(
						"At beginning of Negotiator:makeOffer; mychips are: "
								+ myChips);
		FileLogger.getInstance("Agent").writeln(
				"My personality type is: " + myPersonality.whatIsMyType());
		FileLogger.getInstance("Agent").writeln(
				"Opponent's personality type is: "
						+ oppCurrentPersonalityEstimation.whatIsMyType());

		PlayerStatus opponent = getOpponent();
		ChipSet oppChips = opponent.getChips();

		// Getting all the feasible negotiation paths between me and my opponent
		NegotiationPathsPairs feasibleNegotiationPaths = new NegotiationPathsPairs(
				cgs, getOpponent().getPerGameId());

		// the paths are sorted from the most beneficial to me (in terms of
		// lacking chips) and for each of my possible paths
		// the opponent's paths are also sorted from the most beneficial to it.
		ArrayList<OfferDetails> offerDetArray = new ArrayList<OfferDetails>();

		for (int pairNo = 0; pairNo < feasibleNegotiationPaths.getPairsNo(); ++pairNo) {

			FileLogger.getInstance("Agent").writeln(
					"Checking pair number: " + pairNo);

			Path myPath = feasibleNegotiationPaths.getMyPath(pairNo);
			Path opPath = feasibleNegotiationPaths.getOpPath(pairNo);

			FileLogger.getInstance("Agent").writeln("	My path is: " + myPath);
			FileLogger.getInstance("Agent").writeln("	Opp path is: " + opPath);

			ChipSet myRequiredChipsForPath = myPath.getRequiredChips(cgs
					.getBoard());
			ChipSet myMissing = myChips.getMissingChips(myRequiredChipsForPath);
			ChipSet myExtra = myChips.getExtraChips(myRequiredChipsForPath);

			ArrayList<String> myRequiredChipsForPathArray = myPath
					.getRequiredChipsInArray(cgs.getBoard());
			ArrayList<String> myMissingArray = myChips
					.getMissingChips(myRequiredChipsForPathArray);

			ChipSet opRequiredChipsForPath = opPath.getRequiredChips(cgs
					.getBoard());
			ChipSet opMissing = oppChips
					.getMissingChips(opRequiredChipsForPath);
			ChipSet opExtra = oppChips.getExtraChips(opRequiredChipsForPath);

			ArrayList<String> opRequiredChipsForPathArray = opPath
					.getRequiredChipsInArray(cgs.getBoard());
			ArrayList<String> opMissingArray = oppChips
					.getMissingChips(opRequiredChipsForPathArray);

			// In order to come up with the most beneficial to me and feasible
			// proposal we are going to contemplate
			// various different cases and provide a specific logic for each
			// case.
			// If action 1 to 1 then make possible give and get sets so that we
			// give and take the same
			// amount of chips. When requesting chips to take, check which chips
			// the opponent can spare.
			// Run the utility function for each one of the possible options.
			// If action 1 to 2 then make possible give and get sets so that the
			// get sets have more chips
			// than the give sets. Proceed in the same way as with 1 to 1
			// If action 2 to 1 then make possible give and get sets so that the
			// get sets have less chips
			// than the give sets. Proceed in the same way as with 1 to 1

			Vector<Params> paramsVec = new Vector<Params>();
			Action action = null;
			Action[] possibleActions = availableResponsesMap
					.get(Action.INIT_REQ_MSG);

			try {
				if (myPersonality.whatIsMyType() == PersonalityType.LL) {
					// Always make 1To2 offers regardless of who the responder
					// is.
					action = Action.INIT_1TO2;
					Vector<Params> vps = calcCombinations1To2(myExtra
							.getNumChips(), opExtra.getNumChips(), action,
							myMissing.getNumChips(), opMissing.getNumChips());

					if (vps != null)
						paramsVec.addAll(vps);
				} else if (myPersonality.whatIsMyType() == PersonalityType.HH) {
					// If my personality is HH I negotiate with everybody except
					// for opponent that is LL
					// If proposer (me) is weaker than responder then action is
					// 2 to 1
					// else action is 1 to 1
					if (oppCurrentPersonalityEstimation.whatIsMyType() == PersonalityType.MM
							|| oppCurrentPersonalityEstimation.whatIsMyType() == PersonalityType.MH
							|| oppCurrentPersonalityEstimation.whatIsMyType() == PersonalityType.HM
							|| oppCurrentPersonalityEstimation.whatIsMyType() == PersonalityType.HH
							|| oppCurrentPersonalityEstimation.whatIsMyType() == PersonalityType.LM
							|| oppCurrentPersonalityEstimation.whatIsMyType() == PersonalityType.LH) {

						if (myMissing.getNumChips() > opMissing.getNumChips()) {
							action = Action.INIT_2TO1;
							Vector<Params> vps = calcCombinations2To1(myExtra
									.getNumChips(), opExtra.getNumChips(),
									action, myMissing.getNumChips(), opMissing
											.getNumChips());

							if (vps != null)
								paramsVec.addAll(vps);
							else {
								Params ps = new Params();
								ps.myAction = Action.INIT_1TO1;
								ps.opAction = Action.INIT_1TO1;
								paramsVec.add(ps);
							}
						} else {
							action = Action.INIT_1TO1;
							Vector<Params> vps = calcCombinations1To1(myExtra
									.getNumChips(), opExtra.getNumChips(),
									action, myMissing.getNumChips(), opMissing
											.getNumChips());
							if (vps != null)
								paramsVec.addAll(vps);
						}

					} else
						FileLogger.getInstance("Agent").writeln(
								"I'm HH and opponent is LL-> don't negotiate!");

				} else if (myPersonality.whatIsMyType() == PersonalityType.MM) {

					// If I am TD and opp is TD then only contemplate 1:1
					// offers.
					// If I'm TI (therefore the opponent has to be TD) I have no
					// interest in offering 1:1
					if (!isThereSatisfiablePath(cgs.getMyPlayer())
							&& !isThereSatisfiablePath(opponent)) {
						action = Action.INIT_1TO1;
						Vector<Params> vps = calcCombinations1To1(myExtra
								.getNumChips(), opExtra.getNumChips(), action,
								myMissing.getNumChips(), opMissing
										.getNumChips());
						if (vps != null)
							paramsVec.addAll(vps);

						// If we are both TD, but I'm stronger
						if (myMissing.getNumChips() < opMissing.getNumChips()) {
							action = Action.INIT_1TO2;
							vps = calcCombinations1To2(myExtra.getNumChips(),
									opExtra.getNumChips(), action, myMissing
											.getNumChips(), opMissing
											.getNumChips());
							vps = chooseCombinationsWith1ChipDiff(vps);
							if (vps != null)
								paramsVec.addAll(vps);
						}
					} else {
						// If me (the proposer) is MM and the responder is
						// either MM or MH calculate proposal of type 1To1 only
						// if I'm TD
						// and 1To2 if proposer is stronger or equal forces
						// and 2To1 if proposer is weaker:
						if (oppCurrentPersonalityEstimation.whatIsMyType() == PersonalityType.MM
								|| oppCurrentPersonalityEstimation
										.whatIsMyType() == PersonalityType.MH) {
							// Go over all possible actions that make an offer.
							// In this case the decision of which action to try
							// depends
							// on the distribution of the forces between me and
							// opponent
							// for(int i = 0; i < possibleActions.length; i++){
							// action = possibleActions[i];
							// if(action.equals(Action.INIT_1TO1)){

							// If I am TD and opp is TD then only contemplate
							// 1:1 offers. If I'm TI (therefore the opponent has
							// to be TD) I have no interest
							// in offering 1:1
							/*
							 * if( !isThereSatisfiablePath(cgs.getMyPlayer()) &&
							 * !isThereSatisfiablePath(opponent) ) { action =
							 * Action.INIT_1TO1; Vector<Params> vps =
							 * calcCombinations1To1(myExtra.getNumChips(),
							 * opExtra.getNumChips(), action,
							 * myMissing.getNumChips(),
							 * opMissing.getNumChips()); if(vps!=null)
							 * paramsVec.addAll(vps);
							 * 
							 * if( myMissing.getNumChips() <
							 * opMissing.getNumChips() ){//If I'm stronger
							 * action = Action.INIT_1TO2; vps =
							 * calcCombinations1To2(myExtra.getNumChips(),
							 * opExtra.getNumChips(), action,
							 * myMissing.getNumChips(),
							 * opMissing.getNumChips()); vps =
							 * chooseCombinationsWith1ChipDiff(vps); if(vps !=
							 * null) paramsVec.addAll(vps); } } else{
							 */
							// If equal forces or I'm stronger
							if (myMissing.getNumChips() <= opMissing
									.getNumChips()) {
								action = Action.INIT_1TO2;
								Vector<Params> vps = calcCombinations1To2(
										myExtra.getNumChips(), opExtra
												.getNumChips(), action,
										myMissing.getNumChips(), opMissing
												.getNumChips());
								if (vps != null)
									paramsVec.addAll(vps);
							} else if (myMissing.getNumChips() > opMissing
									.getNumChips()) {
								action = Action.INIT_2TO1;
								Vector<Params> vps = calcCombinations2To1(
										myExtra.getNumChips(), opExtra
												.getNumChips(), action,
										myMissing.getNumChips(), opMissing
												.getNumChips());
								if (vps != null)
									paramsVec.addAll(vps);
							}
							// }

						}
						// else if me (the proposer) is MM and the responder is
						// either HH or HM do calculate proposals of type
						// 1To1 or of type 1To2 regardless of the forces
						// distribution:
						else if ((oppCurrentPersonalityEstimation
								.whatIsMyType() == PersonalityType.HH)
								|| (oppCurrentPersonalityEstimation
										.whatIsMyType() == PersonalityType.HM)) {

							// Go over all possible actions that make an offer.
							for (int i = 0; i < possibleActions.length; i++) {
								action = possibleActions[i];
								if (action.equals(Action.INIT_1TO1)) {
									Vector<Params> vps = calcCombinations1To1(
											myExtra.getNumChips(), opExtra
													.getNumChips(), action,
											myMissing.getNumChips(), opMissing
													.getNumChips());
									if (vps != null)
										paramsVec.addAll(vps);
								} else if (action.equals(Action.INIT_1TO2)) {
									Vector<Params> vps = calcCombinations1To2(
											myExtra.getNumChips(), opExtra
													.getNumChips(), action,
											myMissing.getNumChips(), opMissing
													.getNumChips());
									if (vps != null)
										paramsVec.addAll(vps);
								}
							}

						} else if ((oppCurrentPersonalityEstimation
								.whatIsMyType() == PersonalityType.LM)
								|| (oppCurrentPersonalityEstimation
										.whatIsMyType() == PersonalityType.LH)
								|| (oppCurrentPersonalityEstimation
										.whatIsMyType() == PersonalityType.ML)) {

							// We are both TD, make 1to1 offer
							if (!isThereSatisfiablePath(cgs.getMyPlayer())
									&& !isThereSatisfiablePath(opponent)) {
								action = Action.INIT_1TO1;
								Vector<Params> vps = calcCombinations1To1(
										myExtra.getNumChips(), opExtra
												.getNumChips(), action,
										myMissing.getNumChips(), opMissing
												.getNumChips());
								if (vps != null)
									paramsVec.addAll(vps);
							}
							// If opponent is TI (and obviously I'm TD) then I
							// am willing to check the UF for a proposal that is
							// 2 to 1.
							// Else, if we are both TD or I'm TI check proposal
							// that are 1 to 2
							if (isThereSatisfiablePath(opponent)) {
								action = Action.INIT_2TO1;
								Vector<Params> vps2 = calcCombinations2To1(
										myExtra.getNumChips(), opExtra
												.getNumChips(), action,
										myMissing.getNumChips(), opMissing
												.getNumChips());
								if (vps2 != null)
									paramsVec.addAll(vps2);
							} else {
								action = Action.INIT_1TO2;
								Vector<Params> vps1 = calcCombinations1To2(
										myExtra.getNumChips(), opExtra
												.getNumChips(), action,
										myMissing.getNumChips(), opMissing
												.getNumChips());
								if (vps1 != null)
									paramsVec.addAll(vps1);

							}

							// If we got here and couldn't come up with any
							// proposal then try anyway to make a 1to1 proposal
							// This can happen if I'm TD and opponent TI and I
							// only have one chip left to send, therefore I
							// couldn't
							// make a 2to1 proposal
							if (paramsVec.isEmpty()) {
								action = Action.INIT_1TO1;
								Vector<Params> vps = calcCombinations1To1(
										myExtra.getNumChips(), opExtra
												.getNumChips(), action,
										myMissing.getNumChips(), opMissing
												.getNumChips());
								if (vps != null)
									paramsVec.addAll(vps);
								else {
									vps = calcDesperateCombinations(myExtra
											.getNumChips(), myMissing
											.getNumChips());
									if (vps != null)
										paramsVec.addAll(vps);
								}
							}
							// If me (the proposer) is MM and the responder is
							// LL calculate proposals of type
							// 1To2 regardless of the forces distribution:
						} else if ((oppCurrentPersonalityEstimation
								.whatIsMyType() == PersonalityType.LL)) {

							for (int i = 0; i < possibleActions.length; i++) {
								action = possibleActions[i];
								// Empty proposal..
								if (action.equals(Action.INIT_1TO1)) {
									Params param = new Params();
									param.numToGet = 0;
									param.numToGive = 0;
									param.myAction = Action.INIT_1TO1;
									param.opAction = Action.INIT_1TO1;
									paramsVec.add(param);
								} else if (action.equals(Action.INIT_1TO2)) {
									Vector<Params> vps = calcCombinations1To2(
											myExtra.getNumChips(), opExtra
													.getNumChips(), action,
											myMissing.getNumChips(), opMissing
													.getNumChips());
									if (vps != null)
										paramsVec.addAll(vps);
								}
							}
						}
					}
				}
			} catch (NullPointerException e) {
				FileLogger.getInstance("Agent").writeln(e.toString());
			}

			FileLogger.getInstance("Agent").writeln(
					"About to check UF for " + paramsVec.size()
							+ "sets of params");

			// Going over the params we prepared, and calculating the UF of each
			// proposal
			for (Params param : paramsVec) {
				Proposal proposal = makeProposal(myMissingArray, myExtra,
						opMissingArray, opExtra, param.numToGet,
						param.numToGive);
				FileLogger.getInstance("Agent")
						.writeln(
								"	Action: " + param.myAction.toString()
										+ ", proposal to send: "
										+ proposal.chipsToSend);
				FileLogger.getInstance("Agent").writeln(
						"	Action: " + param.myAction.toString()
								+ ", proposal to receive: "
								+ proposal.chipsToReceive);

				// myFinalCS is the set of chips I will have if this proposal
				// gets accepted and the exchange performed:
				ChipSet myFinalCS = ChipSet.addChipSets(myChips,
						proposal.chipsToReceive);
				myFinalCS = ChipSet
						.subChipSets(myFinalCS, proposal.chipsToSend);
				int myNumChipsLacking = myFinalCS.getMissingChips(
						myRequiredChipsForPath).getNumChips();
				FileLogger.getInstance("Agent").writeln(
						"In makeOfferDetails; myChips: " + myChips);
				FileLogger.getInstance("Agent").writeln(
						"myFinalCS: " + myFinalCS);
				FileLogger.getInstance("Agent").writeln(
						"myMissing: " + myMissing);
				FileLogger
						.getInstance("Agent")
						.writeln(
								"my set of lacking chips: "
										+ myFinalCS
												.getMissingChips(myRequiredChipsForPath));
				FileLogger.getInstance("Agent").writeln(
						"myNumChipsLacking= " + myNumChipsLacking);

				// opFinalCS is the set of chips the opp. will have if this
				// proposal gets accepted and the exchange performed:
				ChipSet opFinalCS = ChipSet.addChipSets(oppChips,
						proposal.chipsToSend);
				opFinalCS = ChipSet.subChipSets(opFinalCS,
						proposal.chipsToReceive);
				int opNumChipsLacking = opFinalCS.getMissingChips(
						opRequiredChipsForPath).getNumChips();
				FileLogger.getInstance("Agent")
						.writeln("oppChips: " + oppChips);
				FileLogger.getInstance("Agent").writeln(
						"opFinalCS: " + opFinalCS);
				FileLogger.getInstance("Agent").writeln(
						"opMissing: " + opMissing);
				FileLogger
						.getInstance("Agent")
						.writeln(
								"op set of lacking chips: "
										+ opFinalCS
												.getMissingChips(opRequiredChipsForPath));
				FileLogger.getInstance("Agent").writeln(
						"opNumChipsLacking= " + opNumChipsLacking);

				// Now we calculate the weighted UF for me and opponent of each
				// possible proposal. The highest will be the proposal we
				// choose.
				// But before we get to calculate we want to introduce another
				// rule: If before the exchange the opponent is TD
				// and the exchange makes it TI and there haven't been any
				// agreements yet then don't even calculate the UF, since
				// we don't want to make the opponent TI at the beginning.
				if (!isThereSatisfiablePath(opponent)) {
					PlayerStatus newOpPlayer = (PlayerStatus) opponent.clone();
					newOpPlayer.setChips(opFinalCS);
					if (isThereSatisfiablePath(newOpPlayer)
							&& !hasThereBeenAgreement) {
						// Skip checking the UF for this proposal since even if
						// it gets a very high value, at the moment we don't
						// want
						// to propose it.
						FileLogger
								.getInstance("Agent")
								.writeln(
										"        The proposal would convert my opp. from TD to TI and we don't know her enough. Skip it");
						continue;
					}
				}

				// Another rule: If before the exchange the opponent is TI and I
				// am TD
				// and the exchange makes ME TI and there haven't been any
				// agreements yet then don't even calculate the UF, since
				// the chance the opponent will agree to such an agreement is
				// very low and it can make the other side annoyed and not
				// willing.
				if (isThereSatisfiablePath(opponent) && !hasThereBeenAgreement)
					if (doesExchangeMakePlayerIndependent(cgs.getMyPlayer(),
							proposal.chipsToSend, proposal.chipsToReceive)) {
						// Skip checking the UF for this proposal since even if
						// it gets a very high value, at the moment we don't
						// want
						// to propose it.
						FileLogger
								.getInstance("Agent")
								.writeln(
										"        The proposal would convert ME from TD to TI while opp. is TI and there haven't been agreements yet. Skip it");
						continue;
					}

				// Now calculate the UF
				FileLogger.getInstance("Agent").writeln(
						"	Calculate my UF for this action:");
				double myUtilVal = utilityFunction(param.myAction,
						proposal.chipsToReceive, proposal.chipsToSend, myChips,
						consecNoMovement, myNumChipsLacking, isGameAboutToEnd,
						numOfIterations);
				FileLogger.getInstance("Agent")
						.writeln(
								"	my utility function for action: "
										+ param.myAction.toString() + " = "
										+ myUtilVal);

				// Trying a different approach. Instead of calculating the op.
				// UF let's only calculate it's expected value.
				FileLogger.getInstance("Agent").writeln(
						"	Calculate opponent expected val for this action:");
				int opConsecNoMovement = (Integer) cgs.getMyPlayer().get(
						"opConsecutiveNoMovement");
				double opUtilVal = utilityFunctionTruncated(
						proposal.chipsToSend, proposal.chipsToReceive,
						oppChips, opConsecNoMovement, opNumChipsLacking,
						isGameAboutToEnd);
				FileLogger.getInstance("Agent").writeln(
						"	expected val: " + opUtilVal);

				boolean bCheckingProposal = false;
				UFWeights ufW = getUFWeight(myPersonality,
						isThereSatisfiablePath(opponent),
						isThereSatisfiablePath(cgs.getMyPlayer()),
						bCheckingProposal);

				// Calculating the final UF and adding the offer with its UF to
				// the array
				double utilVal = ufW.myUfWeight * myUtilVal + ufW.opUfweight
						* opUtilVal;
				FileLogger.getInstance("Agent").writeln(
						"	weighted UF: " + utilVal);
				OfferDetails ufP = new OfferDetails(proposal, myPath, utilVal);
				offerDetArray.add(ufP);
			}
		}

		isMadeOfferFirstTime = false;
		numOfTimesMadeOffer++;

		Collections.sort(offerDetArray, new OfferDetailsComparator());
		FileLogger.getInstance("Agent").writeln("Sorted offer details:");

		// Now creating an array of all the proposal that their diff from the
		// best proposal is less then epsilon
		for (OfferDetails ufP : offerDetArray)
			FileLogger.getInstance("Agent").writeln(ufP.toString());
		Vector<OfferDetails> vec = new Vector<OfferDetails>();

		// At least there should be one element in the vector, the proposal with
		// the highest UF.
		if (!offerDetArray.isEmpty()) {
			vec.add(offerDetArray.get(offerDetArray.size() - 1));
			int reference = offerDetArray.size() - 1;
			if (offerDetArray.size() >= 2) {

				// If I am dependent and opponent is TI then I should include
				// proposals in a wider range:
				double epsilon = EPSILON;
				if (!isThereSatisfiablePath(cgs.getMyPlayer())
						&& isThereSatisfiablePath(opponent))
					epsilon = EPSILON_IF_DEPENDENT;

				for (int i = offerDetArray.size() - 2; i >= 0; i--) {
					if (Math.abs(offerDetArray.get(reference).uf
							- offerDetArray.get(i).uf) < epsilon) {
						vec.add(offerDetArray.get(i));
						// FileLogger.getInstance("Agent").writeln("ufProposal: "
						// + offerDetArray.get(prev));
					}
				}
			}
		}

		FileLogger.getInstance("Agent").writeln(
				"Offer details with Epsilon diff from best UF:");
		for (OfferDetails ufP : vec)
			FileLogger.getInstance("Agent").writeln(ufP.toString());

		// Choosing a random offer from this vector
		OfferDetails of = chooseRandomVal(vec);
		if (of == null) {
			FileLogger
					.getInstance("Agent")
					.writeln(
							"Warning!! Got nul OfferDetails from chooseRandomVal. Making dummy offer.");
			Proposal pr = new Proposal();
			pr.chipsToReceive = new ChipSet();
			pr.chipsToSend = new ChipSet();
			of = new OfferDetails(pr, chosenPath, 0.0);
		}
		FileLogger.getInstance("Agent").writeln("Best proposal is: " + of);
		chosenPath = of.p;

		// Keep track of the already sent proposal so that we don't offer in two
		// contiguous rounds the same proposal.
		alreadySentProposals.add(of.bestProposal);
		return of;
	}

	private UFWeights getUFWeight(Personality myP, boolean bIsOppTI,
			boolean bAmITI, boolean bCheckingProposal) {

		UFWeights ufW = new UFWeights();
		if (bIsOppTI) {
			if (myP.isLowCoopPersonality()) {
				ufW.myUfWeight = 0.75;
				ufW.opUfweight = 0.25;
			} else {
				// ufW.myUfWeight = 0.5;
				// ufW.opUfweight = 0.5;
				ufW.myUfWeight = 0.6666;
				ufW.opUfweight = 0.3333;
			}
		}
		// If I am TI and I'm evaluating a proposal sent to me by opponent then
		// I want to take into consideration
		// the opponent less than me.
		else if (bAmITI && bCheckingProposal) {
			ufW.myUfWeight = 0.85;
			ufW.opUfweight = 0.15;
		} else {
			if (myP.isLowCoopPersonality()) {
				ufW.myUfWeight = 1.0;
				ufW.opUfweight = 0.0;
			} else if (myP.isMediumCoopPersonality()) {
				ufW.myUfWeight = 0.75;
				ufW.opUfweight = 0.25;
			} else if (myP.isHighCoopPersonality()) {
				ufW.myUfWeight = 0.5;
				ufW.opUfweight = 0.5;
			}
		}
		return ufW;
	}

	class UFWeights {
		double myUfWeight = 0.0;
		double opUfweight = 0.0;
	}

	class Params {
		int numToGet = 0;
		int numToGive = 0;
		Action myAction = null;
		Action opAction = null;

		public String toString() {
			String str = "Params: numToGet = " + numToGet + ", numToGive = "
					+ numToGive + ", myAction = " + myAction + ", opAction = "
					+ opAction;
			return str;
		}
	}

	private Vector<Params> chooseCombinationsWith1ChipDiff(
			Vector<Params> paramsVec) {
		Vector<Params> chosenVec = new Vector<Params>();
		for (Params p : paramsVec) {
			if (p.numToGet == p.numToGive + 1)
				chosenVec.add(p);
			else if (p.numToGive == p.numToGet + 1)
				chosenVec.add(p);
		}
		return chosenVec;
	}

	// I get more than I give:
	private Vector<Params> calcCombinations1To2(int myExtraCount,
			int opExtraCount, Action action, int myMissingCount,
			int opMissingCount) {

		FileLogger.getInstance("TestComb").writeln(
				"In calc comb. 1 to 2, myExtraCount = " + myExtraCount
						+ ", opExtraCount = " + opExtraCount);
		FileLogger.getInstance("TestComb").writeln(
				"	action = " + action + " my missing count = " + myMissingCount
						+ ", op missing count = " + opMissingCount);

		// If it doesn't meet minimum requirements return null.
		// The caller of this function is responsible of checking for null
		// values
		if ((myExtraCount == 0) && (opExtraCount == 0))
			return null;
		if ((myMissingCount > 0) && (opExtraCount == 0))
			return null;

		int maxToGet;
		if (myMissingCount >= opMissingCount)
			maxToGet = Math.min(myMissingCount, opExtraCount);
		else
			maxToGet = opExtraCount;

		maxToGet = Math.min(maxToGet, MAX_EXCHANGE_CHIPS);

		int minToGive = 1;
		int minToGet = minToGive + 1;

		int maxToGive;

		if (opMissingCount >= myMissingCount) {
			int temp = Math.min(opMissingCount, myExtraCount);
			maxToGive = Math.min(temp, maxToGet - 1);
		} else
			maxToGive = maxToGet - 1;

		if (maxToGive >= maxToGet) {
			FileLogger.getInstance("Agent").writeln(
					"in 1to2, maxToGive = " + maxToGive + ", maxToGet = "
							+ maxToGet);
			throw (new RuntimeException());
		}
		if (minToGet <= minToGive) {
			FileLogger.getInstance("Agent").writeln(
					"in 1to2, minToGet = " + minToGet + ", minToGive = "
							+ minToGive);
			throw (new RuntimeException());
		}
		FileLogger.getInstance("Agent").writeln(
				"in 1to2, maxToGive = " + maxToGive + ", maxToGet = "
						+ maxToGet);
		FileLogger.getInstance("Agent").writeln(
				"in 1to2, minToGet = " + minToGet + ", minToGive = "
						+ minToGive);

		Vector<Params> combination = new Vector<Params>();
		for (int i = minToGive; i <= maxToGive;) {
			for (int j = minToGet; j <= maxToGet; j++) {
				Params p = new Params();
				p.numToGive = i;
				p.numToGet = j;
				p.myAction = Action.INIT_1TO2;
				p.opAction = Action.INIT_2TO1;

				combination.add(p);
			}
			i++;
			// always keep min to get bigger than min to give by one.
			minToGet = i + 1;
		}
		return combination;
	}

	private Vector<Params> calcCombinations1To1(int myExtraCount,
			int opExtraCount, Action action, int myMissingCount,
			int opMissingCount) {

		FileLogger.getInstance("TestComb").writeln(
				"In calc comb. 1 to 1, myExtraCount = " + myExtraCount
						+ ", opExtraCount = " + opExtraCount);
		FileLogger.getInstance("TestComb").writeln(
				"	action = " + action + " my missing count = " + myMissingCount
						+ ", op missing count = " + opMissingCount);

		// If it doesn't meet minimum requirements return null.
		// The caller of this function is responsible of checking for null
		// values
		if (myMissingCount > 0 && (opExtraCount == 0 || myExtraCount == 0))
			return null;

		int minToGet = 1;

		// The maximum chips to get is the minimum number form the extra or
		// missing chips for both sides.
		int temp = Math.min(myExtraCount, opExtraCount);
		int maxToGet;
		if (myMissingCount > 0)
			maxToGet = Math.min(temp, myMissingCount);
		else
			maxToGet = Math.min(temp, opMissingCount);

		// Anyway, do not get above MAX_EXCHANGE_CHIPS.
		maxToGet = Math.min(maxToGet, MAX_EXCHANGE_CHIPS);

		Vector<Params> combination = new Vector<Params>();
		for (int i = minToGet; i <= maxToGet; i++) {
			Params p = new Params();
			p.numToGive = i;
			p.numToGet = i;
			p.myAction = Action.INIT_1TO1;
			p.opAction = Action.INIT_1TO1;

			combination.add(p);
		}
		return combination;
	}

	private Vector<Params> calcCombinations2To1(int myExtraCount,
			int opExtraCount, Action action, int myMissingCount,
			int opMissingCount) {
		FileLogger.getInstance("TestComb").writeln(
				"In calc comb. 2 to 1, myExtraCount = " + myExtraCount
						+ ", opExtraCount = " + opExtraCount);
		FileLogger.getInstance("TestComb").writeln(
				"	action = " + action + " my missing count = " + myMissingCount
						+ ", op missing count = " + opMissingCount);
		FileLogger.getInstance("TestComb").writeln("	reversing:");

		// Calculate the opposite and then reverse it:
		// calcCombinations1To2(myExtraCount, opExtraCount, action,
		// myMissingCount, opMissingCount);
		Vector<Params> temp = calcCombinations1To2(opExtraCount, myExtraCount,
				action, opMissingCount, myMissingCount);
		Vector<Params> combinations = new Vector<Params>();
		for (Params p : temp) {
			Params newP = new Params();
			newP.numToGet = p.numToGive;
			newP.numToGive = p.numToGet;
			newP.myAction = p.opAction;
			newP.opAction = p.myAction;

			combinations.add(newP);
		}
		return combinations;
	}

	private Vector<Params> calcDesperateCombinations(int myExtraCount,
			int myMissingCount) {
		FileLogger.getInstance("TestComb").writeln(
				"In calcDesperateCombinations, myExtraCount = " + myExtraCount
						+ ", myMissingCount = " + myMissingCount);
		if (myExtraCount > myMissingCount)
			FileLogger.getInstance("TestComb").writeln(
					"Error: myExtraCount > myMissingCount");
		Vector<Params> combination = new Vector<Params>();

		Params p = new Params();
		p.numToGive = myExtraCount;
		p.numToGet = myMissingCount;
		p.myAction = Action.INIT_2TO1;
		p.opAction = Action.INIT_1TO2;
		combination.add(p);
		return combination;
	}

	private void setWeightsMap() {
		double[] arr1 = { 0.50, 0.30, 0.75, 0.10 };
		UFweightsMap.put(Action.INIT_REQ_MSG, arr1);
		/*
		 * double[] arr2 = {1.0, 0.50, 0.95, 0.10 };
		 * UFweightsMap.put(Action.INIT_1TO1, arr2);
		 * 
		 * double[] arr3 = {1.0, 0.50, 0.95, 0.30 };
		 * UFweightsMap.put(Action.INIT_2TO1, arr3);
		 * 
		 * double[] arr4 = {1.0, 0.50, 0.95, 0.30 };
		 * UFweightsMap.put(Action.INIT_1TO2, arr4);
		 */
		double[] arr2 = { 0.95, 0.50, 0.95, 0.10 };
		UFweightsMap.put(Action.INIT_1TO1, arr2);

		double[] arr3 = { 0.95, 0.50, 0.95, 0.30 };
		UFweightsMap.put(Action.INIT_2TO1, arr3);

		double[] arr4 = { 1.0, 0.50, 0.95, 1.0 };
		UFweightsMap.put(Action.INIT_1TO2, arr4);

		double[] arr5 = { 0.50, 0.50, 0.75, 0.05 };
		UFweightsMap.put(Action.REJECT_MSG, arr5);

		double[] arr6 = { 1.0, 0.25, 0.95, 0.50 };
		UFweightsMap.put(Action.COMMIT_EXCHANGE_WITH_CHIPS, arr6);

		double[] arr7 = { 1.0, 0.25, 0.95, 0.50 };
		UFweightsMap.put(Action.COMMIT_EXCHANGE_WITHOUT_CHIPS, arr7);

		double[] arr8 = { 0.25, 0.95, 0.50, 0.40 };
		UFweightsMap.put(Action.SEND_PROMISED_CHIPS, arr8);

		double[] arr9 = { 0.25, 0.95, 0.50, 0.40 };
		UFweightsMap.put(Action.SEND_SUBSET_PROMISED_CHIPS, arr9);

		double[] arr10 = { 0.25, 0.95, 0.50, 0.40 };
		UFweightsMap.put(Action.NOT_KEEP_COMMITMENT, arr10);

		double[] arr11 = { 0.05, 0.25, 0.75, 0.20 };
		UFweightsMap.put(Action.INIT_LIAR_MSG_TO_UNREL, arr11);

		double[] arr12 = { 0.05, 0.25, 0.75, 0.20 };
		UFweightsMap.put(Action.INIT_LIAR_MSG_TO_RELIAB, arr12);

		double[] arr13 = { 0.25, 0.75, 0.95, 0.10 };
		UFweightsMap.put(Action.IGNORE_MSG, arr13);

	}

	private void setAvailableResponsesMap() {
		Action[] arr1 = { Action.INIT_1TO1, Action.INIT_1TO2, Action.INIT_2TO1 };
		availableResponsesMap.put(Action.INIT_REQ_MSG, arr1);

		Action[] arr2 = { Action.COMMIT_EXCHANGE_WITH_CHIPS, Action.REJECT_MSG,
				Action.IGNORE_MSG };
		availableResponsesMap.put(Action.INIT_1TO1, arr2);

		availableResponsesMap.put(Action.INIT_1TO1, arr2);
		availableResponsesMap.put(Action.INIT_2TO1, arr2);
		availableResponsesMap.put(Action.INIT_1TO2, arr2);

		Action[] arr3 = { Action.SEND_PROMISED_CHIPS,
				Action.SEND_SUBSET_PROMISED_CHIPS, Action.NOT_KEEP_COMMITMENT };
		availableResponsesMap.put(Action.COMMIT_EXCHANGE_WITH_CHIPS, arr3);
		availableResponsesMap.put(Action.COMMIT_EXCHANGE_WITHOUT_CHIPS, arr3);

		Action[] arr4 = { Action.SEND_SOMETHING, Action.NOT_SEND_ANYTHING };
		availableResponsesMap.put(Action.REJECT_MSG, arr4);
	}

	private double calcExpectedValForAction(ChipSet possibleNewCs,
			ChipSet chipsToSend, ChipSet chipsToReceive, int numOfChipsLacking,
			int consecutiveNoMovements, EndingReasons isGameAboutToEnd) {

		ClientGameStatus cgs = client.getGameStatus();

		PlayerStatus possibleNewPlayerStatus = (PlayerStatus) cgs.getMyPlayer()
				.clone();
		possibleNewPlayerStatus.setChips(possibleNewCs);
		double distance = cgs.getBoard().getGoalLocations().get(0).dist(
				cgs.getMyPlayer().getPosition());
		if (distance == 0)
			throw (new RuntimeException(
					"Shouldn't be in calcExpectedValForAction if distance to goal is zero"));

		boolean willExchangeAllowMovementAgent = willExchangeAllowMovement(cgs
				.getMyPlayer(), chipsToSend, chipsToReceive);
		boolean willExchangeAllowMovementOther = willExchangeAllowMovement(
				getOpponent(), chipsToReceive, chipsToSend);
		double score = scoring.inProcessScore(possibleNewPlayerStatus, cgs
				.getBoard().getGoalLocations().get(0), numOfChipsLacking,
				isGameAboutToEnd, willExchangeAllowMovementAgent,
				willExchangeAllowMovementOther);
		double expectedVal = score / distance
				- Math.pow(Math.E, consecutiveNoMovements);
		FileLogger.getInstance("Agent").writeln(
				"	in calcExpectedVal, val = " + expectedVal);
		return expectedVal;
	}

	private double calcExpectedCostFutureRamif(ChipSet possibleChipSetToGet,
			ChipSet possibleChipSetToSend) {
		double expectedCost = 0;
		int chipsToGet = possibleChipSetToGet.getNumChips();
		int chipsToSend = possibleChipSetToSend.getNumChips();

		if (chipsToSend == chipsToGet)
			expectedCost = 0;
		else if (chipsToSend > chipsToGet)
			expectedCost = Math.pow(1.1, Math.abs(chipsToSend - chipsToGet));
		else
			expectedCost = -1
					* Math.pow(1.1, Math.abs(chipsToSend - chipsToGet));

		FileLogger.getInstance("Agent").writeln(
				"	In calcExpectedCost, cost = " + expectedCost);
		return expectedCost;
	}

	private double utilityFunction(Action action, ChipSet possibleChipSetToGet,
			ChipSet possibleChipSetToSend, ChipSet myChips,
			int consecNoMovement, int numOfChipsLacking,
			EndingReasons isGameAboutToEnd, int numOfIterations) {

		double certaintyWeight = 0.0;
		return utilityFunction(action, possibleChipSetToGet,
				possibleChipSetToSend, myChips, consecNoMovement,
				numOfChipsLacking, isGameAboutToEnd, numOfIterations,
				certaintyWeight);
	}

	// TODO: don't get myChips as argument but get it inside the function
	private double utilityFunction(Action action, ChipSet possibleChipSetToGet,
			ChipSet possibleChipSetToSend, ChipSet myChips,
			int consecNoMovement, int numOfChipsLacking,
			EndingReasons isGameAboutToEnd, int numOfIterations,
			double certaintyWeight) {

		FileLogger.getInstance("Agent").writeln(
				"In utilityFunction; action is " + action);
		FileLogger.getInstance("Agent").writeln(
				"	chip set to get is: " + possibleChipSetToGet);
		FileLogger.getInstance("Agent").writeln(
				"	chip set to send is: " + possibleChipSetToSend);
		double[] weights = UFweightsMap.get(action);

		// FileLogger.getInstance("Agent").writeln("In UF, w1 = " + weights[0] +
		// " w2 = " + weights[1] + " w3 = " + weights[2] + " w4 = " +
		// weights[3]);

		FileLogger.getInstance("Agent").writeln(
				"	numOfChipsLacking = " + numOfChipsLacking);

		ChipSet possibleFinalChipSet = ChipSet.subChipSets(myChips,
				possibleChipSetToSend);
		possibleFinalChipSet = ChipSet.addChipSets(possibleFinalChipSet,
				possibleChipSetToGet);
		FileLogger.getInstance("Agent").writeln(
				"	my possible final ChipSet: " + possibleFinalChipSet);

		// If by exchanging the proposed chips the opponent becomes TI then the
		// future cost is irrelevant and we don't
		// want to count it.
		// We calculate w4 to be zero if by exchanging the proposed chips the
		// opponent becomes Task Independent(TI)
		// else w4 = 1, that's it it doesn't count.
		int w4 = 1;
		double w5 = 0;
		PlayerStatus opponent = getOpponent();

		// First we check if as it is now the player is TI. Only if it's not TI
		// now we want to check
		// if the exchange makes it TI and then set w4 to zero.
		// We also want to try to lower the possibility of proposing a proposal
		// that makes the opponent TI right at the beginning.
		// To do so we calculate w5 which is a weight that is greater at the
		// beginning of the game and as time goes on decreases.
		if (!isThereSatisfiablePath(opponent)) {
			ChipSet opChips = opponent.getChips();
			ChipSet opPossibleFinalChipSet = ChipSet.subChipSets(opChips,
					possibleChipSetToGet);
			opPossibleFinalChipSet = ChipSet.addChipSets(
					opPossibleFinalChipSet, possibleChipSetToSend);
			PlayerStatus newOpPlayer = (PlayerStatus) opponent.clone();
			newOpPlayer.setChips(opPossibleFinalChipSet);
			if (isThereSatisfiablePath(newOpPlayer)) {
				// w4 = 0; \\checking if this weight is needed or not.
				w5 = Math.pow(0.9, numOfIterations);
			}
		}
		FileLogger.getInstance("Agent").writeln("	w4 = " + w4 + ", w5 = " + w5);

		double val = weights[0]
				* calcExpectedValForAction(possibleFinalChipSet,
						possibleChipSetToSend, possibleChipSetToGet,
						numOfChipsLacking, consecNoMovement, isGameAboutToEnd)
				+ weights[1]
				* (oppCurrentPersonalityEstimation.getCoopLevel() + myPersonality
						.getCoopLevel())
				+ weights[2]
				* (oppCurrentPersonalityEstimation.getReliabLevel() + myPersonality
						.getReliabLevel())
				+ weights[3]
				* (calcExpectedCostFutureRamif(possibleChipSetToGet,
						possibleChipSetToSend)) * w4 - w5;

		FileLogger.getInstance("Agent").writeln(
				"	utilityFunction before certaintyweight: " + val);
		val += certaintyWeight;
		FileLogger.getInstance("Agent").writeln(
				"	utilityFunction result = " + val);

		return val;
	}

	private double utilityFunctionTruncated(ChipSet possibleChipSetToGet,
			ChipSet possibleChipSetToSend, ChipSet myChips,
			int consecNoMovement, int numOfChipsLacking,
			EndingReasons isGameAboutToEnd) {

		ChipSet possibleFinalChipSet = ChipSet.subChipSets(myChips,
				possibleChipSetToSend);
		possibleFinalChipSet = ChipSet.addChipSets(possibleFinalChipSet,
				possibleChipSetToGet);
		FileLogger.getInstance("Agent").writeln(
				"	player's possible final ChipSet: " + possibleFinalChipSet);
		double val = calcExpectedValForAction(possibleFinalChipSet,
				possibleChipSetToSend, possibleChipSetToGet, numOfChipsLacking,
				consecNoMovement, isGameAboutToEnd);

		return val;
	}

	@SuppressWarnings("unchecked")
	private Proposal makeProposal(ArrayList<String> myMissing, ChipSet myExtra,
			ArrayList<String> opMissing, ChipSet opExtra, int maxNumToGet,
			int maxNumToGive) {

		FileLogger.getInstance("Agent").writeln("In makeProposalFor1To1:");
		FileLogger.getInstance("Agent").writeln("	myMissing = " + myMissing);
		FileLogger.getInstance("Agent").writeln("	myExtra = " + myExtra);
		FileLogger.getInstance("Agent").writeln("	opMissing = " + opMissing);
		FileLogger.getInstance("Agent").writeln("	opExtra = " + opExtra);
		FileLogger.getInstance("Agent").writeln("	maxNumToGet= " + maxNumToGet);
		FileLogger.getInstance("Agent").writeln(
				"	maxNumToGive = " + maxNumToGive);
		Proposal proposal = null;
		int trials = 0;
		boolean bShouldTryAgain = false;
		do {
			proposal = new Proposal();
			ChipSet opExtraClone = (ChipSet) opExtra.clone();
			ChipSet myExtraClone = (ChipSet) myExtra.clone();
			int counter = 0;
			for (Iterator<String> it = myMissing.iterator(); it.hasNext()
					&& counter < maxNumToGet;) {
				String color = (String) it.next();
				proposal.chipsToReceive.add(color, 1);
				counter++;
				ChipSet subtract = new ChipSet();
				subtract.add(color, 1);
				opExtraClone = ChipSet.subChipSets(opExtraClone, subtract);
			}

			// if we had less missing chips than the actual amount we want to
			// exchange this time,
			// complement the set to get with more chips from opponentHas up
			// until the actual amount
			while (counter < maxNumToGet) {
				String newChip = get1RandomExistingChip(opExtraClone);
				ChipSet subtract = new ChipSet();
				subtract.add(newChip, 1);
				opExtraClone = ChipSet.subChipSets(opExtraClone, subtract);
				proposal.chipsToReceive.add(newChip, 1);
				counter++;
			}

			FileLogger.getInstance("Agent").writeln(
					"	Prepared chips to receive: " + proposal.chipsToReceive);
			// lets prepare now the set of chips we are willing to send:
			// go over opponent's missing chips. Choose the first one with
			// probability P1
			// The second one with probability (1 - P1), the third one (1 - (P1
			// + P2))/2 and so on.

			ArrayList<String> opMissingClone = (ArrayList<String>) opMissing
					.clone();
			counter = 0;
			double initProb = 0.7;
			while (!opMissingClone.isEmpty() && (counter < maxNumToGive)) {
				double[] array = calculateFlipperProb(initProb, opMissingClone
						.size());
				Random rand = new Random();
				double p = rand.nextDouble();
				int index = -1;
				for (int i = 0; i < array.length; i++) {
					// return the index of the array for which the random value
					// falls in its range.
					if (p <= array[i]) {
						index = i;
						break;
					}
				}
				String colorToSend = opMissingClone.remove(index);
				proposal.chipsToSend.add(colorToSend, 1);

				// remove color to send from myExtraClone.
				ChipSet subtract = new ChipSet();
				subtract.add(colorToSend, 1);
				// myExtra = ChipSet.subChipSets(myExtra, subtract);
				myExtraClone = ChipSet.subChipSets(myExtraClone, subtract);

				counter++;

			}

			// if opponent had less missing chips than the actual amount we want
			// to give this time,
			// complement the set to give, with more chips from my extra up
			// until the actual amount
			while ((counter < maxNumToGive) && !myExtraClone.isEmpty()) {
				String newChip = get1RandomExistingChip(myExtraClone);
				if (newChip == null)
					throw (new RuntimeException(
							"get1RandomExistingChip returned null and myExtraClone is not empty"));
				ChipSet subtract = new ChipSet();
				subtract.add(newChip, 1);
				myExtraClone = ChipSet.subChipSets(myExtraClone, subtract);
				proposal.chipsToSend.add(newChip, 1);
				counter++;
			}

			FileLogger.getInstance("Agent").writeln(
					"	Prepared chips to send: " + proposal.chipsToSend);

			trials++;
			FileLogger.getInstance("Agent").writeln(
					"	trials = " + trials + " bShouldTryAgain = "
							+ bShouldTryAgain);
			if (alreadySentProposals.size() <= 0)
				bShouldTryAgain = false;
			else {
				FileLogger.getInstance("Agent").writeln(
						"	last already sent proposal (to send): "
								+ alreadySentProposals.get(alreadySentProposals
										.size() - 1).chipsToSend);
				FileLogger.getInstance("Agent").writeln(
						"	last already sent proposal (to receive): "
								+ alreadySentProposals.get(alreadySentProposals
										.size() - 1).chipsToReceive);
				if (proposal.equals(alreadySentProposals
						.get(alreadySentProposals.size() - 1))
						&& trials < 2)
					bShouldTryAgain = true;
				else
					bShouldTryAgain = false;
			}

		} while (bShouldTryAgain);

		return proposal;
	}

	private double[] calculateFlipperProb(double initProb, int n) {

		if (n == 0)
			return null;
		double[] array = new double[n];
		array[0] = initProb;
		for (int i = 1; i < n - 1; i++) {
			array[i] = array[i - 1] + (1 - array[i - 1]) / 2;
		}
		array[n - 1] = 1;

		return array;
	}

	private String get1RandomExistingChip(ChipSet chipSet) {
		if (chipSet.isEmpty())
			return null;
		Vector<String> existingColors = new Vector<String>();
		for (String color : chipSet.getColors()) {
			if (chipSet.getNumChips(color) > 0)
				existingColors.add(color);
		}

		Random localrand = new Random();
		int index = localrand.nextInt(existingColors.size());
		return existingColors.elementAt(index);

	}

	private OfferDetails chooseRandomVal(Vector<OfferDetails> valVector) {
		if (valVector.isEmpty())
			return null;
		Random localrand = new Random();
		int index = localrand.nextInt(valVector.size());
		return valVector.elementAt(index);
	}

	// This function checks for a specific player and a specific exchange if the
	// exchange is the one that
	// will allow the movement. Therefore this function returns:
	// 1) false, if the movement was allowed anyway without the exchange
	// 2) false, if the movement is not allowed before the exchange and also not
	// allowed after the exchange.
	// 3) true, if the movement is allowed as a result of this exchange.
	private boolean willExchangeAllowMovement(PlayerStatus player,
			ChipSet chipsToSend, ChipSet chipsToReceive) {
		boolean bWillAllow = false;

		FileLogger.getInstance("Agent").writeln("In willExchangeAllowMovement");
		ChipSet playerChips = player.getChips();
		RowCol ppos = player.getPosition();
		ClientGameStatus cgs = client.getGameStatus();
		LinkedHashSet<RowCol> neighbors = (LinkedHashSet<RowCol>) ppos
				.getNeighbors(cgs.getBoard());
		FileLogger.getInstance("Agent").writeln("neighbors are: " + neighbors);
		FileLogger.getInstance("Agent").writeln(
				"player chips are: " + playerChips);
		boolean bCanMoveToANeigb = false;
		for (RowCol rc : neighbors) {
			String color = cgs.getBoard().getSquare(rc).getColor();
			FileLogger.getInstance("Agent").writeln(
					"for neigbor: " + rc + ", color is " + color);
			if (playerChips.getNumChips(color) > 0) {
				bCanMoveToANeigb = true;
				break;
			}
		}
		FileLogger.getInstance("Agent").writeln(
				"bCanMoveToANeigb = " + bCanMoveToANeigb);

		if (!bCanMoveToANeigb) {
			// Now check if we can move if the exchange was accepted:
			ChipSet chipsAfterExchange = ChipSet.addChipSets(playerChips,
					chipsToReceive);
			chipsAfterExchange = ChipSet.subChipSets(chipsAfterExchange,
					chipsToSend);
			FileLogger.getInstance("Agent").writeln(
					"chips after exchange: " + chipsAfterExchange);
			for (RowCol rc : neighbors) {
				String color = cgs.getBoard().getSquare(rc).getColor();
				if (chipsAfterExchange.getNumChips(color) > 0) {
					bWillAllow = true;
					break;
				}
			}
		}
		if (bWillAllow)
			FileLogger.getInstance("Agent").writeln(
					"The exchange will allow the movement");
		else
			FileLogger.getInstance("Agent").writeln(
					"The exchange will NOT allow the movement");

		return bWillAllow;
	}

	// This function returns the color of the first chip we realize the player
	// could use to move.
	// That is, if the player could move with the chips it had before the
	// exchange the function
	// will return one of them.
	// If the player can move only once the exchange takes effect, then we
	// return the color of the first
	// chip we encounter in the new set of chips as the color used to move.
	// If none found, return null
	private String getChipThatAllowsMovement(PlayerStatus player,
			ChipSet chipsToSend, ChipSet chipsToReceive) {
		String chipThatAllows = null;

		FileLogger.getInstance("Agent").writeln("In getChipThatAllowsMovement");
		ChipSet playerChips = player.getChips();
		RowCol ppos = player.getPosition();
		ClientGameStatus cgs = client.getGameStatus();
		LinkedHashSet<RowCol> neighbors = (LinkedHashSet<RowCol>) ppos
				.getNeighbors(cgs.getBoard());
		FileLogger.getInstance("Agent").writeln("neighbors are: " + neighbors);
		FileLogger.getInstance("Agent").writeln(
				"player chips are: " + playerChips);
		boolean bCanMoveToANeigb = false;
		for (RowCol rc : neighbors) {
			String color = cgs.getBoard().getSquare(rc).getColor();
			FileLogger.getInstance("Agent").writeln(
					"for neigbor: " + rc + ", color is " + color);
			if (playerChips.getNumChips(color) > 0) {
				bCanMoveToANeigb = true;
				chipThatAllows = color;
				break;
			}
		}
		FileLogger.getInstance("Agent").writeln(
				"bCanMoveToANeigb = " + bCanMoveToANeigb);

		if (!bCanMoveToANeigb) {
			// Now check if we can move if the exchange was accepted:
			ChipSet chipsAfterExchange = ChipSet.addChipSets(playerChips,
					chipsToReceive);
			chipsAfterExchange = ChipSet.subChipSets(chipsAfterExchange,
					chipsToSend);
			FileLogger.getInstance("Agent").writeln(
					"chips after exchange: " + chipsAfterExchange);
			for (RowCol rc : neighbors) {
				String color = cgs.getBoard().getSquare(rc).getColor();
				if (chipsAfterExchange.getNumChips(color) > 0) {
					chipThatAllows = color;
					break;
				}
			}
		}

		FileLogger.getInstance("Agent").writeln(
				"chip that allows movement is: " + chipThatAllows);
		return chipThatAllows;
	}

	/**
	 * isProposalBeneficial checks whether a proposal we got is beneficial to us
	 * or not. Logic: 1) Calculate the utility function (UF1) for the current
	 * situation (that is, if there won't be an exchange of chips) 2) Assuming
	 * the exchange is performed as suggested in the proposal, calculate utility
	 * function (UF2) 3) if UF2 <= UF1 (our situation is better off not
	 * exchanging) then return false. 4) else, calculate what would be our
	 * proposal if we were the ones to propose and calculate the utility
	 * function for it (UF3) 5) if UF3 < UF2 return true 6) else if I have to
	 * move or otherwise the game ends, check if by performing this exchange I
	 * will be able to move
	 * 
	 * @param receives
	 *            as input a proposal sent to us by the opponent.
	 * @return 1 if beneficial for sure, -1 not beneficial for sure, 0 if
	 * @author Yael.
	 */

	// TODO!!!! The whole function has to be rewritten!! Is digusting!!!Urgent!!
	// (Yael).
	public boolean isProposalBeneficial(Proposal proposal,
			ArrayList<Path> myShortestPaths, int consecNoMovement,
			int opConsecNoMovement, int maxRoundMove,
			Personality oppCurrentPersonalityEstimation,
			ArrayList<Path> oppShortestPaths, EndingReasons isGameAboutToEnd,
			int numOfIterations, boolean hasThereBeenAgreement,
			boolean bIsCounterOffer) {
		FileLogger.getInstance("Agent").writeln("In isProposalBeneficial:");
		FileLogger.getInstance("Agent").writeln(
				"	chips to get = " + proposal.chipsToReceive);
		FileLogger.getInstance("Agent").writeln(
				"	chips to send = " + proposal.chipsToSend);
		FileLogger.getInstance("Agent").writeln(
				"	bIsGameAboutToEnd = " + isGameAboutToEnd
						+ ", numOfIterations = " + numOfIterations);

		this.oppCurrentPersonalityEstimation = oppCurrentPersonalityEstimation;

		ClientGameStatus cgs = client.getGameStatus();

		// int counterOffersCount =
		// (Integer)cgs.getMyPlayer().get("counterOffersCount");
		// FileLogger.getInstance("Agent").writeln("counterOffersCount = " +
		// counterOffersCount);
		FileLogger.getInstance("Agent").writeln(
				"bIsCounterOffer = " + bIsCounterOffer);
		boolean bIsBenef = false;
		// If the chips to send in the proposal (that is,chips that the agent
		// already has) are needed to traverse the chosen path
		// then reject the offer. We don't even check the UF. What we already
		// have in hand we don't want to loose.
		// We need to calculate the best offer here because we need to set the
		// chosen path!
		OfferDetails myBestOffer = makeOfferDetails(
				oppCurrentPersonalityEstimation, consecNoMovement,
				isGameAboutToEnd, numOfIterations, hasThereBeenAgreement);
		if (hasRequestedNeededChipsIHadAndCantGive(chosenPath, cgs
				.getMyPlayer().getChips(), proposal.chipsToSend)) {
			bIsBenef = false;
			FileLogger
					.getInstance("Agent")
					.writeln(
							"I have been asked to give in chip/s that I need to traverse the path. Reject Offer");
			return bIsBenef;
		}

		PlayerStatus opponent = getOpponent();

		double ratio = (double) ((double) proposal.chipsToReceive.getNumChips() / (double) proposal.chipsToSend
				.getNumChips());
		FileLogger.getInstance("Agent").writeln(
				"The ratio of the proposal is: " + ratio);

		boolean bFlag = false; // TODO give a proper name to this flag!

		// If I am TI (therefore the opp is TD), and opp must move this time.
		// And opp lacks more than one chips to reach the goal
		// and the exchange will let him move and still he will lack more chips
		// for further movement
		// and this is a counter offer then turn on a flag indicating this.
		// When calculating the UF of not performing the exchange instead we
		// will calculate the UF of sending the needed chip
		if (isThereSatisfiablePath(cgs.getMyPlayer())
				&& (isGameAboutToEnd == EndingReasons.OPP_HAS_TO_MOVE || isGameAboutToEnd == EndingReasons.BOTH_HAVE_TO_MOVE)) {
			FileLogger.getInstance("Agent").writeln(
					"I am TI and isGameAboutToEnd = " + isGameAboutToEnd);
			if (bIsCounterOffer
					&& willExchangeAllowMovement(opponent,
							proposal.chipsToReceive, proposal.chipsToSend)) { // We
				// are
				// contemplating
				// a
				// counter
				// offer
				if (!doesExchangeMakePlayerIndependent(opponent,
						proposal.chipsToReceive, proposal.chipsToSend)
						&& areThereExtraChipsForSomePath(oppShortestPaths,
								opponent))
					bFlag = true;
			}
		}
		// If the exchange will not be done the game will end, but if it will be
		// done its not sure I'll be better..
		boolean bFlag2 = false; // TODO give a proper name to this flag!
		if ((isGameAboutToEnd == EndingReasons.OPP_HAS_TO_MOVE || isGameAboutToEnd == EndingReasons.BOTH_HAVE_TO_MOVE)
				&& willExchangeAllowMovement(opponent, proposal.chipsToReceive,
						proposal.chipsToSend))

			if (bIsCounterOffer
					&& (!isThereSatisfiablePath(cgs.getMyPlayer()) || (isThereSatisfiablePath(cgs
							.getMyPlayer()) && doesExchangeMakePlayerIndependent(
							opponent, proposal.chipsToReceive,
							proposal.chipsToSend)))) {
				bFlag2 = true;
			}

		// If I have to move, otherwise the game ends, calculate if accepting
		// the exchange will allow me to move to a next square:
		// If we don't have a chip for any of the neighbors for my current set
		// of chips, then check if with the set obtained if this
		// proposal was accepted we could move to any of the neighbors:
		boolean bWillAllow = false;
		if (isGameAboutToEnd == EndingReasons.I_HAVE_TO_MOVE) {
			FileLogger.getInstance("Agent").writeln("I have to move");
			bWillAllow = willExchangeAllowMovement(cgs.getMyPlayer(),
					proposal.chipsToSend, proposal.chipsToReceive);
		} else if (isGameAboutToEnd == EndingReasons.BOTH_HAVE_TO_MOVE) {
			FileLogger.getInstance("Agent").writeln(
					"I have to move and Opp. has to move");
			boolean b1 = willExchangeAllowMovement(cgs.getMyPlayer(),
					proposal.chipsToSend, proposal.chipsToReceive);

			boolean b2 = willExchangeAllowMovement(opponent,
					proposal.chipsToReceive, proposal.chipsToSend);
			if (b1 || b2)
				bWillAllow = true;
			else
				bWillAllow = false;
		}
		// If the exchange will allow to move then accept it. Otherwise,
		// calculate it's UF in the regular way:
		if (bWillAllow)
			return bWillAllow;

		// I don't have to move right now, but the Opponent is TI, so it's
		// stronger than me and the proposition is such that
		// the ratio between what I give and what I get is >=1/2 then of course
		// I accept it.
		// else if (isThereSatisfiablePath(opponent) && (ratio >= 0.5)){
		if (isThereSatisfiablePath(opponent) && (ratio >= 0.5)) {
			FileLogger.getInstance("Agent").writeln(
					"Opp is TI and the ratio of the proposal is: " + ratio);
			bIsBenef = true;
		} else {
			double uf1;
			double uf2 = calcUFForExchange(proposal, myShortestPaths,
					oppShortestPaths, consecNoMovement, isGameAboutToEnd,
					numOfIterations, hasThereBeenAgreement);
			if (bFlag) {
				Proposal sendNeededChipProp = new Proposal();
				// The function getChipThatAllowsMovement receives as a second
				// argument the chips that the player im question
				// will send and as a third argument the chips the player in
				// question will receive.
				// Therefore, in the proposal we are reviewing
				// proposal.chipsToReceive are the chips I (the agent) will
				// receive, that is the chips that the opponent will send. The
				// same goes for the third argument:
				String neededColor = getChipThatAllowsMovement(opponent,
						proposal.chipsToReceive, proposal.chipsToSend);
				sendNeededChipProp.chipsToSend.add(neededColor, 1);
				FileLogger.getInstance("Agent").writeln(
						"bFlag is true, checking UF if sending needed chip: "
								+ neededColor);
				uf1 = calcUFForExchange(sendNeededChipProp, myShortestPaths,
						oppShortestPaths, consecNoMovement, isGameAboutToEnd,
						numOfIterations, hasThereBeenAgreement);
				if ((uf2 + EPSILON) <= uf1) {
					bIsBenef = false;
					FileLogger.getInstance("Agent").writeln(
							"uf2 <= uf1 " + "uf2 = " + uf2 + ", uf1 = " + uf1
									+ " bIsBenef = false");
				} else {
					bIsBenef = true;
					FileLogger.getInstance("Agent").writeln(
							"uf2 > uf1 " + "uf2 = " + uf2 + ", uf1 = " + uf1
									+ " bIsBenef = true");
				}
				return bIsBenef;
			}
			if (bFlag2) {
				uf1 = calcUFNoExchange(myShortestPaths, consecNoMovement,
						isGameAboutToEnd, numOfIterations);
				if ((uf2 + EPSILON) <= uf1) {
					bIsBenef = false;
					FileLogger.getInstance("Agent").writeln(
							"uf2 <= uf1 " + "uf2 = " + uf2 + ", uf1 = " + uf1
									+ " bIsBenef = false");
				} else {
					bIsBenef = true;
					FileLogger.getInstance("Agent").writeln(
							"uf2 > uf1 " + "uf2 = " + uf2 + ", uf1 = " + uf1
									+ " bIsBenef = true");
				}
				return bIsBenef;
			}
			// else
			uf1 = calcUFNoExchange(myShortestPaths, consecNoMovement,
					isGameAboutToEnd, numOfIterations);
			// double uf2 = calcUFForExchange(proposal, myShortestPaths,
			// oppShortestPaths, consecNoMovement, isGameAboutToEnd,
			// numOfIterations, hasThereBeenAgreement);

			if ((uf2 + EPSILON) <= uf1) {
				bIsBenef = false;
				FileLogger.getInstance("Agent").writeln(
						"uf2 <= uf1 " + "uf2 = " + uf2 + ", uf1 = " + uf1
								+ " bIsBenef = false");
			} else if (!hasThereBeenAgreement
					&& doesExchangeMakePlayerIndependent(opponent,
							proposal.chipsToReceive, proposal.chipsToSend)) {
				bIsBenef = false;
				FileLogger
						.getInstance("Agent")
						.writeln(
								"!hasThereBeenAgreement && doesExchangeMakePlayerIndependent is true");
			} else {
				FileLogger.getInstance("Agent").writeln(
						"uf2 + EPSILON > uf1 " + "uf2 = " + uf2 + ", uf1 = "
								+ uf1 + " calculate my offer:");
				double uf3 = myBestOffer.uf;
				FileLogger.getInstance("Agent").writeln(
						"my offer is: proposal = " + myBestOffer.bestProposal
								+ ", UF = " + myBestOffer.uf
								+ ", chosen path: " + myBestOffer.p);
				if ((uf3 <= uf2) || (uf3 <= (uf2 + EPSILON))) { // If what I
					// would offer
					// is worse than
					// the proposal
					// I got
					// or even if it is a little bit better up to Epsilon then
					// the proposal is beneficial
					FileLogger.getInstance("Agent").writeln(
							"uf3 (" + uf3 + ") <= uf2 + EPSILON (" + uf2
									+ "), bIsBenef = true");
					bIsBenef = true;
				} else if (isGameAboutToEnd == EndingReasons.NOT_ENDING) {
					// I still have more rounds to stay still until the game
					// ends, no need to rush to accept and offer that is not so
					// good!
					FileLogger
							.getInstance("Agent")
							.writeln(
									"I still have more rounds to stay still until the game ends, reject!");
					bIsBenef = false;
				}
			}
		}

		FileLogger.getInstance("Agent").writeln(
				"isProposalBeneficial returns: " + bIsBenef);
		return bIsBenef;
	}

	private boolean areThereExtraChipsForSomePath(ArrayList<Path> shrtP,
			PlayerStatus player) {
		boolean bHaveExtra = false;
		ClientGameStatus cgs = client.getGameStatus();
		for (Path myPossiblePath : shrtP) {
			ChipSet requiredChipsForPath = myPossiblePath.getRequiredChips(cgs
					.getBoard());
			ChipSet myExtra = player.getChips().getExtraChips(
					requiredChipsForPath);
			if (myExtra.getNumChips() > 0) {
				bHaveExtra = true;
				break;
			}
		}
		return bHaveExtra;
	}

	// This function returns:
	// 1) false, if player was already independent
	// 2) false, if player was dependent before exchange and remains dependent
	// after exchange
	// 3) true, if player becomes independent after the exchange
	private boolean doesExchangeMakePlayerIndependent(PlayerStatus player,
			ChipSet chipsToSend, ChipSet chipsToReceive) {
		FileLogger.getInstance("Agent").writeln(
				"In doesExchangeMakePlayerIndependent");
		boolean makesIndep = false;

		if (isThereSatisfiablePath(player)) {
			FileLogger.getInstance("Agent").writeln(
					"Player is already independent");
			return makesIndep;
		}

		ChipSet playerChips = player.getChips();
		ChipSet chipsAfterExchange = ChipSet.addChipSets(playerChips,
				chipsToReceive);
		chipsAfterExchange = ChipSet.subChipSets(chipsAfterExchange,
				chipsToSend);

		PlayerStatus newPlayer = (PlayerStatus) player.clone();
		newPlayer.setChips(chipsAfterExchange);
		// if player became independent after the exchange
		if (isThereSatisfiablePath(newPlayer)) {
			FileLogger.getInstance("Agent").writeln(
					"Player becomes independent with exchange");
			makesIndep = true;
		} else {
			FileLogger.getInstance("Agent").writeln(
					"Player remains dependent after exchange");
			makesIndep = false;
		}

		return makesIndep;
	}

	private boolean hasRequestedNeededChipsIHadAndCantGive(Path pathToCheck,
			ChipSet myChipsBefExch, ChipSet chipsToSend) {
		boolean bHas = false;
		
		System.out.println("hasRequestedNeededChipsIHadAndCantGive");
		ClientGameStatus cgs = client.getGameStatus();
		ChipSet requiredChipsForPath = pathToCheck.getRequiredChips(cgs
				.getBoard());
		ChipSet myMissingBefExch = myChipsBefExch
				.getMissingChips(requiredChipsForPath);
		ChipSet myChipsAfterExch = ChipSet.subChipSets(myChipsBefExch,
				chipsToSend);
		ChipSet myMissingAfterExch = myChipsAfterExch
				.getMissingChips(requiredChipsForPath);

		ChipSet diffCS = ChipSet.subChipSets(myMissingBefExch,
				myMissingAfterExch);
		for (String color : diffCS.getColors()) {
			if (diffCS.getNumChips(color) < 0) {
				bHas = true;
				break;
			}
		}
		return bHas;
	}

	/**
	 * 
	 * @param myShortestPaths
	 * @param consecNoMovement
	 * @return utility function value for best short path if no exchange is
	 *         performed
	 */
	private double calcUFNoExchange(ArrayList<Path> myShortestPaths,
			int consecNoMovement, EndingReasons isGameAboutToEnd,
			int numOfIterations) {
		FileLogger.getInstance("Agent").writeln(
				"	Calculate UF if no exchange is made:");
		ClientGameStatus cgs = client.getGameStatus();
		ChipSet myChips = cgs.getMyPlayer().getChips();

		Action action = Action.INIT_1TO1;
		ChipSet chipsToSend = new ChipSet(); // empty set
		ChipSet chipsToGet = new ChipSet(); // empty set
		double uf = 0.0;
		for (Path myPossiblePath : myShortestPaths) {
			ChipSet requiredChipsForPath = myPossiblePath.getRequiredChips(cgs
					.getBoard());
			ChipSet myMissing = myChips.getMissingChips(requiredChipsForPath);
			double val = utilityFunction(action, chipsToGet, chipsToSend,
					myChips, consecNoMovement, myMissing.getNumChips(),
					isGameAboutToEnd, numOfIterations);
			if (val > uf)
				uf = val;
		}
		return uf;

	}

	private double calcUFForExchange(Proposal proposal,
			ArrayList<Path> myShortestPaths, ArrayList<Path> opShortestPaths,
			int consecNoMovement, EndingReasons isGameAboutToEnd,
			int numOfIterations, boolean hasThereBeenAgreement) {
		FileLogger.getInstance("Agent").writeln(
				"	Calculate UF if exchange is made");
		ClientGameStatus cgs = client.getGameStatus();
		ChipSet myChips = cgs.getMyPlayer().getChips();
		FileLogger.getInstance("Agent").writeln("myChips: " + myChips);
		Action action = null;
		if (proposal.chipsToReceive.getNumChips() == proposal.chipsToSend
				.getNumChips())
			action = Action.INIT_1TO1;
		else if (proposal.chipsToReceive.getNumChips() > proposal.chipsToSend
				.getNumChips())
			action = Action.INIT_1TO2;
		else
			action = Action.INIT_2TO1;

		double uf = 0.0;
		for (Path myPossiblePath : myShortestPaths) {
			for (Path opPossiblePath : opShortestPaths) {
				ChipSet requiredChipsForPath = myPossiblePath
						.getRequiredChips(cgs.getBoard());
				FileLogger.getInstance("Agent").writeln(
						"Calculate my num of chips lacking");
				FileLogger.getInstance("Agent").writeln(
						"requiredChipsForPath: " + requiredChipsForPath);
				ChipSet myMissing = myChips
						.getMissingChips(requiredChipsForPath);
				FileLogger.getInstance("Agent").writeln(
						"myMissing before taking into acc. exchange: "
								+ myMissing);
				// To calculate the number of chips lacking for this path we
				// take the set of missing chips for this path (according to the
				// chips we currently have)
				// and we add the chips to receive. The difference is the amount
				// lacking.
				for (String missingColor : myMissing.getColors()) {
					for (String addColor : proposal.chipsToReceive.getColors()) {
						if (missingColor.equalsIgnoreCase(addColor)) {
							int newNum = myMissing.getNumChips(missingColor)
									- proposal.chipsToReceive
											.getNumChips(addColor);
							myMissing.setNumChips(missingColor, newNum);
						}
					}
				}

				FileLogger.getInstance("Agent").writeln(
						"Final my num of chips lacking: "
								+ myMissing.getNumChips());

				double certaintyWeight = 0.0;// certaintyWeightsArray[0];
				// if I am dependent, and there hasn't been an agreenment yet,
				// that is, my certainty about the opp.'s personality is zero
				// then add a higher weight to the UF so that exchanges in which
				// I send lower chips get a higher value
				if (!isThereSatisfiablePath(cgs.getMyPlayer())
						&& !hasThereBeenAgreement) {
					if (proposal.chipsToSend.getNumChips() > 0
							&& proposal.chipsToSend.getNumChips() <= LOW_NUM_OF_CHIPS)
						certaintyWeight = certaintyWeightsArray[0];
					else if (proposal.chipsToSend.getNumChips() > LOW_NUM_OF_CHIPS
							&& proposal.chipsToSend.getNumChips() <= MED_NUM_OF_CHIPS)
						certaintyWeight = certaintyWeightsArray[1];
					else if (proposal.chipsToSend.getNumChips() > MED_NUM_OF_CHIPS
							&& proposal.chipsToSend.getNumChips() <= HIGH_NUM_OF_CHIPS)
						certaintyWeight = certaintyWeightsArray[2];

				}
				FileLogger.getInstance("Agent").writeln(
						"Calculating my UF with certaintyWeight= "
								+ certaintyWeight);
				double myUtilVal = utilityFunction(action,
						proposal.chipsToReceive, proposal.chipsToSend, myChips,
						consecNoMovement, myMissing.getNumChips(),
						isGameAboutToEnd, numOfIterations, certaintyWeight);
				FileLogger.getInstance("Agent").writeln(
						"my util val: " + myUtilVal);

				PlayerStatus opponent = getOpponent();
				ChipSet oppChips = opponent.getChips();
				FileLogger.getInstance("Agent")
						.writeln("oppChips: " + oppChips);
				ChipSet opFinalCS = ChipSet.addChipSets(oppChips,
						proposal.chipsToSend);
				opFinalCS = ChipSet.subChipSets(opFinalCS,
						proposal.chipsToReceive);
				FileLogger.getInstance("Agent").writeln(
						"opFinalCS: " + opFinalCS);
				ChipSet opRequiredChipsForPath = opPossiblePath
						.getRequiredChips(cgs.getBoard());
				FileLogger.getInstance("Agent").writeln(
						"opRequiredChipsForPath: " + opRequiredChipsForPath);
				ChipSet opMissing = opFinalCS
						.getMissingChips(opRequiredChipsForPath);
				FileLogger.getInstance("Agent").writeln(
						"opMissing : " + opMissing);

				int opNumChipsLacking = opMissing.getNumChips();

				FileLogger.getInstance("Agent").writeln(
						"	opNumChipsLacking = " + opNumChipsLacking);
				int opConsecNoMovement = (Integer) cgs.getMyPlayer().get(
						"opConsecutiveNoMovement");
				double opUtilVal = utilityFunctionTruncated(
						proposal.chipsToSend, proposal.chipsToReceive,
						oppChips, opConsecNoMovement, opNumChipsLacking,
						isGameAboutToEnd);
				FileLogger.getInstance("Agent").writeln(
						"	opp expected val: " + opUtilVal);
				boolean bCheckingProposal = true;
				UFWeights ufW = getUFWeight(myPersonality,
						isThereSatisfiablePath(opponent),
						isThereSatisfiablePath(cgs.getMyPlayer()),
						bCheckingProposal);
				FileLogger.getInstance("Agent").writeln(
						"myUFweight = " + ufW.myUfWeight + " opUFWeight = "
								+ ufW.opUfweight);
				double utilVal = ufW.myUfWeight * myUtilVal + ufW.opUfweight
						* opUtilVal;

				if (utilVal > uf)
					uf = utilVal;
			}
		}
		return uf;

	}

	// TODO: this function was copied from AlternativeOffersUltimatumConfig.
	// We have to think of a way to avoid this redundancy.
	public boolean isThereSatisfiablePath(PlayerStatus player) {
		boolean bThereIs = false;
		ClientGameStatus cgs = client.getGameStatus();
		ArrayList<Path> shortestPaths = ShortestPaths
				.getShortestPathsForMyChips(player.getPosition(), cgs
						.getBoard().getGoalLocations().get(0), cgs.getBoard(),
						scoring, ShortestPaths.NUM_PATHS_RELEVANT, player
								.getChips());

		ChipSet playerChips = player.getChips();
		for (Path path : shortestPaths) {
			ChipSet required = path.getRequiredChips(cgs.getBoard());
			if (playerChips.contains(required)) {
				bThereIs = true;
				break;
			}
		}
		return bThereIs;
	}

	private PlayerStatus getOpponent() {
		PlayerStatus opponent = null;

		ClientGameStatus cgs = client.getGameStatus();
		Set<PlayerStatus> players = cgs.getPlayers();
		for (PlayerStatus ps : players)
			// We assume there are only two players. Go over list of players
			// (there should be two). If it's not me
			// then it's my opponent.
			if (ps != cgs.getMyPlayer()) {
				opponent = ps;
				break;
			}
		return opponent;
	}
}