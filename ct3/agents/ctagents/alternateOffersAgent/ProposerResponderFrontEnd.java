/*
	Colored Trails
	
	Copyright (C) 2006-2007, President and Fellows of Harvard College.  All Rights Reserved.
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ctagents.alternateOffersAgent;

import org.apache.log4j.Logger;

import ctagents.alternateOffersAgent.purbAgent.PurbAgent;

/**
 * 
 * @author Yael Ejgenberg
 *
 */
public final class ProposerResponderFrontEnd {

	private static Logger log = Logger.getRootLogger();

    public static void main(String[] args) {
        if((args.length != 0 && args[0] != null) && ( args[1] != null && args[2] != null) ) {
        	Double coop = new Double(args[1]);
        	Double reliab = new Double(args[2]);
        	PurbAgent agent = new PurbAgent(coop.doubleValue(), reliab.doubleValue());
         	agent.setClientName(args[0]);        	
        	agent.start();
        } else {
        	log.fatal("You need to specify an id or a name for the agent: SimpleAgentFrontEnd [name]");
        }

    }
     

}
