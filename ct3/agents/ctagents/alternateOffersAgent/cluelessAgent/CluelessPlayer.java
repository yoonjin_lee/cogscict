//////////////////////////////////////////////////////////////////////////
// File:		CluelessPlayer.java 					   				//
// Purpose:		Implements the core of the Clueless agent.				//
//////////////////////////////////////////////////////////////////////////
package ctagents.alternateOffersAgent.cluelessAgent;

//Imports from Java common framework
import java.util.ArrayList;
import java.util.Set;
import java.lang.Math;

//Imports from CT framework
import edu.harvard.eecs.airg.coloredtrails.alglib.ShortestPaths;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Phases;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.DiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.BasicProposalDiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.BasicProposalDiscussionDiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;
import edu.harvard.eecs.airg.coloredtrails.shared.types.RowCol;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Path;
import edu.harvard.eecs.airg.coloredtrails.shared.Scoring;
import edu.harvard.eecs.airg.coloredtrails.shared.types.PlayerStatus;
import edu.harvard.eecs.airg.coloredtrails.agent.events.RoleChangedEventListener;
import ctagents.alternateOffersAgent.Proposal;
import ctagents.alternateOffersAgent.SimplePlayer;
import ctagents.FileLogger;

//////////////////////////////////////////////////////////////////////////
// Class:		CluelessPlayer						    				//
// Purpose:		Implements the Clueless player.							//
//////////////////////////////////////////////////////////////////////////
public class CluelessPlayer extends SimplePlayer implements RoleChangedEventListener
{
	// Saves whether Clueless is the first proposer
	private boolean m_IsFirstProposer;
	
	// Saves the last transfer
	private ChipSet m_LastTransfer;
	
	// Saves whether this is the first communications round
	private boolean m_FirstCommunicationsRound;
	
	// Saves the last accepted proposal
	private Proposal m_LastAcceptedProposal;
	
	// Saves the maximum number of dormant rounds
	private int m_MaxDormantRounds;
	
	// Saves the number of transfers so far
	private int m_TransfersSoFar;
	
	// Reliability related members
	private double m_MyReliabilitySum;
	private double m_OpponentReliabilitySum;
	
	// Saves the opponents' last chips
	private ChipSet m_MyLastChips;
	private ChipSet m_OpponentLastChips;
	
	// Saves scores before transfers
	private double m_MyScoreBeforeTransfer;
	private double m_OpponentScoreBeforeTransfer;
	
	// Saves the scoring
	private Scoring m_Scoring;
	
	// Debugging related members
	private static final FileLogger s_Logger = FileLogger.getInstance("CluelessPlayer");
	private static final boolean s_IsDebug = true;
	
	// Maximum number of dormant rounds
	private static final int s_MaxDormantRounds = 2;
	
	// Branching factor
	private static int s_SamplingBranching;
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		logMsg													//
	// Purpose:		Logs a new message to the log file.						//
	// Parameters:	@ message - The message to log.							//
	// Remarks:		* Logs only when debug mode is activated.				//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private static void logMsg(String message)
	{
		// Write a new line with the message if we're in debug mode
		if (s_IsDebug)
		{
			s_Logger.writeln(message);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		CluelessPlayer constructor								//
	// Purpose:		Initializes the Clueless agent.							//
	// Parameters:	@ samplingBranching - the branching factor of sampling.	//
	//				@ initialReliability - the initial reliability.			//
	//				@ lastRoundReliabilityWeight - the last round weight of	//
	//											   the reliability compared	//
	//											   to the history.			//
	//				@ myScoreWeight - my weight for scoring (against the    //
	//								  opponent's weight).					//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public CluelessPlayer(int samplingBranching,
						  double initialReliability,
						  double lastRoundReliabilityWeight,
						  double myScoreWeight)
	{
		super();
		logMsg("CluelessPlayer(): entering.");
		
		// We are a listener for the role changing event
		client.addRoleChangedEventListener(this);
		
		// Set the last round reliability weight
		Reliability.setLastRoundWeight(lastRoundReliabilityWeight);
		
		// Set the scoring weight
		GameSimulator.setMyScoreWeight(myScoreWeight);
		
		// The sampling branching factor
		s_SamplingBranching = samplingBranching;
		
		// I'm not the first proposer unless deduced otherwise
		this.m_IsFirstProposer = false;
		
		// The last accepted proposal
		this.m_LastAcceptedProposal = null;
		
		// The first round of communications
		this.m_FirstCommunicationsRound = true;
		
		// Although no transfers were made till now, we initialize the
		// reliability to a positive value, so in order to take an average
		// we will "simulate" one trasfer of the given reliability to both sides.
		this.m_TransfersSoFar = 1;
		
		// Initialize the reliability measures
		this.m_MyReliabilitySum = initialReliability;
		this.m_OpponentReliabilitySum = initialReliability;
		
		// Initialize the scoring
		this.m_Scoring = null;
		
		// Initialize the last transfer
		this.m_LastTransfer = null;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getMe													//
	// Purpose:		Returns my player status.								//
	// Returns:		My player status.										//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private PlayerStatus getMe()
	{
		// Done easily using the client game status
		return client.getGameStatus().getMyPlayer();
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getOpponent												//
	// Purpose:		Returns the opponent's player status.					//
	// Returns:		The opponent's player status.							//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private PlayerStatus getOpponent()
	{
		// Iterate the players
		for (PlayerStatus playerStatus : client.getGameStatus().getPlayers())
		{
			// If the PIN numbers disagree then it's the opponent
			if (playerStatus.getPin() != getMe().getPin())
			{
				return (PlayerStatus)(playerStatus.clone());
			}
		}
		
		// Not found
		throw new RuntimeException("Opponent ID not found.");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getChipsToTransfer										//
	// Purpose:		Gets the chips to send for a transfer.					//
	// Parameters:	@ proposal - the last accepted proposal.				//
	// Returns:		The chips to send.										//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private ChipSet getChipsToTransfer(Proposal proposal)
	{
		// Initialize
		this.m_LastTransfer = null;
		
		// Look for an empty proposal
		if (null == proposal)
		{
			this.m_LastTransfer = new ChipSet();
			return new ChipSet();
		}
		
		// Get all the colors
		Set<String> allColors = client.getGameStatus().getBoard().getColors();
		
		// Create a simulation of the current game status
		GameSimulator simulation = new GameSimulator(this.m_Scoring,
													 client.getGameStatus().getBoard(),
													 this.m_IsFirstProposer,
													 getMe().getChips(),
													 getOpponent().getChips(),
													 getMe().getPosition(),
													 getOpponent().getPosition(),
													 this.m_MyReliabilitySum,
													 this.m_OpponentReliabilitySum,
													 getMe().getMyDormantRounds(),
													 getMe().getHisDormantRounds(),
													 this.m_TransfersSoFar,
													 s_SamplingBranching);
		
		// Simulate the transfer
		simulation.transfer(proposal);

		// Save the last transfer
		ChipSet bestTransfer = simulation.getBestTransfer();
		
		// Fill with zeros
		for (String color : allColors)
		{
			if (bestTransfer.getNumChips(color) == 0)
			{
				bestTransfer.setNumChips(color, 0);
			}
		}
		
		// Save the last transfer
		this.m_LastTransfer = new ChipSet(bestTransfer);
		
		// Return the simulation's best transfer
		return bestTransfer;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		createProposal											//
	// Purpose:		Generates a proposal.									//
	// Returns:		The best simulated proposal.							//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private Proposal createProposal()
	{
		// Initialize
		this.m_LastAcceptedProposal = null;
		
		// Get all the colors
		Set<String> allColors = client.getGameStatus().getBoard().getColors();
		
		// Create a simulation of the current game status
		GameSimulator simulation = new GameSimulator(this.m_Scoring,
													 client.getGameStatus().getBoard(),
													 this.m_IsFirstProposer,
													 getMe().getChips(),
													 getOpponent().getChips(),
													 getMe().getPosition(),
													 getOpponent().getPosition(),
													 this.m_MyReliabilitySum,
													 this.m_OpponentReliabilitySum,
													 getMe().getMyDormantRounds(),
													 getMe().getHisDormantRounds(),
													 this.m_TransfersSoFar,
													 s_SamplingBranching);
		
		// Simulate the proposition
		if (this.m_FirstCommunicationsRound)
		{
			simulation.firstProposition();
		}
		else
		{
			simulation.secondProposition();
		}

		// Save the best proposal and the best transfer
		Proposal bestProposal = simulation.getBestProposal();
		
		// Fill with zeros
		for (String color : allColors)
		{
			if (bestProposal.chipsToReceive.getNumChips(color) == 0)
			{
				bestProposal.chipsToReceive.setNumChips(color, 0);
			}
			if (bestProposal.chipsToSend.getNumChips(color) == 0)
			{
				bestProposal.chipsToSend.setNumChips(color, 0);
			}
		}
		
		// Return the simulation's best proposal
		return bestProposal;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		respondToProposal										//
	// Purpose:		Responds to a proposal.									//
	// Parameters:	@ proposal - the proposal to respond to.				//
	// Returns:		A boolean which specifies whether to accept or reject.	//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private boolean respondToProposal(Proposal proposal)
	{
		logMsg("respondToProposal(): entering.");
		
		// Initialize
		this.m_LastAcceptedProposal = null;
		
		// Get all the colors
		Set<String> allColors = client.getGameStatus().getBoard().getColors();
		
		// Create a simulation of the current game status
		logMsg("respondToProposal(): my chips: (" + getMe().getChips() + ").");
		logMsg("respondToProposal(): opponent\'s chips: (" + getOpponent().getChips() + ").");
		GameSimulator simulation = new GameSimulator(this.m_Scoring,
													 client.getGameStatus().getBoard(),
													 this.m_IsFirstProposer,
													 getMe().getChips(),
													 getOpponent().getChips(),
													 getMe().getPosition(),
													 getOpponent().getPosition(),
													 this.m_MyReliabilitySum,
													 this.m_OpponentReliabilitySum,
													 getMe().getMyDormantRounds(),
													 getMe().getHisDormantRounds(),
													 this.m_TransfersSoFar,
													 s_SamplingBranching);
		
		// Simulate the response
		if (this.m_FirstCommunicationsRound)
		{
			logMsg("respondToProposal(): simulating first response.");
			simulation.firstResponse(proposal);
		}
		else
		{
			logMsg("respondToProposal(): simulating second response.");
			simulation.secondResponse(proposal);
		}
		
		// Return the simulation's best response
		logMsg("respondToProposal(): simulation is done, response: (" + simulation.getBestResponse() + ").");
		return simulation.getBestResponse();
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		onReceipt												//
	// Purpose:		Receives a discourse message.							//
	// Parameters:	@ discourseMessage - the discourse message.				//
	// Remarks:		* This method is a part of SimplePlayer implementation. //
	//				* The discourse message can either be:					//
	//				  1. A response to my previous proposal.				//
	//				  2. A new proposal by the opponent.					//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public void onReceipt(DiscourseMessage discourseMessage)
	{
		logMsg("onReceipt(): entering.");
		
		// Update scoring
		if (null == this.m_Scoring)
		{
			this.m_Scoring = client.getGameStatus().getScoring();
		}
		
		// If the message is a response to my proposal
		if (discourseMessage instanceof BasicProposalDiscussionDiscourseMessage)
		{
			logMsg("onReceipt(): message is a response to my previous proposal.");
			
			// The opponent responded -- if he accepts my proposal then save it
			BasicProposalDiscussionDiscourseMessage response = (BasicProposalDiscussionDiscourseMessage)discourseMessage;
			logMsg("onReceipt(): opponent\'s answer: (" + response.accepted() + ").");
			
			// Save information regarding to the response
			if (response.accepted())
			{
				this.m_FirstCommunicationsRound = true;
				this.m_LastAcceptedProposal = new Proposal(response.getChipsSentByResponder(), response.getChipsSentByProposer());
			}
			else
			{
				this.m_FirstCommunicationsRound = !this.m_FirstCommunicationsRound;
				this.m_LastAcceptedProposal = null;
			}
			return;
		}
		
		// If the message is a proposal
		if (discourseMessage instanceof BasicProposalDiscourseMessage)
		{
			logMsg("onReceipt(): message is a new proposal.");
			
			// The message is a proposal
			BasicProposalDiscourseMessage proposal = (BasicProposalDiscourseMessage)discourseMessage;
			Proposal proposalToConsider = new Proposal(proposal.getChipsSentByProposer(), proposal.getChipsSentByResponder()); 
			logMsg("onReceipt(): opponent\'s proposal: (" + proposalToConsider.chipsToReceive + ", " + proposalToConsider.chipsToSend + ").");
			
			// Build a response to the proposal
			BasicProposalDiscussionDiscourseMessage response = new BasicProposalDiscussionDiscourseMessage(proposal);
			
			// If the proposal should be accepted by us
			if (respondToProposal(proposalToConsider))
			{
				this.m_LastAcceptedProposal = proposalToConsider;
				this.m_FirstCommunicationsRound = true;
				response.acceptOffer();
			}
			else
			{
				this.m_LastAcceptedProposal = null;
				this.m_FirstCommunicationsRound = !this.m_FirstCommunicationsRound;
				response.rejectOffer();
			}
			
			// Send the response
			client.communication.sendDiscourseRequest(response);
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		roleChanged												//
	// Purpose:		Called whenever a role has changed.						//
	// Remarks:		* This method is a part of SimplePlayer implementation. //
	//				* The discourse message can either be:					//
	//				  1. A response to my previous proposal.				//
	//				  2. A new proposal by the opponent.					//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public void roleChanged()
	{
		logMsg("roleChanged(): entering.");
		
		// Update scoring
		if (null == this.m_Scoring)
		{
			this.m_Scoring = client.getGameStatus().getScoring();
		}
		
		// Get the current phase
		String phaseName = client.getGameStatus().getPhases().getCurrentPhaseName();
		logMsg("roleChanged(): current phase is \'" + phaseName + "\'.");
		
		// Communication phase 
		if (phaseName.equals("Communication Phase"))
		{
			String roleStr = getMe().getRole();
			logMsg("roleChanged(): new role is \'" + roleStr + "\'.");
			
			// If we are a proposer we should make a proposal
			if (roleStr.equals("Proposer"))
			{
				// Get my proposal
				Proposal myProposal = createProposal();
				
				// Get the IDs
				int proposerId = getMe().getPerGameId();
				int responderId = getOpponent().getPerGameId();
				
				// Create the proposal message
				BasicProposalDiscourseMessage proposal = new BasicProposalDiscourseMessage(proposerId, responderId, -1, myProposal.chipsToSend, myProposal.chipsToReceive);
				client.communication.sendDiscourseRequest(proposal);
			}
		}
	}	
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getChipsThatWereSentToMe								//
	// Purpose:		Conclude the chips that were sent to me.				//
	// Remarks:		* Must be called during the movement phase.				//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private ChipSet getChipsThatWereSentToMe()
	{
		// My chips before the transfer
		ChipSet chipsBeforeTransfer = this.m_MyLastChips;
		logMsg("getChipsThatWereSentToMe(): my last chips (" + chipsBeforeTransfer + ").");
		
		// The chips that I sent
		ChipSet chipsSentByMe = new ChipSet();
		if (this.m_LastAcceptedProposal != null)
		{
			chipsSentByMe = this.m_LastTransfer;
		}
		logMsg("getChipsThatWereSentToMe(): chips sent by me (" + chipsSentByMe + ").");

		// My current chips
		ChipSet myCurrentChips = getMe().getChips();
		logMsg("getChipsThatWereSentToMe(): my current chips (" + myCurrentChips + ").");
		
		// The chips after I sent them but without receiving any
		ChipSet chipsAfterSendingWithoutReceiving = new ChipSet();
		if (chipsBeforeTransfer.contains(chipsSentByMe))
		{
			chipsAfterSendingWithoutReceiving = ChipSet.subChipSets(chipsBeforeTransfer, chipsSentByMe);
		}
		
		// The chips that were sent to me
		ChipSet chipsSentToMe = ChipSet.subChipSets(myCurrentChips, chipsAfterSendingWithoutReceiving);
		for (String color : chipsSentToMe.getColors())
		{
			if (chipsSentToMe.getNumChips(color) < 0)
			{
				chipsSentToMe.set(color, 0);
			}
		}
		logMsg("getChipsThatWereSentToMe(): chips sent to me (" + chipsSentToMe + ").");
		
		// Return the result
		return chipsSentToMe;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		updateReliabilitiesAfterTransfer						//
	// Purpose:		Update reliabilities after a transfer.					//
	// Remarks:		* Should be called from the movement phase.				//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private void updateReliabilitiesAfterTransfer()
	{
		// If a proposal was not accepted
		if (this.m_LastAcceptedProposal == null)
		{
			return;
		}
		
		logMsg("updateReliabilitiesAfterTransfer(): last accepted proposal (" + this.m_LastAcceptedProposal.chipsToReceive + ", " + this.m_LastAcceptedProposal.chipsToSend + ").");

		// Get the goal position
		RowCol goalPosition = client.getGameStatus().getBoard().getGoalLocations().get(0);
		
		// Get chips
		ChipSet myChips = getMe().getChips();
		ChipSet opponentChips = getOpponent().getChips();
		logMsg("updateReliabilitiesAfterTransfer(): my new chips (" + myChips + ").");
		logMsg("updateReliabilitiesAfterTransfer(): opponent\'s new chips (" + opponentChips + ").");
		
		// Get the chips that were sent by the players
		ChipSet chipsSentByMe = new ChipSet();
		if (null != this.m_LastTransfer)
		{
			chipsSentByMe = this.m_LastTransfer;
		}
		ChipSet chipsSentByOpponent = getChipsThatWereSentToMe();
		logMsg("updateReliabilitiesAfterTransfer(): chips sent by me (" + chipsSentByMe + ").");
		logMsg("updateReliabilitiesAfterTransfer(): chips ment to be sent by me (" + this.m_LastAcceptedProposal.chipsToSend + ").");
		logMsg("updateReliabilitiesAfterTransfer(): chips sent by opponent (" + chipsSentByOpponent + ").");
		logMsg("updateReliabilitiesAfterTransfer(): chips ment to be sent by opponent (" + this.m_LastAcceptedProposal.chipsToReceive + ").");
		
		// Get my round reliability	and update overall reliability
		double myRoundReliability = Reliability.getRoundReliability(getOpponent().getPosition(),
																	goalPosition,
																	this.m_Scoring,
																	this.m_OpponentLastChips,
																	this.m_LastAcceptedProposal.chipsToSend,
																	chipsSentByMe,
																	this.m_OpponentScoreBeforeTransfer);
		logMsg("updateReliabilitiesAfterTransfer(): my reliability for this round (" + myRoundReliability + ").");
		this.m_MyReliabilitySum = Reliability.getAccumulatedReliability(this.m_MyReliabilitySum, myRoundReliability, this.m_TransfersSoFar);
		logMsg("updateReliabilitiesAfterTransfer(): my new reliability sum (" + this.m_MyReliabilitySum + ").");
		
		// Get opponent's round reliability and update overall reliability
		double opponentRoundReliability = Reliability.getRoundReliability(getMe().getPosition(),
																		  goalPosition,
																		  this.m_Scoring,
																		  this.m_MyLastChips,
																		  this.m_LastAcceptedProposal.chipsToReceive,
																		  chipsSentByOpponent,
																		  this.m_MyScoreBeforeTransfer);
		logMsg("updateReliabilitiesAfterTransfer(): opponent\'s reliability for this round (" + opponentRoundReliability + ").");
		this.m_OpponentReliabilitySum = Reliability.getAccumulatedReliability(this.m_OpponentReliabilitySum, opponentRoundReliability, this.m_TransfersSoFar);
		logMsg("updateReliabilitiesAfterTransfer(): opponent\'s new reliability (" + this.m_OpponentReliabilitySum + ").");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		phaseAdvanced											//
	// Purpose:		A callback for phase advances.							//
	// Parameters:	@ discourseMessage - the discourse message.				//
	// Remarks:		* This method is a part of SimplePlayer implementation. //
	//				* The discourse message can either be:					//
	//				  1. A response to my previous proposal.				//
	//				  2. A new proposal by the opponent.					//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public void phaseAdvanced(Phases phases)
	{
		logMsg("phaseAdvanced(): entering.");

		// Update scoring
		if (null == this.m_Scoring)
		{
			this.m_Scoring = client.getGameStatus().getScoring();
		}
		
		// Get the current phase
		String phaseName = client.getGameStatus().getPhases().getCurrentPhaseName();
		logMsg("phaseAdvanced(): new phase is \'" + phaseName + "\'.");
		
		// Communication phase 
		if (phaseName.equals("Communication Phase"))
		{
			// Get the role
			String roleStr = getMe().getRole();
			logMsg("phaseAdvanced(): new role is \'" + roleStr + "\'.");
			
			// Deduce whether I'm the first proposer
			this.m_IsFirstProposer = roleStr.equals("Proposer");
			
			// Make an offer if I'm the proposer
			if (this.m_IsFirstProposer)
			{
				// Get my proposal
				Proposal myProposal = createProposal();
				
				// Get the IDs
				int proposerId = getMe().getPerGameId();
				int responderId = getOpponent().getPerGameId();
				
				// Create the proposal message
				BasicProposalDiscourseMessage proposal = new BasicProposalDiscourseMessage(proposerId, responderId, -1, myProposal.chipsToSend.removeZeros(), myProposal.chipsToReceive.removeZeros());
				client.communication.sendDiscourseRequest(proposal);
			}
		}
	
		// Move phase
		if (phaseName.equals("Movement Phase"))
		{
			// Update reliabilities
			updateReliabilitiesAfterTransfer();
			
			// Check if the number of dormant rounds requires moving
			if (getMe().getMyDormantRounds() >= s_MaxDormantRounds)
			{
				logMsg("phaseAdvanced(): dormant rounds requires moving.");
				
				// Try to move if it is possible
				RowCol goalPosition = client.getGameStatus().getBoard().getGoalLocations().get(0);

				// Looking for the paths towards the goal
				ArrayList<Path> paths = ShortestPaths.getShortestPaths(getMe().getPosition(), goalPosition, client.getGameStatus().getBoard(), this.m_Scoring, 10, getMe().getChips(), getOpponent().getChips());
				logMsg("phaseAdvanced(): found paths (" + paths.size() + ").");

				// Iterate the paths
				for (Path path : paths)
				{
					RowCol pointToMove = path.getPoint(1);
					
					// Get the needed color for the path
					String neededColor = client.getGameStatus().getBoard().getSquare(pointToMove).getColor();
					
					// Check if the color is in the current chips
					if (getMe().getChips().getNumChips(neededColor) > 0)
					{
						logMsg("phaseAdvanced(): moving.");
						client.communication.sendMoveRequest(pointToMove);
						break;
					}
				}
			}
		}
		
		// Exchange phase
		if (phaseName.equals("Exchange Phase"))
		{
			// Save the last chips
			this.m_MyLastChips = new ChipSet(getMe().getChips());
			this.m_OpponentLastChips = new ChipSet(getOpponent().getChips());
			
			// Get the goal position
			RowCol goalPosition = client.getGameStatus().getBoard().getGoalLocations().get(0);
			
			// Save the last player scores
			this.m_MyScoreBeforeTransfer = Reliability.getPlayerScore(getMe().getPosition(), this.m_MyLastChips, goalPosition, this.m_Scoring);
			this.m_OpponentScoreBeforeTransfer = Reliability.getPlayerScore(getOpponent().getPosition(), this.m_OpponentLastChips, goalPosition, this.m_Scoring);
			
			// Send only if we accepted a proposal
			ChipSet chipsToTransfer = new ChipSet();
			if (this.m_LastAcceptedProposal != null)
			{
				chipsToTransfer = getChipsToTransfer(this.m_LastAcceptedProposal);
				if (!(this.m_LastAcceptedProposal.chipsToSend.contains(chipsToTransfer)))
				{
					System.out.println("Somehow got something wrong...");
					chipsToTransfer = new ChipSet(getMe().getChips());
					this.m_LastTransfer = new ChipSet(chipsToTransfer);
				}
			}
			client.communication.sendTransferRequest(getOpponent().getPerGameId(), chipsToTransfer);
			
			// Update the number of transfers
			if (this.m_LastAcceptedProposal != null)
			{
				this.m_TransfersSoFar++;
				logMsg("phaseAdvanced(): transfers so far (" + (this.m_TransfersSoFar - 1) + ").");
			}
			
			// Finish
			this.m_LastAcceptedProposal = null;
		}
		
		// Feedback phase
		if (phaseName.equals("Feedback Phase"))
		{
			// Initialize everything
			this.m_LastTransfer = null;
			this.m_LastAcceptedProposal = null;
		}
		
		// Strategy prepreration phase
		if (phaseName.equals("Strategy Prep Phase"))
		{
			// Nothing to do here...
		}
		
	}	
}
