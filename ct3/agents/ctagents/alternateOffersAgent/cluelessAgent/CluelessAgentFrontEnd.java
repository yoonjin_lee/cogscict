//////////////////////////////////////////////////////////////////////////
// File:		CluelessAgentFrontEnd.java 				   				//
// Purpose:		Implements the front end of Clueless.					//
//////////////////////////////////////////////////////////////////////////
package ctagents.alternateOffersAgent.cluelessAgent;

//Imports from Java common framework
import java.util.*;

//////////////////////////////////////////////////////////////////////////
// Class:		CluelessAgentFrontEnd				    				//
// Purpose:		Implements the front end of Clueless.					//
//////////////////////////////////////////////////////////////////////////
public final class CluelessAgentFrontEnd 
{
	//////////////////////////////////////////////////////////////////////////
	// Method:		main							    					//
	// Purpose:		Implements the main function, which starts the player.	//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public static void main(String[] args)
	{
		// Parse arguments
		int samplingBranching = Integer.parseInt(args[1]);
		double initialReliability = Double.parseDouble(args[2]);
		double lastRoundReliabilityWeight = Double.parseDouble(args[3]);
		double myScoreWeight = Double.parseDouble(args[4]);
		
		// Create the agent
		CluelessPlayer agent = new CluelessPlayer(samplingBranching, initialReliability, lastRoundReliabilityWeight, myScoreWeight);
		
		// Give the agent a proper PIN number and start it
		agent.setClientName(args[0]);
		agent.start();
	}
}
