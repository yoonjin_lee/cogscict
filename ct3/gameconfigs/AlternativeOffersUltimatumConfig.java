/*
	Colored Trails

	Copyright (C) 2006-2007, President and Fellows of Harvard College.  All Rights Reserved.

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import edu.harvard.eecs.airg.coloredtrails.server.ServerData;
import edu.harvard.eecs.airg.coloredtrails.server.ServerPhases;
import edu.harvard.eecs.airg.coloredtrails.shared.Constants;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.DiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.BasicProposalDiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.BasicProposalDiscussionDiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.RoleChangedMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.RoundNotificationMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.TransferDiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.types.*;
import edu.harvard.eecs.airg.coloredtrails.alglib.BestUse;
import edu.harvard.eecs.airg.coloredtrails.alglib.ShortestPaths;
import edu.harvard.eecs.airg.coloredtrails.controller.AltOffersGameDetails;
import edu.harvard.eecs.airg.coloredtrails.shared.Scoring;
import edu.harvard.eecs.airg.coloredtrails.shared.GameBoardCreator;
import edu.harvard.eecs.airg.coloredtrails.shared.VideoServerConnection;

import java.io.IOException;
import java.lang.Boolean;
import java.util.*;

//import com.sun.tools.javac.tree.Tree.Case;

import ctagents.FileLogger;

/**
 * This configuration implements much of the setup and functionality. Including
 * automatic exchange after accept an offer, and automatic movement.
 */
public class AlternativeOffersUltimatumConfig extends GameConfigDetailsRunnable
		implements PhaseChangeHandler {

	private static final long serialVersionUID = -3751478802171903828L;

	// gs is a member variable of GameConfigDetailsRunnable,
	// defined as: protected ServerGameStatus gs;
	/**
	 * The scoring function used for players in the game. 100 for a player
	 * reaching the goal, -10 per unit distance if the player does not reach the
	 * goal, 5 for each chip remaining after the player has reached the goal or
	 * cannot move any farther towards the goal.
	 */
	Scoring s = new Scoring(100, -10, 5);

	/** Local random generator for creating chipsets */
	static Random localrand = new Random();

	/** determines if there will be automatic movement */
	boolean automaticMovement = false;
	/**
	 * determines if the chips will automatically transfer after a proposal has
	 * been accepted
	 */
	boolean automaticChipTransfer = false;
	/** determines if the phases will loop. */
	boolean phaseLoop = true;// false;

	// The following two booleans will help us regulate the transfer of chips so
	// that the actual sending is performed
	// 1)only at the end of the time of the exchange phase
	// or 2) if both players have already "pressed" the send button
	// the idea is that we don't want each player to know before sending (or
	// not) her chips whether the
	// other player has sent (or not) his chips.
	// Each time the protocol gets a transfer request it turns the respective
	// boolean on and saves the chipset to transfer
	// until both players have requested to transfer or the exchange phase time
	// is up.
	boolean bProposerRequestedTransfer = false;
	boolean bResponderRequestedTransfer = false;
	ChipSet csProposerToTransfer = null;
	ChipSet csResponderToTransfer = null;

	// Number of rounds to play, each round we switch roles
	int numOfRounds = 3;
	int round = 0;
	String CurrentPhaseName = null;

	// used for switching roles.
	int proposerID = 0;
	int responderID = 1;

	// Keep track of the first player to propose in each round.
	// In the communication phase the first to propose alternates from round to
	// round.
	// Also within each communication phase the player that got a proposal from
	// the proposer
	// is entitled to reject and if wanted to make a counter offer. After that
	// no more offers are allowed until the next round.
	// Possible cases:
	// 1) Player 1 makes an offer, Player 2 accepts it --> next phase
	// 2) Player 1 makes an offer, Player 2 rejects, Player 2 makes counter
	// offer, Player 1 either accepts or reject --> in any case next phase
	// 3) Player 1 makes an offer, Player 2 rejects, Player 2 doesn't want to
	// counter offer--> wait until the end of the time of the phase and then
	// next phase
	// 4) In second round Player 2 swap roles with Player 1, that is he is the
	// one to make the first offer. Game continues as explained above.
	Vector<Integer> vFirstProposerInRound = new Vector<Integer>();

	// Number of counter offers allowed in each round
	static final int maxNumOfCounterOffers = 1;
	int counterOffersCount = 0;
	boolean[] bPlayersMovedInRound = new boolean[2];// each player is allowed to
													// move one step at a time,
	// that is, at each movement phase

	Vector<Boolean> bPlayer1MovedInRound = new Vector<Boolean>();
	Vector<Boolean> bPlayer2MovedInRound = new Vector<Boolean>();

	private int prepPhaseCounter = 0;

	// permission flags
	boolean ProposerCommunicationAllowed = false;
	boolean ProposerRevelationAllowed = false;
	boolean ProposerTransfersAllowed = false;
	boolean ProposerMovesAllowed = false;
	boolean ResponderCommunicationAllowed = false;
	boolean ResponderRevelationAllowed = false;
	boolean ResponderTransfersAllowed = false;
	boolean ResponderMovesAllowed = false;

	private String logName;
	private static String LOG_PREFIX = "AlternativeOffersConfig_";

	private VideoServerConnection videoServer;
	private boolean useVideoServer;

	public void resetPermissionFlags() {
		ProposerCommunicationAllowed = false;
		ProposerRevelationAllowed = false;
		ProposerTransfersAllowed = false;
		ProposerMovesAllowed = false;
		ResponderCommunicationAllowed = false;
		ResponderRevelationAllowed = false;
		ResponderTransfersAllowed = false;
		ResponderMovesAllowed = false;
	}

	public void setPermissions() {
		PlayerStatus Responder = gs.getPlayerByPerGameId(responderID);
		PlayerStatus Proposer = gs.getPlayerByPerGameId(proposerID);

		Responder.setCommunicationAllowed(ResponderCommunicationAllowed);

		Responder.setTransfersAllowed(ResponderTransfersAllowed);
		Responder.setMovesAllowed(ResponderMovesAllowed);

		Proposer.setCommunicationAllowed(ProposerCommunicationAllowed);

		Proposer.setTransfersAllowed(ProposerTransfersAllowed);
		Proposer.setMovesAllowed(ProposerMovesAllowed);
	}

	public void swapRoles() {
		int tempID = proposerID;
		proposerID = responderID;
		responderID = tempID;
		gs.getPlayerByPerGameId(proposerID).setRole("Proposer");
		gs.getPlayerByPerGameId(responderID).setRole("Responder");
	}

	public AlternativeOffersUltimatumConfig() {

		// Setting the log name according to the timestemp
		Long currTS = (new Date()).getTime();
		logName = LOG_PREFIX + currTS;

		getLog().writeln("A new Alternative Offers game has began");
		vFirstProposerInRound.add(proposerID);
	}

	/**
	 * Returns score of specified player, according to player's current state
	 */
	public int getPlayerScore(PlayerStatus ps) {
		RowCol gpos = gs.getBoard().getGoalLocations().get(0); // get first goal
																// in list
		int sc = (int) Math.floor(s.score(ps, gpos)); // should change to double
														// at some point
		return sc;
	}

	/**
	 * Called by GameConfigDetailsRunnable methods when calculation and
	 * assignment of player scores is desired
	 */
	protected void assignScores() {
		for (PlayerStatus ps : gs.getPlayers()) {
			ps.setScore(getPlayerScore(ps));
			getLog().writeln(
					"assignScores; Player: " + ps.getPin() + "  Score: "
							+ ps.getScore());
		}
	}

	public int getMaxNonConsecRounds() {
		return numOfRounds;
	}

	/**
	 * Called by server when a phase begins
	 */
	public void beginPhase(String phasename) {
		getLog().writeln("Protocol: A New Phase Began: " + phasename);

		CurrentPhaseName = phasename;
		resetPermissionFlags();

		if (phasename.equals(ServerPhases.COMM_PH)) {
			getLog().writeln("Protocol: Communication phase, Round: " + round);
			// Tell the action history window which round we are in, for
			// display.
			ServerData sd = ServerData.getInstance();

			// RoundNotificationMessage rnm = new
			// RoundNotificationMessage(proposerID, responderID, -1, round + 1);
			// sd.sendDiscourseMessage(gs,rnm);

			RoundNotificationMessage rnm = new RoundNotificationMessage(
					proposerID, responderID, -1, round + 1);
			sd.sendDiscourseMessage(gs, rnm);

			ProposerCommunicationAllowed = true;
			// At the beginning of the iteration no player has moved yet.
			bPlayersMovedInRound[0] = false;
			bPlayersMovedInRound[1] = false;

		} else if (phasename.equals(ServerPhases.EXCH_PH)) {

			getLog().writeln("Protocol: Exchange phase, Round: " + round);
			ProposerTransfersAllowed = true;
			ResponderTransfersAllowed = true;
			bProposerRequestedTransfer = false;
			bResponderRequestedTransfer = false;
			counterOffersCount = 0;
			int temp1 = (Integer) gs.getPlayerByPerGameId(0).get(
					"counterOffersCount");
			getLog().writeln(
					"In begin exch.phase, get counter offers returned: "
							+ temp1);
		} else if (phasename.equals(ServerPhases.MOVEMENT_PH)) {
			getLog().writeln("Protocol: Movement phase, Round: " + round);
			ProposerMovesAllowed = true;
			ResponderMovesAllowed = true;
			if (automaticMovement) {
				doAutomaticMovement(s);

				// calculate scores after all players have moved
				// (e.g, in case a player's score depends on others' locations)
				assignScores();
			}

			if (prepPhaseCounter == 0) {
				ServerPhases ph = gs.getServerPhases();
				ph.removePhase(ServerPhases.STRATEGY_PH);
				prepPhaseCounter++;
			}
		} else if (phasename.equals(ServerPhases.FEEDBACK_PH)) {
			getLog().writeln("Protocol: Feedback phase, Round: " + round);

			if (bPlayersMovedInRound[0])
				bPlayer1MovedInRound.add(new Boolean(true));
			else
				bPlayer1MovedInRound.add(new Boolean(false));

			if (bPlayersMovedInRound[1])
				bPlayer2MovedInRound.add(new Boolean(true));
			else
				bPlayer2MovedInRound.add(new Boolean(false));

			int consecutiveNoMovementCounterPlayer1 = 0;
			int consecutiveNoMovementCounterPlayer2 = 0;
			Iterator<Boolean> vIter1 = bPlayer1MovedInRound.iterator();
			boolean bMoved = false;
			while (vIter1.hasNext()) {
				bMoved = ((Boolean) vIter1.next()).booleanValue();
				if (bMoved)
					consecutiveNoMovementCounterPlayer1 = 0;
				else
					consecutiveNoMovementCounterPlayer1++;

			}

			Iterator<Boolean> vIter2 = bPlayer2MovedInRound.iterator();
			bMoved = false;
			while (vIter2.hasNext()) {
				bMoved = ((Boolean) vIter2.next()).booleanValue();
				if (bMoved)
					consecutiveNoMovementCounterPlayer2 = 0;
				else
					consecutiveNoMovementCounterPlayer2++;

			}

			// This way we can let each player know how many consecutive no
			// movements the OTHER has
			gs.getPlayerByPerGameId(0).set("opConsecutiveNoMovement",
					consecutiveNoMovementCounterPlayer2);
			gs.getPlayerByPerGameId(1).set("opConsecutiveNoMovement",
					consecutiveNoMovementCounterPlayer1);
			getLog().writeln(
					"Number of consecutive no movements for player "
							+ gs.getPlayerByPerGameId(0).getPin() + " is "
							+ consecutiveNoMovementCounterPlayer1);
			getLog().writeln(
					"Number of consecutive no movements for player "
							+ gs.getPlayerByPerGameId(1).getPin() + " is "
							+ consecutiveNoMovementCounterPlayer2);
			// update game status about dormant moves
			gs.getPlayerByPerGameId(0).setMyNumDormantRounds(
					consecutiveNoMovementCounterPlayer1);
			gs.getPlayerByPerGameId(0).setHisNumDormantRounds(
					consecutiveNoMovementCounterPlayer2);

			gs.getPlayerByPerGameId(1).setMyNumDormantRounds(
					consecutiveNoMovementCounterPlayer2);
			gs.getPlayerByPerGameId(1).setHisNumDormantRounds(
					consecutiveNoMovementCounterPlayer1);

			if ((consecutiveNoMovementCounterPlayer1 >= numOfRounds)
					|| (consecutiveNoMovementCounterPlayer2 >= numOfRounds)) {

				endGame("Ending game, all players move as much as they can towards goal");
				return;
			}

			// Check if both players already have all the chips they need
			// to reach the goal. If so, then end the game, it doesn't make
			// sense to negotiate on the chips if
			// both players have what they need.
			int count = 0;
			for (PlayerStatus player : gs.getPlayers()) {
				if (isThereSatisfiablePath(player))
					count++;
			}

			// if all the players have the chips they need end the game

			if (count == gs.getPlayers().size()) {
				ProposerCommunicationAllowed = false;

				endGame("All players have all needed chips to get to goal, end the game");
				return;
			}

			round++;

		}

		else if (phasename.equals(ServerPhases.STRATEGY_PH)) {
			getLog().writeln(
					"Protocol: Strategy Preparation phase, Round: " + round);
			getLog().writeln(
					"Maximum number of rounds with no consecutive movement is "
							+ numOfRounds);
			gs.getPlayerByPerGameId(0).set("maxRoundNoMovement", numOfRounds);
			gs.getPlayerByPerGameId(1).set("maxRoundNoMovement", numOfRounds);
			// This way we can let each player know how many consecutive no
			// movements the OTHER has
			gs.getPlayerByPerGameId(0).set("opConsecutiveNoMovement", 0);
			gs.getPlayerByPerGameId(1).set("opConsecutiveNoMovement", 0);
			gs.getPlayerByPerGameId(0).set("counterOffersCount",
					counterOffersCount);
			gs.getPlayerByPerGameId(1).set("counterOffersCount",
					counterOffersCount);
			getLog().writeln(
					"In begin comm. phase, set counter offers count to: "
							+ counterOffersCount);
			int temp1 = (Integer) gs.getPlayerByPerGameId(0).get(
					"counterOffersCount");
			getLog().writeln(
					"In begin comm. phase, get counter offers returned: "
							+ temp1);

		}

		if (useVideoServer) {
			videoServer.sendTickMessage(phasename);
		}
		
		setPermissions();
	}

	/**
	 * Called by server when a phase ends
	 */
	public void endPhase(String phasename) {
		getLog().writeln("A Phase Ended: " + phasename);

		// if end of feedback phase
		if (phasename.equals(ServerPhases.FEEDBACK_PH)) {
			gs.sendArbitraryMessage(Constants.NEWPHASE);
		} else if (phasename.equals(ServerPhases.COMM_PH)) {
			gs.sendArbitraryMessage(Constants.NEWPHASE);
		} else if (phasename.equals(ServerPhases.EXCH_PH)) {
			// Got to the end of exchange phase, now we can actually transfer
			// whatever has been requested.
			doTransfer();

			resetPermissionFlags();
			// swapRoles();
			ProposerCommunicationAllowed = true;
			setPermissions();

			// We had to set moves allowed to true here because the agent tried
			// making its first movement before
			// we had time at the beginning of the movement phase to set moves
			// to allowed to true, causing the agent to not move.
			gs.getPlayerByPerGameId(responderID).setMovesAllowed(true);
			// getLog().writeln("set moves allowed for responder(" + responderID
			// + ") to " + true);

			gs.getPlayerByPerGameId(proposerID).setMovesAllowed(true);
			// getLog().writeln("set moves allowed for proposer(" + proposerID +
			// ") to " + true);

		} else if (phasename.equals(ServerPhases.MOVEMENT_PH)) {
			// check if the player designated to be the proposer had already
			// been the proposer in the last
			// round if so, swap roles, otherwise just leave it like that. (This
			// can happen in the case a counter offer was made
			// in last round
			if (vFirstProposerInRound.get(round) == proposerID) {
				swapRoles();
				getLog().writeln("Swaping roles:");
				getLog().writeln(
						"Proposer is: "
								+ gs.getPlayerByPerGameId(proposerID).getPin());
				getLog().writeln(
						"Responder is: "
								+ gs.getPlayerByPerGameId(responderID).getPin());
			}
			vFirstProposerInRound.add(proposerID);

		}
	}

	public boolean doTransfer(int perGameId, int toPerGameId, ChipSet chips) {

		// getLog().writeln("In doTransfer");
		if (perGameId == proposerID) {
			bProposerRequestedTransfer = true;
			csProposerToTransfer = chips;
			// getLog().writeln("Sender is the proposer sending: " + chips);
		} else if (perGameId == responderID) {
			bResponderRequestedTransfer = true;
			csResponderToTransfer = chips;
			// getLog().writeln("Sender is the responder sending: " + chips);
		}

		// if both players requested to transfer chips then we can finish this
		// phase.
		// At the end of this phase whatever needs to be transfered will be
		// transfered
		if (bProposerRequestedTransfer && bResponderRequestedTransfer) {
			gs.getPhases().advancePhase();
		}
		return true;
	}

	private boolean doTransfer() {
		boolean bSent1 = false;
		boolean bSent2 = false;
		// getLog().writeln("bProposerRequestedTransfer is " +
		// bProposerRequestedTransfer);
		// getLog().writeln("bResponderRequestedTransfer is " +
		// bResponderRequestedTransfer);
		if (bProposerRequestedTransfer) {
			bSent1 = super.doTransfer(proposerID, responderID,
					csProposerToTransfer);
			// kobi. Update transfer message in history window
			ServerData sd = ServerData.getInstance();

			TransferDiscourseMessage tdm = new TransferDiscourseMessage(
					proposerID, responderID, -1, csProposerToTransfer);
			sd.sendDiscourseMessage(gs, tdm);
			getLog().writeln(
					"Proposer " + gs.getPlayerByPerGameId(proposerID).getPin()
							+ " sent: " + csProposerToTransfer);
		}
		if (bResponderRequestedTransfer) {
			ServerData sd = ServerData.getInstance();
			bSent2 = super.doTransfer(responderID, proposerID,
					csResponderToTransfer);
			TransferDiscourseMessage tdm = new TransferDiscourseMessage(
					responderID, proposerID, -1, csResponderToTransfer);
			sd.sendDiscourseMessage(gs, tdm);
			getLog().writeln(
					"Responder "
							+ gs.getPlayerByPerGameId(responderID).getPin()
							+ " sent: " + csResponderToTransfer);
		}

		// getLog().writeln("bSent1 && bSent2 are: " + bSent1 + ", " + bSent2);
		if (bSent1 && bSent2)
			return true;
		return false;

	}

	public void doAutomaticMovement(Scoring s) {
		getLog().writeln("AUTOMATIC MOVEMENT");

		// for each player in game, make best use of their chips and move
		// accordingly
		for (PlayerStatus ps : gs.getPlayers()) {
			ps.setMovesAllowed(true); // allow movement
			BestUse bu = new BestUse(gs, ps, s, 0); // calculate the best use of
													// player's chips
			Path p = bu.getPaths().get(0); // use the first best-path in list
			gs.doPathMove(ps.getPerGameId(), new Path(p.getStartPoint())); // move
																			// the
																			// player
																			// one
																			// step
																			// at
																			// a
																			// time
			getLog().writeln(bu.toString()); // print BU instance to console
			getLog().writeln(p.getStartPoint().toString()); // print first point
															// of path to
															// console
		}
	}

	public boolean doMove(int perGameId, RowCol newpos) {
		// If this player was already moved in this round then don't allow
		// it to move, otherwise move.
		boolean bMoved = false;
		if (!bPlayersMovedInRound[perGameId]) {
			// getLog().writeln("Player " + perGameId +
			// "didn't move in round yet, try to move it to " + newpos);
			bMoved = gs.doMove(perGameId, newpos);
			if (bMoved) {
				bPlayersMovedInRound[perGameId] = true;
				getLog().writeln(
						"Player " + gs.getPlayerByPerGameId(perGameId).getPin()
								+ " moved to " + newpos);
			}
		} else
			getLog().writeln(
					"Player " + gs.getPlayerByPerGameId(perGameId).getPin()
							+ "already moved in this round, can't move it now");

		// if perGameId player made it to the goal, then have the other advance
		// automatically towards the
		// goal and end the game:
		RowCol goalPos = gs.getBoard().getGoalLocations().get(0); // get first
																	// goal in
																	// list
		PlayerStatus ps = gs.getPlayerByPerGameId(perGameId);
		if (ps.getPosition().equals(goalPos)) {
			// Pause for 4 seconds
			try {
				Thread.sleep(4000);
				getLog().writeln("Slept 4 seconds");
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
			}

			endGame("Player " + ps.getPin()
					+ " got to goal, advance the other and end game");
		}

		return bMoved;
	}

	// Override super method do discourse in order to make an automatic transfer
	// after accept a proposal
	public boolean doDiscourse(DiscourseMessage dm) {
		getLog().writeln("Received communication message ");
		// getLog().writeln("Class: " + dm.getClass() );
		getLog().writeln(
				"From player: "
						+ gs.getPlayerByPerGameId(dm.getFromPerGameId())
								.getPin());
		getLog().writeln(
				"To player: "
						+ gs.getPlayerByPerGameId(dm.getToPerGameId()).getPin());
		getLog().writeln("Message type: " + dm.getMsgType());

		boolean result = gs.doDiscourse(dm);
		if (dm instanceof BasicProposalDiscussionDiscourseMessage) {
			// getLog().writeln(
			// "---- BasicProposalDiscussionDiscourseMessage ----" );

			BasicProposalDiscussionDiscourseMessage bpddm = (BasicProposalDiscussionDiscourseMessage) dm;

			if (bpddm.accepted()) {
				getLog().writeln(
						"Player "
								+ gs.getPlayerByPerGameId(
										bpddm.getFromPerGameId()).getPin()
								+ " accepted offer");
				counterOffersCount = 0;
				gs.getPlayerByPerGameId(0).set("counterOffersCount",
						counterOffersCount);
				gs.getPlayerByPerGameId(1).set("counterOffersCount",
						counterOffersCount);
				getLog().writeln(
						"Prop accepted, set counter offers count to: "
								+ counterOffersCount);
				int temp1 = (Integer) gs.getPlayerByPerGameId(0).get(
						"counterOffersCount");
				getLog().writeln(
						"Prop accepted, get counter offers returned: " + temp1);
				gs.getPhases().advancePhase();
			} else {

				// The offer was rejected. closing the discussion windows
				gs.sendArbitraryMessage(Constants.ENDDISCUSSION);

				if (counterOffersCount < maxNumOfCounterOffers) {
					getLog().writeln(
							"Player "
									+ gs.getPlayerByPerGameId(
											bpddm.getFromPerGameId()).getPin()
									+ " rejected offer");
					
					if (useVideoServer) {
						videoServer.sendTickMessage("Counter Offer");
					}
					resetPermissionFlags();
					swapRoles();
					getLog().writeln("Swaping roles:");
					getLog().writeln(
							"Proposer is: "
									+ gs.getPlayerByPerGameId(proposerID)
											.getPin());
					getLog().writeln(
							"Responder is: "
									+ gs.getPlayerByPerGameId(responderID)
											.getPin());
					ProposerCommunicationAllowed = true;
					setPermissions();
					counterOffersCount++;
					gs.getPlayerByPerGameId(0).set("counterOffersCount",
							counterOffersCount);
					gs.getPlayerByPerGameId(1).set("counterOffersCount",
							counterOffersCount);
					getLog().writeln(
							"Prop rejected, set counter offers count to: "
									+ counterOffersCount);
					int temp1 = (Integer) gs.getPlayerByPerGameId(0).get(
							"counterOffersCount");
					getLog().writeln(
							"Prop rejected, get counter offers returned: "
									+ temp1);
					// Pause for 4 seconds
					try {
						Thread.sleep(4000);
						getLog().writeln("Slept 4 seconds");
					} catch (InterruptedException e) {
						System.out.println(e.getMessage());
					}
					
					ServerData sd = ServerData.getInstance();
					RoleChangedMessage rcm1 = new RoleChangedMessage(
							proposerID, "Proposer");
					sd.sendRoleChangedMessage(gs, rcm1);
					RoleChangedMessage rcm2 = new RoleChangedMessage(
							responderID, "Responder");
					sd.sendRoleChangedMessage(gs, rcm2);
					getLog().writeln("Sent role changed msgs");
				} else {
					getLog().writeln(
							"Player "
									+ gs.getPlayerByPerGameId(
											bpddm.getFromPerGameId()).getPin()
									+ " rejected offer");
					counterOffersCount = 0;
					gs.getPlayerByPerGameId(0).set("counterOffersCount",
							counterOffersCount);
					gs.getPlayerByPerGameId(1).set("counterOffersCount",
							counterOffersCount);
					getLog().writeln(
							"Set counter offers count to: "
									+ counterOffersCount);
					gs.getPhases().advancePhase();
				}
			}
		} else if (dm instanceof BasicProposalDiscourseMessage) {
			BasicProposalDiscourseMessage bdm = (BasicProposalDiscourseMessage) dm;
			getLog().writeln(
					"Chips to send by proposer: "
							+ bdm.getChipsSentByProposer());
			getLog().writeln(
					"Chips to send by responder: "
							+ bdm.getChipsSentByResponder());
		}

		return result;
	}

	/**
	 * Start a new game and set up all of the game specifications.
	 */
	public void run() {
		System.out.println("Let the game begin...");

		System.out.println("game id= " + gs.getGameId());

		bPlayersMovedInRound[0] = false;
		bPlayersMovedInRound[1] = false;

		// generating an ID (hopefully unique) based in timestamp
		Long currTS = (new Date()).getTime();
		// keep ID number
		currTS = currTS % 10000;

		AltOffersConfigArguments par = (AltOffersConfigArguments) gs
				.getDataFromController();

		useVideoServer = par.isVideoOn();		
		if (useVideoServer) {
			
			// Opening a connection to the video server
			try {
				videoServer = new VideoServerConnection();
				videoServer.connect();
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
			
			// Sending the initialization message to the video server and waiting
			// for all the video clients to connect
			try {

				String player1Name = String.valueOf(gs.getPlayerByPerGameId(0).getPin());
				String player2Name = String.valueOf(gs.getPlayerByPerGameId(1).getPin());
				
				ServerData sd = ServerData.getInstance();
				String player1IP = sd.getIpByPlayerName(player1Name);
				String player2IP = sd.getIpByPlayerName(player2Name);

				videoServer.sendVideoInitMessage(player1Name,player2Name,player1IP, player2IP);
				
			} catch (IOException e) {
				System.err
						.println("Couldn't get a response from the video server to the init message.");
				e.printStackTrace();
				System.exit(1);
			}
			
			videoServer.sendStartMessage();			
		}

		String dep = par.getDep();
		String rltp = par.getRelationship();
		String name = new String();
		int digit;
		String relationship = new String();

		System.out.println("inout args: dep=" + dep + "rel=" + rltp);
		if (rltp.equals("FRIEND")) {
			relationship = "friend";

			// human independent
			if (dep.equals(AltOffersGameDetails.dependencies[0])) {
				name = "boardXml_GUI_TI.txt";
				digit = 2;
			}
			// human dependent
			else if (dep.equals(AltOffersGameDetails.dependencies[1])) {
				name = "boardXml_AGENT_TI.txt";
				digit = 5;
			}
			// all interdependent
			else if (dep.equals(AltOffersGameDetails.dependencies[2])) {
				name = "boardXml_both_need_three.txt";
				digit = 1;
			}
			// all asymmetric interdeoendent
			else if (dep.equals(AltOffersGameDetails.dependencies[3])) {
				name = "boardXml_one_need_three_one_need_two.txt";
				digit = 1;
			}
			// all asymmetric interdeoendent: Strong agent
			else if (dep.equals(AltOffersGameDetails.dependencies[4])) {
				name = "boardXml_agent_need_three_human_need_two.txt";
				digit = 1;
			}
			// Jonathan Bar Or Sanity
			else if (dep.equals(AltOffersGameDetails.dependencies[5])) {
				name = "boardXml_JonathanSanity.txt";
				digit = 1;
			}
			// defaut: all interdependent
			else {
				name = "boardXml_both_need_three.txt";
				digit = 1;
			}
		} else if (rltp.equals("STRANGER")) {
			relationship = "stranger";

			// human independent
			if (dep.equals(AltOffersGameDetails.dependencies[0])) {
				name = "boardXml_GUI_TI.txt";
				digit = 4;
			}
			// human dependent
			else if (dep.equals(AltOffersGameDetails.dependencies[1])) {
				name = "boardXml_AGENT_TI.txt";
				digit = 6;
			}
			// all interdependent
			else if (dep.equals(AltOffersGameDetails.dependencies[2])) {
				name = "boardXml_both_need_three.txt";
				digit = 3;
			}
			// all asymmetric interdeoendent
			else if (dep.equals(AltOffersGameDetails.dependencies[3])) {
				name = "boardXml_one_need_three_one_need_two.txt";
				digit = 3;
			}
			// all asymmetric interdeoendent: Strong agent
			else if (dep.equals(AltOffersGameDetails.dependencies[4])) {
				name = "boardXml_agent_need_three_human_need_two.txt";
				digit = 3;
			}
			// Jonathan Bar Or Sanity
			else if (dep.equals(AltOffersGameDetails.dependencies[5])) {
				name = "boardXml_JonathanSanity.txt";
				digit = 1;
			}
			// defaut: all interdependent
			else {
				name = "boardXml_both_need_three.txt";
				digit = 3;
			}
			// no friends or stranger specified
		} else {
			// human independent
			if (dep.equals(AltOffersGameDetails.dependencies[0])) {
				name = "boardXml_GUI_TI.txt";
				digit = 4;
			}
			// human dependent
			else if (dep.equals(AltOffersGameDetails.dependencies[1])) {
				name = "boardXml_AGENT_TI.txt";
				digit = 6;
			}
			// all interdependent
			else if (dep.equals(AltOffersGameDetails.dependencies[2])) {
				name = "boardXml_both_need_three.txt";
				digit = 3;
			}
			// all asymmetric interdeoendent
			else if (dep.equals(AltOffersGameDetails.dependencies[3])) {
				name = "boardXml_one_need_three_one_need_two.txt";
				digit = 3;
			}
			// all asymmetric interdeoendent: Strong agent
			else if (dep.equals(AltOffersGameDetails.dependencies[4])) {
				name = "boardXml_agent_need_three_human_need_two.txt";
				digit = 3;
			}
			// Jonathan Bar Or Sanity
			else if (dep.equals(AltOffersGameDetails.dependencies[5])) {
				name = "boardXml_JonathanSanity.txt";
				digit = 1;
			}
			// default: all interdependent
			else {
				name = "boardXml_both_need_three.txt";
				digit = 3;
			}
		}

		currTS = currTS + (digit * 10000);

		for (PlayerStatus ps : gs.getPlayers()) {
			ps.setTimeStampID(currTS.intValue() + ps.getPerGameId());
			ps.setRelationship(relationship);
		}

		getLog().writeln("ID Number: " + currTS);

		GameBoardCreator gameBoardCreator = new GameBoardCreator(gs, logName);
		gameBoardCreator.createGameBoard(name);

		PhasesLength phL = new PhasesLength();
		phL.getPhasesTimes();

		ServerPhases ph = new ServerPhases(this);
		ph.addPhase(ServerPhases.STRATEGY_PH, phL.stratPrepPhTime);
		ph.addPhase(ServerPhases.COMM_PH, phL.commPhTime);
		ph.addPhase(ServerPhases.EXCH_PH, phL.exchngPhTime);
		ph.addPhase(ServerPhases.MOVEMENT_PH, phL.movPhTime);
		ph.addPhase(ServerPhases.FEEDBACK_PH, phL.fdbckPhTime);

		gs.setScoring(this.s);

		ph.setLoop(phaseLoop);
		gs.setPhases(ph);

		// Setting the first proposer to be the perGameId=0 that also gets the
		// first players chips
		proposerID = 0;
		responderID = 1;

		gs.getPlayerByPerGameId(responderID).setRole("Responder");
		gs.getPlayerByPerGameId(proposerID).setRole("Proposer");

		gs.sendArbitraryMessage(Constants.NEWROUND);
		gs.setInitialized(); // will generate GAME_INITIALIZED message
	}

	private boolean isThereSatisfiablePath(PlayerStatus player) {
		boolean bThereIs = false;
		ArrayList<Path> shortestPaths = ShortestPaths
				.getShortestPathsForMyChips(player.getPosition(), gs.getBoard()
						.getGoalLocations().get(0), gs.getBoard(), s,
						ShortestPaths.NUM_PATHS_RELEVANT, player.getChips());

		ChipSet playerChips = player.getChips();
		for (Path path : shortestPaths) {
			ChipSet required = path.getRequiredChips(gs.getBoard());
			if (playerChips.contains(required)) {
				bThereIs = true;
				break;
			}
		}
		return bThereIs;
	}
	
	private void endGame(String endMessage) {
		
		getLog().writeln(endMessage);
		
		// Move each player as far as he can go
		super.doAutomaticMovement(s);
		((ServerPhases) gs.getPhases()).setLoop(false);		
		
		gs.setEnded();
		for (PlayerStatus p : gs.getPlayers()) {
			getLog().writeln(
					"Player: " + p.getPin() + " ,role: " + p.getRole()
							+ " score is: " + p.getScore());
		}
		
		gs.getPlayerByPerGameId(0).setRole("GameEnded");
		gs.getPlayerByPerGameId(1).setRole("GameEnded");
		
		if (useVideoServer) {
			// Closing the connection to the video server
			try {
				videoServer.sendStopMessage();
				videoServer.close();
			} catch (IOException e) {
				System.out
						.println("Problem closing connection to the video server");
				e.printStackTrace();
			}		
		}
	}

	private FileLogger getLog() {
		return FileLogger.getInstance(logName);
	}
}