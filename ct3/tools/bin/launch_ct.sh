#!/bin/bash
# Bash script to launch Colored Trails.
# Author: Bart Kamphorst (Bart.Kamphorst@phil.uu.nl)
# October 2008.

######################### Section Variable declarations ##############

CTROOT="<path to ct>/ct3"                       			# Path to root directory CT.
CTJARPATH="$CTROOT/dist"									# Path to ct3.jar.
CTJAR="$CTJARPATH/ct3.jar"									# The actual jar.
CTSERVER="java -jar $CTJAR -s --server_localport" 			# Takes a <port> as argument.
CTADMIN="java -jar $CTJAR -a"  							 	# Takes $CONFIG as input (<).
CTCONFIG="$ROOT/lib/adminconfig/CT.txt" 					# Path to  config file.
CTCLIENT="java -jar $CTJAR -c ctgui.original.GUI"			#  --pin 10  --client_hostip $2
DATE="`/bin/date +%Y%m%d-%H:%M`"
######################### Section Functions ##########################

usage_exit() {
 echo "Usage: `basename $0` <arguments>"
 echo " Start ctserver  : -s <port> " 
 echo " Start ctadmin   : -A <name of config file> "
 echo " Start ctclient  : -c <pin> " 
 echo " Start ctagent   : -a <type> "
 echo " Stop ctserver   : -k "
 exit 1
}

log_error() {
 ME="$(basename $0)"
 echo "$ME: $DATE: $1" 
 logger -p daemon.err "$ME: $DATE $1"
}

error_exit() {
 ME="$(basename $0)"
 echo "ERROR EXIT $DATE: $ME: $1"
 exit 1
}

pidof() {
ps -Av | grep $1 | cut -d ' ' -f 1
}

check_sanity() {
 	[ -d $CTROOT ] || error_exit "$CTROOT does not exist."
 	[ -f $CTJAR ] || error_exit "Could not locate $CTJAR."
 	
 	export PATH="$PATH:$CTROOT"
 	echo "PATH is set to: $PATH"
 	if [ -f $(which java) ] ; then  
  		echo "Java is present: $(which java)"
  	else error_exit "No java present."
 	fi
	if [ ! -d $CTROOT/logs ] ; then 
		echo "The logs directory in $CTROOT does not exist. Trying to create it..."
		if $(mkdir -p $CTROOT/logs/CTserver $CTROOT/logs/CTClient $CTROOT/logs/CTAdmin $CTROOT/logs/CTAgent) ; then
			echo "...succesfully created the logs directories in $CTROOT."
		else 
			error_exit "Failed to create the logs directories. Make sure you have the right permissions."
		fi
	fi
}

start_ctserver() {
 	if (cd $CTJARPATH) ; then
	 	echo "cd to $CTJARPATH succeeded."
	fi
 	if [ -f $CTJAR ] ; then 
		echo "Trying to start the CT server..."
  		$CTSERVER $1 >> $CTROOT/logs/CTserver/CTserverlog$DATE.log 2>&1 &
  		echo "... CT server has been started."
  		export SERVER_PID=$!
  		echo "The server has process ID: $SERVER_PID."
 	else 
		log_error "$DATE: Could not locate $CTJAR." && error_exit "Could not start the CT server."
fi
}

start_ctclient() {
 	cd $CTJARPATH && echo "cd to $CTJARPATH succeeded."
 	if [ -f $CTJAR ] ; then
  		$CTCLIENT --pin $1 >> $CTROOT/logs/CTClient/CTclient$1log$DATE.log 2>&1 & 	# --client_hostip $2
  		echo "Client $1 has been started."
 	else 
		log_error "$DATE: Could not locate $CTJAR." && error_exit "Could not start the CT client."
	fi
}

start_ctadmin() {
 	cd $CTROOT && echo "cd to $CTROOT succeeded."
 	export CTCONFIG="$CTROOT/lib/adminconfig/$1"
  	if [ -f $CTJAR ] ; then
  		$CTADMIN < $CTCONFIG >> $CTROOT/logs/CTAdmin/CTadminlog$DATE.log 2>&1 &
  		echo "Admin loaded configuration file."
 	else 
		log_error "$DATE: Could not locate $CTJAR." && error_exit "Could not start admin."
fi
}

start_ctagent() {
 	cd $CTROOT && echo "cd to $CTROOT succeeded."
 	AGENT=$1
 	eval \$"${AGENT}" >> $CTROOT/logs/CTAgent/CT$AGENT$DATE.log 2>&1 & 
 	echo "$AGENT agent has been started."
}

stop_ctserver() {
	echo "Trying to bring down the CT server..."
	if (kill `pidof $CTJAR` 2>/dev/null) ; then
		echo "... the server has been stopped."
 	else {
 		log_error "$DATE: The CT server was not running."
		error_exit "Could not stop the server, for it was not running."
		}
 fi
}


######################### Section getopts ######################

# The script needs arguments

[ -z "$1" ] && usage_exit

# Check if the environment is sane.

check_sanity

# Parse the commandline arguments

 while getopts ":a:c:s:A:k" Option

# Initial declaration.
# a, c, s, A, k, and t are the flags expected.
# The : after some flags shows it will have an option passed with it.
# Use $OPTARG

do
case $Option in
a ) start_ctagent $OPTARG ;;
c ) start_ctclient $OPTARG ;;
s ) start_ctserver $OPTARG ;;
A ) start_ctadmin $OPTARG ;;
k ) stop_ctserver ;;
* ) usage_exit ;;
esac
done
shift $(($OPTIND - 1))

######################### Extra info ###########################
# runtime.jar = ct3.jar
# ct3.jar -s = server
# ct3.jar -a = admin (-Dconfigfile=../CT.txt)
# ct3.jar -c ctgui.original.GUI --pin 10 = runclient1
# ct3.jar -c ctgui.original.GUI --pin 20 = runclient2
# ct3.jar -c ctgui.example.interruptible.GUI --pin 10 --client_localport 8010

# Usage
# =====
# Standard Usage:
# Standalone Server: java -jar ct3.jar -s
# Standalone Client: java -jar ct3.jar -c <gui class name> --pin <pin number>
# Standalone Admin:  java -jar ct3.jar -a