echo off
cls
echo Updating current directory, please wait...
tortoiseproc.exe /command:update /path:%cd%\.. /closeonend:1
echo Compiling...
ant
echo Finished!
pause

