/*
	Colored Trails

	Copyright (C) 2006-2008, President and Fellows of Harvard College.  All Rights Reserved.

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package edu.harvard.eecs.airg.coloredtrails.shared;

import edu.harvard.eecs.airg.coloredtrails.server.ServerGameStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.BasicProposalDiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.BasicProposalDiscussionDiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.DiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.types.PlayerStatus;
import java.util.ArrayList;
import org.apache.log4j.Logger;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * This class wraps the generic ExperimentLogger.
 * Please regard this class as an example of what is
 * possible with the ExperimentLogger.
 *
 * @author Bart Kamphorst (bart.kamphorst@phil.uu.nl)
 * April 2009
 */

public class ExampleExperimentLoggerWrapper {

    private static ExampleExperimentLoggerWrapper INSTANCE = null;
    Logger logger = Logger.getLogger(this.getClass().getName());
    ExperimentLogger elogger = null;
    ExperimentLogger messagelogger = null;
    ServerGameStatus gs;
    int numberofplayers = 0;

    /**
     * Constructor
     * @param ServerGameStatus gs
     * @param String table
     */
    public ExampleExperimentLoggerWrapper(ServerGameStatus gs, String table) {
        this.gs = gs;
        this.numberofplayers = gs.getPlayers().size();

        // Instantiate a new ExperimentLogger object with a variable
        // table name.
        try {
            elogger = new ExperimentLogger(table + numberofplayers + "players" + getTimeStamp(), "jdbc:mysql://localhost/test", "root", "");
        } catch (Exception sqle) {
            logger.error("[DBC] Error instantiating elogger.");
        }

        // Instantiate a new ExperimentLogger object to log DiscourseMessages
        // to a separate table.
        try {
            messagelogger = new ExperimentLogger("messagelog" + getTimeStamp(), "jdbc:mysql://localhost/test", "root", "");
        } catch (Exception sqle) {
            logger.error("[DBC] Error instantiating messagelogger.");
        }

    }

    /**
     * Constructor
     * @param ServerGameStatus gs
     */
    public ExampleExperimentLoggerWrapper(ServerGameStatus gs) {
        this(gs, "log");
    }

    /**
     * Log a String statement, together with a predefined number of
     * variables, to the table.
     * @param String logStatement
     */
    public void log(String logStatement){

        ArrayList logList = new ArrayList();
        
        logList.add(getDateTime());
        logList.add(gs.getGameId());
        logList.add(logStatement);

        for (PlayerStatus player : gs.getPlayers()) {
            logList.add(player.getPin());
            logList.add(player.getPerGameId());
            logList.add(player.getChips());
            logList.add(player.getPosition());
            logList.add(player.getTeamId());
        }

        Object[] logArray = logList.toArray();

        elogger.log(logArray);
    }

    /**
     * Log DiscourseMessages to a separate table.
     * @param DiscourseMessage dm
     */
    public void logMessage(DiscourseMessage dm) {
        ArrayList logList = new ArrayList();

        logList.add(getDateTime());
        logList.add(gs.getGameId());
        logList.add(dm.getMsgType());

        if (dm.getMsgType().equals("basicproposal")) {
            BasicProposalDiscourseMessage bpdm = (BasicProposalDiscourseMessage) dm;
            logList.add("proposal");
            logList.add(bpdm.getMessageId());
            logList.add(bpdm.getProposerID());
            logList.add(bpdm.getResponderID());
            logList.add(bpdm.getChipsSentByProposer());
        }
        else if (dm.getMsgType().equals("basicproposaldiscussion")) {
            BasicProposalDiscussionDiscourseMessage bpddm = (BasicProposalDiscussionDiscourseMessage) dm;
            if (bpddm.accepted()) {
                logList.add("accept");
            }
            else {
                logList.add("reject");
            }
            logList.add(bpddm.getMessageId());
            logList.add(bpddm.getProposerID());
            logList.add(bpddm.getResponderID());
            logList.add(bpddm.getChipsSentByProposer());
        }

        Object[] logArray = logList.toArray();
        messagelogger.log(logArray);
    }

    /**
     * Record metadata of a game. This can be anything that remains
     * unchanged for the total duration of game.
     * @param natures
     */
    public void logMetaData() {
        ExperimentLogger md = null;
        try {
            md = new ExperimentLogger("metadata" + numberofplayers + "players" + getTimeStamp(), "jdbc:mysql://localhost/test", "root", "");
        } catch (Exception sqle) {
            logger.error("[DBC] Error instantiating metadata (md).");
        }

        ArrayList logList = new ArrayList();
        logList.add(getDateTime());
        logList.add(gs.getGameId());
        logList.add(gs.getBoard().getGoalLocations(0));
        logList.add(gs.getBoard().getCols());
        logList.add(gs.getBoard().getRows());
        logList.add(gs.getPlayers().size());
        logList.add(gs.getConfigName());

        Object[] logArray = logList.toArray();
        md.log(logArray);
        md.disconnect();
    }

    /**
     * Get a proper timestamp to add to each log statement.
     * @return String date
     */
    private String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }
    
    /**
     * Get a no-nonsense timestamp (no hyphens or slashes).
     * @return String date
     */
    private String getTimeStamp() {
          DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
          Date date = new Date();
          return dateFormat.format(date);
    }
    
    /**
    * Synchronized creator to defend against multi-threading issues
    * another if check here to avoid multiple instantiation
    */
    private static void createInstance(ServerGameStatus gs) {
        if (INSTANCE == null) {
            INSTANCE = new ExampleExperimentLoggerWrapper(gs);
        }
    }

    /**
     * Get the instance of the Wrapper class to log to. If no such object
     * yet exists, it will be created.
     * @param ServerGameStatus gs
     * @return ExampleExperimentLoggerWrapper INSTANCE
     */
    public static ExampleExperimentLoggerWrapper getInstance(ServerGameStatus gs) {
        if (INSTANCE == null) createInstance(gs);
        return INSTANCE;
    }
}
