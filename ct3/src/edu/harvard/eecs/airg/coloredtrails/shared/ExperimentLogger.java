/*
	Colored Trails

	Copyright (C) 2006-2008, President and Fellows of Harvard College.  All Rights Reserved.

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package edu.harvard.eecs.airg.coloredtrails.shared;

import java.util.Random;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.util.LinkedHashMap;
import java.util.Iterator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * This is a general database logging mechanism class.
 * The ExperimentLogger class is designed to log experimental data only. Please
 * use log4J logging for logging server output.
 *
 * The ExperimentLogger accepts any number of Objects and logs them as
 * SQL datatypes to a database table that is passed to the constructor. If
 * the table already exists and the Objects passed to the log method do not
 * match the table layout, an Exception is thrown.
 * 
 * Note: you need to compile mysql-connector-java-5.1.7-bin.jar into ct3.jar
 *
 * @author Maarten Engelen
 * @author Bart Kamphorst
 * {maarten.engelen, bart.kamphorst}@phil.uu.nl
 * April 2009
 */


public class ExperimentLogger
{
    private static String defaultDbClass = "com.mysql.jdbc.Driver";
    private Connection conn;
    private Statement stmt;
    private DatabaseMetaData meta;
    private Logger logger;

    private String tableName;
    // We need to preserve iteration order so use LinkedHashMap
    private LinkedHashMap<String, String> columns;
    private boolean tableRepresented = false;

    PreparedStatement pstmt = null;

    /** 
     * Constructor with default MySQL connection
     */
    public ExperimentLogger(String tableName, String uri, String username, String password) throws Exception {
        this(tableName, defaultDbClass, uri, username, password);
    }

    /**
     * Constructor which loads the JDBC driver and builds a connection and
     * statement after which we are ready to execute queries.
     */
    public ExperimentLogger(String tableName, String dbClass, String uri, String username, String password) throws Exception {
        Class.forName(dbClass);
        this.tableName = tableName;
        this.conn = DriverManager.getConnection(uri, username, password);
        this.stmt = this.conn.createStatement();
        this.meta = conn.getMetaData();
        this.columns = new LinkedHashMap<String, String>();

        logger = Logger.getLogger(this.getClass().getName());
        logger.debug("[EXL] Initialized logger and database connection");
    }

    /**
     * The log method that writes data into the database table.
     * @param Object[] fields
     * @param boolean checkFields
     * @return boolean success
     */
    private boolean doLog(Object[] fields, boolean checkFields) {

        String fieldValue = "";
        int fieldnumber = 1;
        Iterator<String> it = this.columns.keySet().iterator();

        try {
            if (pstmt == null) {
                String qm = "(?";
                for (int i=1; i < fields.length; i++) {
                    qm += " , ?";
                }
                qm += ");";
                pstmt = conn.prepareStatement("INSERT INTO " + tableName + " VALUES " + qm);
            }

            for(Object field : fields) {

                String name = it.next();

                if (field == null) {
                    fieldValue = "NULL";
                }
                else {
                    if(checkFields && !sqlTypeFor(field).startsWith(this.columns.get(name))) {
                        // The field does not match the table column
                        // type on this position.
                        logger.error("[EXL] log method called with incorrect Object types for this table");
                        return false;
                    }
                    fieldValue = field.toString();
                }
                pstmt.setString(fieldnumber, fieldValue);
                fieldnumber++;
            }

            pstmt.execute();

            return true;
        }
        catch (SQLException sqle) {
            logger.error("[EXL] Encountered an error: " + sqle.getMessage());
		    return false;
        }
    }

  
    /**
     * Simple logger function. Takes a list of parameters,
     * convert the parameters to strings and build a query. Execute it and
     * log any exceptions.
     * @returns boolean
     * @param any number of Objects in variable fields
     */
     public boolean log(Object ... fields) {
            if (this.tableRepresented) {
                // We already have an internal representation. Check and log
                return doLog(fields, true);
            } else {
                if(tableExists()) {
                    // We already have a table, so make representation and log
                    readTableData();
                    return doLog(fields, true);
                } else {
                    // We have a clean slate. Create table and log
                    createTable(fields);
                    return doLog(fields, false);
                }
            }
	}

    /**
     * Convert a Java datatype into a basic SQL datatype.
     * @param Object o
     * @return String sqlType
     */
	private String sqlTypeFor(Object o) {
	    if(o instanceof Integer) {
	        return "INTEGER";
	    }
	    if(o instanceof java.math.BigDecimal) {
	        return "NUMERIC";
	    }
	    if(o instanceof Boolean) {
	        return "VARCHAR(10)";
	    }
	    if(o instanceof Integer) {
	        return "INTEGER";
	    }
	    if(o instanceof Long) {
	        return "BIGINT";
	    }
	    if(o instanceof Float) {
	        return "REAL";
	    }
	    if(o instanceof Double) {
	        return "DOUBLE";
	    }
	    if(o instanceof byte[]) {
	        return "VARBINARY";
	    }
	    if(o instanceof java.sql.Date) {
	        return "DATE";
	    }
	    if(o instanceof java.sql.Time) {
	        return "TIME";
	    }
	    if(o instanceof java.sql.Timestamp) {
	        return "TIMESTAMP";
	    }

	    return "VARCHAR(255)";
	}

    /**
     * Read in the table data from the JDBC connection.
     */
	private void readTableData() {
	    try {
            ResultSet r = meta.getColumns(this.conn.getCatalog(), "%", this.tableName, "%");
            this.columns.clear();
    		while(r.next()) {
    		    // Store name (4) and type (6)
                logger.debug("put '" + r.getString(4) + "' : '" + r.getString(6) + "'");
    		    this.columns.put(r.getString(4), r.getString(6));
    		}
    		this.tableRepresented = true;
        } catch(SQLException e) {
            logger.log(Level.ERROR, "[EXL] SQL Error in reading database data: " +
                    e.getMessage());
        }
	}

    /**
     * Create a table in a certain database, given the
     * the table and the Objects passed to the method.
     */
    private void createTable(Object[] fields) {
        try {
              String query = "CREATE TABLE `" + this.tableName + "` (";

              int fieldId = 1;
              for(Object field : fields) {
                  String type = sqlTypeFor(field);
                  String name = "field" + fieldId;
                  query += "`" + name + "`" + type + ",";
                  this.columns.put(name, type);
                  fieldId++;
              }

              query = query.substring(0, query.length() - 1);

              query += ");";

              // logger.debug("[EXL] createTable query: " + query);
              this.stmt.executeUpdate(query);
        } catch (SQLException sqle) {
            logger.error("[EXL] SQL Error in creating database: " +
                    sqle.getMessage().toString());
        }
        
    }


    /**
     * Return a boolean to see if a table exists.
     * @param String table
     * @return boolean
     */
    private boolean tableExists() {
        try {
            return meta.getColumns(this.conn.getCatalog(), "%", this.tableName, "%").last();
        } catch(SQLException e) {
            logger.fatal("[EXL] SQL Exception while retrieving meta data: " + e.getMessage());
        }
        return false;
    }

    /**
     * Disconnect from the database by closing the connection.
     */
    public void disconnect() {
        if (conn != null) {
            try {
                pstmt.close();
                conn.close();
                logger.info("Database connection closed.");
            }
            catch (Exception e) {
            // ignore close errors
            }
        }
    }


/******************************************************************************
 ** TESTING
 **************/
    
    /**
     * @param args
     * Test method. This works with a local table named 'log' in the 'test' db.
     * Fields are of types (int, varchar, int) with a mysql autoincrement on
     * the first column.
     */
    public static void main(String[] args) {
        try {
			System.out.println("Opening connection..");
            ExperimentLogger e = new ExperimentLogger("log", "jdbc:mysql://localhost/test", "root", "");
            Random r = new Random();
			System.out.println("Running tests...");

			System.out.println(e.tableExists());

            for(int i = 0; i < 500; i++) {
                String randomString = Long.toString(Math.abs(r.nextLong()), 32);
                int randomInt = r.nextInt();
                // java null to sql null.
                if(!e.log(null, randomString, randomInt)) {
                    System.out.println("Error logging entry no.: " + i);
                } else {
                    System.out.println("Logged entry no.: " + i);
                }
            }

            e.disconnect();

			System.exit(0);

        } catch(Exception e) {
            System.out.println("Fatal Error: " + e.getClass() + " - " + e.getMessage());
            e.printStackTrace();
        }
    }
}
