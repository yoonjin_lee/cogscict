package edu.harvard.eecs.airg.coloredtrails.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import edu.harvard.eecs.airg.coloredtrails.shared.PlayerConnection;

/**
 * @author KPozin
 *
 */
public abstract class RoundAllocator
{
	/**
	 * Name of config class's directory
	 */
	protected String configDir;
	/**
	 * Config class name
	 */
	protected String className;
	protected int playersPerGame;
	/**
	 * Arbitrary data to be passed to the controller. Currently this is the same for all games,
	 * but later, we might need to expand it to be customized by a functor of some sort.
	 */
	protected Serializable data;
	protected int numSimultGames = 1;
	protected int numActivePlayers;
	protected ControlImpl ci;
	protected ArrayList<Round> rounds = new ArrayList<Round>();
	
	/**
	 * Determines whether the ControlImpl should be polled on each round in case the 
	 * list of connected players has changed
	 */
	public boolean refreshOnEachRound = false;
	
	/**
	 * List of <em>all</em> connected players.
	 */
	protected List<PlayerConnection> allPlayers;
	
	/**
	 * List of players who are active in the current round. This will be equivalent
	 * to allPlayers if no one is sitting out.
	 */
	protected static Random rand = new Random();
	
	/**
	 * 
	 * @param controlImpl
	 * @param configDir Path to config file
	 * @param className Name of config file
	 * @param refreshOnEachRound Whether the server should be polled on each round (to check for disconnected or new players)
	 * @param playersPerGame
	 */
	public RoundAllocator(ControlImpl controlImpl, String configDir, String className, boolean refreshOnEachRound,
			int playersPerGame, Serializable data)
	{
		this.configDir = configDir;
		this.className = className;
		this.refreshOnEachRound = refreshOnEachRound;
		this.playersPerGame = playersPerGame;
		this.data = data;
		ci = controlImpl;
		allPlayers = ci.getPlayersWait();
		numSimultGames = allPlayers.size() / playersPerGame;

	}

	/**
	 * If <code>refreshOnEachRound</code> is true, refreshes the list of players
	 */
	public void optionallyRefreshPlayers() {
		
		if (refreshOnEachRound)
		{
			allPlayers = ci.getPlayersWait();
			numSimultGames = allPlayers.size() / playersPerGame;
		}
		
		numActivePlayers = playersPerGame * numSimultGames;		
	}
	
	public abstract Round getNewRound();
}