package edu.harvard.eecs.airg.coloredtrails.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Arrays;

import edu.harvard.eecs.airg.coloredtrails.shared.PlayerConnection;
import edu.harvard.eecs.airg.coloredtrails.shared.types.AltOffersConfigArguments;

/**
 * A fair round allocator that assigns players to games randomly, making sure
 * that the conditions are switched between rounds. Used by the
 * AltOffersMultiRoundsControl (full explanation in the comment of the
 * controller's class)
 * 
 * @author kpozin
 * @author Yael blumberg
 */
public class MultiRoundsAllocatorAltOffers extends RoundAllocator {

	protected List<PlayerConnection> humans1;
	protected List<PlayerConnection> humans2;
	protected List<PlayerConnection> agents;

	protected List<List<PlayerConnection>> hhAHumans;
	protected List<List<PlayerConnection>> hhBHumans;
	protected List<PlayerConnection> hcHumans;

	RosterContainer rc;

	protected List<AltOffersGameDetails> gameDetailsHistory;

	List<Round> rounds;

	boolean roundsGenerated = false;

	protected AltOffersConfigArguments fullArgs;

	protected int roundIndex = 0;

	public MultiRoundsAllocatorAltOffers(ControlImpl controlImpl,
			String configDir, String className, boolean refreshOnEachRound,
			int playersPerGame, Serializable data) {
		super(controlImpl, configDir, className, refreshOnEachRound,
				playersPerGame, data);

		fullArgs = (AltOffersConfigArguments) data;

		rounds = new ArrayList<Round>(2);

		gameDetailsHistory = new ArrayList<AltOffersGameDetails>();
	}

	public void generateRounds() {

		humans1 = new ArrayList<PlayerConnection>();
		humans2 = new ArrayList<PlayerConnection>();
		agents = new ArrayList<PlayerConnection>();

		// Separate humans and agents
		for (PlayerConnection pc : allPlayers) {
			if (pc.getClientClassType().equals("HumanGUI")) {
				humans1.add(pc);
			} else {
				agents.add(pc);
			}
		}

		// Sorting the human players according to their pins
		Collections.sort(humans1, new Comparator<PlayerConnection>() {

			public int compare(PlayerConnection pc1, PlayerConnection pc2) {
				if (pc1.getPin() == pc2.getPin()) {
					return 0;
				} else if (pc1.getPin() < pc2.getPin()) {
					return -1;
				} else {
					return 1;
				}
			}
		});

		// Separating the humans into two teams (probably sitting in different
		// rooms) in order to make sure that each player will play only with a
		// player from a different team
		humans2 = humans1.subList(humans1.size() / 2, humans1.size());
		humans1 = humans1.subList(0, humans1.size() / 2);

		// Shuffling the humans
		Collections.shuffle(humans1);
		Collections.shuffle(humans2);
		Collections.shuffle(agents);

		// Separate humans into conditions, in human-human games also keeping
		// the separation into groups
		hhAHumans = new ArrayList<List<PlayerConnection>>();
		hhAHumans.add(new ArrayList<PlayerConnection>());
		hhAHumans.add(new ArrayList<PlayerConnection>());

		hhBHumans = new ArrayList<List<PlayerConnection>>();
		hhBHumans.add(new ArrayList<PlayerConnection>());
		hhBHumans.add(new ArrayList<PlayerConnection>());

		hcHumans = new ArrayList<PlayerConnection>();

		// New scope here
		{
			int index = -1;

			for (int i = 0; i < fullArgs.numHumanHumanA / 2; ++i) {
				++index;
				hhAHumans.get(0).add(humans1.get(index));
				hhAHumans.get(1).add(humans2.get(index));
			}

			for (int i = 0; i < fullArgs.numHumanHumanB / 2; ++i) {
				++index;
				hhBHumans.get(0).add(humans1.get(index));
				hhBHumans.get(1).add(humans2.get(index));
			}

			for (int i = 0; i < fullArgs.numHumanComputer / 2; ++i) {
				++index;
				hcHumans.add(humans1.get(index));
				hcHumans.add(humans2.get(index));
			}
		}

		try {
			rc = RosterContainer.loadFromFile(fullArgs.getExperimentId());
		} catch (ClassNotFoundException e) {

		}

		// If loading failed
		if (rc == null) {
			rc = new RosterContainer();
			generateHumanHumanGameDetails();
			generateHumanComputerGameDetails();

			try {
				rc.saveToFile(fullArgs.getExperimentId());
				System.out.println("Wrote new roster to file "
						+ fullArgs.getExperimentId() + ".roster");
			} catch (IOException e) {
				System.out.println("Failed to write roster to file "
						+ fullArgs.getExperimentId() + ".roster");
				e.printStackTrace();
			}
		} else {
			System.out.println("Successfully loaded "
					+ fullArgs.getExperimentId() + ".roster");
		}

		Round round0 = new Round(0);
		addDetailsToRound(round0, rc.hhAGameDetails[0]);
		addDetailsToRound(round0, rc.hhBGameDetails[0]);
		addDetailsToRound(round0, rc.hcGameDetails[0]);

		Round round1 = new Round(1);
		addDetailsToRound(round1, rc.hhAGameDetails[1]);
		addDetailsToRound(round1, rc.hhBGameDetails[1]);
		addDetailsToRound(round1, rc.hcGameDetails[1]);

		rounds.add(round0);
		rounds.add(round1);

		System.out.println("Rounds:");
		System.out.println("Human-Human A");
		System.out.println(Arrays.toString(rc.hhAGameDetails[0]));
		System.out.println(Arrays.toString(rc.hhAGameDetails[1]));
		System.out.println("Human-Human B");
		System.out.println(Arrays.toString(rc.hhBGameDetails[0]));
		System.out.println(Arrays.toString(rc.hhBGameDetails[1]));
		System.out.println("Human-Computer");
		System.out.println(Arrays.toString(rc.hcGameDetails[0]));
		System.out.println(Arrays.toString(rc.hcGameDetails[1]));

		roundsGenerated = true;

	}

	private void addDetailsToRound(Round round, AltOffersGameDetails[] details) {
		if (details == null || details.length == 0) {
			return;
		}
		int lastRel = 0;
		for (AltOffersGameDetails aogd : details) {
			AltOffersConfigArguments args = new AltOffersConfigArguments(
					fullArgs);
			args.roundNum = round.getRoundNum();
			args.perRoundNum = round.size();
			args.setDep(aogd.getDependency());

			// Changing relationships in a round robin way to create an equal
			// weight for each type
			lastRel = 1 - lastRel;
			args.setRelationship(AltOffersGameDetails.relationships[lastRel]);

			Game game = new Game(ci, aogd.playerPins, configDir, className,
					args);
			round.add(game);
		}
	}

	private void generateHumanComputerGameDetails() {
		int numGamesFirst = fullArgs.numHumanComputer / 2;

		rc.hcGameDetails = new AltOffersGameDetails[2][fullArgs.numHumanComputer];

		// First
		for (int i = 0; i < fullArgs.numHumanComputer; ++i) {
			AltOffersGameDetails round0 = rc.hcGameDetails[0][i] = new AltOffersGameDetails();
			AltOffersGameDetails round1 = rc.hcGameDetails[1][i] = new AltOffersGameDetails();

			round0.setDependency(AltOffersGameDetails.dependencies[2]);
			round1.setDependency(AltOffersGameDetails.dependencies[0]);

			round0.playerPins[0] = round1.playerPins[0] = hcHumans.get(i)
					.getPin();
			round0.playerAgent[0] = round1.playerAgent[0] = false;

			round0.playerPins[1] = round1.playerPins[1] = agents.get(i)
					.getPin();
			round0.playerAgent[1] = round1.playerAgent[1] = true;
		}

		// Flip the board order for half of the players
		for (int i = numGamesFirst; i < (numGamesFirst * 1.5); ++i) {
			rc.hcGameDetails[0][i]
					.setDependency(AltOffersGameDetails.dependencies[0]);
			rc.hcGameDetails[1][i]
					.setDependency(AltOffersGameDetails.dependencies[2]);
		}

		for (int i = (int) (numGamesFirst * 1.5); i < fullArgs.numHumanComputer; ++i) {
			rc.hcGameDetails[0][i]
					.setDependency(AltOffersGameDetails.dependencies[1]);
			rc.hcGameDetails[1][i]
					.setDependency(AltOffersGameDetails.dependencies[2]);
		}

		boolean toggler = rand.nextBoolean();

		for (int i = 0; i < fullArgs.numHumanComputer; ++i) {
			if (toggler) {
				rc.hcGameDetails[0][i].switchOrder();
			}
			if (!toggler) {
				rc.hcGameDetails[1][i].switchOrder();
			}
			toggler = !toggler;
		}

	}

	/**
	 * Sets up the matches for the human-human games.
	 */
	private void generateHumanHumanGameDetails() {
		int numHHAGames = fullArgs.numHumanHumanA / 2;
		int numHHBGames = fullArgs.numHumanHumanB / 2;

		rc.hhAGameDetails = new AltOffersGameDetails[2][numHHAGames];
		rc.hhBGameDetails = new AltOffersGameDetails[2][numHHBGames];

		assignHumanMatches(hhAHumans, rc.hhAGameDetails,
				AltOffersGameDetails.dependencies[2],
				AltOffersGameDetails.dependencies[0]);
		assignHumanMatches(hhBHumans, rc.hhBGameDetails,
				AltOffersGameDetails.dependencies[0],
				AltOffersGameDetails.dependencies[2]);

	}

	private void assignHumanMatches(
			List<List<PlayerConnection>> playerConnections,
			AltOffersGameDetails[][] gameDetails, String dep0, String dep1) {
		if (playerConnections.size() == 0) {
			return;
		}
		int numGames = (playerConnections.get(0).size() + playerConnections
				.get(1).size()) / 2;
		for (int i = 0; i < numGames; ++i) {
			AltOffersGameDetails round0 = gameDetails[0][i] = new AltOffersGameDetails();
			AltOffersGameDetails round1 = gameDetails[1][i] = new AltOffersGameDetails();
			round0.setDependency(dep0);
			round1.setDependency(dep1);
			// playerAgent is false by default

			round0.playerPins[0] = playerConnections.get(0).get(i).getPin();
			round0.playerPins[1] = playerConnections.get(1).get(i).getPin();

			round1.playerPins[0] = playerConnections.get(1).get(
					(i + 1) % numGames).getPin();
			round1.playerPins[1] = playerConnections.get(0).get(i).getPin();

			// In case of TD-TI games, changing from ID to DI in a round robin
			// way so that in half of the games the first proposer will be TI
			// and the other half first proposer is TD
			if (dep0.equals(AltOffersGameDetails.dependencies[0])) {
				dep0 = AltOffersGameDetails.dependencies[1];
			} else if (dep0.equals(AltOffersGameDetails.dependencies[1])) {
				dep0 = AltOffersGameDetails.dependencies[0];
			}
			if (dep1.equals(AltOffersGameDetails.dependencies[0])) {
				dep1 = AltOffersGameDetails.dependencies[1];
			} else if (dep1.equals(AltOffersGameDetails.dependencies[1])) {
				dep1 = AltOffersGameDetails.dependencies[0];
			}
		}
	}

	@Override
	public Round getNewRound() {
		if (!roundsGenerated) {
			generateRounds();
		}
		return rounds.get(roundIndex++);
	}

	public Round getRound(int num) {
		if (!roundsGenerated) {
			generateRounds();
		}
		roundIndex = num;
		return rounds.get(num);
	}

	public static String pickRandomElement(String[] source) {
		return source[rand.nextInt(source.length)];
	}
}