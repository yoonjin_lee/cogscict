echo off
cls
echo starting FW...
start /B java -jar dist/ct3.jar -s
ping 127.0.0.1 -n 7 -w 1000 > nul
echo starting human player...
start /B java -jar dist/ct3.jar -c ctgui.original.GUI --pin 200 --client_hostip localhost
ping 127.0.0.1 -n 7 -w 1000 > nul

echo starting clueless...
rem Clueless parameters
set SAMPLING_BRANCHING=1
set INITIAL_RELIABILITY=0.6
set LAST_ROUND_RELIABILITY_WEIGHT=0.7
set MY_SCORE_WEIGHT=0.9

echo Clueless params are (%SAMPLING_BRANCHING%, %INITIAL_RELIABILITY%, %LAST_ROUND_RELIABILITY_WEIGHT%, %MY_SCORE_WEIGHT%)
start /B java -classpath dist/ct3.jar ctagents.alternateOffersAgent.cluelessAgent.CluelessAgentFrontEnd 337 %SAMPLING_BRANCHING% %INITIAL_RELIABILITY% %LAST_ROUND_RELIABILITY_WEIGHT% %MY_SCORE_WEIGHT%
rem start /B java -Xdebug -Xnoagent -Xrunjdwp:transport=dt_socket,address=6079,server=y,suspend=n -classpath dist/ct3.jar ctagents.alternateOffersAgent.cluelessAgent.CluelessAgentFrontEnd 337
ping 127.0.0.1 -n 7 -w 1000 > nul

echo starting game config...
rem Board parameters
rem BoardType Could be DD, ID or DI
set BoardType=DD
rem BoardRelation could be STRANGER or FRIEND
set BoardRelation=STRANGER
echo Board params are (%BoardType%, %BoardRelation%)
start /B java -classpath "dist/ct3.jar;gameconfigs" AltOffersControl %BoardType% %BoardRelation%
