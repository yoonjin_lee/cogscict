set origDir=%cd%
echo off
cls
cd /d "%1"
if errorlevel 1 goto hell
echo Renaming all files in %cd%
pause
for %%i in (*.analysis.txt) do (cmd /v /c "set a=%%i && ren %%i !a:~0,-14!")
cd /d %origDir%
goto eof
:hell
echo Quitting, path not found.
:eof
